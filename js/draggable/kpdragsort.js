
$.fn.kpdragsort = function(options) {
    var container = this;

    $(container).children().off("mousedown").mousedown(function(e) {

    	
        if (e.which != 1 || $(e.target).is("input, textarea") || window.kp_only)
            return;

        e.preventDefault(); 

        var x = e.pageX;
        var y = e.pageY;
        var _this = $(this); 
        var w = _this.width();
        var h = _this.height();
        var w2 = w / 2;
        var h2 = h / 2;
        var p = _this.offset();
        var left = p.left;
        var top = p.top;
        window.kp_only = true;

     
        _this.before('<div id="kp_widget_holder"></div>');
        var wid = $("#kp_widget_holder");
        
        var marginTop = _this.css("margin-top") || "0";
        var marginLeft = _this.css("margin-left") || "0";
        var marginRight = _this.css("margin-right") || "0";
        var marginBottom = _this.css("margin-bottom") || "0";
        
        wid.css({
            "box-sizing": "border-box",
            "border": "2px dashed #ccc",
            "height": _this.outerHeight(true) - 4,
            "margin": [marginTop, marginRight, marginBottom, marginLeft].join(" ")
        });

   
        _this.css({
            "width": w,
            "height": h,
            "position": "absolute",
            opacity: 0.8,
            "z-index": 999,
            "left": left,
            "top": top
        });


        $(document).mousemove(function(e) {
            e.preventDefault();


            var l = left + e.pageX - x;
            var t = top + e.pageY - y;
            _this.css({"left": l, "top": t});

       
            var ml = l + w2;
            var mt = t + h2;

    
            $(container).children().not(_this).not(wid).each(function(i) {
                var obj = $(this);
                var p = obj.offset();
                var a1 = p.left;
                var a2 = p.left + obj.width();
                var a3 = p.top;
                var a4 = p.top + obj.height();

        
                if (a1 < ml && ml < a2 && a3 < mt && mt < a4) {
                    if (!obj.next("#kp_widget_holder").length) {
                        wid.insertAfter(this);
                    } else {
                        wid.insertBefore(this);
                    }
                    return;
                }
            });
        });


        $(document).mouseup(function() {
            $(document).off('mouseup').off('mousemove');

      
            $(container).each(function() {
                var obj = $(this).children();
                var len = obj.length;
                if (len == 1 && obj.is(_this)) {
                    $("<div></div>").appendTo(this).attr("class", "kp_widget_block").css({"height": 10});
                } else if (len == 2 && obj.is(".kp_widget_block")) {
                    $(this).children(".kp_widget_block").remove();
                }
            });

         
            var p = wid.offset();
            _this.animate({"left": p.left, "top": p.top}, 100, function() {
                _this.removeAttr("style");
                wid.replaceWith(_this);
                window.kp_only = null;

          
                if (options && typeof options.onEnd == "function")
                    options.onEnd(this);

            });

        });

    });

};
