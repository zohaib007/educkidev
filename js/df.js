//JVS



$(document).ready(function() {
// Miniaturas producto
	var maxWidth = 0;
	var contMin = 0;
	var is_chrome = window.chrome;
	jQuery("#pager a").each(function(){
		contMin++;
	});
	maxWidth = 458 / contMin;
	
	var photoWidth = jQuery("#pager .photo").parent().width();
	
	//jQuery("#pager .photo img").attr('style',"margin-top:29px;");
	//jQuery("#pager .photo, #pager a").width(maxWidth+2);
	//jQuery("#pager .timer_off").width(maxWidth+4);
	/*
	if (is_chrome) {
		jQuery("#pager .photo, #pager a").width(maxWidth+1);
		jQuery("#pager .timer_off").width(maxWidth+3);
	}
	if (IS_IE7) {
		jQuery("#pager .photo, #pager a").css("margin-right", "5px");
		jQuery("#pager .photo").width(maxWidth);
	}
	*/
	$('#carousel li').css("display","inline-block");
});

jQuery(".slideshow .arrow2b a").click(function(){
	if(jQuery(".slideshow .arrow2b a").hasClass("collapse")){
		jQuery("#pager, .slideshow .arrow2b a").removeClass("collapse");
		jQuery("#pager .photo").slideDown('slow');
	} else {
		jQuery("#pager .photo").slideUp('slow', function() {
			jQuery("#pager, .slideshow .arrow2b a").addClass("collapse");
		});
	}
});

jQuery("#pager a").click(function(){
	setTimeout(function (){$('#carousel').trigger("pause");
		if(jQuery("#pager").hasClass("collapse")){
			jQuery(this).find(".photo").stop(true, true).show();
		}},350);
});


jQuery(".slideshow .size ul, .slideshow .add_b1").mouseenter(function(){
	$('#carousel').trigger("pause");
	if(jQuery("#pager").hasClass("collapse")){
		jQuery(this).find(".photo").stop(true, true).show();
	}
});

jQuery(".slideshow .size ul, .slideshow .add_b1").mouseleave(function(){
	$('#carousel').trigger("resume");
	if($("#pager").hasClass("collapse")){
		jQuery(this).find(".photo").stop(true, true).hide();
	}
});


// Slideshow producto
function prevTimers() {
	return allTimers().slice(0, jQuery('#pager a.selected').index());
}
function allTimers() {
	return $('#pager a .timer');
}

$(function() {
	/*if (IS_IE7 || IS_IPAD || IS_APP) {
		$('#carousel').carouFredSel(
				{
					items : 1,
					auto : {
						play: false
					},
					scroll : {
						fx : 'crossfade',
						duration : 300
					},
					pagination : {
						container : '#pager',
						anchorBuilder : false
					},
					swipe : {
						onTouch : true,
						onMouse : true
					}
				});
	}*/ //else {
		$('#carousel').carouFredSel(
				{
					items : 1,
					auto : {
						pauseOnHover : 'resume',
						progress : {
							bar : '#pager a:first .timer'
						}
					},
					scroll : {
						fx : 'crossfade',
						duration : 300,
						timeoutDuration : 20000,
						pauseOnHover : 'resume',
						onAfter : function() {
							allTimers().stop().width(0);
							$(this).trigger(
									'configuration',
									[ 'auto.progress.bar',
											'#pager a.selected .timer' ]);
						}
					},
					pagination : {
						container : '#pager',
						anchorBuilder : false
					},
					swipe : {
						onTouch : true,
						onMouse : true
					}
				});
	//}
});
var IS_IE7 = jQuery("body").hasClass("ie7");