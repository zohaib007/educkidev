<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['url'] = base_url();
        $this->load->model('front/cart/cart_model');
        date_default_timezone_set('America/New_York');

    }


    function setCartSession($cartId, $cookieAlso = false, $userId = false) {
        $this->session->set_userdata('isCart', TRUE);
        $this->session->set_userdata('cartId', $cartId);
        $expires = time() + (5400000);
        if ($cookieAlso) {
            $cookie1 = array(
                'name' => 'isCart',
                'value' => TRUE,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie2 = array(
                'name' => 'cartId',
                'value' => $cartId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie3 = array(
                'name' => 'userId',
                'value' => $userId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            
//            set_cookie($cookie1);
//            set_cookie($cookie2);
//            set_cookie($cookie3);

            // delete_cookie('isCart');
            // delete_cookie('cartId');
            // delete_cookie('userId');
            // delete_cookie('educki_isCart');
            // delete_cookie('educki_cartId');
            // delete_cookie('educki_userId');

            setcookie('isCart',"TRUE",$expires,$this->data['url'],'/','educki_');
            setcookie('cartId',$cartId,$expires,$this->data['url'],'/','educki_');
            setcookie('userId',$userId,$expires,$this->data['url'],'/','educki_');
            setcookie('educki_isCart', "TRUE", $expires, '/');
            setcookie('educki_cartId', $cartId, $expires, '/');
            setcookie('educki_userId', $userId, $expires, '/');
        }
    }
    

    public function login_user()
    {
        if(!$this->input->is_ajax_request()){
            $this->session->unset_userdata('tier_id');
            $email    = $this->input->post('email');
            $password = $this->input->post('password');
            $check    = $this->input->post('check');
            $regData  = $this->db->query("
                                SELECT *
                                FROM tbl_user
                                WHERE user_email ='".$email."'
                                AND user_password ='".$password."'
                                AND user_status = 1 AND user_is_delete = 0
                            ")->row();
            $creds                  = array();
            $creds['user_login']    = $email;
            $creds['user_password'] = $password;
            $creds['remember']      = true;
            $user                   = wp_signon($creds, false);

            /*$cookie = array(
                        'name'   => 'educki_kmli',
                        'value'  => $check,
                        'secure' => false,
                        );*/
                           
            if (count($regData) > 0) {
                if($check === 'true'){
                     $cookie = array(
                            'name'   => 'kmli',
                            'value'  => $email,
                            'expire' => time()+40000,    //set expire time to a year
                            'domain' => $this->data['url'],
                            'path' => '/',
                            'prefix' => 'educki_',
                         );     
                    
                    set_cookie($cookie);
                    setcookie('educki_kmli', $email, time()+40000, '/');       
                }else{
                    delete_cookie('educki_kmli');
                }

                // $this->input->set_cookie($cookie);
                // echo '<pre>';
                // print_r($_COOKIE);
                // exit;
                //    $cartId = $CI->session->userdata('cartId');
                $result = $this->common_model->commonSelect("tbl_cart", "cart_user_id", $regData->user_id)->result();
                $cartId = $result[0]->cart_id;
                
                $sessiondata = array(
                    'userid'    => $regData->user_id,
                    'useremail' => $regData->user_email,
                    'userfname' => $regData->user_fname,
                    'userlname' => $regData->user_lname,
                    'cartId'    => $cartId,
                    'usertype' => $regData->user_type,
                    'logged_in' => true,
                );            

                $this->session->set_userdata($sessiondata);
                $loginData['login_user_id']    = $regData->user_id;
                $loginData['login_date']       = date('Y-m-d H:i:s');
                $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
                $this->common_model->commonSave('tbl_user_login', $loginData);
                $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData->user_id);
                echo 1;
            } else {
                echo 0;
            }
        }else{
            $email    = $this->input->post('email');
            $password = $this->input->post('password');
            $check    = $this->input->post('check');
            $regData  = $this->db->query("
                                SELECT *
                                FROM tbl_user
                                WHERE user_email ='".$email."'
                                AND user_password ='".$password."'
                                AND user_status = 1 AND user_is_delete = 0
                            ")->row();
            $creds                  = array();
            $creds['user_login']    = $email;
            $creds['user_password'] = $password;
            $creds['remember']      = true;
            $user                   = wp_signon($creds, false);

            /*$cookie = array(
                        'name'   => 'educki_kmli',
                        'value'  => $check,
                        'secure' => false,
                        );*/
                           
            if (!empty($regData) ) {
                if($check === 'true'){
                     $cookie = array(
                            'name'   => 'kmli',
                            'value'  => $email,
                            'expire' => time()+60*60*24*365,    //set expire time to a year
                            'domain' => $this->data['url'],
                            'path' => '/',
                            'prefix' => 'educki_',
                         );     
                    
                    set_cookie($cookie);
                    setcookie('educki_kmli', $email, time()+60*60*24*365, '/');       
                }else{
                    delete_cookie('educki_kmli');
                }

                // $this->input->set_cookie($cookie);
                // echo '<pre>';
                // print_r($_COOKIE);
                // exit;
                
                $result = $this->common_model->commonSelect("tbl_cart", "cart_user_id", $regData->user_id)->row();
                $cartId = $result->cart_id;
                if(!empty($result)){
                    $is_prod =  $this->common_model->commonSelect("tbl_cart_detail", "cart_id", $result->cart_id)->row();
                    if(!empty($is_prod)){
                        $cartId = $result->cart_id;
                    }else{
                        $this->cart_model->removeCart($result->cart_id);

                        $cartId = $this->session->userdata('cartId');
                        if ($cartId == '') {
                            $cartId = get_cookie('educki_cartId');
                        }
                        $addUser['cart_user_id'] = $regData->user_id;

                        $this->db->where('cart_id', $cartId);
                        $this->db->update('tbl_cart', $addUser);
                    }
                }else{
                    $cartId = get_cookie('educki_cartId');
                    $addUser['cart_user_id'] = $regData->user_id;
                    $this->db->where('cart_id', $cartId);
                    $this->db->update('tbl_cart', $addUser);
                }

                
                $sessiondata = array(
                    'userid'    => $regData->user_id,
                    'useremail' => $regData->user_email,
                    'userfname' => $regData->user_fname,
                    'userlname' => $regData->user_lname,
                    'cartId'    => $cartId,
                    'usertype' => $regData->user_type,
                    'logged_in' => true,
                );            

                $this->session->set_userdata($sessiondata);
                $loginData['login_user_id']    = $regData->user_id;
                $loginData['login_date']       = date('Y-m-d H:i:s');
                $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
                $this->common_model->commonSave('tbl_user_login', $loginData);
                $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData->user_id);
                $return['status'] = 1;
                $return['tier_check'] = $this->session->userdata('pricing_benefits_clicked');
            } else {
                $return['status'] = 0;
                $return['tier_check'] = $this->session->userdata('pricing_benefits_clicked');
            }

            echo json_encode($return);
        }
    }

    public function loginWithFacebook()
    {
        $this->load->library('facebook');
        $data['user'] = array();

        // Check if user is logged in
        if ($this->facebook->is_authenticated()) {
            // User logged in, get user details
            $user = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender');
            if (!isset($user['error']) && $user['id'] != '') {
                $data['user'] = $user;
            } else {
                $this->facebook->destroy_session();
                $this->session->unset_userdata('userid');
                $this->session->unset_userdata('useremail');
                $this->session->unset_userdata('userfname');
                $this->session->unset_userdata('userlname');
                $this->session->unset_userdata('logged_in');
                redirect(base_url());
                exit;
            }
        }
        if (@$data['user']['id'] != '') {
            $array['user_facebook_id']   = $data['user']['id'];
            $array['user_email']         = $data['user']['email'];
            $array['user_fname']         = $data['user']['first_name'];
            $array['user_lname']         = $data['user']['last_name'];
            $array['user_sex']           = $data['user']['gender'];
            $array['user_register_date'] = date('Y-m-d H:i:s');
            $array['user_last_active']   = date('Y-m-d H:i:s');
            $regData                     = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_facebook_id  = '" . $array['user_facebook_id'] . "' AND user_is_delete = 0
                        ")->row();
            if (count($regData) > 0 && $regData->user_email == $array['user_email']) {
                $sessiondata = array(
                    'userid'    => $regData->user_id,
                    'useremail' => $regData->user_email,
                    'userfname' => $regData->user_fname,
                    'userlname' => $regData->user_lname,
                    'usertype'  => $regData->user_type,
                    'logged_in' => true,
                );
                require_once ABSPATH . 'wp-load.php';
                $userdata = get_user_by('email', $regData->user_email); //get user news-advice Table id by email
                $remember = true;
                wp_set_auth_cookie($userdata->ID, $remember);
                $this->session->set_userdata($sessiondata);
                
                delete_cookie('educki_kmli');
                $loginData['login_user_id']    = $regData->user_id;
                $loginData['login_date']       = date('Y-m-d H:i:s');
                $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
                $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData->user_id);
                $this->common_model->commonSave('tbl_user_login', $loginData);
                redirect(base_url());
                exit;
            } else if (count($regData) > 0 && $regData->user_email != $array['user_email']) {
                $regData2 = $this->db->query("
                                SELECT *
                                FROM tbl_user
                                WHERE user_email  = '" . $array['user_email'] . "' AND user_is_delete = 0
                            ")->row();
                if (count($regData2) <= 0) {
                    $update['user_email'] = $array['user_email'];
                    $this->common_model->commonUpdate('tbl_user', $update, 'user_facebook_id', $data['user']['id']);
                    $sessiondata = array(
                        'userid'    => $regData2->user_id,
                        'useremail' => $array['user_email'],
                        'userfname' => $regData2->user_fname,
                        'userlname' => $regData2->user_lname,
                        'usertype'  => $regData2->user_type,
                        'logged_in' => true,
                    );
                    require_once ABSPATH . 'wp-load.php';
                    $userdata = get_user_by('email', $array['user_email']); //get user news-advice Table id by email
                    $remember = true;
                    wp_set_auth_cookie($userdata->ID, $remember);
                    $this->session->set_userdata($sessiondata);
                    
                    delete_cookie('educki_kmli');

                    $loginData['login_user_id']    = $regData2->user_id;
                    $loginData['login_date']       = date('Y-m-d H:i:s');
                    $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
                    $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData2->user_id);
                    $this->common_model->commonSave('tbl_user_login', $loginData);
                    redirect(base_url());
                    exit;
                } else {
                    $this->session->set_flashdata('fbReg', 'This email already exist.');
                    redirect(base_url());
                    exit;
                }
            } else {
                // fb_id not exist
                $regData3 = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_email  = '".$array['user_email']."' AND user_is_delete = 0
                        ")->row();
                if (count($regData3) > 0) {
                    $this->session->set_flashdata('fbReg', 'This email already exist.');
                    redirect(base_url());
                    exit;
                } else {
                    $array['user_status'] = 1;
                    $array['user_type'] = 'Buyer';
                    $user_id              = $this->common_model->commonSave('tbl_user', $array);
                    $sessiondata          = array(
                        'userid'    => $user_id,
                        'useremail' => $array['user_email'],
                        'userfname' => $array['user_fname'],
                        'userlname' => $array['user_lname'],
                        'usertype'  => 'Buyer',
                        'logged_in' => true,
                    );
                    require_once ABSPATH . 'wp-load.php';
                    $userdata = get_user_by('email', $array['user_email']); //get user news-advice Table id by email
                    $remember = true;
                    wp_set_auth_cookie($userdata->ID, $remember);
                    $this->session->set_userdata($sessiondata);

                    delete_cookie('educki_kmli');
                    
                    $loginData['login_user_id']    = $user_id;
                    $loginData['login_date']       = date('Y-m-d H:i:s');
                    $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
                    $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData2->user_id);
                    $this->common_model->commonSave('tbl_user_login', $loginData);
                    redirect(base_url());
                    exit;
                }
            }
        } else {
            $this->session->set_flashdata('fbReg', 'There was a problem connecting to Facebook.');
            redirect(base_url());
            exit;
        }
    }

    public function logout()
    {
        $this->facebook->destroy_session();
        
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('tier_id');
        $this->session->unset_userdata('useremail');
        $this->session->unset_userdata('userfname');
        $this->session->unset_userdata('usertype');
        $this->session->unset_userdata('userlname');
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('cartId');
        $this->session->unset_userdata('isCart');
//        echo "<pre>";print_r($_COOKIE);
        if(isset($_COOKIE['educki_kmli'])){
//            delete_cookie('educki_kmli', "", time() - 3600);
            delete_cookie('educki_kmli');
        }
        

        $expires = time() - 50;
        setcookie('isCart',"FALSE",$expires,$this->data['url'],'/','educki_');
        setcookie('cartId',0,$expires,$this->data['url'],'/','educki_');
        setcookie('userId',0,$expires,$this->data['url'],'/','educki_');
        setcookie('educki_isCart', "FALSE", $expires, '/');
        setcookie('educki_cartId', 0, $expires, '/');
        setcookie('educki_userId', 0, $expires, '/');
        wp_logout();
        //echo "<pre>";print_r($_COOKIE);die;
        redirect(base_url());
        exit;
    }

    public function is_valid_email($email)
    {
        $regData = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_email = '" . $email . "'
                            AND user_status = 1
                        ")->row();
        if (count($regData) > 0) {
            if($regData->user_facebook_id!=''){
                $this->form_validation->set_message('is_valid_email', 'Our records do not indicate this account exists.');
                return false;
            }else{
                return true;
            }
        } else {
            $this->form_validation->set_message('is_valid_email', 'Our records do not indicate this account exists.');
            return false;
        }
    }

    public function forgotPassword()
    {

        $email         = $this->input->post('email');
        $data['title'] = "Forgot Password";
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|callback_is_valid_email');
        $this->form_validation->set_message('email', 'Please enter a valid email address.');

        if ($this->form_validation->run() == false) {
            $data['content_view'] = "forgot-password";
            $this->load->view("front/custom_template", $data);
        } else {
            $regData = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_email = '" . $this->input->post('email') . "'
                            AND user_status = 1  AND user_is_delete = 0
                        ")->row();

            $data_save['user_activation_code_password'] = md5($email . time());

            $this->common_model->commonUpdate('tbl_user', $data_save, "user_id", $regData->user_id);

            $url   = base_url('reset-password/' . $data_save['user_activation_code_password']);
            $click = "<a href='" . $url . "'>Click here </a>";

            $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'password-reset')->row();
            $emailData['subject'] = $email_data->e_email_subject;
            $message              = $email_data->e_email_text;
            $message              = str_replace("{{username}}", $regData->user_fname, $message);
            $message              = str_replace("{{link}}", $url, $message);
            //$message              = str_replace("{{clickHere}}", $click, $message);
            $emailData['message'] = $message;
            $emailData['to']      = $regData->user_email;
            
            $is_send = send_email_2($regData->user_email, 'noreply@educki.com', $email_data->e_email_subject, $message);

            if ($is_send) {
                $this->session->set_flashdata('forgotEmail1', "We have sent an email to " . $this->input->post('email') . ".");
                redirect(base_url('forgot-password'));
                exit;
            } else {
                $this->session->set_flashdata('forgotEmail2', "An error occur due to some problem.");
                redirect(base_url('forgot-password'));
                exit;
            }
        }
    }

    public function resetPassword($uniqueStr = '')
    {
        $this->facebook->destroy_session();
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('useremail');
        $this->session->unset_userdata('userfname');
        $this->session->unset_userdata('userlname');
        $this->session->unset_userdata('logged_in');
        if ($uniqueStr == '') {
            $this->session->userdata('resetPass', 'No Password Activation key is found.');
            redirect(base_url());
            exit;
        } else {
//            $where = array('user_activation_code_password' => mysql_real_escape_string($uniqueStr));
            $where  = array('user_activation_code_password' => $uniqueStr);
            $result = $this->common_model->commonselect_array('tbl_user', $where);
            if ($result->num_rows() > 0) {
                $row = $result->row();
                redirect(base_url("retrive-password/" . $uniqueStr));
                exit;
            } else {
                $this->session->userdata('resetPass', 'No such Password Activation key exists in our database.');
                redirect(base_url());
                exit;
            }
        }
    }

    public function retrivepassword($uniqueStr = '')
    {
        $this->facebook->destroy_session();
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('useremail');
        $this->session->unset_userdata('userfname');
        $this->session->unset_userdata('userlname');
        $this->session->unset_userdata('logged_in');
        if ($uniqueStr == '') {
            $this->session->userdata('resetPass', 'No Password Activation key is found.');
            redirect(base_url());
            exit;
        }
        $where  = array('user_activation_code_password' => $uniqueStr);
        $result = $this->common_model->commonselect_array('tbl_user', $where);
        if ($result->num_rows() <= 0) {
            $data['result'] = $result->row();
            $this->session->set_flashdata('msg', 'No such Password Activation key exists in our database.');
            redirect(base_url());
            exit;
        }

        $data['title'] = "New Password";
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[new_password]');

        $this->form_validation->set_message('required', 'This field is required.');
//        $this->form_validation->set_message('matches', 'These password do not match.');

        if ($this->form_validation->run() == false) {
            $data['content_view'] = "update-password";
            $this->load->view("front/custom_template", $data);
        } else {

            $data_save['user_password']                 = $this->db->escape_str($this->input->post('new_password'));
            $data_save['user_password_encryp']          = md5($this->db->escape_str($this->input->post('confirm_password')));
            $data_save['user_activation_code_password'] = '';

            $this->common_model->commonUpdate('tbl_user', $data_save, "user_activation_code_password", $uniqueStr);
            $this->session->set_flashdata('pass_chnage_msg', "Your password changed successfully");
            redirect(base_url());
            exit;
        }
    }

}
