<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contact extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/contact_model');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
        //auth();
        date_default_timezone_set('America/New_York');
    }

    public function index()
    {
        $data['pagedata']         = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 15)->row();
        $data['title']            = $data['pagedata']->sub_pg_title;
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;

        /*echo "<pre>";
        print_r($data);
        exit;*/

        $this->form_validation->set_rules('name', 'Full Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('message', 'Message', 'required|trim');
        $this->form_validation->set_rules('category', 'Category', 'trim');
        $this->form_validation->set_rules('subject', 'Subject', 'trim');

        if ($this->form_validation->run() == false) {
            $data['content_view'] = "contact/contact";
            $this->load->view("front/custom_template", $data);
        } else {
            $dataDB['fname']        = stripcslashes($this->input->post('name'));
            $dataDB['subject']      = stripcslashes($this->input->post('subject'));
            $dataDB['email']        = stripcslashes($this->input->post('email'));
            $dataDB['comment']      = stripcslashes($this->input->post('message'));
            $dataDB['date_created'] = date('Y-m-d h:i:s');

            if ($this->input->post('category') != "") {
                $dataDB['cat_id']   = stripcslashes($this->input->post('category'));
                $dataDB['cat_name'] = stripcslashes($this->input->post('cat_name'));
            }

            $File1      = $_FILES['fileupload'];
            $field_name = 'fileupload';
            $result     = $this->file_upload($File1, $field_name);

            if ($result) {
                $dataDB['filename'] = $result;
                $file               = base_url() . "resources/contact_form_file/" . $dataDB['filename'];
            } else {
                $file = '';
            }

            $message = "<b>Name :</b>" .$dataDB['fname'] . "<br>";
            $message .= "<b>Email :</b> " . $dataDB['email'] . "<br>";
            if ($dataDB['subject'] != '') {
                $message .= "<b>Subject :</b> ".$dataDB['subject'] . "<br>";
            } else {
                $message .= "<b>Subject :</b> N/A" . "<br>";
            }
            if ($this->input->post('category') != "") {
                $message .= "<b>Category :</b> " . $dataDB['cat_name'] . "<br>";
            } else {
                $message .= "<b>Category :</b> N/A" . "<br>";
            }
            $message .= "<b>Message :</b> " . $dataDB['comment'] . "<br>";
            $to   = getSiteSettings('site_email');
            $from = 'noreply@educki.com';

            send_email_2($to, $from, 'Contact form inquiry', $message, $file);
            
            //sending mail to user
            $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'contact-us')->row();
            $emailData['subject'] = $email_data->e_email_subject;
            $message              = $email_data->e_email_text;

            
            $message              = str_replace("{{username}}", $dataDB['fname'], $message);
            //$message              = str_replace("{{clickHere}}", $click, $message);
            $emailData['message'] = $message;

            /*echo "<pre>";
            print_r(htmlentities($emailData['message']));
            exit;*/
			            
            send_email($dataDB['email'], "noreply@educki.com", $emailData['subject'], $message);
            //sending mail to user
            $this->contact_model->add_contact_form($dataDB);
            /* $this->sendEmailAlert($dataDB);*/

            redirect(base_url('thank-you'));
            exit;
        }
    }

    public function contact_thanks()
    {
        $data['title']            = "Thank You";
        $data['meta_description'] = "eDucki";
        $data['meta_keyword']     = "eDucki";
        $data['message']          = "Form Submitted Successfully.";
        $data['content_view']     = "contact/thank-you";
        $this->load->view("front/custom_template", $data);
    }

    public function register_subscriber()
    {
        $data['sub_email'] = $this->input->post('email');
        $check['is_exist']  = $this->common_model->commonselect2('tbl_subscriber', 'sub_email', $data['sub_email'],'sub_is_delete',0)->row();
        if (count($check['is_exist']) <= 0) {
            $data['sub_created_date'] = date('Y-m-d h:i:s');
            
            if ( $this->common_model->commonSave('tbl_subscriber', $data) ) {

                send_email(getSiteSettings('site_email'), "noreply@educki.com", "Subscriptions", $data['sub_email']." has Subscribed");

            }
            
            echo 1;
        }
        else{
            echo 5;
        }
    }

    public function what_is_educki()
    {
        $data['pagedata']         = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 5)->row();
        $data['pageurl']          = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 5)->row();
        $data['content_view']     = "about/what-is-educki";
        $data['work_data']        = $this->common_model->commonSelect('tbl_how_work_page', 'work_id', 1)->row();
        $data['title']            = $data['work_data']->work_page_title;
        $data['meta_title']       = $data['work_data']->meta_title;
        $data['meta_description'] = $data['work_data']->meta_desc;
        $data['meta_keyword']     = $data['work_data']->meta_keywords;
        $this->load->view("front/custom_template", $data);
    }

    public function community_guidelines()
    {
        $data['pagedata']         = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 9)->row();
        $data['pageurl']          = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 5)->row();
        $data['title']            = "Community Guidelines";
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['page_data']        = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 9)->row();
        $data['content_view']     = "about/about-community";
        $this->load->view("front/custom_template", $data);
    }

    public function guide_to_educki()
    {
        $data['pagedata']         = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 10)->row();
        $data['title']            = "Guide to eDucki";
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "about/guide-to-educki";
        $this->load->view("front/custom_template", $data);
    }

    public function faq_educki()
    {
        $data['pagedata']         = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 6)->row();
        $data['title']            = "FAQ";
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "faq";
        $this->load->view("front/custom_template", $data);
    }

    private function file_upload($Files1, $field_name)
    {

        $filename = array();
        if ($Files1['name'] !== "") {
            $config = array(
                'allowed_types' => 'pdf|doc|docx|ppt|txt|jpg|jpeg|png', //doc|docx|ppt|ppt|txt|
                'upload_path'   => FCPATH . 'resources/contact_form_files/',
                'file_name'     => 'file_' . date('Y_m_d_h_i_s'),
            );
            $this->load->library('upload');
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($field_name)) {

                //Print message if file is not uploaded
                $data = array($field_name . '_error' => $this->upload->display_errors());
                return false;
            } else {
                $dataDP          = $this->upload->data();
                return $filename = $dataDP['file_name'];
            }
        }
    }

    private function sendEmailAlert($data)
    {
        /*$config = array(
        'protocol'    => 'smtp',
        'smtp_host'   => 'smtp.gmail.com',
        'smtp_port'   => 465,
        '_smtp_auth'  => true,
        'smtp_user'   => 'mikesmith1166@gmail.com',
        'smtp_pass'   => 'S1234mike',
        'smtp_crypto' => 'ssl',
        'mailtype'    => 'html',
        'charset'     => 'utf-8',
        'validate'    => true,
        );
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");*/

        //$this->load->library('email');
        if (array_key_exists('cat_name', $data)) {$data['category'] = $data['cat_name'];} else { $data['category'] = "N/A";}
        if ($data['subject'] != '') {$data['subject_email'] = $data['subject'];} else { $data['subject_email'] = "N/A";}
        if (array_key_exists('filename', $data)) {$data['file'] = $data['filename'];} else { $data['file'] = "No Attachment";}
        $this->email->set_mailtype('html');
        $this->email->from($data['email'], $data['fname']);
        $this->email->to($this->get_admin_email());
        $this->email->subject("New Contact us form");
        $message = "Hello Admin,<br/>Name : " . $data['fname'] . "<br/>Subject : " . $data['subject_email'] . "<br/>Category : " . $data['category'] . "<br/>Comment : " . $data['comment'] . "<br/>File Attached : " . $data['file'];
        if (array_key_exists('filename', $data)) {
            $this->email->attach(base_url() . 'resources/contact_form_files/' . $data['filename']);
        }
        $this->email->message($message);
/*        echo "<pre>";
print_r($data);
echo $message;
$this->email->print_debugger();
exit;*/
        $this->email->send();
    }

    private function get_admin_email()
    {
        $this->db->select('site_email');
        $this->db->from('tbl_site_setting');
        $query  = $this->db->get();
        $result = $query->row();
        return $result->site_email;
    }
}
