<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
        $this->load->model('setting_model');
        auth();
		
    }
	
	
	public function valid_with_zero($value)
	{
		if($value >= 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('valid_with_zero', "This field must be greater than or equal to zero.");
			return false;
		}
	}
	
	public function valid_time($stand_time, $exp_time)
	{
		
		if(strtotime($exp_time) >= strtotime($stand_time))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('valid_time', "This field must be less than or equal to expedited time.");
			return false;
		}
		//date('H:i:s', time());
		
	}
	
	public function validate_zipcode($zipcode) 
	{
		//$test = preg_match("/^[0-9-]{11}*$/",$zipcode);
		$test = preg_match('/^[0-9]{5}([- ]?[0-9]{4})?$/', $zipcode);
		
		if ($test == 0)
		{
			$this->form_validation->set_message('validate_zipcode', 'Please enter a valid zip code.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function valid_file($name)
    {
        if ($name != "" && !empty($name) && $name != "Upload File") {
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png") {
                return true;
            } else {
                $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');
                return false;
            }
        } else {
            $this->form_validation->set_message('valid_file', 'This field is required.');
            return false;
        }
    }
	
	public function index()
	{
		//read_only_user();
		$data['title'] = "Settings";
		$data['value'] = $this->setting_model->listing();
		
		
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('site_email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('site_phone', 'zip code', 'trim|required');
		//$this->form_validation->set_rules('tax', 'Tax', 'trim|numeric|callback_valid_with_zero');
		$this->form_validation->set_rules('mailing_address', 'mailing address', 'trim');
		$this->form_validation->set_rules('site_fax', 'fax no.', 'trim');
		$this->form_validation->set_rules('facebook', 'facebook', 'trim');
		$this->form_validation->set_rules('pinterest', 'pinterest', 'trim');
		$this->form_validation->set_rules('twitter', 'twitter', 'trim');
		$this->form_validation->set_rules('google', 'Google Plus', 'trim');
		$this->form_validation->set_rules('linkedin', 'linkedin', 'trim');
		//$this->form_validation->set_rules('youtube', 'youtube', 'trim');
		$this->form_validation->set_rules('filehidden', 'logo', 'trim|required|callback_valid_file');

		$this->form_validation->set_message('required', 'This field is required.');
		$this->form_validation->set_message('valid_email', 'Please enter a valid email address.');
		$this->form_validation->set_message('numeric', 'This field must be numeric.');
		
		if($this->form_validation->run() == FALSE) {
			
			$this->load->view('admin/setting/add_setting' ,$data);			
		}
		else {
			
			$arrImg = '';
			
			if (@$_FILES['file']['name'] == '' && $this->input->post('filehidden') != '') {
                $arrImg = $this->input->post('filehidden');
            } else {
            	if (isset($_FILES['file'])) {
                    $myFiles = $_FILES['file'];

                    $_FILES['file']['name']     = $myFiles['name'];
                    $_FILES['file']['type']     = $myFiles['type'];
                    $_FILES['file']['tmp_name'] = $myFiles['tmp_name'];
                    $_FILES['file']['error']    = $myFiles['error'];
                    $_FILES['file']['size']     = $myFiles['size'];

                    $file_name = $myFiles['name'];
                    $file_ext       = explode('.', $file_name);
                    $file_extension = end($file_ext);

                    $image_path = $myFiles['tmp_name'];

                    $this->load->library('upload');
                    $file_Post = '';

                    if ($myFiles['name'] != '') {
                        $new_prod_image_name = "img_" . rand(1, 1000) . time();
                        $arrImg = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                        //If user selected the photo to upload .
                        $uploaddir_11               = FCPATH."resources/company_logo/";
                        $uploaddir_11               = str_replace(" ", "", $uploaddir_11);
                        $config_11['upload_path']   = $uploaddir_11;
                        $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                        $config_11['overwrite']     = false;
                        $config_11['remove_spaces'] = true;
                        $config_11['file_name']     = $new_prod_image_name_ext;

                        $this->upload->initialize($config_11);
                        if (!$this->upload->do_upload('file')) {
                        	$this->session->set_flashdata('msg_err', $this->upload->display_errors());
                            redirect(base_url() . "admin/setting");
                            exit();
                        } else {
                            $uploadthumb = FCPATH."resources/company_logo/thumb/";
                            $uploadthumb = str_replace(" ", "", $uploadthumb);
                            $width            = '80';
                            $height           = '';
                            $thumb_image_name = $new_prod_image_name_ext;
                            ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                            $config_thumb['upload_path'] = $uploadthumb;
                            $this->upload->initialize($config_thumb);
                            $dataDP = $this->upload->data();
                        } //else upload END
                    } //if (file[name] != '') END
                } //if isset(file) END
            }

			$array = array(
						'name' => $this->input->post('name'),
						'site_email' => $this->input->post('site_email'),
						'mailing_address' => $this->input->post('mailing_address'),
						'site_phone' => $this->input->post('site_phone'),
						'site_fax' => $this->input->post('site_fax'),
						'facebook' => $this->input->post('facebook'),
						'twitter' => $this->input->post('twitter'),
						'google_plus' => $this->input->post('google'),
						'linkedin' => $this->input->post('linkedin'),
						//'youtube' => $this->input->post('youtube'),
						'pinterest' => $this->input->post('pinterest'),
						'site_logo' => $arrImg,
						// 'tax' => $this->input->post('tax')
					);
			$data['result'] = $this->setting_model->add_setting($array);
			$data['msg'] = 'Item updated successfully.';
			$this->session->set_flashdata('msg', $data['msg']);
			redirect(base_url('admin/setting'));
			exit;
		}
    }
}

?>