<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Career extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('careers/career_model');
//        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
        auth();
    }

    public function index() {
        $where = ' WHERE career_is_deleted = 0';
        $data['total_records'] = count($this->common_model->get_data_page("tbl_career", $where)->result_array());
        $pagewhere = 'career_is_active = 0';
        $data['career_page_id'] = $this->common_model->getSingleValue("tbl_career_page", $pagewhere, "carrier_page_id");
        $data['list'] = $this->common_model->get_data_page("tbl_career", $where, '', '', $is_count = 0, 'career_id', $order = 'DESC')->result_array();
        $this->load->view('admin/career/career_list', $data);
    }

    public function add_career() {
        $data_['title'] = "Add Career";
        $this->form_validation->set_rules('title', 'blog name', 'trim|required');
        $this->form_validation->set_rules('career_description', 'description', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        // $this->form_validation->set_rules('link_url', 'staxcxctus', 'trim|required');
        $this->form_validation->set_rules('career_area', 'Area', 'trim|required');
        $this->form_validation->set_rules('city', 'category', 'trim|required');
        $this->form_validation->set_rules('state', 'category', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');
        $this->form_validation->set_message('required', 'This field is required.');
        $data_['allstate'] = $this->db->query("
										SELECT stat_id, stat_name 
										FROM tbl_states
										")->result_array();
        // echo '<pre>';
        // print_r($_POST);
        // exit;
        if ($this->form_validation->run() == False) {
            $this->load->view('admin/career/career_add', $data_);
        } else {
            $slug = stripcslashes($this->input->post('title')). "-" . $this->input->post('state') ;
            $data['career_title'] = stripcslashes($this->input->post('title'));
            $data['career_area'] = stripcslashes($this->input->post('career_area'));
            $data['career_url'] = $this->common_model->create_slug($slug, 'tbl_career','career_url');
            $data['career_description'] = stripcslashes(($this->input->post('career_description')) ? $this->input->post('career_description') : '');
            $data['career_city'] = ($this->input->post('city')) ? stripcslashes($this->input->post('city')) : '';
            $data['career_state'] = ($this->input->post('state')) ? stripcslashes($this->input->post('state')) : '';
            $data['career_is_active'] = $this->input->post('status');
            $data['meta_title'] = ($this->input->post('meta_title')) ? stripcslashes($this->input->post('meta_title')) : '';
            $data['meta_keywords'] = ($this->input->post('meta_keywords')) ? stripcslashes($this->input->post('meta_keywords')) : '';
            $data['meta_description'] = ($this->input->post('meta_description')) ? stripcslashes($this->input->post('meta_description')) : '';
            $data['career_created_date'] = date('Y-m-d h:i:s');
            $data['career_admin_id'] = $this->session->userdata('admin_id');
            /*$j = 0;
            for ($j = 0; $j < 2; $j++) {#$totalFiles
                if (isset($_FILES['file' . ($j + 1)])) {
                    $myFiles = $_FILES['file' . ($j + 1)];
                    $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                    $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                    $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                    $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                    $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];
                    $file_name = $myFiles['name'];
                    $file_ext = explode('.', $file_name);
                    $file_extension = end($file_ext);
                    $image_path = $myFiles['tmp_name'];
                    $this->load->library('upload');
                    $file_Post = '';
                    if ($myFiles['name'] != '') {
                        $new_prod_image_name = "img_" . rand(1, 1000) . time();
                        $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;
                        //If user selected the photo to upload .
                        $uploaddir_11 = FCPATH . "resources/career_image/";
                        $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                        $config_11['upload_path'] = $uploaddir_11;
                        $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                        $config_11['overwrite'] = false;
                        $config_11['remove_spaces'] = true;
                        $config_11['file_name'] = $new_prod_image_name_ext;
                        $this->upload->initialize($config_11);
                        if (!$this->upload->do_upload('file' . ($j + 1))) {
                            $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                            redirect(base_url() . "admin/career/career_add/");
                            exit;
                        } else {
                            $uploadthumb = FCPATH . "resources/career_image/thumb/";
                            $uploadthumb = str_replace(" ", "", $uploadthumb);
                            $width = 200;
                            $height = 200; //200
                            $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                            ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                            $config_thumb['upload_path'] = $uploadthumb;
                            $this->upload->initialize($config_thumb);
                            $dataDP = $this->upload->data();
                        } //else upload END
                    }//if (file[name] != '') END
                }//if isset(file) END
            }//for loop END
            $data['career_image'] = $arrImg[0];
            $data['career_image_thumb'] = $arrImg[1];
            */
            $career_id = $this->common_model->commonSave('tbl_career', $data);
            if ($career_id != '') {
                $this->session->set_flashdata('msg', 'Item added successfully.');
                redirect(base_url() . "admin/career");
                exit;
            } else {
                $this->session->set_flashdata('msg', 'Error occur Please contact Administrator.');
                redirect(base_url() . "admin/career");
                exit;
            }
        }
    }

    public function edit_career($career_id = '') {

        if ($career_id == '' || !is_numeric($career_id)) {
            redirect(base_url('admin/career'));
            exit;
        }
        $this->form_validation->set_rules('title', 'blog name', 'trim|required');

        $this->form_validation->set_rules('description', 'description', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        $this->form_validation->set_rules('area', 'category', 'trim|required');
        $this->form_validation->set_rules('city', 'category', 'trim|required');
        $this->form_validation->set_rules('state', 'category', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');
        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == FALSE) {
            $data_['allstate'] = $this->db->query("
						SELECT stat_id, stat_name 
						FROM tbl_states
						")->result_array();
            $data_['career_data'] = $this->common_model->commonselect('tbl_career', 'career_id', $career_id)->row();
            $this->load->view('admin/career/career_edit', $data_);
        } else {
            $slug = stripcslashes($this->input->post('title')). "-" . $this->input->post('state') ;
            $data['career_title'] = stripcslashes($this->input->post('title'));
            $data['career_area'] = stripcslashes($this->input->post('area'));
            $data['career_url'] = $this->common_model->create_slug($slug, 'tbl_career','career_url');
            $data['career_description'] = stripcslashes(($this->input->post('description')) ? stripcslashes($this->input->post('description')) : '');
            $data['career_city'] = ($this->input->post('city')) ? stripcslashes($this->input->post('city')) : '';
            $data['career_state'] = ($this->input->post('state')) ? stripcslashes($this->input->post('state')) : '';
            $data['career_is_active'] = $this->input->post('status');
            $data['meta_title'] = ($this->input->post('meta_title')) ? stripcslashes($this->input->post('meta_title')) : '';
            $data['meta_keywords'] = ($this->input->post('meta_keywords')) ? stripcslashes($this->input->post('meta_keywords')) : '';
            $data['meta_description'] = ($this->input->post('meta_description')) ? stripcslashes($this->input->post('meta_description')) : '';
            $data['career_created_date'] = date('Y-m-d h:i:s');
            $data['career_admin_id'] = $this->session->userdata('admin_id');
            /*$j = 0;
            for ($j = 0; $j < 2; $j++) {#$totalFiles
                if (@$_FILES['file' . ($j + 1)]['name'] == '' && $this->input->post('hidden_file' . ($j + 1)) != '') {
                    $arrImg[$j] = $this->input->post('hidden_file' . ($j + 1));
                } else {
                    if (isset($_FILES['file' . ($j + 1)])) {
                        $myFiles = $_FILES['file' . ($j + 1)];

                        $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                        $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                        $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                        $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                        $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];
                        $file_name = $myFiles['name'];
                        $file_ext = explode('.', $file_name);
                        $file_extension = end($file_ext);
                        $image_path = $myFiles['tmp_name'];
                        $this->load->library('upload');
                        $file_Post = '';

                        if ($myFiles['name'] != '') {
                            $new_prod_image_name = "img_" . rand(1, 1000) . time();

                            $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                            //If user selected the photo to upload .
                            $uploaddir_11 = FCPATH . "resources/career_image/";
                            $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                            $config_11['upload_path'] = $uploaddir_11;
                            $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                            $config_11['overwrite'] = false;
                            $config_11['remove_spaces'] = true;
                            $config_11['file_name'] = $new_prod_image_name_ext;

                            $this->upload->initialize($config_11);
                            if (!$this->upload->do_upload('file' . ($j + 1))) {
                                $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                                redirect(base_url() . "admin/categories/edit_category/" . $nId);
                                exit();
                            } else {
                                $uploadthumb = FCPATH . "resources/career_image/thumb/";
                                $uploadthumb = str_replace(" ", "", $uploadthumb);
                                $width = 200;
                                $height = 200; //200
                                $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                                ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                                $config_thumb['upload_path'] = $uploadthumb;
                                $this->upload->initialize($config_thumb);
                                $dataDP = $this->upload->data();
                            } //else upload END
                        }//if (file[name] != '') END
                    }//if isset(file) END
                }
            }//for loop END
            $data['career_image'] = $arrImg[0];
            $data['career_image_thumb'] = $arrImg[1];
*/
            $this->common_model->commonUpdate('tbl_career', $data, 'career_id', $career_id);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/career");
            exit;
        }
    }

    public function delete_career($career_id) {

        if ($career_id == '' || !is_numeric($career_id)) {
            redirect(base_url() . "admin/career");
            exit;
        }
        $data['record'] = $this->common_model->commonselect('tbl_career', 'career_id', $career_id)->row_array();
        if ($data['record'] == '') {
            redirect(base_url() . "admin/career");
            exit;
        }
        //$this->common_model->commonDelete('tbl_career', 'career_id', $career_id);
        $data_save['career_is_deleted'] = 1;
        $this->common_model->commonUpdate('tbl_career', $data_save, 'career_id', $career_id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url() . "admin/career");
        exit;
    }

    public function career_page() {

        $data_['title'] = "Add Career Page";
        $this->form_validation->set_rules('pg_title', 'Page name', 'trim|required');
        $this->form_validation->set_rules('pg_description', 'page description', 'trim|required');
        $this->form_validation->set_rules('banner_title', 'banner title', 'trim|required');
        $this->form_validation->set_rules('first_image_title', 'First image title', 'trim|required');
        $this->form_validation->set_rules('first_image_description', 'First image description', 'trim|required');
        $this->form_validation->set_rules('second_image_title', 'second image title', 'trim|required');
        $this->form_validation->set_rules('second_image_description', 'secong image description', 'trim|required');
        $this->form_validation->set_rules('third_image_title', 'Third image title', 'trim|required');
        $this->form_validation->set_rules('third_image_description', 'Third image description', 'trim|required');
        $this->form_validation->set_message('required', 'This field is required.');
        if ($this->form_validation->run() == False) {
            $this->load->view('admin/career/add_career_page', $data_);
        } else {

            $career = $this->career_model->add_carrier_page();
            if ($career == TRUE) {
                $this->session->set_flashdata('msg', 'Career page added successfully.');
                redirect(base_url() . "admin/career");
            }
        }
    }

    public function edit_career_page($page_id) {
        if ($page_id == '' || !is_numeric($page_id)) {
            redirect(base_url('admin/career'));
            exit;
        }
        $data_['title'] = "Edit Career Page";
        $this->form_validation->set_rules('pg_title', 'Page name', 'trim|required');
        $this->form_validation->set_rules('pg_description', 'page description', 'trim|required');
        $this->form_validation->set_rules('banner_title', 'banner title', 'trim|required');
        $this->form_validation->set_rules('first_image_title', 'First image title', 'trim|required');
        $this->form_validation->set_rules('first_image_description', 'First image description', 'trim|required');
        $this->form_validation->set_rules('second_image_title', 'second image title', 'trim|required');
        $this->form_validation->set_rules('second_image_description', 'secong image description', 'trim|required');
        $this->form_validation->set_rules('third_image_title', 'Third image title', 'trim|required');
        $this->form_validation->set_rules('third_image_description', 'Third image description', 'trim|required');

        $this->form_validation->set_rules('b_image', 'file1', 'trim|callback_valid_file');
        $this->form_validation->set_rules('fir_name', 'file1', 'trim|callback_valid_file');
        $this->form_validation->set_rules('sec_image', 'file1', 'trim|callback_valid_file');
        $this->form_validation->set_rules('thir_image', 'file1', 'trim|callback_valid_file');


        $this->form_validation->set_message('required', 'This field is required.');
        if ($this->form_validation->run() == False) {
            $data_['page_data'] = $this->common_model->commonselect('tbl_career_page', 'carrier_page_id', $page_id)->row();
            $this->load->view('admin/career/edit_career_page', $data_);
        } else {
            $career = $this->career_model->edit_carrier_page($page_id);
            if ($career == TRUE) {
                $this->session->set_flashdata('msg', 'Career page Updated Successfully.');
                redirect(base_url() . "admin/career");
            }
        }
    }

    public function valid_file($name)
    {
        if ($name != "" && !empty($name) && $name != "Upload File") {
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png") {
                return true;
            } else {
                $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');
                return false;
            }
        } else {
            $this->form_validation->set_message('valid_file', 'This field is required.');
            return false;
        }
    }

    public function delete_page_img() {
        if (!$this->input->is_ajax_request()) {
            
        } else {
            $id = $this->input->post('nId');
            $data = array($this->input->post('fieldname') => "");
            $this->db->where('carrier_page_id', $id);
            $this->db->update('tbl_career_page', $data);
        }
    }

}

?>