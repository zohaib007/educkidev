<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blogs extends CI_Controller {

    public function __construct() {

        parent::__construct();


        $this->load->model('categories/categories_model');
        $this->load->model('blogs_model');

        $this->load->library('upload');

        auth();
        set_time_limit(0);
    }

    public function index() {
        $post_order_by = $this->input->post('order');
        $post_field_name = $this->input->post('field_name');
        $data['sort_order'] = 'DESC';
        $data['field_data'] = $post_field_name;

        if ($post_field_name != "") {
            if ($post_order_by == 'DESC') {
                $data['sort_order'] = 'ASC';
            } else {
                $data['sort_order'] = 'DESC';
            }
        } else {
            $data['field_data'] = "blog_created_date";
        }

        $data['arrResponce'] = @$arrResponce['Response']['Success']['Media'];
        $data['resultsChannel'] = $this->blogs_model->listing_pages($post_order_by, $post_field_name);
        var_dump($data);die;
        $this->load->view('admin/blogs/list', $data);
    }

    public function des_check($str) {
        $test = strip_tags($str);

        if ($test == '') {
            $this->form_validation->set_message('des_check', 'This field is required.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function add() {
        read_only_user();
        $this->form_validation->set_rules('blog_title', 'Blog title', 'required');
        $this->form_validation->set_rules('blog_description', 'Page description', 'trim|required|callback_des_check');
        $this->form_validation->set_rules('auth_name', 'Author name', 'trim|required');
        $this->form_validation->set_rules('blog_created_date', 'blog_created_date', 'trim|required');
        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/blogs/add_blog');
        } else {
            $date = date('Y-m-d H:i:s');

            $slug = SEF_URL($this->input->post('blog_title'));
            $result = $this->blogs_model->check_url($slug);
            $number = count($result);
            if ($number == 0) {
                $slug = $slug;
            } elseif ($number == 1) {
                $slug = $slug . "-" . $number;
            } else {
                $valu = $number + 1;
                $slug = $slug . "-" . $valu;
            }
            if ($this->input->post('pg_sefurl')) {
                $slug = SEF_URL($this->input->post('pg_sefurl'));
                $data = $this->blogs_model->check_url($slug);
                $number = count($data);
                $number;
                if ($number == 0) {
                    $slug = $slug;
                } elseif ($number == 1) {
                    $slug = $slug . "-" . $number;
                } else {
                    $valu = $number + 1;
                    $slug = $slug . "-" . $valu;
                }
            }
            ////////////////////////////////////////////           upload multiypal image
            $myFiles = $_FILES['file'];
            $fileDP = array();
            $procount = count($_FILES['file']['name']);
            for ($j = 0; $j < $procount; $j++) {
                if ($myFiles['name'][$j] !== "") {
                    $myFiles['name'][$j];
                    $_FILES['file']['name'] = $myFiles['name'][$j];
                    $_FILES['file']['type'] = $myFiles['type'][$j];
                    $_FILES['file']['tmp_name'] = $myFiles['tmp_name'][$j];
                    $_FILES['file']['error'] = $myFiles['error'][$j];
                    $_FILES['file']['size'] = $myFiles['size'][$j];
                    $config = array(
                        'allowed_types' => 'jpg|jpeg|gif|png',
                        'upload_path' => './images/blog_image/',
                        'file_name' => time(),
                    );
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    //$this->upload->do_upload('file');
                    if (!$this->upload->do_upload('file')) { //Print message if file is not uploaded
                        $this->session->set_flashdata('msg', $this->upload->display_errors());
                        header("Location: " . base_url() . "admin/blogs/add/" . $id);
                        exit();
                    } else {

                        // //////////////////////////////////////////////               uplaod thumbnail image
                        $dataDP = $this->upload->data();
                        $fileDP = $dataDP['file_name'];
                        $this->load->library('image_lib');
                        $configt['image_library'] = 'gd2';
                        $configt['source_image'] = $dataDP['full_path'];
                        $configt['new_image'] = "./images/blog_image/thumb/";
                        $configt['maintain_ratio'] = FALSE;
                        $configt['width'] = 80;
                        $configt['height'] = 80;
                        $this->image_lib->initialize($configt);
                        $this->image_lib->resize();
                    }
                } else {
                    $fileDP = "";
                }
            }
            ///////////////////////////////////////image upload code end///////////////////////////////////////////////

            $set_time = $this->input->post('blog_created_date');
            $array = array(
                'blog_title' => $this->input->post('blog_title'),
                'blog_des' => $this->input->post('blog_description'),
                'auth_name' => $this->input->post('auth_name'),
                'pg_tags' => $this->input->post('pg_tags'),
                'pg_keyword' => $this->input->post('pg_keyword'),
                'pg_meta_des' => $this->input->post('pg_meta_des'),
                'is_active' => $this->input->post('is_active'),
                'pg_sefurl' => $slug,
                'blog_created_date' => $set_time,
                'blog_image' => $fileDP
            );

            $this->blogs_model->submit_blog_data($array);

            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/blogs");
            exit;
        }
    }

    public function edit($id) {
        read_only_user();
        $this->form_validation->set_rules('blog_title', 'Blog title', 'required');
        $this->form_validation->set_rules('auth_name', 'Author name', 'trim|required');
        $this->form_validation->set_rules('blog_created_date', 'blog_created_date', 'trim|required');
        $this->form_validation->set_message('required', 'This field is required.');

        $data['prod_edit_data'] = $this->blogs_model->edit_page($id);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/blogs/edit_blog', $data);
        } else {
            $pg_id = $this->input->post('blog_id');

            $date = date('Y-m-d H:i:s');

            $slug = SEF_URL($this->input->post('blog_title'));
            if ($this->input->post('blog_title') != $data['prod_edit_data'][0]['blog_title']) {
                $result = $this->blogs_model->check_url($slug);
            } else {
                $slug = $data['prod_edit_data'][0]['pg_sefurl'];
            }

            $number = count($result);
            $number;
            if ($number == 0) {
                $slug = $slug;
            } else {
                $valu = $number + 1;
                $slug = $slug . "-" . $valu;
            }

            ////////////////////////////////////////////           upload multiypal image
            $myFiles = $_FILES['file'];
            $fileDP = array();
            $procount = count($_FILES['file']['name']);
            for ($j = 0; $j < $procount; $j++) {
                if ($myFiles['name'][$j] !== "") {
                    $myFiles['name'][$j];
                    $_FILES['file']['name'] = $myFiles['name'][$j];
                    $_FILES['file']['type'] = $myFiles['type'][$j];
                    $_FILES['file']['tmp_name'] = $myFiles['tmp_name'][$j];
                    $_FILES['file']['error'] = $myFiles['error'][$j];
                    $_FILES['file']['size'] = $myFiles['size'][$j];
                    $config = array(
                        'allowed_types' => 'jpg|jpeg|gif|png',
                        'upload_path' => './images/blog_image/',
                        'file_name' => time(),
                    );
                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('file')) { //Print message if file is not uploaded
                        $this->session->set_flashdata('msg', $this->upload->display_errors());
                        header("Location: " . base_url() . "admin/blogs/edit/" . $id);
                        exit();
                    } else {
                        ///////////////////////////////////////////////               uplaod thumbnail image
                        $dataDP = $this->upload->data();
                        $fileDP = $dataDP['file_name'];
                        $this->load->library('image_lib');
                        $configt['image_library'] = 'gd2';
                        $configt['source_image'] = $dataDP['full_path'];
                        $configt['new_image'] = "./images/blog_image/thumb/";
                        $configt['maintain_ratio'] = FALSE;
                        $configt['width'] = 80;
                        $configt['height'] = 80;
                        $this->image_lib->initialize($configt);
                        $this->image_lib->resize();
                    }
                } else {
                    $fileDP = $this->input->post('file');
                }
            }
            ///////////////////////////////////////image upload code end///////////////////////////////////////////////


            $array = array(
                'blog_title' => $this->input->post('blog_title'),
                'auth_name' => $this->input->post('auth_name'),
                'blog_des' => $this->input->post('blog_description'),
                'pg_tags' => $this->input->post('pg_tags'),
                'pg_keyword' => $this->input->post('pg_keyword'),
                'blog_created_date' => $this->input->post('blog_created_date'),
                'pg_meta_des' => $this->input->post('pg_meta_des'),
                'is_active' => $this->input->post('is_active'),
                'pg_sefurl' => $slug, 'blog_image' => $fileDP
            );

            $this->blogs_model->edit_page_update($id, $array);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/blogs");
            exit;
        }
    }

    public function detail($id) {
        $data['blog_detail'] = $this->blogs_model->edit_page($id);
        $this->load->view('admin/blogs/blog_detail_list', $data);
    }

    public function delete_blog_img() {
        read_only_user();
        $id = $this->input->post('nId');
        $data = $this->blogs_model->image_delete($id);
    }

    public function blog_delete($id) {
        read_only_user();
        $this->blogs_model->commonDelete('tbl_blog', "blog_id", $id);

        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect("admin/blogs/");
        exit;
    }

    public function edit_blog_bg() {
        read_only_user();

        $result['title'] = "Edit Background Image";

        if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
            list($width, $height) = getimagesize($_FILES['file']['tmp_name']);

            $extention = end(explode('.', $_FILES['file']['name']));

            if ($width < 1400 || $height < 675) {
                $this->form_validation->set_rules('file', 'image', 'required');
                $this->form_validation->set_message('required', 'File image should be 1400 x 675.');
            } else {
                $this->form_validation->set_rules('file', 'image', 'xss_clean');
                $this->form_validation->set_message('required', 'This field is required.');
            }

            if (!in_array($extention, array('jpg', 'JPG'))) {
                $this->form_validation->set_rules('file', 'image', 'required');
                $this->form_validation->set_message('required', 'File image should be in .jpg format.');
            }
        } else {
            $this->form_validation->set_rules('file', 'image', 'required');
            $this->form_validation->set_message('required', 'This field is required.');
        }

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('admin/blogs/edit_blog_bg', $result);
        } else {
            /* echo '<pre>';
              print_r($_FILES);
              exit; */

            $new_file_name2 = "blog-bg." . "jpg";
            $path = "./images/" . $new_file_name2;

            $copied = move_uploaded_file($_FILES['file']['tmp_name'], $path);

            /*
              //////////////////////////////////////////// upload single image
              $myFiles = $_FILES['file'];
              $fileDP = array();

              if($myFiles['name'] !== "")
              {

              $config = array(
              'allowed_types' => 'jpg|jpeg|gif|png',
              'upload_path' => './resources/blog_image/',
              'file_name' => time(),

              );
              $this->load->library('upload');
              $this->upload->initialize($config);
              //$this->upload->do_upload('file');
              if ( ! $this->upload->do_upload('file'))
              { //Print message if file is not uploaded
              $this->session->set_flashdata('msg', $this->upload->display_errors());
              header("Location: ".base_url()."admin/blog/edit_blog_bg/".$id);
              exit();
              }
              else
              {

              // //////////////////////////////////////////////uplaod thumbnail image
              $dataDP = $this->upload->data();
              $fileDP = $dataDP['file_name'];
              $this->load->library('image_lib');
              $configt['image_library'] = 'gd2';
              $configt['source_image'] = $dataDP['full_path'];
              $configt['new_image'] = "./resources/blog_image/thumb/";
              $configt['maintain_ratio'] = FALSE;
              $configt['width'] = 80;
              $configt['height'] = 80;
              $this->image_lib->initialize($configt);
              $this->image_lib->resize();
              }
              }
              else
              {
              $fileDP = "";
              }

              ///////////////////////////////////////image upload code end///////////////////////////////////////////////
              $array = array('home_title' => $this->input->post('title'),'home_status' => $this->input->post('home_status'),'home_date' => date('Y-m-d'), 'home_image' => $fileDP,'home_type' => $this->input->post('feature_type'));

              $this->common_model->commonSave("tbl_homepage",$array);
             */

            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/blogs");
            exit;
        }
    }

}
