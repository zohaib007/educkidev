<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Admin_accounts extends CI_Controller {

 	public function __construct()
 	{
		parent::__construct();
        
		$this->load->model('admin/admin_model');		
 		auth();
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	public function check_edit_email($str, $nId)
	{
		$result = $this->db->query("
									SELECT *
									FROM tbl_admin
									WHERE
									admin_email = '".$str."'
									AND
									admin_id != ".$nId."
									");
		
		if($result->num_rows() > 0)
		{
			$this->form_validation->set_message('check_edit_email','This email address already exists.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function check_edit_username($str, $nId)
	{
		$result = $this->db->query("
									SELECT *
									FROM tbl_admin
									WHERE
									admin_username = '".$str."'
									AND
									admin_id != ".$nId."
									");
		
		if($result->num_rows() > 0)
		{
			$this->form_validation->set_message('check_edit_username','This username already exists.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	

	public function index()
 	{
		$data['admin_list'] = $this->admin_model->get_adminlist();
		$this->load->view('admin/admin_accounts/account_listing' ,$data);	
	}


	public function add()
	{
		######## Set Validation ########
		$this->form_validation->set_rules('fName', 'first name', 'trim|required');
		$this->form_validation->set_rules('lName', 'Last name', 'trim|required');
		$this->form_validation->set_rules('username', 'username', 'trim|required|is_unique[tbl_admin.admin_username]|alpha_numeric');
		$this->form_validation->set_rules('email', 'email address', 'trim|required|valid_email|is_unique[tbl_admin.admin_email]');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_rules('admin_status', 'Admin status', 'trim|required');
		######## Set Error Messages ########
		
		$this->form_validation->set_message('is_unique', 'This %s already exists.');
		$this->form_validation->set_message('required', 'This field is required.');
		$this->form_validation->set_message('valid_email', 'Please enter a valid email address.');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/admin_accounts/account_add');
		}
		else
		{
			$data_save['admin_fname'] = $this->input->post('fName');
			$data_save['admin_lname'] = $this->input->post('lName');
			$data_save['admin_username'] = $this->input->post('username');
			$data_save['admin_email'] = $this->input->post('email');
			$data_save['admin_password_md5'] = md5($this->input->post('password'));
			$data_save['admin_password'] = $this->input->post('password');
			$data_save['admin_status'] = $this->input->post('admin_status');
			$data_save['admin_date'] = date('Y-m-d h:i:s');
			
			$user_id = $this->admin_model->save_admin('tbl_admin',$data_save);
			
			$this->session->set_flashdata('msg', 'Item added successfully.');
			redirect(base_url()."admin/admin_accounts");
     		exit;
		}
	}

	public function edit($id)
	{
		######## Set Validation ########
		if($id != 1)
		{
			$this->form_validation->set_rules('admin_status', 'Admin status', 'trim|required');
		}
		
		$data['result'] = $this->admin_model->get_admin($id);
		$this->form_validation->set_rules('fName', 'name', 'trim|required');
		$this->form_validation->set_rules('lName', 'name', 'trim|required');
		$this->form_validation->set_rules('username', 'username', 'trim|required|alpha_numeric|callback_check_edit_username['.$id.']');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|callback_check_edit_email['.$id.']');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		
		######## Set Error Messages ########
		$this->form_validation->set_message('required', 'This field is required.');
		$this->form_validation->set_message('valid_email', 'Please enter a valid email address.');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/admin_accounts/editAccount' , $data);	
		}
		else
		{
			
			$data_save['admin_fname'] = $this->input->post('fName');
			$data_save['admin_lname'] = $this->input->post('lName');
			$data_save['admin_username'] = $this->input->post('username');
			$data_save['admin_email'] = $this->input->post('email');
			$data_save['admin_password'] = $this->input->post('password');
			$data_save['admin_password_md5'] = md5($this->input->post('password'));
			if($id != 1)
			{
				$data_save['admin_status'] = $this->input->post('admin_status');
			}
		
			$user_id = $this->admin_model->save_admin_edit('tbl_admin',$data_save ,"admin_id", $id);
			
			$this->session->set_flashdata('msg', 'Item updated successfully.');
			redirect(base_url()."admin/admin_accounts");
			exit;
		}
	}

	public function delete($id = 0)
	{
		if($id != 0){
			if($id == 1) 
			{
				$this->session->set_flashdata('msg', 'Unable to delete super admin.');
			} else {
				$user_id = $this->admin_model->del_admin('tbl_admin',"admin_id", $id);
				$this->session->set_flashdata('msg', 'Item deleted successfully.');
			}
		}
		redirect(base_url()."admin/admin_accounts");
		exit;
	}
}
?>
