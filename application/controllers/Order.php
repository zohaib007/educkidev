<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Order extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/order/order_model');
        $this->load->library('pagination'); // load Pagination library
        $this->load->helper('url'); // load URL helper
        authFront();
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $this->output->set_header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        date_default_timezone_set('America/New_York');
    }

    public function index() {
        $data['title'] = "Pending Shipping";
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $step = 1;
        $order_status = array('Awaiting Tracking', 'Awaiting Pickup');
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/order-pending";
        $limit_per_page = 10;
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $data['total_records'] = count($this->order_model->get_user_orders($store_id, $step, $order_status));
        $config['base_url'] = base_url() . "manage-order/";
        $config['total_rows'] = $data['total_records'];
        $config["uri_segment"] = 2;
        $config['per_page'] = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open'] = '&nbsp;<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        if($page > 0){
            $data['to'] = $page * $limit_per_page;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }else{
            $data['to'] = $page * $limit_per_page;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }

        $start = 1;
        $end = 10;
        $data['to'] = $start;
        $data['from'] = $end;
        if($page > 0){
            for($i=1; $i<=$page; $i++){
                $data['to'] += $limit_per_page;
                $data['from'] += $limit_per_page;
            }
        }
        $data['results'] = $this->order_model->get_user_orders($store_id, $step, $order_status, $limit_per_page, $page * $limit_per_page);
        $this->load->view("front/custom_template", $data);
    }

    public function order_waiting() {
        $data['title'] = "Awaiting Delivery";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/order-waiting";
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $step = 2;
        $order_status = array('In Process', 'Awaiting Pickup');
        $limit_per_page = 10;
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $data['total_records'] = count($this->order_model->get_user_orders($store_id, $step, $order_status));
        $config['base_url'] = base_url() . "order-waiting/";
        $config['total_rows'] = $data['total_records'];
        $config["uri_segment"] = 2;
        $config['per_page'] = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open'] = '&nbsp;<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        if($page > 0){
            $data['to'] = $page+$limit_per_page - 1;
            $data['from'] = $page +$limit_per_page+ $config['per_page'];
        }else{
            $data['to'] = $page+1;
            $data['from'] = $page + $config['per_page'];
        }
        $data['results'] = $this->order_model->get_user_orders($store_id, $step, $order_status, $limit_per_page, $page * $limit_per_page);
        $this->load->view("front/custom_template", $data);
    }

    public function order_pickup() {

        $data['title'] = "Awaiting Pickup";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/order-pickup";
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $step = 3;
        $order_status = 'Awaiting Pickup';
        $limit_per_page = 10;
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $data['total_records'] = count($this->order_model->get_user_orders($store_id, $step, $order_status));
        $config['base_url'] = base_url() . "order-pickup/";
        $config['total_rows'] = $data['total_records'];
        $config["uri_segment"] = 2;
        $config['per_page'] = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open'] = '&nbsp;<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        if($page > 0){
            $data['to'] = $page * $limit_per_page+1;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }else{
            $data['to'] = $page * $limit_per_page+1;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }
        $data['results'] = $this->order_model->get_user_orders($store_id, $step, $order_status, $limit_per_page, $page * $limit_per_page);
        $this->load->view("front/custom_template", $data);
    }

    public function order_history() {
        $data = array();
        $data['title'] = "Order History";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/order-history";
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $step = 4;
        $order_status = array('completed', 'cancel');
        $limit_per_page = 10;
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $data['total_records'] = count($this->order_model->get_user_orders($store_id, $step, $order_status));
        $config['base_url'] = base_url() . "order-history/";
        $config['total_rows'] = $data['total_records'];
        $config["uri_segment"] = 2;
        $config['per_page'] = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open'] = '&nbsp;<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        if($page > 0){
            $data['to'] = $page * $limit_per_page+1;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }else{
            $data['to'] = $page * $limit_per_page+1;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }
        // echo '<pre>';
        // print_r($data);
        // exit;
        $data['results'] = $this->order_model->get_user_orders($store_id, $step, $order_status, $limit_per_page, $page * $limit_per_page);
        $this->load->view("front/custom_template", $data);
    }

    public function order_details($OrderId) {
        $data['order_id'] = $OrderId;
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $data['order_detail_results'] = $this->order_model->view_order_details($OrderId, $store_id);
        $data['results'] = $this->order_model->view_order($OrderId, $store_id);
        $data['order_pickup'] = 0;
        $data['order_history'] = 0;
        $data['default_shipping_address'] = json_decode($data['results']->order_shipping_address);
        $data['default_billing_address'] = json_decode($data['results']->order_billing_address);

        $date1 = date_create(date("Y-m-d", strtotime($data['results']->order_date)));
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date1,$date2);
        $data['dayz'] =  7 - $diff->format("%a");

        $data['title'] = "Order Detail";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/order-details";
        $this->load->view("front/custom_template", $data);
    }

    public function search_order($search = '', $val ='', $steps='') {

        $data = array();

        $data['search'] = $this->input->post('search_value');
        $data['val'] = $this->input->post('select_search');
        $data['step'] = $step = $this->input->post('step');
        

        if($search != ''){
            $data['search'] = $search;
            $data['val'] = $val;
            $data['step'] = $step = $steps;
        }

        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $data['title'] = "Manage Orders";
        if ($step == 1) {
            $order_status = 'Awaiting Tracking';
            $data['content_view'] = "order/order-pending";
            $config['base_url'] = base_url() . "search_order/".$data['search'].'/'.$data['val'].'/'.$data['step'];
        } else if ($step == 2) {
            $order_status = array('In Process', 'Awaiting Pickup');
            $data['content_view'] = "order/order-waiting";
            $config['base_url'] = base_url() . "search_order/".$data['search'].'/'.$data['val'].'/'.$data['step'];
        } else if ($step == 3) {
            $order_status = 'Awaiting Tracking';
            $data['content_view'] = "order/order-pickup";
            $config['base_url'] = base_url() . "search_order/".$data['search'].'/'.$data['val'].'/'.$data['step'];
        } else {
            $order_status = array('completed', 'cancel');
            $data['content_view'] = "order/order-history";
            $config['base_url'] = base_url() . "search_order/".$data['search'].'/'.$data['val'].'/'.$data['step'];
        }
        $limit_per_page = 10;

        $page = ($this->uri->segment(5)) ? $this->uri->segment(5)-1 : 0;

        $from = $page * $limit_per_page;

        $data['total_records'] = count($this->order_model->search_order($store_id, $to = 0, $from = '', $is_limit = 0, $step, $order_status, $search, $val));

        $config['total_rows'] = $data['total_records'];
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open'] = '&nbsp;<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $data['to'] = $page;
        $data['from'] = $page + $config['per_page'];
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();


        if($page == 0){
            $data['from'] = 10;
            $data['to'] = 1;
        }else{
            $data['from'] = (($page+1) * $config['per_page']);
            $data['to'] = ((($page+1) * $config['per_page'])-$config['per_page'])+1;
        }

        $data['results'] = $this->order_model->search_order($store_id, $limit_per_page, $page * $limit_per_page, 1, $step, $order_status, $search, $val);

        $this->load->view("front/custom_template", $data);
    }

    public function user_orders() {

        $data['title'] = "My Orders";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/my-orders";

        $data['buyer_id'] = $this->session->userdata('userid');

        $limit_per_page = 10;
        $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $data['total_records'] = count($this->order_model->view_my_order($data['buyer_id']));
        $config['base_url'] = base_url() . "my-orders/";
        $config['total_rows'] = $data['total_records'];
        $config["uri_segment"] = 2;
        $config['per_page'] = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open'] = '&nbsp;<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        if($page > 0){
            $data['to'] = $page * $limit_per_page;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }else{
            $data['to'] = $page * $limit_per_page;
            $data['from'] = $page * $limit_per_page+$limit_per_page;
        }

        
        $data['results'] = $this->order_model->get_my_orders($data['buyer_id'], $limit_per_page, $page * $limit_per_page);
        
        $this->load->view("front/custom_template", $data);
    }

    public function user_order_detail($order_id) {
        $data['buyer_id'] = $this->session->userdata('userid');
        $data['cancel_flag'] = 1;
        $data['order_details'] = $this->db->query("
                            SELECT * FROM tbl_orders o
                            WHERE o.`order_id` = " . $order_id . " AND o.buyer_id = " . $data['buyer_id'] . "")->row();
        $data['order_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . "")->result();
        $data['total_prods'] = count($data['order_products']);
        if($data['order_details']->order_status!='completed'){
            foreach ($data['order_products'] as $i => $prod) {
                if(!empty($prod->order_prod_tracking_number) && $prod->order_prod_shipping!='Local Pick Up'){
                    $data['cancel_flag'] = 0;
                    break;
                }else{
                    $data['cancel_flag'] = 1;
                }
            }
        }else{
            $data['cancel_flag'] = 0;
        }

        $date1 = date_create(date("Y-m-d", strtotime($data['order_details']->order_date)));
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date1,$date2);
        $data['dayz'] =  7 - $diff->format("%a");        
        $data['order_id'] = $order_id;
        $data['title'] = "My Order Detail";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/my-order-details";

        $this->load->view("front/custom_template", $data);
    }

    public function update_tracking() {
        $order_id = $this->input->post('order_id');
        $response = $this->order_model->update_tracking();
        if ($response == 1) {
            echo json_encode(array('status' => 'error', 'msg' => 'Network Error'));
            die;
        } else {
            $dbdata['order_status'] = 'In Process';
            $this->common_model->commonUpdate('tbl_orders', $dbdata, "order_id", $order_id);
            /*$order_id = $this->input->post('order_id');
            $data['order_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . "")->result();
            $total_prods = count($data['order_products']);
            $completed = 0;
            foreach ($data['order_products'] as $i => $prod) {
                if($prod->order_prod_status=='completed'){
                    $completed++;
                }
            }
            if($total_products==$completed){
                $dbdata['order_status'] = 'completed';
                $this->common_model->commonUpdate('tbl_orders', $dbdata, "order_id", $order_id);
            }*/
            echo json_encode(array('status' => 'Available', 'msg' => 'Order has been updated successfully.'));
            die;
        }
    }

    public function search_my_order($search = '', $val ='') {

        $data = array();

        $data['search'] = $this->input->post('search_value');
        $data['val'] = $this->input->post('select_search');
        if($search != ''){
            $data['search'] = $search;
            $data['val'] = $val;
        }


        $data['buyer_id'] = $this->session->userdata('userid');
        $data['title'] = "My Orders";
        $data['content_view'] = "order/my-orders";
        $config['base_url'] = base_url() . "my-orders/";
        $limit_per_page = 10;

        $page = ($this->uri->segment(4)) ? ($this->uri->segment(4)-1) : 0;

        $data['total_records'] = count($this->order_model->search_my_order($data['buyer_id'], $to = 0, $from = '', $is_limit = 0, $search, $val));

        $config['base_url'] = base_url() . "search_my_order/".$data['search'].'/'.$data['val'];
        $config['total_rows'] = $data['total_records'];
        $config["uri_segment"] = 4;
        $config['per_page'] = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open'] = '&nbsp;<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        if($page == 0){
            $data['from'] = 10;
            $data['to'] = 0;
        }else{
            $data['from'] = (($page+1) * $config['per_page']);
            $data['to'] = ((($page+1) * $config['per_page'])-$config['per_page']);
        }

        $data['results'] = $this->order_model->search_my_order($data['buyer_id'], $limit_per_page, $page * $limit_per_page, 1, $search, $val);

        $this->load->view("front/custom_template", $data);
    }

    public function confirm_order() {
        $order_id = $this->input->post('order_id');
        $response = $this->order_model->confirm_order();
        if ($response == 1) {
            echo json_encode(array('status' => 'error', 'msg' => 'Network Error'));
            die;
        } else {
            $this->disburse_funds( $order_id );
            $data['order_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . "")->result();
            $data['completed_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . " AND order_prod_status = 'completed'")->result();
            if(count($data['order_products'])==count($data['completed_products'])){
                $dbdata['order_status'] = 'completed';
                $dbdata['order_completion_date'] = date('Y-m-d H:i:s');
                $this->common_model->commonUpdate('tbl_orders', $dbdata, "order_id", $order_id);
            }
            echo json_encode(array('status' => 'Available', 'msg' => 'Order has been updated successfully.'));
            die;
        }
    }

    private function disburse_funds($order_id){
        $data['completed'] = $this->db->query("
                                    SELECT * 
                                    FROM tbl_order_products o 
                                    WHERE o.`order_id` = " . $order_id . " AND 
                                    order_prod_status = 'completed' AND
                                    seller_id = ".$this->session->userdata('userid')."
                                    ")->result();
        $data['all'] = $this->db->query("
                                    SELECT * 
                                    FROM tbl_order_products o 
                                    WHERE o.`order_id` = " . $order_id . " AND 
                                    seller_id = ".$this->session->userdata('userid')."
                                    ")->result();
        if(count($data['completed']) == count($data['all'])) {
            $store_id = $this->db->query("select store_id  from tbl_stores where user_id = ".$this->session->userdata('userid')."")->row();
            $capture_id = $this->db->query("
                                    SELECT paypal_capture_id 
                                    FROM tbl_order_seller_info s 
                                    WHERE s.`order_id` = " . $order_id . " AND 
                                    s. store_id = $store_id->store_id
                                    ")->row();
            if($capture_id->paypal_capture_id != ''){
                $var = $this->paypal->GetToken();
                $var = json_decode($var);
                $disburse_api['paypalCaptureDetail'] = $this->paypal->DisburseFunds($var->token_type, $var->access_token, $capture_id->paypal_capture_id);
                $this->common_model->commonUpdate2('tbl_order_seller_info', $disburse_api, "order_id", $order_id, 'store_id',$store_id->store_id );
                return 'Updated';
            }else{
                return NULL;
            }
            
        }else {
            return NULL;
        }
    }

    public function print_order() {
        /*****************************************************************************************************************************************/
        $data['title'] = "Manage Order";
        $html=$this->load->view('pdf/manage-order', $data, true); 
        //this the the PDF filename that user will get to download
        //$pdfFilePath = base_url()."PDF/Transaction ID_".$data['bill_info']['order_id']."_".date('m-d-Y').".pdf";
        
        $pdfFilePath = $_SERVER['DOCUMENT_ROOT']."/educki/PDF/order ID_2".date('m-d-Y').".pdf";
        //load mPDF library
        $this->load->library('pdf');
        //actually, you can pass mPDF parameter on this load() function
        $pdf = $this->pdf->load();
        //generate the PDF!
        $pdf->WriteHTML($html);
        //offer it to user via browser download! (The PDF won't be saved on your server HDD) , D, F, I
        $pdf->Output($pdfFilePath,"D");
        
    }

    public function my_print_order() {
        /*****************************************************************************************************************************************/
        $data['title'] = "My Order";
        $html=$this->load->view('pdf/my-order', $data, true); 
        //this the the PDF filename that user will get to download
        //$pdfFilePath = base_url()."PDF/Transaction ID_".$data['bill_info']['order_id']."_".date('m-d-Y').".pdf";
        
        $pdfFilePath = $_SERVER['DOCUMENT_ROOT']."/educki/PDF/order ID_2".date('m-d-Y').".pdf";
        //load mPDF library
        $this->load->library('pdf');
        //actually, you can pass mPDF parameter on this load() function
        $pdf = $this->pdf->load();
        //generate the PDF!
        $pdf->WriteHTML($html);
        //offer it to user via browser download! (The PDF won't be saved on your server HDD) , D, F, I
        $pdf->Output($pdfFilePath,"D");
        
    }

    

    public function save_shipping() {

        $return['status'] = 'error';
        $return['message'] = 'You are not authorized to access this page';

        if (!$this->input->post()) {
            return $return;
        }

        $post = $this->input->post();

        $data['order_prod_shipping'] = $post['shipping_methods'];
        if($data['order_prod_shipping']!='Local Pick Up'){
                $data['order_prod_status'] = 'Awaiting Tracking';
        }else{
            $data['order_prod_status'] = 'Awaiting Pickup';
        }

        $data['order_prod_shipping_methods'] = $post['shipping'];

        /*echo "<pre>";
        print_r($data);
        print_r($post);
        exit;*/

        $updated = $this->common_model->commonUpdate('tbl_order_products', $data, "order_products_id", $post['order_id']);

        $is_all_local = 1;

        $order_id = $this->common_model->commonselect('tbl_order_products', 'order_products_id', $post['order_id'])->row()->order_id;

        $data['orders'] = $this->common_model->commonselect('tbl_order_products', 'order_id', $order_id)->result();

        foreach ($data['orders'] as $i => $prod) {
            if($prod->order_prod_shipping!='Local Pick Up'){
                $is_all_local = 0;
                break;
            }
        }

        if($is_all_local == 0){
            $dbdata['order_status'] = 'Awaiting Tracking';
        }else{
            $dbdata['order_status'] = 'Awaiting Pickup';
        }

        $updated = $this->common_model->commonUpdate('tbl_orders', $dbdata, "order_id", $order_id);

        if ($updated) {
            $return['status'] = "success";
            $return['message'] = 'Shipping Method Updated.';
        }

        echo json_encode($return);
    }

    public function order_pickup_details($OrderId) {
//        echo "<pre>";
        $data['order_id'] = $OrderId;
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $data['order_detail_results'] = $this->order_model->view_order_pickup_details($OrderId, $store_id);
        // echo '<pre>';
        // print_r($data['order_detail_results']);
        // echo '</pre>';
        $data['results'] = $this->order_model->view_order($OrderId, $store_id);
        $data['order_pickup'] = 1;
        $data['order_history'] = 0;
//        echo "<pre>"; print_r($data['results']);die;
        $data['default_shipping_address'] = json_decode($data['results']->order_shipping_address);
        $data['default_billing_address'] = json_decode($data['results']->order_billing_address);
        $data['title'] = "Pickup Order Detail";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/order-details";
        $date1 = date_create(date("Y-m-d", strtotime($data['results']->order_date)));
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date1,$date2);
        $data['dayz'] =  7 - $diff->format("%a");
//        echo "<pre>"; print_r($data);die;
        $this->load->view("front/custom_template", $data);
    }

    public function order_history_details($OrderId) {
//        echo "<pre>";
        $data['order_id'] = $OrderId;
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $data['order_detail_results'] = $this->order_model->view_order_history_details($OrderId, $store_id);
        $data['results'] = $this->order_model->view_order($OrderId, $store_id);
        $data['order_pickup'] = 0;
        $data['order_history'] = 1;
//        echo "<pre>"; print_r($data['results']);die;
        $data['default_shipping_address'] = json_decode($data['results']->order_shipping_address);
        $data['default_billing_address'] = json_decode($data['results']->order_billing_address);
        $data['title'] = "Order History Detail";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "order/order-details";
        //echo "<pre>"; print_r($data);die;
        $this->load->view("front/custom_template", $data);
    }

    public function cancelorder() {
        $order_id = $this->input->post('order_id');
        if ($order_id) {
            $response = $this->order_model->cancel_order($order_id);
            if ($response == 0) {
                echo json_encode(array('status' => 'Available'));
                die;
            }
        }
    }

    public function reorder() {
        $order_id = $this->input->post('order_id');
        if ($order_id) {
            $is_cart = $this->session->userdata('isCart');
            $cartId = $this->session->userdata('cartId');
            $c_id = $this->session->userdata('userid');
            if (!$is_cart) {
                $c_id = $this->session->userdata('userid');
                if ($c_id > 0) {
                    $cartId = $this->cart_model->getCartId($c_id);
                    $this->setCartSession($cartId);
                } else {
                    $this->load->helper('cookie');
                    $cartCookie = get_cookie('educki_isCart');
                    if ($cartCookie) {
                        $cartId = get_cookie('educki_cartId');
                        $this->setCartSession($cartId);
                    } else {
                        $this->load->helper('string');
                        $user_id = random_string('alnum', 16);
                        $cartId = $this->cart_model->addToCart($user_id);
                        $this->setCartSession($cartId, true, $user_id);
                    }
                }
            } else {
                $cartId = $this->session->userdata('cartId');
            }
            $order_id = $this->input->post('order_id');
        }
    }

}
