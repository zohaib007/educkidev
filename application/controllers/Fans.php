<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Fans extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/wishlist/wishlist_model');
        authFront();
    }

    public function index($page = 0)
    {
        $data['title']            = "My Fans";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword']     = "meta_keywords";
        $data['content_view']     = "fans/fans";

        $data['userid']   = $this->session->userdata('userid');
        $data['store_id'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row();
        $data['store_id'] = $data['store_id']->store_id;

        /*-----------Previous Week ----------------*/

        $previous_week = strtotime("-1 week +1 day");
        $start_week    = strtotime("last sunday midnight", $previous_week);
        $end_week      = strtotime("next saturday", $start_week);
        $start_week    = date("Y-m-d", $start_week);
        $end_week      = date("Y-m-d", $end_week);

        $data['last_weeks'] = $this->db->query("
                                        SELECT COUNT(*) as lastweek
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND 
                                        fan_date <= '" . $end_week . "' AND fan_date >= '" .
            $start_week . "' AND store_id = " . $data['store_id'] . "
                                    ")->row();        
        $data['one_month'] = $this->db->query("
                                        SELECT COUNT(*) as month
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND 
                                        fan_date <= '" . date("Y-n-j", strtotime("last day of previous month")) . "' AND fan_date >= '" .
            date("Y-n-j", strtotime("first day of previous month")) . "' AND store_id = " . $data['store_id'] . "
                                    ")->row();        

        /*-----------------6 Months-------------------*/

        $until    = new DateTime(date('m-01-Y'));
        $interval = new DateInterval('P6M'); //6 months
        $from     = $until->sub($interval); //$until->format('Y-m-d')

        $data['six_month'] = $this->db->query("
                                        SELECT COUNT(*) as sixmonths
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND 
                                        fan_date <= '" . date("Y-n-j", strtotime("last day of previous month")) . "' AND fan_date > '" .
            $from->format('Y-m-d') . "' AND store_id = " . $data['store_id'] . "
                                        ")->row();

        $data['year'] = $this->db->query("
                                        SELECT COUNT(*) as year
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0 AND user.user_status = 1 AND
                                        fan_date <= '" . date("Y-n-j", strtotime("last year December 31st")) . "' AND fan_date >= '" .
            date("Y-n-j", strtotime("last year January 1st")) . "' AND store_id = " . $data['store_id'] . "
                                        ")->row();        

        $data['total'] = $this->db->query("
                                        SELECT COUNT(*) as total
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND fan.store_id = " . $data['store_id'] . "
                                        ")->row();

        $data['fans'] = $this->db->query("
                                        SELECT user.user_city, user.user_state,user.user_profile_picture,
                                        user.user_fname, user.user_lname,user.user_register_date,
                                        store.store_name, store.store_logo_image, store.store_city,
                                        store.store_state, store.store_is_hide, store.store_url
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                                        LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                                        WHERE user.user_status = 1 AND user.user_is_delete = 0 AND user.user_flagged_status = 0 AND fan.store_id = ".$data['store_id']." ORDER BY fan.fan_id DESC")->result();

        $data['total'] = count($data['fans']);

        $mypaing['total_rows']  = count($data['fans']);
        $mypaing['base_url']    = base_url() . "seller-fans/";
        $mypaing['per_page']    = 20;
        $mypaing['uri_segment'] = 2;

        $mypaing['full_tag_open']  = '<div class="paginationInner2">';
        $mypaing['full_tag_close'] = '</div>';
        $mypaing['cur_tag_open']   = '<a class="active">';
        $mypaing['cur_tag_close']  = '</a>';
        $mypaing['next_tag_open']  = '<div class="page-nextBtn">';
        $mypaing['next_tag_close'] = '</div>';
        $mypaing['next_link']      = '';
        $mypaing['prev_tag_open']  = '<div class="page-preBtn">';
        $mypaing['prev_tag_close'] = '</div>';
        $mypaing['prev_link']      = '';
        $mypaing['last_link']      = '';
        $mypaing['first_link']     = '';

        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();
        $data['page']       = ($this->uri->segment(2) != '') ? $this->uri->segment(2) : 0;
        //$data['page']       = $page;

        $name = $this->input->get('utxtbox');
        if(empty($name)){
            $data['Allfans'] = $this->db->query("
                                        SELECT user.user_city, user.user_state,user.user_profile_picture,
                                        user.user_fname, user.user_lname,user.user_register_date,
                                        store.store_name, store.store_logo_image, store.store_city,
                                        store.store_state, store.store_is_hide, store.store_url
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                                        LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                                        WHERE user.user_status = 1 AND user.user_is_delete = 0 AND user.user_flagged_status = 0 AND fan.store_id = " . $data['store_id'] . "  ORDER BY fan.fan_id DESC LIMIT " . $data['page'] . "," . $mypaing['per_page'] . "")->result();
        }
        else{
            $data['Allfans'] = $this->db->query("
                                        SELECT user.user_city, user.user_state,user.user_profile_picture,
                                        user.user_fname, user.user_lname,user.user_register_date,
                                        store.store_name, store.store_logo_image, store.store_city,
                                        store.store_state, store.store_is_hide, store.store_url
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                                        LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                                        WHERE (store.store_name LIKE '%".$name."%' OR user.user_fname LIKE '%".$name."%' OR user.user_lname LIKE '%".$name."%') AND user.user_status = 1 AND user.user_is_delete = 0 AND user.user_flagged_status = 0 AND fan.store_id = " . $data['store_id'] . "  ORDER BY fan.fan_id DESC LIMIT " . $data['page'] . "," . $mypaing['per_page'] . "")->result();
        }
        /*echo "<pre>";
        print_r($data);
        exit;*/
        $this->load->view("front/custom_template", $data);
    }

}
