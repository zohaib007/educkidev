<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Store_settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/setting/setting_model');
        authFront();
    }

    public function index() {
        $data['title'] = "Store Setting";
        $data['meta_title']       = "meta_title";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['userid'] = $this->session->userdata('userid');
        $data['page_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row();
        
        $this->form_validation->set_rules('perDiscount', '', 'trim|required|is_numeric|greater_than[0]|less_than[100]');
        $this->form_validation->set_rules('numberItems', '', 'trim|required|is_numeric');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('greater_than', 'This field must contain a number greater than 0.');
        $this->form_validation->set_message('less_than', 'This field must contain a number less than 100.');
        $this->form_validation->set_message('is_numeric', 'This field must contain only numeric characters.');        

        if ($this->form_validation->run() == false) {
            $data['content_view'] = "store_settings/store_settings";
            $this->load->view("front/custom_template", $data);
        } else {
            $dataS['store_discount'] = $this->input->post('perDiscount');
            $dataS['store_discount_item'] = $this->input->post('numberItems');
            $this->common_model->commonUpdate('tbl_stores', $dataS, 'store_id', $data['page_data']->store_id);
            $this->session->set_flashdata('storeSetting', 'Item updated successfully.');
            redirect(base_url('seller-store-settings'));
            exit;
        }
    }
    
    public function hide_store(){
        $response = $this->setting_model->hide_store();
        if ($response == 1) {
            echo json_encode(array('status' => 'error'));
            die;
        } else {
            echo json_encode(array('status' => 'Available'));
            die;
        }
    }

}
