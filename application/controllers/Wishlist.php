<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Wishlist extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/wishlist/wishlist_model');
        authFront();
        date_default_timezone_set('America/New_York');
    }

    public function index($page = 0)
    {
        $data['title'] = "My Wishlist";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";

        $user_id = $this->session->userdata('userid');
        if($page == 0) { $page = 0;}
        $data['active'] = "wish_list";
        $sort = 'DESC';
        $data['per_page'] = 10;
        
        if( @$this->input->get('per_page') != '' ) {
            $data['per_page'] = @$this->input->get('per_page');
        }
        
        $data['total_rows'] = count($this->wishlist_model->get_wish_list($user_id, $sort, $to=0, $from = '', $is_limit = 0)->result_array());
        
        $mypaing['per_page'] = $data['per_page'];
        $mypaing['total_rows'] = $data['total_rows'];
        $mypaing['base_url'] = base_url()."my-wishlist/";
        $mypaing['uri_segment'] = 2;
        $mypaing['next_link'] = '&nbsp;';
        $mypaing['prev_link'] = '&nbsp;';
        $mypaing['prev_tag_open'] = '<li class="page-left" >';
        $mypaing['prev_tag_close'] = '</li>';
        $mypaing['next_tag_open'] = '<li class="page-right">';
        $mypaing['next_tag_close'] = '</li>';
        $mypaing['num_tag_open'] = '<li>';
        $mypaing['num_tag_close'] = '</li>';
        $mypaing['cur_tag_open'] = '<li><a class = "active">';
        $mypaing['cur_tag_close'] = '</a></li>';
        $mypaing['num_links'] = 5;
        /*$mypaing['first_link'] = "FIRST" ;
        $mypaing['last_link'] = "LAST";*/
        $mypaing['first_tag_open'] = '<div style="display:none;">';
        $mypaing['first_tag_close'] = '</div>';
        $mypaing['last_tag_open'] = '<div style="display:none;">';
        $mypaing['last_tag_close'] = '</div>';
        
        $data['wish_list'] = $this->wishlist_model->get_wish_list($user_id, $sort, $page, $mypaing['per_page'], 1)->result_array();

        //$data['sort'] = $sort;
        $data['to'] =  $page;
        $data['from'] = $page + $mypaing['per_page'];
            
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();


        $data['content_view'] = "wishlist/wishlist";
        $this->load->view("front/custom_template", $data);
    }

    public function add_to_wish_list()
    {
        $data_save = array();
        if ($this->session->userdata('userid') != '') {
            $result = $this->common_model->commonselect2('tbl_wish_list', 'product_id', $this->input->post('prod_id'), 'user_id', $this->session->userdata('userid'));
            if ($result->num_rows() > 0) {
                $this->session->set_flashdata('msg', 'Item already exists in your wishlist.');
                echo "reload";
            } else {
                $data_save['user_id']       = $this->session->userdata("userid");
                $data_save['product_id']    = $this->input->post('prod_id');
                $data_save['wish_datetime'] = date("Y-m-d H:i:s");
                $res                        = $this->common_model->commonSave('tbl_wish_list', $data_save);
                if ($res) {
                    $this->session->set_flashdata('msg', 'Item added to your wishlist.');
                    echo "done";
                } else {
                    $this->session->set_flashdata('msg', 'Error occur.');
                    echo "error";
                }
            }
        } else {
            $this->session->set_flashdata('msg', 'Please Login.');
            echo "error";
        }
    }

    public function remove_wish_item($wish_id = '')
    {
        if ($wish_id == '' || !is_numeric($wish_id)) {
            redirect(base_url('wish-list'));
            exit;
        }
        $query = $this->db->query("SELECT * FROM tbl_wish_list WHERE id = " . $wish_id . " AND user_id = " . $this->session->userdata('userid'))->row_array();

        if (empty($query)) {
            redirect(base_url('wish-list'));
            exit;
        }

        $this->common_model->commonDelete('tbl_wish_list', 'id', $wish_id);

        $this->session->set_flashdata('msg', 'Your item has been deleted successfully.');
        redirect(base_url('my-wishlist'));
        exit;
    }

    public function add_to_wishlist_shop()
    {
        $data_save = array();
        if ($this->session->userdata('userid') != '') {
            $result = $this->common_model->commonselect2('tbl_wish_list', 'product_id', $this->input->post('prod_id'), 'user_id', $this->session->userdata('userid'));
            if ($result->num_rows() > 0) {
                echo 2;
            } else {
                $data_save['user_id']       = $this->session->userdata("userid");
                $data_save['product_id ']    = $this->input->post('prod_id');
                $data_save['wish_datetime'] = date("Y-m-d H:i:s");
                $res                        = $this->common_model->commonSave('tbl_wish_list', $data_save);
                if ($res) {
                    echo 1;
                } else {
                    $this->session->set_flashdata('msg', 'Error occur.');
                    echo 0;
                }
            }
        } else {
            echo 0;
        }
    }
}
