<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Homepage extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['title']            = "eDucki";
        $data['meta_title']       = "eDucki";
        $data['meta_description'] = "eDucki";
        $data['meta_keyword']     = "eDucki";
        $data['home_data']        = $this->common_model->commonselect('tbl_homepage', 'home_id', 1)->row();
        $data['home_banner']      = $this->common_model->getCombox('tbl_homepage_banner', 'banner_id', 'ASC')->result();
        $data['home_member']      = $this->common_model->getCombox('tbl_homepage_member', 'mem_id', 'ASC')->result();
        $data['home_category']    = $this->common_model->commonselect('tbl_categories', 'cat_level', 1, 'cat_id', 'ASC')->result();

        $data['user']         = $this->db->query("SELECT * FROM tbl_user WHERE user_id = " . $this->session->userdata('userid') . " AND user_status = 1")->row();
        $data['content_view'] = "homepage";
        $this->load->view("front/custom_template", $data);
    }

}
