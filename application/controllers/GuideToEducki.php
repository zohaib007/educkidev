<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class GuideToEducki extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/guidetoeducki/Guidetoeducki_model');
    }

    public function index()
    {

        $data                     = array();
        $data['pagedata']         = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 10)->row();
        $data['pageurl']          = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 5)->row();
        
        $data['title']            = $data['pagedata']->sub_pg_title;
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "about/guide-to-educki";
        $data['list']             = $this->Guidetoeducki_model->view_list(10);
        $this->load->view("front/custom_template", $data);
    }

    public function listing_details($url)
    {
        $data['detail'] = $this->common_model->commonSelect('tbl_pages_listing', 'listing_url', $url, 'listing_id')->row();
        if (count($data['detail']) == 0) {
            $path = get_page_url(10, 'tbl_pages_sub', 'sub_id')->sub_pg_url;
            redirect(base_url($path));
        }
        $data                     = array();
        $data['detail']           = $this->common_model->commonSelect('tbl_pages_listing', 'listing_url', $url, 'listing_id')->row();
        $data['pagedata']         = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 10)->row();
        $data['list']             = $this->Guidetoeducki_model->listing_pages($url);
        $data['title']            = $data['detail']->listing_title;
        $data['meta_title']       = $data['detail']->meta_title;
        $data['meta_description'] = $data['detail']->meta_description;
        $data['meta_keyword']     = $data['detail']->meta_keywords;
        $data['content_view']     = "about/listing-page";
        $this->load->view("front/custom_template", $data);
    }

}
