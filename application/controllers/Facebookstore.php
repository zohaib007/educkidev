<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Facebookstore extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/fb_store/facebookstore_model');
        authFront();
    }

    public function index() {
        $data['title'] = "Facebook Store";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "fb_store/facebook_store";
        $this->load->view("front/custom_template", $data);
    }

}
