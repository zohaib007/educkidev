<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Store_details extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/users/users_model');
        $this->load->model('front/seller/seller_model');
        $this->load->model('front/product/product_model');
        date_default_timezone_set('America/New_York');
    }

    public function index($store_slug = '', $page = 0)
    {
        if ($store_slug == '') {
            redirect(base_url());
            exit;
        }

        $data['title']        = "Store Details";
        $data['no_map']        = 0;
        $data['content_view'] = "seller/store_details";
        $data['userid']       = $this->session->userdata('userid');
        /*$where                = "store_is_hide = '0' AND store_url ='" . $store_slug . "'";
        $data['pagedata'] = $this->common_model->commonselect_array('tbl_stores', $where, 'store_id')->row();*/
        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores store
                                              INNER JOIN tbl_user user  ON store.user_id = user.user_id
                                              WHERE store.store_is_hide = 0 AND store.store_url = '".$store_slug."' AND user.user_is_delete = 0 AND user.user_status = 1")->row();
        /*echo "<pre>";
        print_r($data['pagedata']);
        exit;*/

        if (empty($data['pagedata'])) {
            redirect(base_url());
            exit;
        }

        $data['meta_title']       = $data['pagedata']->store_name;
        $data['meta_description'] = $data['pagedata']->store_description;
        $data['meta_keyword']     = $data['pagedata']->store_name;

        $data['share_title'] = $data['pagedata']->store_name;
        $data['share_image'] = base_url() . "resources/seller_account_image/banner/" . $data['pagedata']->store_banner_image;

        $data['seller_data'] = $this->common_model->commonselect('tbl_user', 'user_id', $data['pagedata']->user_id)->row();
        $data['userdata']    = $this->common_model->commonselect('tbl_user', 'user_id', $data['userid'])->row();

        $data['fans'] = count($this->db->query("
                            SELECT user.user_city, user.user_state,user.user_profile_picture,
                            user.user_fname, user.user_lname,user.user_register_date,
                            store.store_name, store.store_logo_image, store.store_city,
                            store.store_state, store.store_is_hide, store.store_url
                            FROM tbl_fans fan
                            INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                            LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                            WHERE store.store_is_hide = 0 AND user.user_status = 1 
                            AND user.user_is_delete = 0 AND user.user_flagged_status = 0 
                            AND fan.store_id = " . $data['pagedata']->store_id. "
                        ")->result());

        $data['prod_price'] = $this->db->query("
                                        SELECT MAX(CASE WHEN prod.`prod_onsale` = 1 THEN prod.`sale_price` ELSE prod.`prod_price` END) as MAX,
                                        MIN(CASE WHEN prod.`prod_onsale` = 1 THEN prod.`sale_price` ELSE prod.`prod_price` END) as MIN
                                        FROM tbl_products prod
                                        INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                        INNER JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
                                        INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                                        INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                                        LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                                        LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id

                                        WHERE 
                                        prod_is_delete = 0
                                        AND
                                        prod_status = 1
                                        AND
                                        prod.prod_store_id = " .$data['pagedata']->store_id . " 
                                        ")->row();

        
        $where2 = "fan_id ='" . $data['userid'] . "' AND store_id ='" . $data['pagedata']->store_id . "'";

        $data['is_fan'] = count($this->common_model->commonselect_array('tbl_fans', $where2, 'fan_id')->row());
        // echo '<pre>';
        // print_r($this->db->last_query());
        // echo '</pre>';
        // exit;

        $data['per_page'] = 10;
        if ($page == 0) {$page = 0;}
        $data['sort_by']  = "new";
        $sort             = 'prod.prod_id DESC';
        if (@$this->input->get('per_page') != '') {
            $data['per_page'] = @$this->input->get('per_page');
        }

        if (@$this->input->get('sort_by') != '') {
            $data['sort_by'] = @$this->input->get('sort_by');
            if ($data['sort_by'] == "new") {
                $sort = 'prod.prod_id DESC';
            } else if ($data['sort_by'] == "featured") {
                $sort = 'prod.prod_feature DESC';
            } else if ($data['sort_by'] == "price_lh") {
                $sort = 'prod.prod_price ASC';
            } else if ($data['sort_by'] == "pricehl") {
                $sort = 'prod.prod_price DESC';
            }
        }

        $data['tabNo'] = 1;
        if (@$this->input->get('tabNo') != '') {
            $data['tabNo'] = @$this->input->get('tabNo');
        }

        $price['price_range_one'] = 0;
        $price['price_range_two'] = 0;

        if (@$this->input->get('rangeOne') != '' && @$this->input->get('rangeTwo') != '') {
            $price['price_range_one'] = @$this->input->get('rangeOne');
            $price['price_range_two'] = @$this->input->get('rangeTwo');
        }else{
            $price = 0;
        }

        $data['total_rows'] = count($this->users_model->get_prod_list($data['pagedata']->store_id, $sort, $to = 0, $from = '', $is_limit = 0,$price)->result_array());

        $get = http_build_query($_GET, '', "&");
        if($get!=''){
            $get = "?".$get;
        }
        
        $mypaing['per_page']       = $data['per_page'];
        $mypaing['total_rows']     = $data['total_rows'];
        $mypaing['base_url']       = base_url() . "store/".$store_slug.'/';
        if($get!=''){
        $mypaing['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $mypaing['uri_segment']    = 3;
        $mypaing['next_link']      = '&nbsp;';
        $mypaing['prev_link']      = '&nbsp;';
        $mypaing['prev_tag_open']  = '<li class="page-left" >';
        $mypaing['prev_tag_close'] = '</li>';
        $mypaing['next_tag_open']  = '<li class="page-right">';
        $mypaing['next_tag_close'] = '</li>';
        $mypaing['num_tag_open']   = '<li>';
        $mypaing['num_tag_close']  = '</li>';
        $mypaing['cur_tag_open']   = '<li><a class = "active">';
        $mypaing['cur_tag_close']  = '</a></li>';
        $mypaing['num_links']      = 5;
        // $mypaing['reuse_query_string'] = true;
        // $mypaing['first_link'] = "FIRST" ;
        // $mypaing['last_link'] = "LAST";
        $mypaing['first_tag_open']  = '<div style="display:none;">';
        $mypaing['first_tag_close'] = '</div>';
        $mypaing['last_tag_open']   = '<div style="display:none;">';
        $mypaing['last_tag_close']  = '</div>';

        $data['wish_list'] = $this->users_model->get_prod_list($data['pagedata']->store_id, $sort, $page, $mypaing['per_page'], 1,$price)->result_array();

        $data['to']   = $page;
        $data['from'] = $page + $mypaing['per_page'];

        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();

        // foreach

        /*echo "<pre>";
        echo $this->db->last_query();
        print_r($data);
        exit;*/

        $this->load->view("front/custom_template", $data);
    }

    public function add_fan()
    {
        $where = "fan_id ='" . $this->input->post('fan_id') . "' AND store_id ='" . $this->input->post('store_id') . "'";

        $data['is_fan'] = count($this->common_model->commonselect_array('tbl_fans', $where, 'fan_id')->row());

        if ($data['is_fan'] == 0) {
            $array['store_id'] = $this->input->post('store_id');
            $array['fan_id']   = $this->input->post('fan_id');
            $array['fan_date'] = date('Y-m-d H:i:s');
            $user_id           = $this->common_model->commonSave('tbl_fans', $array);
            echo 1;
        } else {

            $user_id = $this->session->userdata('userid');
            $data['stores_id'] = $this->input->post('store_id');
            $this->common_model->commonDelete2('tbl_fans', 'fan_id', $user_id, 'store_id', $data['stores_id']);
            echo 2;
        }
    }

    public function my_story($store_slug)
    {
        $data['store_url']        = $store_slug;
        $data['title']            = "My Story";
        $data['meta_title']       = "meta_title";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword']     = "meta_keywords";
        $data['content_view']     = "seller/store_story";
        $store_user_id            = get_store_user_id($this->uri->segment(2))->user_id;
        $data['userid']           = $this->session->userdata('userid');

        /*$where                    = "store_is_hide = 0 AND store_url ='" . $store_slug . "'";
        $data['pagedata']         = $this->common_model->commonselect_array('tbl_stores', $where, 'user_id')->row();*/
        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores store
                                              INNER JOIN tbl_user user  ON store.user_id = user.user_id
                                              WHERE store.store_is_hide = 0 AND store.store_url = '".$store_slug."' AND user.user_is_delete = 0 AND user.user_status = 1")->row();
        if(empty($data['pagedata'])){
            redirect(base_url());
            exit;
        }

        $data['seller_data']      = $this->common_model->commonselect('tbl_user', 'user_id', $data['pagedata']->user_id)->row();
        $data['userdata']         = $this->common_model->commonselect('tbl_user', 'user_id', $data['userid'])->row();
        
        // $data['fans']             = count($this->db->query("SELECT *
        //                                               FROM tbl_fans fan
        //                                               INNER JOIN tbl_stores store  ON store.store_id = fan.store_id
        //                                               INNER JOIN tbl_user user  ON store.user_id = user.user_id
        //                                               WHERE store.store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1 AND store.store_url = '$store_slug'")->result());

        $data['fans'] = count($this->db->query("
                                        SELECT user.user_city, user.user_state,user.user_profile_picture,
                                        user.user_fname, user.user_lname,user.user_register_date,
                                        store.store_name, store.store_logo_image, store.store_city,
                                        store.store_state, store.store_is_hide, store.store_url
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                                        LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                                        WHERE store.store_is_hide = 0 AND user.user_status = 1 AND user.user_is_delete = 0 AND user.user_flagged_status = 0 AND fan.store_id = " . $data['pagedata']->store_id. "")->result());

        

        //$data['is_fan'] = count($this->common_model->commonselect('tbl_fans', 'fan_id', $data['userid'])->row());
        
        $where2 = "fan_id ='" . $data['userid'] . "' AND store_id ='" . $data['pagedata']->store_id . "'";
        $data['is_fan'] = count($this->common_model->commonselect_array('tbl_fans', $where2, 'fan_id')->row());


        $data['stores'] = $this->seller_model->get_favourite_shops($store_user_id, 'DESC', 0, 3, 1)->result_array();

        $sort              = 'prod.prod_id DESC';
        $data['wish_list'] = $this->users_model->get_prod_list($data['pagedata']->store_id, $sort, 0, 3, 1)->result_array();

        $this->load->view("front/custom_template", $data);
    }

    public function user_report()
    {
        $userId = $this->input->post('userId');
        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores
                                              WHERE store_id = '".$userId."'")->row();        
        $report_count = $data['pagedata']->store_reported + 1;
        $update_data = array('store_reported' => $report_count);
        $this->common_model->commonUpdate('tbl_stores', $update_data, 'store_id', $userId);
        $history['flag_user_id'] = $data['pagedata']->user_id;
        $history['flag_date'] = date('Y-m-d H:i:s');
        $this->common_model->commonSave('tbl_user_flag_history', $history);
        $user['user_flagged_status'] = 1;
        $this->common_model->commonUpdate('tbl_user', $user,'user_id',$data['pagedata']->user_id);
        echo true;
    }

    public function product_report() {
        $response = $this->product_model->product_report();
        if ($response == 1) {
            echo json_encode(array('msg' => 'Selected Products reported successfully.'));
            die;
        }
    }

}
