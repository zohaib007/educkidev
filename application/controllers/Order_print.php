<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Order_print extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/order/order_model');
        $this->load->library('pagination'); // load Pagination library
        $this->load->helper('url'); // load URL helper
        authFront();
    }

    public function print_order($OrderId) {
        $data['order_id'] = $OrderId;
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $data['order_detail_results'] = $this->order_model->view_order_details_print($OrderId, $store_id);
        $data['results'] = $this->order_model->view_order($OrderId, $store_id);        
        $data['order_pickup'] = 0;
        $data['order_history'] = 0;

        $date1 = date_create(date("Y-m-d", strtotime($data['results']->order_date)));
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date1,$date2);
        $data['dayz'] =  7 - $diff->format("%a");
        $data['default_shipping_address'] = json_decode($data['results']->order_shipping_address);
        $data['default_billing_address'] = json_decode($data['results']->order_billing_address);
        $data['title'] = "Manage Orders";
        
        /*echo '<pre>';
        print_r($data);
        exit;*/
        /*****************************************************************************************************************************************/
        $html=$this->load->view('pdf/manage-order', $data, true); 
        //this the the PDF filename that user will get to download
        //$pdfFilePath = base_url()."PDF/Transaction ID_".$data['bill_info']['order_id']."_".date('m-d-Y').".pdf";
        
        $pdfFilePath = "OrderID_".$OrderId.date('m-d-Y').".pdf";
        //load mPDF library
        $this->load->library('pdf');
        //actually, you can pass mPDF parameter on this load() function
        $pdf = $this->pdf->load();
        //generate the PDF!
        $pdf->WriteHTML($html);
        //offer it to user via browser download! (The PDF won't be saved on your server HDD) , D, F, I
        $pdf->Output($pdfFilePath,"D");
    }

    public function print_my_order($order_id){
        $data['buyer_id'] = $this->session->userdata('userid');
        $data['order_details'] = $this->db->query("
                            SELECT * FROM tbl_orders o
                            WHERE o.`order_id` = " . $order_id . " AND o.buyer_id = " . $data['buyer_id'] . "")->row();
        $data['order_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . "")->result();
        $data['total_prods'] = count($data['order_products']);
        $date1 = date_create(date("Y-m-d", strtotime($data['order_details']->order_date)));
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date1,$date2);
        $data['dayz'] =  7 - $diff->format("%a");
        /*echo "<pre>";
        print_r($data);
        exit;*/
        //$data['order_id'] = $order_id;


        /*****************************************************************************************************************************************/
        $html=$this->load->view('pdf/manage-my-order', $data, true); 
        //this the the PDF filename that user will get to download
        //$pdfFilePath = base_url()."PDF/Transaction ID_".$data['bill_info']['order_id']."_".date('m-d-Y').".pdf";
        
        $pdfFilePath = "OrderID_".$OrderId.date('m-d-Y').".pdf";
        //load mPDF library
        $this->load->library('pdf');
        //actually, you can pass mPDF parameter on this load() function
        $pdf = $this->pdf->load();
        //generate the PDF!
        $pdf->WriteHTML($html);
        //offer it to user via browser download! (The PDF won't be saved on your server HDD) , D, F, I
        $pdf->Output($pdfFilePath,"D");
    }

}
