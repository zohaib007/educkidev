<?php

/**
 * - Code by Haseeb
 * - Code for EDucki
 * - Code for Cart System.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['url'] = base_url();
        $this->load->model('front/cart/cart_model');
        $this->load->model('front/address/address_model');
        if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
        if ( !empty($_POST) ) $_POST = array_stripslash($_POST);
        date_default_timezone_set('America/New_York');
        //ini_set('display_errors', 0);

    }

    public function index() {
       
        $cartId = $this->session->userdata('cartId');
        if ($cartId == '') {
            $cartId = get_cookie('educki_cartId');
            if($cartId != ''){
                $isCartExist = $this->db->query('SELECT * FROM tbl_cart WHERE cart_id = '.$cartId)->row();
                
                if(count($isCartExist) > 0){
                    $this->setCartSession($cartId);
                }else{
                    $this->cart_model->removeCart($cartId);
                }
            }
        }
        if ($cartId != '') {
            $data['cart_product_details'] = getProductPrices();
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }else{
                    $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_id=$cartId 
                                                AND cart_prod_id=$product->cart_prod_id")->row();
                    if($product_details->qty < $getCartTotalQty->total){
                        $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                    }
                }
            }
            $data['cart_product_details'] = getProductPrices();
            $data['shipment_sub_total'] = get_shipment_sub_total();
            $data['cart_sub_total'] = getCartSubTotal();
            $data['discount_amount'] = getStoreDiscount($cartId);
            $data['total_shipment_amount'] = $data['shipment_sub_total'] - $data['discount_amount'] + $data['cart_sub_total'];
            $data['main_category_url'] = getFirstMainCategory()->cat_url;
//        echo $this->db->last_query();die;
            $data['title'] = "Shopping Cart";
            $data['content_view'] = "cart/cart";
            /*echo "<pre>";
            print_r($data);
            exit;*/
            $this->load->view("front/custom_template", $data);
        } else {
            redirect(base_url());
        }
    }

    public function cart_shipping() {        
        if ($this->session->userdata('userid') != '') {
            $sessiondata = array(
                'same_as_shipping' => false,
            );
            $this->session->set_userdata($sessiondata);
            $data['title'] = "Shipping Address";
            $this->session->set_userdata('cart_proceed', TRUE);
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('nick_name', 'Nick Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('street', 'street', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('country', 'country', 'required');            
            $this->form_validation->set_rules('zip_code', 'zip code', 'required');
            $this->form_validation->set_message('required', 'This field is required.');
            $data['userid'] = $this->session->userdata('userid');
            $cartId = $this->session->userdata('cartId');
            $update_data = array('cart_user_id' => $data['userid']);
            $this->common_model->commonUpdate('tbl_cart', $update_data, 'cart_id', $cartId);

            $data['current'] = json_decode($this->session->userdata('shipping_address_info'));

            //$data['address_list'] = $this->address_model->getuseraddress($data['userid']);
            $data['address_list'] = $this->address_model->getuseraddress_2($data['userid'],1);
            $data['default_address_list'] = $this->common_model->default_shipping($data['userid']);
            $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
            $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
            $data['cart_product_details'] = getProductPrices();
            $data['shipment_sub_total'] = get_shipment_sub_total();
            $data['cart_sub_total'] = getCartSubTotal();
            $data['discount_amount'] = getStoreDiscount($this->session->userdata('cartId'));
            $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
            $data['main_category_url'] = getFirstMainCategory()->cat_url;

            /*echo "<pre>";
            print_r($data['current']);
            exit;*/
            if ($this->form_validation->run() == false) {
                $data['content_view'] = "cart/cart_shipping_address";
                $this->load->view("front/custom_template", $data);
            } else {
                /*echo "<pre>";
                echo $this->input->post('save_address');
                print_r($_POST);
                exit;*/
                if ($this->input->post('save_address') != '') {
                    $address = $this->address_model->cart_address_json();
                    /*echo "<pre>";print_r($address);die;*/
                    $this->session->set_userdata(($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_address_info', $address);
                    $response = $this->address_model->cart_add_address();
                    if ($response == 1) {
                        redirect(base_url() . "cart-payment");
                    }
                } else {
                    
                    $address = $this->address_model->cart_address_json();
//                    echo "<pre>";print_r($address);die;
                    $this->session->set_userdata(($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_address_info', $address);
                    redirect(base_url() . "cart-payment");
                }
            }
        } else {
            redirect(base_url() . "cart");
        }
    }

    public function cart_billing_payment() {        
        $data['title'] = "Payment & Billing";
        if ($this->session->userdata('shipping_add_user_id') != '') {
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('street', 'street', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('country', 'country', 'required');
            $this->form_validation->set_rules('state', 'state', 'required');
            $this->form_validation->set_rules('zip_code', 'zip code', 'required');
            $this->form_validation->set_message('required', 'This field is required.');
            $data['userid'] = $this->session->userdata('userid');
            $cartId = $this->session->userdata('cartId');
            if ($cartId == '') {
            $cartId = get_cookie('educki_cartId');
        }
            $update_data = array('cart_user_id' => $data['userid']);
            $this->common_model->commonUpdate('tbl_cart', $update_data, 'cart_id', $cartId);

            $data['current'] = json_decode($this->session->userdata('billing_address_info'));
            if($this->session->userdata('use_default')=='1'){
                $data['restore'] = 1;
            }elseif($this->session->userdata('same_as_shipping')=='1'){
                $data['restore'] = 2;
            }else{
                $data['restore'] = 3;
            }

            //$data['address_list'] = $this->address_model->getuseraddress($data['userid']);
            $data['address_list'] = $this->address_model->getuseraddress_2($data['userid'],2);
            $data['default_address_list'] = $this->common_model->default_billing($data['userid']);
            $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
            $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
            $data['cart_product_details'] = getProductPrices();
            $data['shipment_sub_total'] = get_shipment_sub_total();
            $data['cart_sub_total'] = getCartSubTotal();
            $data['discount_amount'] = getStoreDiscount($this->session->userdata('cartId'));
            $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
            $data['main_category_url'] = getFirstMainCategory()->cat_url;
            /*echo "<pre>";
            print_r($data);
            exit;*/
            if ($this->form_validation->run() == false && !isset($_POST['us'])) {
                $data['content_view'] = "cart/cart_billing_payment";
                $this->load->view("front/custom_template", $data);
            } else {
                if ($this->input->post('save_address') != '') {
                    $address = $this->address_model->cart_address_json();
                    $this->session->set_userdata(($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_address_info', $address);
                    $response = $this->address_model->cart_add_address();
                    if ($response == 1) {
                        $sessiondata = array(
                            'same_as_shipping' => false,
                            'use_default' => false,
                        );
                        $this->session->set_userdata($sessiondata);
                        $paymentdata = array(
                            'payment_method' => $this->input->post('payment_method'),
                        );
                        $this->session->set_userdata($paymentdata);
                        redirect(base_url() . "cart-confirm-order");
                    }
                } else {
                    if ($this->input->post('us') == '1') {
                        $address = $this->address_model->cart_address_json();
                        $sessiondata = array(
                            'same_as_shipping' => false,
                            'use_default' => True
                        );
                        $this->session->set_userdata($sessiondata);
                        $this->session->set_userdata("billing_address_info", $address);
                    } else if ($this->input->post('us') == '2') {
                        $sessiondata = array(
                            'same_as_shipping' => True,
                            'billing_add_user_id' => True,
                            'use_default' => false,
                        );
                        $this->session->set_userdata($sessiondata);
                        $address = $this->address_model->cart_address_json();
                        $this->session->set_userdata("billing_address_info", $address);
                    } else {
                        $sessiondata = array(
                            'same_as_shipping' => false,
                            'use_default' => false,
                        );
                        $this->session->set_userdata($sessiondata);
                        $address = $this->address_model->cart_address_json();
                        $this->session->set_userdata(($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_address_info', $address);
                    }
                    $paymentdata = array(
                        'payment_method' => $this->input->post('payment_method'),
                    );
                    $this->session->set_userdata($paymentdata);
                    redirect(base_url() . "cart-confirm-order");
                }
            }
        } else {
            redirect(base_url() . "cart-shipping");
        }
    }

    public function cart_confirm_payment() {
        $data['title'] = "Confirm Order";
        if ($this->session->userdata('userid') != '' && $this->session->userdata('billing_add_user_id') != '') {
            $data['userid'] = $this->session->userdata('userid');
            $cartId = $this->session->userdata('cartId');
            if ($cartId == '') {
            $cartId = get_cookie('educki_cartId');
        }
            $data['cart_product_details'] = getProductPrices();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }
            $data['shipment_sub_total'] = get_shipment_sub_total();
            $data['cart_sub_total'] = getCartSubTotal();
            $data['discount_amount'] = getStoreDiscount($this->session->userdata('cartId'));
            $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
            $data['default_shipping_address'] = json_decode($this->session->userdata('shipping_address_info'));
            $data['default_billing_address'] = json_decode($this->session->userdata('billing_address_info'));
            $data['title'] = "Confirm Order";
            $this->payWithPaypal();
             
            $data['content_view'] = "cart/cart_confirm_order";
            $this->load->view("front/custom_template", $data);
        } else {
            redirect(base_url() . "cart-payment");
        }
    }

    public function add_to_cart_ajax() {
        //session_destroy();
        $product_id = $this->input->post('prod_id');
        $filters = stripslashes($this->input->post('filter_data'));
        /*echo "<pre>";
        print_r($filters);
        exit;*/
        $is_cart = $this->session->userdata('isCart');
        $cartId = $this->session->userdata('cartId');
        if($cartId == ''){
            $cartId = get_cookie('educki_cartId');
        }

        
        if($cartId == ''){
            $expires = time() - 500;
            // setcookie('isCart',"FALSE",$expires,$this->data['url'],'/','educki_');
            // setcookie('cartId',0,$expires,$this->data['url'],'/','educki_');
            // setcookie('educki_isCart', "FALSE", $expires, '/');
            // setcookie('educki_cartId', 0, $expires, '/');

            delete_cookie('isCart');
            delete_cookie('cartId');
            delete_cookie('educki_isCart');
            delete_cookie('educki_cartId');
            delete_cookie('educki_userId');
            $this->session->unset_userdata('cartId');
            $this->session->unset_userdata('isCart');
        }


        
        $is_cart = $this->session->userdata('isCart');
        $cartId = $this->session->userdata('cartId');
        if($cartId == ''){
            $cartId = get_cookie('educki_cartId');
        }
        $userId = $this->session->userdata('userid');
        $resp = array();
        if($cartId <= 0){ // If cart ID Not exist.
            if($userId > 0){ // If user Login.
                $cart_data = $this->db->query("SELECT cart_id FROM tbl_cart WHERE cart_user_id = $userId")->row();
                if(count($cart_data) <= 0){ // If not in cart.
                    $this->db->insert('tbl_cart', array('cart_user_id'=>$userId));
                    $cartId = $this->db->insert_id();
                    $this->session->set_userdata('cartId', $cartId);
                }else{ // if cart exist.
                    $cartId = $cart_data->cart_id;
                    $this->session->set_userdata('cartId', $cartId);
                }
                $this->setCartSession($cartId);
            }else{ // If the user is not login.
                $this->load->helper('cookie');
                $this->load->helper('string');
                $cartCookie = get_cookie('educki_isCart');
                if ($cartCookie) {
                    $cartId = get_cookie('educki_cartId');
                    $isCartExist = $this->db->query('SELECT * FROM tbl_cart WHERE cart_id = '.$cartId)->row();
                    if(count($isCartExist) > 0){
                        $this->setCartSession($cartId);
                    }else{
                        $this->cart_model->removeCart($cartId);

                        $user_id = random_string('alnum', 16); // Make the random string as user ID
                        // Add UserID to DB
                        $this->db->insert('tbl_cart', array('cart_user_id'=>$user_id));
                        $cartId = $this->db->insert_id();
                        $this->session->set_userdata('cartId', $cartId);
                        $this->setCartSession($cartId, true, $user_id);
                    }
                }else{
                    $user_id = random_string('alnum', 16); // Make the random string as user ID
                    // Add UserID to DB
                    $this->db->insert('tbl_cart', array('cart_user_id'=>$user_id));
                    $cartId = $this->db->insert_id();
                    $this->session->set_userdata('cartId', $cartId);
                    $this->setCartSession($cartId, true, $user_id);
                }
            }
        }else {

            $cartId = $this->session->userdata('cartId');
        }

        // Prod Reorder Case
        $order_id = $this->input->post('order_id');
        if ($order_id) { // Reorder product
            $this->load->model('front/order/order_model');
            $this->data['order_info'] = $this->order_model->get_order($order_id);
            
            if (count($this->data['order_info']) > 0) {
                $resp['message'] ='';
                foreach ($this->data['order_info'] as $key => $prds) {
                    $prod_ids = $prds['order_prod_id'];
                    $r_inventory = $this->cart_model->getProduct($prod_ids)->result();
                    $r_inventory = cgetproduct_details_2($prod_ids);
                    
                    

                    if ($r_inventory[0]->qty > 0) {
                        if ($r_inventory[0]->qty >= $prds['order_prod_qty']) {
                            $qty = $prds['order_prod_qty'];
                        } else {
                            $qty = $r_inventory[0]->qty;
                        }
                        $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_id=$cartId 
                                                AND cart_prod_id=$prod_ids")->row();
                        if( ($getCartTotalQty->total+($qty)) > $r_inventory[0]->qty ){
                            //Not Update
                            $isUpdate= 0;
                            //$resp['message'] = "Not added";
                            $resp['message'] .= getProdName($prod_ids)->prod_title . " out of stock.<br/>";
                            $resp['cart_items'] = totalCartItems();
                            $resp['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
                            $resp['status'] = 'limit-ok';

                        }else{
                             //update
                            $isUpdate= 1;
                            $qty = $getCartTotalQty->total+($qty);
                            $stat = $this->cart_model->addreorderProduct($cartId, $prod_ids, $qty, $prds['order_prod_filters']);

                            //var_dump($stat);die;


                            // echo $this->db->last_query();
                           
                            $resp['cart_items'] = totalCartItems();
                            $resp['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
                            if($stat == 1){
                                $resp['status'] = 'ok';
                                $resp['message'] .= getProdName($prod_ids)->prod_title . " is added to cart.<br/>";
                            }else{
                                $resp['message'] .= getProdName($prod_ids)->prod_title . " is updated to cart.<br/>";
                                $resp['status'] = 'ready-ok';
                            }
                        }
                    }else{
                        $resp['message'] .= getProdName($prod_ids)->prod_title . " out of stock.<br/>";
                        $resp['status'] = 'limit-ok';
                        
                    }
                }
            }else {
                $resp['status'] = 'not-ok';
            }

            $data['cart_product_details'] = getProductPrices();
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }

            return print_r(json_encode($resp));
        } else { // add product into the cart
           
            $data['productData'] = $this->common_model->commonselect('tbl_products', 'prod_id', $product_id);
            if ($data['productData']->num_rows() > 0) {
                
                $row = $data['productData']->row();
                if ($row->qty > 0) {
                    
                    $qty = 1;
                    $data['qtyAdded'] = $qty;
                    
                    $returnType = $this->cart_model->addProduct($cartId, $product_id, $qty);
                    // echo $returnType;
                    $resp['cart_items'] = totalCartItems();
                    $resp['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
                    //echo $returnType;
                    if($returnType == 1){
                        $resp['status'] = 'ok';
                    }else if($returnType == 2){
                        
                        $resp['status'] = 'ready-ok';
                    }else{
                        $resp['status'] = 'limit-ok';
                    }
                } else {
                    
                    $resp['status'] = 'not-ok';
                }
            }
            
            $data['cart_product_details'] = getProductPrices();
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }
            print_r(json_encode($resp));
        }
    }

    function setCartSession($cartId, $cookieAlso = false, $userId = false) {
        $this->session->set_userdata('isCart', TRUE);
        $this->session->set_userdata('cartId', $cartId);
        $expires = time() + (5400000);
        if ($cookieAlso) {
            $cookie1 = array(
                'name' => 'isCart',
                'value' => TRUE,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie2 = array(
                'name' => 'cartId',
                'value' => $cartId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie3 = array(
                'name' => 'userId',
                'value' => $userId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            
//            set_cookie($cookie1);
//            set_cookie($cookie2);
//            set_cookie($cookie3);

            // delete_cookie('isCart');
            // delete_cookie('cartId');
            // delete_cookie('userId');
            // delete_cookie('educki_isCart');
            // delete_cookie('educki_cartId');
            // delete_cookie('educki_userId');

            setcookie('isCart',"TRUE",$expires,$this->data['url'],'/','educki_');
            setcookie('cartId',$cartId,$expires,$this->data['url'],'/','educki_');
            setcookie('userId',$userId,$expires,$this->data['url'],'/','educki_');
            setcookie('educki_isCart', "TRUE", $expires, '/');
            setcookie('educki_cartId', $cartId, $expires, '/');
            setcookie('educki_userId', $userId, $expires, '/');
        }
    }

    public function removeCartItem() {
        $cardId = $this->input->post('cartId');
        $qty = $this->input->post('qty');
        $prodId = $this->input->post('prodId');
        $detail_id = $this->input->post('detail_id');
        $response = $this->cart_model->removeCartItem($cardId, $prodId,$detail_id);
        
        if ($response == True) {
            $data['quantity'] = totalCartItems();
            $data['status'] = 'Avaliable';
            $data['msg'] = 'Product has been removed Successfully';
            $data['shipment_sub_total'] = number_format(get_shipment_sub_total(), 2, '.', '');
            $data['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
            $data['discount_amount'] = number_format(getStoreDiscount($cardId), 2, '.', '');
            $total_shipment_amount = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
            $data['total_shipment_amount'] = number_format($total_shipment_amount, 2, '.', '');
            echo json_encode($data);
            die;
        }
    }

    public function changeCartItem() {
        
        $cardId = $this->input->post('cartId');
        $this->setCartSession($cardId);
        $cardDetailId = $this->input->post('cart_detail_id');
        $qty = $this->input->post('qty');
        $oldQty = $this->input->post('oldQty');
        $prodId = $this->input->post('prodId');
        $prod_details = getproduct_details($prodId);
        if ($prod_details == '') {
            $this->cart_model->removeCartItem($cardId, $prodId);
            echo json_encode(array('status' => 'Error', 'msg' => 'Product is Not Avaliable'));
            die;
        } else {
            $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_detail_id=$cardDetailId 
                                                AND cart_prod_id=$prodId")->row();
            $isUpdate= 1;
            if(($getCartTotalQty->total+($qty-$oldQty)) > $prod_details->qty){
                //Not Update
                 $isUpdate= 0;
            }else{
                //update
                 $isUpdate= 1;
            }
            if($isUpdate == 1){
                $prodPrice = 0.0;
                $sDate = date('Y-m-d', strtotime($prod_details->sale_start_date));
                $eDate = date('Y-m-d', strtotime($prod_details->sale_end_date));
                $tDate = date('Y-m-d');
                if ($prod_details->prod_onsale== 1) {
                    if ($prod_details->sale_start_date != '' && $prod_details->sale_end_date != '') {
                        if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                            $prodPrice = number_format($prod_details->sale_price, 2, '.', '');                        
                        } else {
                            $prodPrice = number_format($prod_details->prod_price, 2, '.', '');                        
                        }
                    } else if ($prod_details->sale_start_date != '') {
                        if ($tDate >= $sDate) {                        
                            $prodPrice = number_format($prod_details->sale_price, 2, '.', '');
                        } else {
                            $prodPrice = number_format($prod_details->prod_price, 2, '.', '');
                        }
                    } else if($prod_details->sale_end_date != '') {
                        if($tDate <= $eDate) {
                            $prodPrice = number_format($prod_details->sale_price, 2, '.', '');
                        } else {
                            $prodPrice = number_format($prod_details->prod_price, 2, '.', '');
                        }
                    }else {
                        $prodPrice = number_format($prod_details->sale_price, 2, '.', '');
                    }
                } else {
                    $prodPrice = number_format($prod_details->prod_price, 2, '.', '');
                }

                $total_price = $prodPrice * $qty;
                $response = $this->cart_model->updateCartInfo($cardDetailId, $prodId, $qty);
                $data['status'] = 'Avaliable';
                $data['total_price'] = number_format($total_price, 2, '.', '');
                $subTotal = getCartSubTotal();
                
                $data['cart_sub_total'] = number_format($subTotal,2,'.','');
                $data['shipment_sub_total'] = number_format(get_shipment_sub_total(), 2, '.', '');
                $data['discount_amount'] = number_format(getStoreDiscount($cardId), 2, '.', '');
                $total_shipment_amount = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
                $data['total_shipment_amount'] =  number_format($total_shipment_amount, 2, '.', '');
                echo json_encode($data);
            }else{
                echo json_encode(array('status' => 'Error', 'msg' => 'Product is Not Avaliable'));
            }
            
        }
    }

    public function load_address() {
        $address_id = $this->input->post('addressId');
        $tabel_name = 'tbl_user_addreses address';
        $fields = '*';
        $where = "add_id =" . $address_id;
        $jointable = 'tbl_country country';
        $joincondition = 'country.iso = address.country';
        $jointype = 'inner';
        $data = $this->common_model->get_table_listing($tabel_name, $fields, $where, $jointable, $joincondition, $jointype, '', '', '')->row();
        echo json_encode($data);
        die;
    }

    public function payWithPaypal() {
        $cartId = $this->session->userdata('cartId');
        if ($cartId == '') {
            $cartId = get_cookie('educki_cartId');
        }
        //$commision = getTotalCommision($cartId);
        
        $var = $this->paypal->GetToken();
        $var = json_decode($var);
        
        //if($var->access_token != ''){
            $this->session->set_userdata('access_token',$var->access_token);
            $this->session->set_userdata('token_type',$var->token_type);
            
            $order_api = $this->paypal->CreateOrder($var->token_type, $var->access_token, $cartId,$this->session->userdata('shipping_address_info'));
            $order_api = json_decode($order_api);
            
            /*echo "<pre>";
            print_r($order_api);
            exit;*/
            $this->session->set_userdata('paypal_order_id',$order_api->id);
            $this->session->unset_userdata('payPalURL');
            if(!empty($order_api->links[1]->href)){
                $mySession['payPalURL'] = $order_api->links[1]->href;
            }else{
                $mySession['payPalURL'] = $order_api->message;
            }
            $this->session->set_userdata('payPalURL', $mySession);

        // }else{
        //     $this->session->set_flashdata('paypal_error', $var);
        //     redirect(base_url('cart-payment'));
        //     exit;
        // }

    }

    public function cart_thanks() {

        
        
        #- Code By Haseeb Start
        // $orderId = $this->session->flashdata('order_id');
        //  if($this->session->flashdata('order_id') == ''){
        // }
        $orderId = $this->session->userdata('order_id');
        
        #- Code By Haseeb End
        if($orderId != ''){
            $data['order'] = $orderId;
            $data['title'] = "Thank You";
            $data['meta_description'] = "eDucki";
            $data['meta_keyword'] = "eDucki";
            $data['message'] = "Order Submitted Successfully";
            //$this->session->unset_userdata('order_id');
            // echo '<pre>';
            // print_r('Thank you Controller');
            // print_r($orderId);
            // echo '</pre>';
            $this->load->view("front/cart/thank-you-new", $data);
        }else{
            redirect(base_url());
            // echo 1212;
            exit;
        }
    }

    public function paypaltest() {
        $cartId = $this->session->userdata('cartId');
        echo '<pre>';
        print_r("Cart Id : " . $cartId);

        $seller_total = getSellerTotal($cartId);
        echo '<pre>';
            print_r($seller_total);
        foreach ($seller_total as $seller) {
            //foreach ($seller as $value) {
                echo '<pre>';
                print_r($seller['store_email']);
            //}
        }
        $commision = getTotalCommision($cartId);
        //$commision += $commision; 
        echo '<pre>';
        print_r("Total commision : " . $commision);
        //$g_total = $seller_total + $commision;
        //echo '<pre>';
        //print_r("Grand Total : ".$g_total);

        exit;
    }

    public function payOrderAPI(){
        $pay_api = $this->paypal->PayForOrder($this->session->userdata('token_type'), $this->session->userdata('access_token'), $this->session->userdata('paypal_order_id'));
        $this->load->view('front/cart/wait-for-paypal');
    }

    public function paymentSave() {
        
        sleep(3);
        
        $orderDetail = $this->paypal->GetOrderDetail($this->session->userdata('paypal_order_id'),$this->session->userdata('token_type'), $this->session->userdata('access_token'));
        
        $cartId = $this->session->userdata('cartId');
        if ($cartId == '') {
            $cartId = $_COOKIE['educki_cartId'];
        } 

        $this->session->all_userdata();
        
        $data['orderDetail'] = $orderDetail;
        
        $data['cartid'] = $cartId;
        $data['userid'] = $this->session->userdata('userid');
        $data['shipment_sub_total'] = get_shipment_sub_total();
        $data['cart_sub_total'] = getCartSubTotal();
        $data['discount_amount'] = getStoreDiscount($data['cartid']);
        $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];

        $data['cart_product_details'] = getProductPrices();
        $data['default_shipping_address'] = $this->session->userdata('shipping_address_info');
        $data['default_billing_address'] = $this->session->userdata('billing_address_info');

        $data['order_total_commission'] = getTotalCommision($data['cartid']);

        $seller_total = getSellerTotal($cartId);
        $data['order_seller_info'] = json_encode($seller_total);

        $data['order_paypal_response'] = $orderDetail;

        $order_id = $this->cart_model->confirm_order($data);
       
        
        if ($order_id > 0) {
            

            $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'purchase-product')->row();
            $subject              = $email_data->e_email_subject;
            $message              = $email_data->e_email_text;
            $message              = str_replace("{{username}}", $this->session->userdata('userfname'), $message);
            $message              = str_replace("{{order_id}}", $order_id, $message);
            
            send_email($this->session->userdata('useremail'), "noreply@educki.com", $subject, $message);
			//$this->session->set_flashdata('order_id',$order_id);
            $this->session->set_userdata('order_id',$order_id); // code by Haseeb
            
            redirect(base_url() . "cart-thank-you");
            exit;
        }else{
             redirect(base_url() . "cart-thank-you");
             exit;
        }
    }


    public function whichProdNotAddCart(){
        $order_id = $this->input->post('order_id');
        $getOrderProduct = $this->db->query("
                                SELECT p.prod_id, p.qty, p.prod_title, p.prod_price, p.qty, p.prod_onsale, p.sale_price, prod_status,
                                p.prod_is_delete, p.prod_cat_id, op.order_prod_id, op.order_prod_name, op.order_prod_unit_price, 
                                op.order_prod_sale_price, c.cat_status, c.cat_is_delete
                                FROM tbl_order_products op
                                INNER JOIN tbl_products p ON p.prod_id = op.order_prod_id
                                INNER JOIN tbl_categories c ON p.prod_cat_id = c.cat_id
                                WHERE op.order_id = $order_id
                            ")->result();
        
        
        $priceMsg = $delMsg = $addMsg = '';
        if(count($getOrderProduct) > 0){
            foreach($getOrderProduct as $price){
                if($price->cat_status == 1 && $price->cat_is_delete == 0 && $price->prod_is_delete == 0 && $price->prod_status == 1){
                    if($price->prod_onsale > 0){
                        if($price->sale_price != $price->order_prod_unit_price){
                            $priceMsg .= "<p>$price->order_prod_name price has been changed.</p>";
                        }else{
                            // $addMsg .= "<p>$price->order_prod_name added to cart successfully.</p>";
                        }
                    }else{
                        if($price->prod_price != $price->order_prod_unit_price){
                            $priceMsg .= "<p>$price->order_prod_name price has been changed.</p>";
                        }else{
                            // $addMsg .= "<p>$price->order_prod_name added to cart successfully.</p>";
                        }
                    }
                }else{
                    // $delMsg .= "<p>$price->order_prod_name out of stock.</p>";
                }
            }
            // if($priceMsg != ''){
            //     $priceMsg = rtrim( $priceMsg , ", ");
            //     $priceMsg = "<p>$priceMsg price has been changed.</p>";
            // }

            // if($delMsg != ''){
            //     $delMsg = rtrim( $delMsg, ", ");
            //     $delMsg = "<p>$delMsg item(s) out of stock.</p>";
            // }

            // if($addMsg != ''){
            //     $addMsg = rtrim( $addMsg,", ");
            //     $addMsg = "<p>$addMsg item(s) added to cart successfully.</p>";
            // }
            //echo $priceMsg.$delMsg;
            echo $addMsg.$priceMsg.$delMsg;
        }else{
            //echo "Item(s) out of stock.";
            echo "";
        }
    }

}
