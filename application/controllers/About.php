<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class About extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        //authFront();
    }

    public function index($value = '')
    {
        $page_data['segment'] = $this->uri->segment(2);
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_pg_url', $page_data['segment'])->row();

        if (count($data['pagedata']) > 0) {
            $data['title']        = $data['pagedata']->sub_pg_title;
            $data['pageurl']      = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 5)->row();
            $data['content_view'] = "about-new-page/new-page";
            $this->load->view("front/custom_template", $data);
        }
    }

    public function load_con_filters()
    {
        $prod_id = $this->input->post('prod_id');
        $filter_id = $this->input->post('filter_id');
        $title = $this->input->post('title');
        $id = $this->input->post('id');

        $data['status'] = 201;
        $data['message'] = "There was a problem.";

        $data['html'] = "";

        if($prod_id != '' && $filter_id!=''){
            $values = $this->db->query("SELECT * 
                                                FROM tbl_product_filters_detail f 
                                                INNER JOIN tbl_cat_filter_detail fd ON fd.filter_detail_id = f.filter_detail_id 
                                                WHERE prod_id = ".$prod_id." AND f.filter_detail_id = ".$filter_id."
                                                ")->result_array();
            /*echo "<pre>";
            print_r($data['values'][0]["filter_title"]);
            exit;*/
            $data['html'] .= '<div class="row pf-row" id="'.$id.'-filters"><div class="col-md-3 col-sm-12 col-xs-12"><label>'.$title.':</label></div><div class="col-md-6 col-sm-12 col-xs-12">';
            $data['html'] .= '<select class="required-filters sub-filters" data-name="'.$values[0]["filter_title"].'" name="'.$values[0]['filter_slug'].'" id="'.$values[0]['filter_slug'].'" onchange="filtervalue('.$prod_id.',\''.$values[0]['filter_slug'].'\');">';
            foreach ($values as $i => $op) {
                $data['html'] .= '<option value="'.$op['filter_value'].'" data-prod="'.$prod_id.'" data-id="'.$op['filter_detail_id'].'">'.stripcslashes($op['filter_value']).'</option>';
            }

            $data['html'] .= '</select><input type="hidden" name="'.$prod_id.'_'.$values[0]['filter_slug'].'" data-name="'.$values[0]["filter_title"].'" data-slug="'.$values[0]['filter_slug'].'" class="required_filters" id="'.$prod_id.'_'.$values[0]['filter_slug'].'"></div></div>';

            $data['status'] = 200;
            $data['message'] = "Filters retreived successfully.";
        }

        echo json_encode($data);
    }
}
