<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class My_dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        authFront();
    }

    public function index()
    {
        //echo "<pre>";
        //print_r($this->session->all_userdata());
        $userid = $this->session->userdata('userid');

        $user = $this->db->query("
									        SELECT *
									        FROM tbl_user
									        WHERE
									        user_id = " . $userid . "
									        AND
									        user_status = 1
        ");
        //$data['user'] = $this->common_model->commonselect('tbl_user', 'user_id', $userid);
        if ($user->num_rows() > 0) {
            //echo "<pre>";
            //print_r($this->session->all_userdata());
            //print_r($data['user']->result_array());
            //exit;
            $data['user'] = $user->row();
            //echo "<pre>";
            //print_r($this->session->all_userdata());
            //print_r($data['user']);
            //exit;
            $data['title']        = 'Dashboard';
            $data['content_view'] = "my-dashboard";
            $this->load->view("front/custom_template", $data);
        } else {
            redirect(base_url('logout'));
        }
    }
}
