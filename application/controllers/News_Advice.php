<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class News_Advice extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        //authFront();
    }

    public function index() {
        redirect(base_url('news-advice'));
        exit;
    }
    public function admin() {
//        var_dump(base_url('news-advice/wp-admin'));
        redirect(base_url('news-advice/wp-login.php'));
        exit;
    }

}
