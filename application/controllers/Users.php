<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/users/users_model');
        authFront();
    }

    public function index() {
        
    }

    public function edit_profile() {
        if ($this->input->post('user_id') != '') {
            $this->users_model->edit_profile();
            redirect(base_url('my-dashboard'));
            exit;
        } else {
            $data['title'] = "My Profile";
            $data['meta_title']       = "meta_title";
            $data['meta_description'] = "meta_description";
            $data['meta_keyword'] = "meta_keywords";
            $data['content_view'] = "member/profile";
            $data['userid'] = $this->session->userdata('userid');
            $data['user'] = $this->db->query("SELECT * FROM tbl_user WHERE user_id = " . $data['userid'] . " AND user_status = 1")->row();
            $this->load->view("front/custom_template", $data);
        }
    }

    public function delete_page_img() {
        if (!$this->input->is_ajax_request()) {
            
        } else {
            $id = $this->input->post('nId');
            $data = array($this->input->post('fieldname') => "");
            $this->db->where('user_id', $id);
            $this->db->update('tbl_user', $data);
        }
    }

    public function change_password() {
        $data['title'] = "Change Password";
        $data['meta_title']       = "meta_title";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "change_password";
        $data['userid'] = $this->session->userdata('userid');
        $this->load->view("front/custom_template", $data);
    }

    public function check_password() {
        $response = $this->users_model->change_password();
        if ($response == true) {
            $this->session->set_flashdata('msg', 'Password updated Successfully.');
             redirect(base_url() . "my-dashboard");
        } else {
            
        }
    }
    public function check_current_password() {
        $response = $this->users_model->check_current_password();
        if ($response == 1) {
            echo json_encode(array('status' => 'Ok'));
            die;
        } else {
            echo json_encode(array('status' => 'error'));
            die;
        }
    }

}
