<?php
require 'vendor/autoload.php';

class Aftership {
	
	private $AFTERSHIP_API_KEY = "236b2c1c-c665-44c0-967c-3ce084c1733d";
	
	
	/////////////////////////////////////////////////////////////
	////////////////// [ CREATED TRACKING DATA ] ////////////////
	/////////////////////////////////////////////////////////////
	
	public function created_tracking($traking_no = '', $tracking_info = '')
	{
		$trackings = new AfterShip\Trackings($this->AFTERSHIP_API_KEY);
		$tracking_info = array(
								'slug' => 'dhl',
								'title' => 'My Title',
								'emails' => 'email@yourdomain.com',
            					'order_id' => '1234',
        						'product_name' => 'iPhone Case',
								'product_price' => 'USD19.99'
								);
			
		$response = $trackings->create($traking_no, $tracking_info);
		return $response;
	
	}
	
	////////////////// [ CREATED TRACKING DATA ] ////////////////
	/////////////////////////////////////////////////////////////
	
#####################################################################################################################
	
	/////////////////////////////////////////////////////////////
	/////////////////// [ DELETE TRACKING DATA ] ////////////////
	/////////////////////////////////////////////////////////////
	
	public function delete_tracking_by_slug_and_tracking_no($tracking_no = '', $slug = 'dhl' )
	{
		$trackings = new AfterShip\Trackings($this->AFTERSHIP_API_KEY);
		$response = $trackings->delete($slug, $tracking_no);
		return $response;
		
	}
	
	public function delete_tracking_by_tracking_id($tracking_id = '53df4a90868a6df243b6efd8', $slug = 'dhl' )
	{
		$trackings = new AfterShip\Trackings($this->AFTERSHIP_API_KEY);
		$response = $trackings->delete_by_id($tracking_id);
		return $response;
		
	}
	/////////////////// [ DELETE TRACKING DATA ] ////////////////
	/////////////////////////////////////////////////////////////
	
#####################################################################################################################
	
	/////////////////////////////////////////////////////////////
	/////////////////// [ GET TRACKING DATA ] ///////////////////
	/////////////////////////////////////////////////////////////
	
	public function get_tracking_by_slug_and_tracking_no($tracking_no = '123465798', $slug = 'dhl' )
	{
		$trackings = new AfterShip\Trackings($this->AFTERSHIP_API_KEY);
		$response = $trackings->get($slug, $tracking_no, array('title','order_id'));
		return $response;
		
	}
	
	public function get_tracking_by_tracking_id($tracking_id = '53df4a90868a6df243b6efd8' )
	{
		$trackings = new AfterShip\Trackings($this->AFTERSHIP_API_KEY);
		$response = $trackings->get_by_id($tracking_id, array('title','order_id'));
		return $response;
		
	}
	
	/////////////////// [ GET TRACKING DATA ] ///////////////////
	/////////////////////////////////////////////////////////////
	
#####################################################################################################################

	/////////////////////////////////////////////////////////////
	//////////////// [ UPDATE TRACKING DATA ] ///////////////////
	/////////////////////////////////////////////////////////////
	public function update_tracking_by_slug_and_tracking_no($tracking_no = '123465798', $slug = 'dhl', $update_array)
	{
		$trackings = new AfterShip\Trackings($this->AFTERSHIP_API_KEY);
		$params = array(
							'title' => 'My Title update',
							'emails' => 'email@yourdomain.com',
							'order_id' => '1234',
							'product_name' => 'iPhone Case',
							'product_price' => 'USD19.99'
						);
		$response = $trackings->update($slug, $tracking_no, $params);
		return $response;
		
	}
	
	public function update_tracking_by_tracking_id($tracking_id = '53df4a90868a6df243b6efd8', $update_array )
	{
		$trackings = new AfterShip\Trackings($this->AFTERSHIP_API_KEY);
		$params = array(
							'title' => 'My Title update',
							'emails' => 'email@yourdomain.com',
							'order_id' => '1234',
							'product_name' => 'iPhone Case',
							'product_price' => 'USD19.99'
						);
		$response = $trackings->update_by_id($tracking_id, $params);
		return $response;
		
	}
	
	//////////////// [ UPDATE TRACKING DATA ] ///////////////////
	/////////////////////////////////////////////////////////////
	
#####################################################################################################################

	/////////////////////////////////////////////////////////////
	//////////////// [ DETECT COURIES DATA ] ////////////////////
	/////////////////////////////////////////////////////////////


	public function detect_courier_by_tracking_no($tracking_no = '')
	{
		
		$couriers = new AfterShip\Couriers($this->AFTERSHIP_API_KEY);
		$response = $couriers->detect($tracking_no);
		//$response = $couriers->get();
		return $response;
	}
	
	public function detect_courier_by_tracking_id($tracking_id = '')
	{
		
		$couriers = new AfterShip\Couriers($this->AFTERSHIP_API_KEY);
		$response = $couriers->detect($tracking_id);
		//$response = $couriers->get();
		return $response;
	}
	
	//////////////// [ DETECT COURIES DATA ] ///////////////////
	////////////////////////////////////////////////////////////
	
#####################################################################################################################



	

}