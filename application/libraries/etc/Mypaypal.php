<?php

class Mypaypal {

    private $PP_API_USERNAME = 'Deleon-facilitator_api1.boomslangit.com';
    private $PP_API_PASSWORD = 'UZZW5WCRV7W923VN';
    private $PP_API_SIGNATURE = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AA4LpqRSiqF2R8QgUlIAjYynvOa0';
    private $PP_CURRENCYCODE = 'USD';
    private $PP_MODE = 'sandbox';
    private $PP_RETURN_URL = 'http://educki.dev/seller/ipn';
    private $PP_CANCEL_URL = 'http://educki.dev/seller/cancel';
    private $PPL_LOGO_IMG = 'http://educki.dev/front_resources/images/logo.png';
    private $PPL_LANG = 'EN';

    public function setReturnUrl($url) {
        $this->PP_RETURN_URL = $url;
    }

    public function setCancelUrl($url) {
        $this->PP_CANCEL_URL = $url;
    }

    function GetItemTotalPrice($item) {
//        var_dump($item);die;
        //(Item Price x Quantity = Total) Get total amount of product;
        return $item['ITEMAMT'] * $item['QTY0'];
    }

    function GetGrandTotal($products, $charges) {

        //Grand total including all tax, insurance, shipping cost and discount

        $GrandTotal = $this->GetProductsTotalAmount($products);

        foreach ($charges as $charge) {

            $GrandTotal = $GrandTotal + $charge;
        }

        return $GrandTotal;
    }

    function GetProductsTotalAmount($products) {

        $ProductsTotalAmount = 0;

        foreach ($products as $p => $item) {

            $ProductsTotalAmount = $ProductsTotalAmount + $this->GetItemTotalPrice($item);
        }

        return $ProductsTotalAmount;
    }

    public function SetExpressCheckout($products, $charges = '', $noshipping = '1') {


//        var_dump($products);die;
        //Parameters for SetExpressCheckout, which will be sent to PayPal

        $padata = '';

        $padata = '&METHOD=SetExpressCheckout';
        $padata .= '&RETURNURL=' . urlencode($this->PP_RETURN_URL);
        $padata .= '&CANCELURL=' . urlencode($this->PP_CANCEL_URL);
//        var_dump($padata);die;
        foreach ($products as $p => $item) {

            $padata .= '&PAYMENTREQUEST_' . $p . '_CURRENCYCODE=' . urlencode($this->PP_CURRENCYCODE);

            $padata .= '&PAYMENTREQUEST_' . $p . '_AMT=' . urlencode($item['AMT']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_ITEMAMT=' . urlencode($item['ITEMAMT']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_TAXAMT=' . urlencode($item['TAXAMT']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_PAYMENTACTION=' . urlencode($item['PAYMENTACTION']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_DESC=' . urlencode($item['DESC']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_SELLERPAYPALACCOUNTID=' . urlencode($item['SELLERPAYPALACCOUNTID']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_PAYMENTREQUESTID=' . urlencode($item['PAYMENTREQUESTID']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_NAME0=' . urlencode($item['NAME0']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_NUMBER0=' . urlencode($item['NUMBER0']);

            $padata .= '&PAYMENTREQUEST_' . $p . '_QTY0=' . urlencode($item['QTY0']);
        }



        /*



          //Override the buyer's shipping address stored on PayPal, The buyer cannot edit the overridden address.



          $padata .=	'&ADDROVERRIDE=1';

          $padata .=	'&PAYMENTREQUEST_0_SHIPTONAME=J Smith';

          $padata .=	'&PAYMENTREQUEST_0_SHIPTOSTREET=1 Main St';

          $padata .=	'&PAYMENTREQUEST_0_SHIPTOCITY=San Jose';

          $padata .=	'&PAYMENTREQUEST_0_SHIPTOSTATE=CA';

          $padata .=	'&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=US';

          $padata .=	'&PAYMENTREQUEST_0_SHIPTOZIP=95131';

          $padata .=	'&PAYMENTREQUEST_0_SHIPTOPHONENUM=408-967-4444';



         */



        $padata .= '&NOSHIPPING=' . $noshipping; //set 1 to hide buyer's shipping address, in-case products that does not require shipping
        $padata .= '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($this->GetProductsTotalAmount($products));
        $padata .= '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($charges['TotalTaxAmount']);
        $padata .= '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($charges['ShippinCost']);
        $padata .= '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($charges['HandalingCost']);
        $padata .= '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($charges['ShippinDiscount']);
        $padata .= '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($charges['InsuranceCost']);
//        paypal custom template
        $padata .= '&LOCALECODE=' . $this->PPL_LANG; //PayPal pages to match the language on your website;
        $padata .= '&LOGOIMG=' . $this->PPL_LOGO_IMG; //site logo
        $padata .= '&CARTBORDERCOLOR=FFFFFF'; //border color of cart

        $padata .= '&ALLOWNOTE=1';

        ############# set session variable we need later for "DoExpressCheckoutPayment" #######
        $_SESSION['ppl_products'] = $products;
        $_SESSION['ppl_charges'] = $charges;
//        echo '<pre>'; print_r($padata); echo '</pre>';exit();
        $httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $padata);
        
        //Respond according to message we receive from Paypal

        if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

            $paypalmode = ($this->PP_MODE == 'sandbox') ? '.sandbox' : '';

            //Redirect user to PayPal store with Token received.

            $paypalurl = 'https://www' . $paypalmode . '.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $httpParsedResponseAr["TOKEN"] . '';
            header('Location: ' . $paypalurl);
            exit();
        } else {

            //echo "Show error message";

            echo '<div style="color:red"><b>Error : </b>' . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '</div>';

            echo '<pre>';

            print_r($httpParsedResponseAr);

            echo '</pre>';
        }
    }

    public function GetTransactionDetails($token, $products) {

        // we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
        // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut

        $padata = '&TOKEN=' . urlencode($token);

        $httpParsedResponseAr = $this->PPHttpPost('GetExpressCheckoutDetails', $padata, $this->PP_API_USERNAME, $this->PP_API_PASSWORD, $this->PP_API_SIGNATURE, $this->PP_MODE);

        if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

            echo '<br /><b>Stuff to store in database :</b><br /><pre>';

            #### SAVE BUYER INFORMATION IN DATABASE ###
            //see (http://www.sanwebe.com/2013/03/basic-php-mysqli-usage) for mysqli usage

            $buyerName = $httpParsedResponseAr["FIRSTNAME"] . ' ' . $httpParsedResponseAr["LASTNAME"];
            $buyerEmail = $httpParsedResponseAr["EMAIL"];
            $itemname = $httpParsedResponseAr["DESC"];
            echo "<pre>";
            var_dump($httpParsedResponseAr);
            echo "</pre>";

//              //Open a new connection to the MySQL server
            $mysqli = new mysqli('34.210.116.132', 'ahmed', 'pakistan47', 'educki');
//
//              //Output any connection error
//              if ($mysqli->connect_error) {
//              die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
//              }
            var_dump($_SESSION);
            die;
            $insert_row = $mysqli->query("INSERT INTO tbl_buyer_details
              (BuyerName,BuyerEmail,TransactionID,ItemName,ItemNumber, ItemAmount,ItemQTY,user_id)
              VALUES ('$buyerName','$buyerEmail','$transactionID','$itemname',$products[0]['ItemNumber'], $products[0]['ItemTotalPrice'],1,$this->session->userdata('userid'))");

            if ($insert_row) {
                print 'Success! ID of last inserted record is : ' . $mysqli->insert_id . '<br />';
            } else {
                die('Error : (' . $mysqli->errno . ') ' . $mysqli->error);
            }


            echo '<pre>';

            print_r($httpParsedResponseAr);

            echo '</pre>';
        } else {

            echo '<div style="color:red"><b>GetTransactionDetails failed:</b>' . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '</div>';

            echo '<pre>';

            print_r($httpParsedResponseAr);

            echo '</pre>';
        }
    }

    public function DoExpressCheckoutPayment($token, $payerID, $products ,$charges) {

        
        if (!empty($products)) {

            $padata = '&TOKEN=' . urlencode($token);
            $padata .= '&PAYERID=' . urlencode($payerID);

            $padata .= '&METHOD=DoExpressCheckoutPayment';

            foreach ($products as $p => $item) {

                $padata .= '&PAYMENTREQUEST_' . $p . '_CURRENCYCODE=' . urlencode($this->PP_CURRENCYCODE);
                $padata .= '&PAYMENTREQUEST_' . $p . '_AMT=' . urlencode($item['AMT']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_ITEMAMT=' . urlencode($this->GetProductsTotalAmount($products));
                $padata .= '&PAYMENTREQUEST_' . $p . '_TAXAMT=' . urlencode($item['TAXAMT']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_PAYMENTACTION=' . urlencode($item['PAYMENTACTION']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_SELLERPAYPALACCOUNTID=' . urlencode($item['SELLERPAYPALACCOUNTID']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_PAYMENTREQUESTID=' . urlencode($item['PAYMENTREQUESTID']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_NAME0=' . urlencode($item['NAME0']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_NUMBER0=' . urlencode($item['NUMBER0']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_DESC=' . urlencode($item['DESC']);
                $padata .= '&PAYMENTREQUEST_' . $p . '_QTY0=' . urlencode($item['QTY0']);
            }
            $padata .= '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($this->GetProductsTotalAmount($products));
            $padata .= '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($charges['TotalTaxAmount']);
            $padata .= '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($charges['ShippinCost']);
            $padata .= '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($charges['HandalingCost']);
            $padata .= '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($charges['ShippinDiscount']);
            $padata .= '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($charges['InsuranceCost']);


        //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.

        $httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $padata);

        var_dump($httpParsedResponseAr);
        echo "<pre>";
        var_dump($padata);
        die;
        //Check if everything went ok..
        if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

            echo '<h2>Success</h2>';
            echo 'Your Transaction ID : ' . urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);

            /*
              //Sometimes Payment are kept pending even when transaction is complete.
              //hence we need to notify user about it and ask him manually approve the transiction
             */

            if ('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {

                echo '<div style="color:green">Payment Received! Your product will be sent to you very soon!</div>';
            } elseif ('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {

                echo '<div style="color:red">Transaction Complete, but payment may still be pending! ' .
                'If that\'s the case, You can manually authorize this payment in your <a target="_new" href="http://www.paypal.com">Paypal Account</a></div>';
            }

            $this->GetTransactionDetails($token, $padata);
        } else {

            echo '<div style="color:red"><b>Errorcc : </b></div>';
            echo '<div style="color:red"><b>Error : </b>' . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '</div>';
            echo '<pre>';

            print_r($httpParsedResponseAr);

            echo '</pre>';
        }
        } else {

            // Request Transaction Details

            $this->GetTransactionDetails();
        }
    }

    public function PPHttpPost($methodName_, $nvpStr_) {



        // Set up your API credentials, PayPal end point, and API version.



        $API_UserName = urlencode($this->PP_API_USERNAME);

        $API_Password = urlencode($this->PP_API_PASSWORD);

        $API_Signature = urlencode($this->PP_API_SIGNATURE);



        $paypalmode = ($this->PP_MODE == 'sandbox') ? '.sandbox' : '';



        $API_Endpoint = "https://api-3t" . $paypalmode . ".paypal.com/nvp";

        $version = urlencode('109.0');



        // Set the curl parameters.

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);

        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        //curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
        // Turn off the server and peer verification (TrustManager Concept).

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);



        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POST, 1);



        // Set the API operation, version, and API signature in the request.

        $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";



        // Set the request as a POST FIELD for curl.

        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);



        // Get response from the server.

        $httpResponse = curl_exec($ch);



        if (!$httpResponse) {

            exit("$methodName_ failed: " . curl_error($ch) . '(' . curl_errno($ch) . ')');
        }



        // Extract the response details.

        $httpResponseAr = explode("&", $httpResponse);



        $httpParsedResponseAr = array();

        foreach ($httpResponseAr as $i => $value) {



            $tmpAr = explode("=", $value);



            if (sizeof($tmpAr) > 1) {



                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
        }



        if ((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {



            exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
        }



        return $httpParsedResponseAr;
    }

}
