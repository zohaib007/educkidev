<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
 	{
 		parent::__construct();
		$this->load->helper('url');
		$this->load->library('upload');
		$this->load->model('product/product_model');
		$this->load->model('articles/articles_model');
		$this->load->model('categories/categories_model');
		$this->load->model('common/common_model');
		$this->load->model('company/company_model');
		$this->load->model('user/user_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('email');
		$this->load->library('cart');
		$this->load->library('pagination');
		//error_reporting(0);
 		//auth();
		UrlRferer();
 		
		/*$config = array(
						'key' => "1234567890123456",
						'iv' => "fedcba9876543210"
						);
						
		$this->load->library('padCrypt');
		$this->load->library('AES_Encryption', $config);*/
 	}
	
///////////////////////////////////////////////Index//////////////////////////////////////////
	function stats()
	{  
		authFront();
	//$_POST['nId']=25;
 		$nProdId=$this->input->post('nId',true);
		$nPageType=$this->input->post('nPageType',true);		
		if($nProdId>0 && $nProdId!="")
		{
			$this->product_model->prodStats($nProdId,$nPageType);
  			$arr=array(
						"stat_prod_id"=>$nProdId,						
						"stat_user_id"=>$this->session->userdata('userid'),
						"stat_page_type"=>$nPageType,
						"stat_datetime"=>date("Y-m-d h:i"),
						"stat_user_ip"=>$this->input->ip_address()		
 						);
		  $this->common_model->commonSave('tbl_stats',$arr);	
 		}
 	}
	
	function statics($nProdId,$nPageType)
	{  
		authFront();
		//$_POST['nId']=25;
 		if($nProdId>0 && $nProdId!="")
		{
			$this->product_model->prodStats($nProdId,$nPageType);
  			$arr=array(
						"stat_prod_id"=>$nProdId,						
						"stat_user_id"=>$this->session->userdata('userid'),
						"stat_page_type"=>$nPageType,
						"stat_datetime"=>date("Y-m-d h:i"),
						"stat_user_ip"=>$this->input->ip_address()		
 						);
		  return $nId=$this->common_model->commonSave('tbl_stats',$arr);	
 		}
 	}
	public function index()
	{
		$data="";
		//if($this->session->userdata('logged_in')==TRUE)
		//{
		   
			$data['channel']="";
			$data['rstArt']=$this->articles_model->getArticles(" AND art_status = 1 AND art_feature=2  ORDER BY art_date Asc LIMIT 0,8");
			$data['results'] = $this->common_model->getCombox('tbl_content');
			$this->load->view('index1',$data);
		//}
		/*else		     
		 {
			$this->load->view('index1_soon',$data);
			if($this->input->post('subemail')!="")
			{
				$message="";
				$subject = 'Thank you for signing up with BEYE!';				
				$message.='Thank you for your interest in Beye.com. Your email address has been recorded. Please keep an eye on your inbox for exciting announcements from BEYE over the next few months. If you have any questions in the meantime please email ContactUs@Beye.com.<br /><br />Regards, Team BEYE';
				$site_email = 'Admin@beye.com';
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from("Admin@beye.com");
				$this->email->to($this->input->post('subemail'));
				$this->email->subject($subject);
				$this->email->message($message);
				$data_email['uemail']=$this->input->post('subemail');
				$this->email->send();
				$this->email->clear(TRUE);
				$emails=$this->comingsoonemail($data_email);
				//$this->email->clear(TRUE);
				$this->session->set_flashdata('emailsend', "Thank you.");
				header("Location: ".base_url()."");
		  }
		}*/
	}
	////////////////////////////////emai coming soon /////////////////////////	
	 function comingsoonemail($data_email)
	{	
		
		$subjects = 'You have a new COMING SOON sign-up!';
		$messages="";
		$messages.="Dear Beye Admin,<br/> You have a new sign-up on your coming soon page.Please find the user's email below:<br/>		                     <br />".$data_email['uemail'];
		$site_email = 'Admin@beye.com';
		$this->load->library('email');
		$this->email->set_mailtype('html');
		$this->email->from('Admin@beye.com');
		$this->email->to('sspinelli@bmctoday.com');
		/*$this->email->to("dotlogics_usa@yahoo.com");*/
		$this->email->subject($subjects);
		$this->email->message($messages);
		$this->email->send();
		$this->email->clear(TRUE);
	}
	////////////////////////////////////////comin son login////////////////////////////////
	function  beta_login_user()
	{
			
								$checklogin=array(
									'user_email'	 =>$this->input->post('uname'),
									'user_password'=>$this->input->post('upass'),
									'user_status'=>'1'
								);
								$result=$this->common_model->common_where('tbl_user',$checklogin);
								
								   if($result->num_rows()>0)
									 {
										  $row = $result->row_array();
										  $sessiondata = array(
										  'userid'  => $row['user_id'],
										  'user_name'  => $row['user_user_name'],
										  'country'  => $row['user_country'],
										  'useremail'=> $row['user_email'],
										  'userfname'=>$row['user_fname'],
										  'userphone'=>$row['user_phone'],
										  'usersuffix'=>$row['user_suffix'],
										  'userlname'=>$row['user_lname'],
										  'usertype'=>$row['user_type'],
										  'logged_in' => TRUE
										  );
										$this->session->set_userdata($sessiondata);
										echo "fine";
										//redirect('accountinfo');
										}
										else
										{
											echo "Invalid Email or Password";
										}
	}
/*-------------------------------User Register----------------------------------------*/
public function register_user()
{	
			//authFront();
			$this->form_validation->set_rules('ufname', 'First Name', 'trim|required|xss_clean');
			/*$this->form_validation->set_rules('ulname', 'Last Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('usuffix', 'Suffix', 'trim|required|xss_clean');
			if($this->input->post('usuffix')=='Other')
			{
			$this->form_validation->set_rules('otherusuffixname', 'Other Suffix', 'trim|required|xss_clean');	
			}*/
		
	if ($this->form_validation->run() == FALSE )
	{
			$data['results'] = $this->common_model->getCombox2('tbl_company');
			$data['rstCountry']=$this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
			$data['rstState']=$this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();
			$this->load->view('register',$data);	
	}else{
			
			$data_save['user_fname'] = $this->input->post('ufname');
			$data_save['user_lname'] = $this->input->post('ulname');
			$data_save['user_suffix'] = $this->input->post('usuffix');
			$data_save['user_other_suffix'] = $this->input->post('otherusuffixname');
			$data_save['user_suffix'] 		 	=$this->input->post('usuffix');
			//$data_save['user_user_name'] 	 	=$this->input->post('uuname');
			$data_save['user_gender'] 		 	=$this->input->post('ugender');
			$data_save['user_address'] = $this->input->post('ustreet');
			$data_save['user_address1'] = $this->input->post('ustreet2');
			$data_save['user_email'] 		 	=$this->input->post('uemail');
			$data_save['user_password'] 	 	=$this->input->post('upass');
			$data_save['user_passwordm_d5']	 	=md5($this->input->post('upass'));
			$data_save['user_city'] = $this->input->post('ucity');
			$data_save['user_state'] = $this->input->post('ustate');
			$data_save['user_zip'] = $this->input->post('uzip');
			$data_save['user_country'] = $this->input->post('ucountry');
			$data_save['user_address_type'] = $this->input->post('uaddtype');
			$data_save['user_phone'] = $this->input->post('uphone');
			$data_save['user_type']				=$this->input->post('iam');
			$data_save['user_op_md_cataract']	=$this->input->post('cataract');
			$data_save['user_op_md_refractive_cataract']=$this->input->post('refractive');
			$data_save['user_op_md_cornea']		=$this->input->post('Cornea');
			$data_save['user_op_md_dry_eye_allergy']=$this->input->post('dry_eye');
			$data_save['user_op_md_general_ophthalmology']=$this->input->post('non-surgery');
			$data_save['user_op_md_glaucoma']	=$this->input->post('glaucoma');
			$data_save['user_op_md_laser_vision_correction']=$this->input->post('laser_vision');
			$data_save['user_op_md_retina']		=$this->input->post('retina');
			$data_save['user_op_md_oculoplastics']=$this->input->post('oculoplastics');
			$data_save['user_op_md_pediatric']	=$this->input->post('pediatric');
			$data_save['user_op_md_graduation_year']=$this->input->post('mschool');
			$data_save['user_op_md_practice_owner']=$this->input->post('owner');
			$data_save['user_op_md_solo_practice']=$this->input->post('solo');
			$data_save['user_op_md_partnership']=$this->input->post('partnership');
			$data_save['user_op_md_franchise_chain']=$this->input->post('franchise');
			$data_save['user_op_md_hospital']	=$this->input->post('hospital');
			$data_save['user_op_md_academia']	=$this->input->post('academia');
			$data_save['user_op_md_other']		=$this->input->post('other');
			$data_save['user_op_md_not_currently_practice']=$this->input->post('not');
			$data_save['user_ms_cataract'] 		=$this->input->post('mscataract');
			$data_save['user_ms_refractive_cataract']  =$this->input->post('msrefractive');
			$data_save['user_ms_cornea']		=$this->input->post('mscornea');
			$data_save['user_ms_dry_eye']		=$this->input->post('msdry_eye');
			$data_save['user_ms_general_ophthalmology']=$this->input->post('msnon-surgery');
			$data_save['user_ms_glaucoma']		=$this->input->post('msglaucoma');
			$data_save['user_ms_laser_vision_correction']=$this->input->post('mslaser_vision');
			$data_save['user_ms_retina']		=$this->input->post('msretina');
			$data_save['user_ms_oculoplastics']	=$this->input->post('msoculoplastics');
			$data_save['user_ms_pediatric']		=$this->input->post('mspediatric');
			$data_save['user_ms_graduation_year']=$this->input->post('msmschool');
			$data_save['user_ms_undecided']		= $this->input->post('msundecided');
			$data_save['user_opt_contact_lenses']=$this->input->post('optocontactlenses');
			$data_save['user_opt_allergy']		=$this->input->post('optoallergy');
			$data_save['user_opt_dry_eye']		=$this->input->post('optodry_eye');
			$data_save['user_opt_glaucoma']		=$this->input->post('optoglaucoma');
			$data_save['user_opt_low_vision']	=$this->input->post('optolowvision');
			$data_save['user_opt_ocular_disease']=$this->input->post('optooculardisease');
			$data_save['user_opt_refractive']	=$this->input->post('optorefractive');
			$data_save['user_opt_practice_owner']=$this->input->post('optoowner');
			$data_save['user_opt_partnership']  =$this->input->post('optopartnership');
			$data_save['user_opt_franchise_chain']=$this->input->post('optofranchise');
			$data_save['user_opt_hospital']		=$this->input->post('optohospital');
			$data_save['user_opt_academia']		=$this->input->post('optoacademia');
			$data_save['user_opt_other']		=$this->input->post('optoother');
			$data_save['user_opt_not_practicing']=$this->input->post('optonot');
			$data_save['user_opt_ascpatner']=$this->input->post('optopatner');
			$data_save['user_opt_work']    =$this->input->post('workinopt');
			$data_save['user_opt_graduation_year']=$this->input->post('optomschool');
			$data_save['user_opts_contact_lenses']=$this->input->post('msoptocontactlenses');
			$data_save['user_opts_allergy']		=$this->input->post('msoptoallergy');
			$data_save['user_opts_dry_eye']		=$this->input->post('msoptodry_eye');
			$data_save['user_opts_glaucoma']	=$this->input->post('msoptoglaucoma');
			$data_save['user_opts_low_vision']	=$this->input->post('msoptolowvision');
			$data_save['user_opts_ocular_disease']=$this->input->post('msoptooculardisease');
			$data_save['user_opts_refractive']	=$this->input->post('msoptorefractive');
			$data_save['user_opts_undecided']	=$this->input->post('msopundecided');
			
			$data_save['user_opts_graduation_year']=$this->input->post('msoptomschool');
			$data_save['user_register_date']=date("y-m-d,h:i:s");
			$data_save['user_nur_cataract']		=$this->input->post('nurcataract');
			$data_save['user_nur_refractive_cataract']=$this->input->post('nurrefractive');
			$data_save['user_nur_cornea']		=$this->input->post('nurCornea');
			$data_save['user_nur_dry_eye']		=$this->input->post('nurdry_eye');
			$data_save['user_nur_general_ophthalmology']=$this->input->post('nurnon-surgery');
			$data_save['user_nur_glaucoma']		=$this->input->post('nurglaucoma');
			$data_save['user_nur_laser_vision']	=$this->input->post('nurlaser_vision');
			$data_save['user_nur_retina']		=$this->input->post('nurretina');
			$data_save['user_nur_oculoplastics']=$this->input->post('nuroculoplastics');
			$data_save['user_nur_pediatric']	=$this->input->post('nurpediatric');
			$data_save['user_pa_ophthalmologists_practice'] = $this->input->post('opht1');
			$data_save['user_pa_optometrists_practice']		=$this->input->post('opto1');
			$data_save['user_pa_workin']		=$this->input->post('adminowner');
			$data_save['user_other_company']	=$this->input->post('othercompanynameselect');
			$data_save['user_supply_companyname']		=$this->input->post('supplycompany');
			$data_save['user_op_md_ascpartner']		=$this->input->post('ascpartner');
			
			$activation=md5($this->input->post('uemail').time());
			$data_save['user_activation_code']	=$activation;
			$data_save['user_status']='0';
			$this->common_model->commonSave('tbl_user',$data_save);
			if($data_save['user_type']=='supplier')
			{
				$message="";
				$subject = 'Thank you for your Registration';
				$message.='Hello '.$data_save['user_fname'].',<br /><br />';
				$message.='Thank you for registering with us. Your registration information is being processed and you will be notified of your account activation shortly.  <br /><br /><br />Beye Team';
				$emails=$this->userregisteremail($data_save);
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from("Admin@beye.com");
				$this->email->to($this->input->post('uemail'));
				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->send();
				$this->email->clear(TRUE);
				redirect("register-thank");
			}
			else
			{
				$message="";
				$subject = 'Thank you for your Registration';
				$message.='Hello '.$data_save['user_fname'].',<br /><br />';
				$message.='Thank you for registering with us. Please click on the link below to active your account.We hope you enjoy your online experience.  <br/> <br/> <a href="'.base_url().'activation/'.$activation.'">'.base_url().'activation/'.$activation.'</a><br /><br /> Team BEYE';
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from("Admin@beye.com");
				$this->email->to($this->input->post('uemail'));
				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->send();
				$this->email->clear(TRUE);
				/*$data['cData'] = 'This account is already activated. Please Login to Use Our Services';
				$this->session->set_flashdata('inValidLogin', $data['cData']);*/
				//$this->session->set_flashdata('msg', 'Cannels added successfully');
				redirect("register-thanks");
				
			}
			/*$data['cData'] = 'This account is already activated. Please Login to Use Our Services';
			$this->session->set_flashdata('inValidLogin', $data['cData']);*/
			//$this->session->set_flashdata('msg', 'Cannels added successfully');
			
		}
}
/*--------------End User Register--------------*/
/*--------------Admin email when user register--------*/
function userregisteremail($data)
{	
		$data['title'] ='BEYE';
		$subject = 'New User Register';
		$message="";
		$message.='New Vendor registration has been received awaiting your approval.';
		$site_email = 'Admin@beye.com';
		$this->load->library('email');
		$this->email->set_mailtype('html');
		/*$this->email->from('sspinelli@bmctoday.com');*/
		$this->email->from('Admin@beye.com');
		$this->email->to("sspinelli@bmctoday.com");
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
		$this->email->clear(TRUE);
}
///////////////////////////////////	
function reg_thanks()
{ 
		 //authFront();
		 $this->load->view('reg_thanks',$data="");	
}
function reg_thank()
{ 
		 //authFront();
		 $this->load->view('reg_thanks_vendor',$data="");	
}
/*------------------ activation code user registration ----------------------------------*/ 
function activate($str)
{
		$data['title']        =  'Users Account Activation';
		$data['keywords']     =  'Users Account Activation';
		$data['description']  = 'Users Account Activation';			
		if($str == '')
		{
			$data['cData']    = 'No Activation key is found.';			
		}
		else
		{
			
			$where = array('user_activation_code' => mysql_real_escape_string($str));
			$result=$this->common_model->common_where('tbl_user',$where);
			if($result->num_rows() > 0)
            {
				$row = $result->row(); 
				if($row->user_status != '1')
				{ 
					$where_update = array('user_id' => $row->user_id);
					$update_data = array('user_status' => '1'); 
					$rs = $this->common_model->update('tbl_user',$update_data,$where_update);
					if($rs)
					{
						
					session_start();
					/*$this->session->set_userdata(
											array(
												'userid'     => $row->user_id,
												'logged_in'   => true,
												'useremail'  => $row->user_email,
												'userfname'   => $row->user_fname,
												'userlname'=>$row->user_lname,
												'logged_in' => TRUE											
												)
											);*/
											
						//$data['rstRow'] 	 = $this->db->query("SELECT  * FROM tbl_user WHERE user_id =".$this->session->userdata('user_id'))->row();
		               // $_SESSION['my_user_data'] =  $data['rstRow'];
								
						// insert all operations here//
						$data['cData'] = 'your account is now active.';
						$this->session->set_flashdata('inValidLogin', $data['cData']); 
		               // redirect('myaccount');
					  	header("Location: ".base_url()."login");
					}
					else
					{
					echo	$data['cData'] = 'Opps! there is some issue in your activation code. Your account is not active';
						$this->session->set_flashdata('inValidLogin', $data['cData']); 
		          		header("Location: ".base_url()."login");
					}
				}
				else
				{
						$data['cData'] = 'This account is already activated. Please Login to Use Our Services';
						$this->session->set_flashdata('inValidLogin', $data['cData']); 
		        		header("Location: ".base_url()."login");
				}	
			}
			else
			{
			 		$data['cData'] = 'No such activation key exists in our database.';
					$this->session->set_flashdata('inValidLogin', $data['cData']); 
		      		header("Location: ".base_url()."login");
			}			
		}
	
}
	
/*------------ end   activation ---------------------------*/
/*-----------------user name Check already exit in registration---------------------- */
public function username_check()
	{		
	/*	
		$checkuser=array('user_user_name'=>$this->input->post('user_name'));
		$result=$this->common_model->common_where('tbl_user',$checkuser);
		//echo $this->db->last_query();
		if($result->num_rows()>0)
			  {
				echo "user name already exists";
			  }
			  else
			  {
				echo "";
			  }*/
		
}
/*-----------------user email Check already exit in registration ---------------------- */
public function useremail_check()
{		
		
		$checkuser=array('user_email'=>$this->input->post('user_email'));
		$result=$this->common_model->common_where('tbl_user',$checkuser);
		if($result->num_rows()>0)
			  {
				echo "user email already exists";
			  }else{
				  echo "";
			  }
		
	}

/*---------------------------- User Login--------------------------------------------*/
	function login_user()
	{
	
		if($this->input->cookie('user')!="" && $this->input->cookie('user_pass')!="")
					{
						redirect('accountinfo');
					}
		 // authFront();
		$this->form_validation->set_rules('useremail', 'User Email', 'trim|required|xss_clean|valid_email');
		$this->form_validation->set_rules('upass', 'Password', 'trim|required|xss_clean');
		if ($this->form_validation->run() == FALSE)
			{
				$data['rstCountry']=$this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
				$data['results'] = $this->common_model->getCombox('tbl_company');	 
				$data['rstState']=$this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();;
				$this->load->view('login',$data);	
		    }
			else
			{
					
					$checklogin=array(
									'user_email'	 =>$this->input->post('useremail'),
									'user_password'=>$this->input->post('upass'),
									'user_status'=>'1'
									);
					$result=$this->common_model->common_where('tbl_user',$checklogin);
					
					if($result->num_rows()>0)
					{
					  $row = $result->row_array();
					  
						 $sessiondata = array(
										  'userid'  => $row['user_id'],
										  'user_name'  => $row['user_user_name'],
										  'country'  => $row['user_country'],
										  'useremail'=> $row['user_email'],
										  'userfname'=>$row['user_fname'],
										  'userphone'=>$row['user_phone'],
										  'usersuffix'=>$row['user_suffix'],
										  'userlname'=>$row['user_lname'],
										  'usertype'=>$row['user_type'],
										  'logged_in' => TRUE
										  );
						$this->session->set_userdata($sessiondata);
						$refererUrl = $this->session->userdata('refererUrl');
							if($this->input->post('remember')!="")
							{
								
								$this->input->set_cookie('user', md5($row['user_email']),86500);
								$this->input->set_cookie('user_pass', md5($row['user_password']),86500);
							}
								if($refererUrl=="")
								{
									$refererUrl=base_url();
								}
						header("Location: ".$refererUrl."");
						redirect('accountinfo');
					}
					else
					{
						$data['cData']="Invalid Password or Email";
						$this->session->set_flashdata('No active user', $data['cData']);
						redirect('login');
					}
				 
			}
	}
/////////////////////////////////login_out////////////////////////////////////////////////
function login_out()
{
	$this->input->set_cookie('user','',-86500);
	$this->input->set_cookie('user_pass', '',-86500);
	$unsetdata = array('user_name' => '', 'useremail' => '','userid'=>'','logged_in'=>FALSE);
	$this->input->set_cookie('paymentpassword', '',-1000);

	$this->session->unset_userdata($unsetdata);
	header("Location: ".base_url()."login");
}
/*----------------------validation user password and email------------------------------*/
	function check_password($str)
	{
		$checklogin=array('user_passwordm_d5'	 =>md5($this->input->post('upass')),
						  'user_email'	         =>$this->input->post('useremail'));
		$result=$this->common_model->common_where('tbl_user',$checklogin);
		if($result->num_rows()>0)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_password','Invalid Email or Password  ');
			return FALSE;
		}
	}
	
/*--------------------------retrive password----------------------------------*/
   function retrivepassword()
   {  
   //authFront();
	   $this->form_validation->set_rules('useremail', 'User Email', 'trim|required|xss_clean|valid_email|callback_check_email');
	   //$this->form_validation->set_message('valid_email', 'Register for the site using a valid email address');
		if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('forgot-password');	
			}else
			{
				
				$checklogin=array('user_email'	 =>$this->input->post('useremail'));
				$result=$this->common_model->common_where('tbl_user',$checklogin);
					if($result->num_rows()>0)
					{
						 	$row = $result->row_array();
						 	$password = $row['user_password'];	
						 	$email=$row['user_email'];
						 	$nId=$row['user_id'];
							$data_save['user_status_password']=0;
							$activationpassword=md5($password.''.$email.time());
							$data_save['user_activation_code_password']	=$activationpassword;
							$this->common_model->commonUpdate('tbl_user',$data_save ,"user_id",$nId);
						 
					}
				$message="";
				$subject = 'Password Recovery';
				$message.='Dear User <br /><br />Please click on the link below to create a new password for your account. <br/> <br/> <a href="'.base_url().'activationpassword/'.$activationpassword.'">'.base_url().'activationpassword/'.$activationpassword.'</a>';
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from("Admin@beye.com");
				$this->email->to($email);
				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->send();
				$this->email->clear(TRUE);								
				redirect('forgot_password_thank');
				//forgot_password_thanks
			}
	  	
   }
  

 

 




 ///////////////////////////start activation password/////////////////////////////////  
    function activationpassword($str)
	{
		$data['title']        =  'Users Password Activation';
		$data['keywords']     =  'Users Password Activation';
		$data['description']  = 'Users Password Activation';			
		if($str == '')
		{
			$data['cData']    = 'No Password Activation key is found.';			
		}
		else
		{
			
			$where = array('user_activation_code_password' => mysql_real_escape_string($str));
			$result=$this->common_model->common_where('tbl_user',$where);
			if($result->num_rows() > 0)
            {
				$row = $result->row(); 
				if($row->user_status_password != '1')
				{ 
					$where_update = array('user_id' => $row->user_id);
					$update_data = array('user_status_password' => '1'); 
					$rs = $this->common_model->update('tbl_user',$update_data,$where_update);
					if($rs)
					{
						
					session_start();
					$this->session->set_userdata(
											array(
												'userid'     => $row->user_id
												)
											);
											
						//$data['rstRow'] 	 = $this->db->query("SELECT  * FROM tbl_user WHERE user_id =".$this->session->userdata('user_id'))->row();
		               // $_SESSION['my_user_data'] =  $data['rstRow'];
								
						// insert all operations here//
						//$data['cData'] = 'your account is now active.';
						//$this->session->set_flashdata('active', $data['cData']); 
		               // redirect('myaccount');
					  header("Location: ".base_url()."restpassword");
					}
					else
					{
					echo	$data['cData'] = 'Opps! there is some issue in your Password Activation code. Your account is not active';
						$this->session->set_flashdata('inValidLogin', $data['cData']); 
		          		header("Location: ".base_url()."login");
					}
				}
				else
				{
						$data['cData'] = 'This Password Activation is already activated. Please Login to Use Our Services';
						$this->session->set_flashdata('inValidLogin', $data['cData']); 
		        		header("Location: ".base_url()."login");
				}	
			}
			else
			{
			 		$data['cData'] = 'No such Password Activation key exists in our database.';
					$this->session->set_flashdata('inValidLogin', $data['cData']); 
		      		header("Location: ".base_url()."login");
			}			
		}
	
	}
	
	
/*------------ end   activation password---------------------------*/
  
  //////////////////////////////////////restpassword/////////////////////
	function restpassword()
	{
		//authFront();
		$this->form_validation->set_rules('cpassword_new','New Password', 'trim|required|xss_clean|min_length[8]|matches[cpassword_conf]');
		$this->form_validation->set_rules('cpassword_conf','Password Confirmation', 'trim|required|xss_clean');	
		
		if($this->form_validation->run() == false)
		{		 
			$this->load->view('set_password_email');
		}else
		{
			$data_save['user_password'] = $this->input->post('cpassword_new');
			$data_save['user_passwordm_d5'] = md5($this->input->post('cpassword_new'));
			$this->common_model->commonUpdate('tbl_user',$data_save ,"user_id",$this->session->userdata('userid'));
			$this->session->set_flashdata('pass_chnage_msg',"Your Password Changed Successfully");	
			redirect("login");
		}
	}
  
/////////////////////////retrive pasword thanks//////////////////
	function forgot_password_thanks()
	{	
		//authFront();
		$this->load->view('forgot_password_thanks');
	}
 /*---------------------retrive user email check ------------------*/
	function check_email($str)
	{
		$checklogin=array('user_email'	 =>$this->input->post('useremail'));
		$result=$this->common_model->common_where('tbl_user',$checklogin);
		if($result->num_rows()>0)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_email','Our records do not indicate this account exists.');
			return FALSE;
		}
	}
/***********************************about us footer***************************************/	
 function about()
 {
	//authFront();
	 $this->load->view('about-07');
	 
 }
 /////////////////////////privacy footer//////////////////////////////////////
  function privacy()
 {
	 //authFront();
	 $this->load->view('privacy-11');
 }
 /////////////////////////terms in footer/////////////////////////////////
  function terms()
 {
	 //authFront();
	 $this->load->view('terms-12');
 }
 //////////////////////////contact us footer//////////////////////////////
  function contactus()
 {
	 //authFront();
		 $this->form_validation->set_rules('ufname', 'First Name', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('ulname', 'Last Name', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('ucomments', 'Comment', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('uemail', 'Email', 'trim|required|xss_clean|valid_email');
		if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('contact-10');
			}else
			{
				
				$array['cont_fname']=$data['ufname']=$this->input->post('ufname');
				$array['cont_lname']=$data['ulname']=$this->input->post('ulname');
				$array['cont_comment']=$data['ucomments']=$this->input->post('ucomments');
				$array['cont_email']=$data['uemail']=$this->input->post('uemail');
				$array['cont_phone']=$data['uphone']=$this->input->post('uphone');
				$array['cont_suffix']=$data['usuffix']=$this->input->post('usuffix');
				$array['cont_date']=date('m-d-Y ');
				$this->common_model->commonSave('tbl_contact_us',$array);
				$message="";
				$subject = 'Thank you for contacting BEYE';
				$message.='Hello,<br /><br />';
				$message.='Thank you for contacting BEYE. One of your representatives will be in touch with you shortly.<br /> <br />Regards,<br /><br />Team BEYE';
				$site_email = 'Admin@beye.com';
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from("Admin@beye.com");
				$this->email->to($this->input->post('uemail'));
				$this->email->subject($subject);
				$this->email->message($message);
				
				$this->email->send();
				$emails=$this->contactEmail1($data);
				$this->email->clear(TRUE);
				$this->session->set_flashdata('emailsend', "Email sent");
				redirect('thanks-contact');
			}
 }
 function thanks_contact()
 {
	 //authFront();
 	 $this->load->view('add-contact-thankyou-08-2');
	 
 }
 ////////////////////////contact use email function footer/////////////////////////////////
 	function contactEmail1($data)
	{	
		$data['title'] ='BEYE';
		$subject = 'You have a new CONTACT FORM sign-up!';
		$message = $this->load->view('emails/contact_email', $data, true );
		$site_email = 'Admin@beye.com';
		$this->load->library('email');
		$this->email->set_mailtype('html');
		$this->email->from("Admin@beye.com");
		$this->email->to("sspinelli@bmctoday.com");
		/*$this->email->to("dotlogics159@gmail.com");*/
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
		$this->email->clear(TRUE);
	}
 /////////////////////////footer advertising///////////////////////////
   function advertising()
 {
	 //authFront();
	 $this->load->view('advertising-09');
 }
///////////////////////////////////////addproduct/////////////////
    function addproduct()
	 {
		//authFront();
		 $this->form_validation->set_rules('ufname', 'First Name', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('ulname', 'Last Name', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('ucompany', 'Company Name', 'trim|required|xss_clean');
		  $this->form_validation->set_rules('uemail', 'User Email', 'trim|required|xss_clean|valid_email');
		 $this->form_validation->set_rules('ucategoy', 'Product Category', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('uproname', 'Product Name', 'trim|required|xss_clean');
		 $this->form_validation->set_rules('upropage', 'Product Page on Company Site', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
			{
				$data['resultsChannel']=$this->common_model->common_where('tbl_categories',array('cat_type' =>'1','cat_status'=>'1'  ),'cat_name','ASC');
				$data['resultsCompany']=$this->common_model->common_where('tbl_company',array(),'com_name','ASC');
			    $this->load->view('add-product-08-1',$data);
	 		}
			else
			{
				////////////////////////////////img1
				 $fileDP1=$this->input->post('saved_dp1');
				 
				// =======================File Display 
				 if($_FILES['dp1']['name'] != ''){ //If user selected the photo to upload.
					
					$uploaddir_11 					= FCPATH."images/add_prod/";
					$uploaddir_11 					= str_replace(" ","",$uploaddir_11);
					$config_11['upload_path'] 		= $uploaddir_11;
					$config_11['allowed_types'] 	= 'doc|docx|xls|xlsx|pdf|gif|jpg|png|jpeg';
					$config_11['overwrite'] 		= false;
					$config_11['remove_spaces'] 	= true;
					$config_11['file_name'] 		= time();
					
					
					
					$this->upload->initialize($config_11);
					if ( ! $this->upload->do_upload('dp1')){ //Print message if file is not uploaded
						
						$this->session->set_userdata('msg_err', $this->upload->display_errors());
						header("Location: ".base_url()."addproduct");
						exit();
						
					}else{
							$dataDP = $this->upload->data();
							$fileDP1	= $dataDP['file_name'];
						
 													
							//fileDP2=$file_Post; 
 						
						}
				 }
//////////////1 img  end//////
/////////////2nd img///////////////////////////////////////////////
							$fileDP2=$this->input->post('saved_dp2');
				// =======================File Display 
			
				 if($_FILES['dp2']['name'] != ''){ //If user selected the photo to upload.
					
					$uploaddir_11 					= FCPATH."images/add_prod/";
					$uploaddir_11 					= str_replace(" ","",$uploaddir_11);
					$config_11['upload_path'] 		= $uploaddir_11;
					$config_11['allowed_types'] 	= 'doc|docx|xls|xlsx|pdf|gif|jpg|png|jpeg';
					$config_11['overwrite'] 		= false;
					$config_11['remove_spaces'] 	= true;
					$config_11['file_name'] 		= "doc_".time();
					$this->upload->initialize($config_11);
					if ( ! $this->upload->do_upload('dp2')){ //Print message if file is not uploaded
						
						$this->session->set_flashdata('msg_err', $this->upload->display_errors());
						header("Location: ".base_url()."addproduct");
						exit();
					}else
						{
 							$dataDP2 = $this->upload->data();
							$fileDP2	= $dataDP2['file_name'];							
							//fileDP2=$file_Post; 
 						} 
					
				}
////////////2nd end////////////////
//////////////3rd//////////////////////
					$fileDP3=$this->input->post('saved_dp3');
				// =======================File Display 
			
				 if($_FILES['dp3']['name'] != ''){ //If user selected the photo to upload.
					
					$uploaddir_11 					= FCPATH."images/add_prod/";
					$uploaddir_11 					= str_replace(" ","",$uploaddir_11);
					$config_11['upload_path'] 		= $uploaddir_11;
					$config_11['allowed_types'] 	= 'doc|docx|xls|xlsx|pdf|gif|jpg|png|jpeg';
					$config_11['overwrite'] 		= false;
					$config_11['remove_spaces'] 	= true;
					$config_11['file_name'] 		= "doc_".time();
					
					$this->upload->initialize($config_11);
					if ( ! $this->upload->do_upload('dp3')){ //Print message if file is not uploaded
						
						$this->session->set_flashdata('msg_err', $this->upload->display_errors());
						header("Location: ".base_url()."addproduct");
						exit();
						
					} else
						{
 							$dataDP3 = $this->upload->data();
							$fileDP3	= $dataDP3['file_name'];							
							//fileDP2=$file_Post; 
 						} 
				}
						
				///////////////////////////3rd end
			 	$data['ufname']=$this->input->post('ufname');
				$data['ulname']=$this->input->post('ulname');
		 		$data['ucompany']=$this->input->post('ucompany');
				$data['uemail']=$this->input->post('uemail');
				$data['ucategoy']=$this->input->post('ucategoy');
		 		$data['upnumber']=$this->input->post('upnumber');
				$data['uproname']=$this->input->post('uproname');
				$data['upropage']=$this->input->post('upropage');
				$data['r1']=$this->input->post('r1');
				$data['ucomments']=$this->input->post('ucomments');
				$data['prod_img1']= $fileDP1;
				$data['prod_img2']=$fileDP2;
				$data['prod_img3']= $fileDP3;
				$message="";
				$subject = 'Thank you for contacting BEYE';
				$message.='Hello,<br /><br />';
				$message.='Thank you for contacting BEYE. One of your representatives will be in touch with you shortly.<br /> <br />Regards,<br /><br />Team BEYE';
				$site_email = 'Admin@beye.com';
				$this->load->library('email');
				$this->email->set_mailtype('html');
				$this->email->from("Admin@beye.com");
				$this->email->to($this->input->post('uemail'));
				$this->email->subject($subject);
				$this->email->message($message);
				$data_email['uemail']=$this->input->post('subemail');
				$this->email->send();
				$this->email->clear(TRUE);
				$emails=$this->productemail1($data);				
				 redirect('thanks');
			}
 }
///////////////////////// footer add product thanks/////////////////
 function productaddthanks()
 {	
 	//authFront();
 	 	$this->load->view('add-product-thankyou-08-2');
 }
 ////////////////////////////////////product email//////////////
 function productemail1($data)

 	{	
		$data['title'] ='BEYE';
		$subject = 'You have a new ADD PRODUCT sign-up!';
		$message = $this->load->view('emails/product_email', $data, true );
		$site_email = 'sspinelli@bmctoday.com';
		$this->load->library('email');
		$this->email->set_mailtype('html');
		$this->email->from('Admin@beye.com');
		$this->email->to('sspinelli@bmctoday.com');
		
		$this->email->subject($subject);
		if($data['prod_img1']!=''){
		$this->email->attach('/'.FCPATH.'./images/add_prod/'.$data['prod_img1'].'');}
		if($data['prod_img2']!=''){
		$this->email->attach('/'.FCPATH.'./images/add_prod/'.$data['prod_img2'].'');}
		if($data['prod_img3']!=''){
		$this->email->attach('/'.FCPATH.'./images/add_prod/'.$data['prod_img3'].'');}
		$this->email->message($message);
		$this->email->send();
		//echo  $this->email->print_debugger();
		 $this->email->clear(TRUE);
	}
/////////////////////////productlistings//////////////////
 function productlistings()
 {	
 //authFront();
 	 	
			$this->load->view('product-listings');
		 
 }
/////////////////accountinfo-13-01  my account  user account/////////////////////////////////////////
	public function validate_alpha_spaces($value)
	{
		if ( preg_match ("/^[a-zA-Z\s]+$/",$value))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_alpha_spaces', 'The %s field Must be Alphabet with Spaces');
			return false;
			
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
function accountinfo()
{
			
	authFront();
			
	$data['actives']="accountinfo"; 
	$checklogin=array(
						'user_id'=>$this->session->userdata('userid')
					  );
	$data['results'] = $this->common_model->getCombox2('tbl_company');
	$data['result'] = $this->common_model->common_where('tbl_user',$checklogin)->result();
	$data['rstCountry'] = $this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
	$data['rstState'] = $this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();
	
	/*echo '<pre>';
	print_r($data['result']);
	exit;*/
	
	$data['active'] = 'active';
	
	$this->form_validation->set_rules('ufname', 'first name', 'trim|required|xss_clean|callback_validate_alpha_spaces');
	$this->form_validation->set_rules('ulname', 'last name', 'trim|required|xss_clean|callback_validate_alpha_spaces');
	$this->form_validation->set_rules('usuffix', 'suffix', 'trim|required|xss_clean');
	$this->form_validation->set_rules('ustreet', 'street address', 'trim|required|xss_clean');
	$this->form_validation->set_rules('ucity', 'city', 'trim|required|xss_clean|callback_validate_alpha_spaces');
	$this->form_validation->set_rules('uzip', 'zip code', 'trim|required|xss_clean|alpha_numeric');
	$this->form_validation->set_rules('uaddtype', 'address type', 'trim|required|xss_clean|callback_validate_alpha_spaces');
	$this->form_validation->set_rules('phone', 'Phone No', 'trim|required|xss_clean|numeric');
	if($this->input->post('iam')=='supplier')
	{
	$this->form_validation->set_rules('otheremail', 'Email', 'trim|xss_clean|valid_email');
	}
	if ($this->form_validation->run() == FALSE)
	{
		
		$this->load->view('13-1-Account-Info', $data);//accountinfo-13-01
	}
	else
	{
			$data_save['user_fname'] = $this->input->post('ufname');
			$data_save['user_lname'] = $this->input->post('ulname');
			$data_save['user_suffix'] = $this->input->post('usuffix');
			$data_save['user_other_suffix'] = $this->input->post('otherusuffixname');
			$data_save['user_address'] = $this->input->post('ustreet');
			$data_save['user_city'] = $this->input->post('ucity');
			$data_save['user_state'] = $this->input->post('ustate');
			$data_save['user_zip'] = $this->input->post('uzip');
			$data_save['user_country'] = $this->input->post('ucountry');
			$data_save['user_phone'] = $this->input->post('phone');
			$data_save['user_address_type'] = $this->input->post('uaddtype');
			$data_save['user_address1']      =$this->input->post('ustreet1');
			$data_save['user_type']				=$this->input->post('iam');
			$data_save['user_op_md_cataract']	=$this->input->post('cataract');
			$data_save['user_op_md_cataract']	=$this->input->post('cataract');
			$data_save['user_op_md_refractive_cataract']=$this->input->post('refractive');
			$data_save['user_op_md_cornea']		=$this->input->post('Cornea');
			$data_save['user_op_md_dry_eye_allergy']=$this->input->post('dry_eye');
			$data_save['user_op_md_general_ophthalmology']=$this->input->post('non-surgery');
			$data_save['user_op_md_glaucoma']	=$this->input->post('glaucoma');
			$data_save['user_op_md_laser_vision_correction']=$this->input->post('laser_vision');
			$data_save['user_op_md_retina']		=$this->input->post('retina');
			$data_save['user_op_md_oculoplastics']=$this->input->post('oculoplastics');
			$data_save['user_op_md_pediatric']	=$this->input->post('pediatric');
			
			$data_save['user_op_md_graduation_year']=$this->input->post('mschool');
			$data_save['user_op_md_practice_owner']=$this->input->post('owner');
			$data_save['user_op_md_solo_practice']=$this->input->post('solo');
			$data_save['user_op_md_partnership']=$this->input->post('partnership');
			$data_save['user_op_md_franchise_chain']=$this->input->post('franchise');
			$data_save['user_op_md_hospital']	=$this->input->post('hospital');
			$data_save['user_op_md_academia']	=$this->input->post('academia');
			$data_save['user_op_md_other']		=$this->input->post('other');
			$data_save['user_op_md_not_currently_practice']=$this->input->post('not');
			$data_save['user_op_md_ascpartner']=$this->input->post('ascpartner');
			$data_save['user_ms_cataract'] 		=$this->input->post('mscataract');
			$data_save['user_ms_refractive_cataract']  =$this->input->post('msrefractive');
			$data_save['user_ms_cornea']		=$this->input->post('mscornea');
			$data_save['user_ms_dry_eye']		=$this->input->post('msdry_eye');
			$data_save['user_ms_general_ophthalmology']=$this->input->post('msnon-surgery');
			$data_save['user_ms_glaucoma']		=$this->input->post('msglaucoma');
			$data_save['user_ms_laser_vision_correction']=$this->input->post('mslaser_vision');
			$data_save['user_ms_retina']		=$this->input->post('msretina');
			$data_save['user_ms_oculoplastics']	=$this->input->post('msoculoplastics');
			$data_save['user_ms_pediatric']		=$this->input->post('mspediatric');
			$data_save['user_ms_graduation_year']=$this->input->post('msmschool');
			$data_save['user_ms_undecided']=$this->input->post('msoundecided');
			
			$data_save['user_opt_contact_lenses']=$this->input->post('optocontactlenses');
			$data_save['user_opt_allergy']		=$this->input->post('optoallergy');
			$data_save['user_opt_dry_eye']		=$this->input->post('optodry_eye');
			$data_save['user_opt_glaucoma']		=$this->input->post('optoglaucoma');
			$data_save['user_opt_low_vision']	=$this->input->post('optolowvision');
			$data_save['user_opt_ocular_disease']=$this->input->post('optooculardisease');
			$data_save['user_opt_refractive']	=$this->input->post('optorefractive');
			$data_save['user_opt_practice_owner']=$this->input->post('optoowner');
			$data_save['user_opt_partnership']  =$this->input->post('optopartnership');
			$data_save['user_opt_franchise_chain']=$this->input->post('optofranchise');
			$data_save['user_opt_hospital']		=$this->input->post('optohospital');
			$data_save['user_opt_academia']		=$this->input->post('optoacademia');
			$data_save['user_opt_other']		=$this->input->post('optoother');
			$data_save['user_opt_not_practicing']=$this->input->post('optonot');
			$data_save['user_opt_work']    =$this->input->post('workinopt');
			$data_save['user_opt_graduation_year']=$this->input->post('optomschool');
			$data_save['user_opts_contact_lenses']=$this->input->post('msoptocontactlenses');
			$data_save['user_opts_allergy']		=$this->input->post('msoptoallergy');
			$data_save['user_opts_dry_eye']		=$this->input->post('msoptodry_eye');
			$data_save['user_opts_glaucoma']	=$this->input->post('msoptoglaucoma');
			$data_save['user_opts_low_vision']	=$this->input->post('msoptolowvision');
			$data_save['user_opts_ocular_disease']=$this->input->post('msoptooculardisease');
			$data_save['user_opts_refractive']	=$this->input->post('msoptorefractive');
			$data_save['user_opts_graduation_year']=$this->input->post('msoptomschool');
			$data_save['user_opts_undecided'] =$this->input->post('msopundecided');
			
			$data_save['user_nur_cataract']		=$this->input->post('nurcataract');
			$data_save['user_nur_refractive_cataract']=$this->input->post('nurrefractive');
			$data_save['user_nur_cornea']		=$this->input->post('nurCornea');
			$data_save['user_nur_dry_eye']		=$this->input->post('nurdry_eye');
			$data_save['user_nur_general_ophthalmology']=$this->input->post('nurnon-surgery');
			$data_save['user_nur_glaucoma']		=$this->input->post('nurglaucoma');
			$data_save['user_nur_laser_vision']	=$this->input->post('nurlaser_vision');
			$data_save['user_nur_retina']		=$this->input->post('nurretina');
			$data_save['user_nur_oculoplastics']=$this->input->post('nuroculoplastics');
			$data_save['user_nur_pediatric']	=$this->input->post('nurpediatric');
			$data_save['user_pa_ophthalmologists_practice']=$this->input->post('opht1');
			$data_save['user_pa_optometrists_practice']		=$this->input->post('opto1');
			$data_save['user_pa_workin']		=$this->input->post('adminowner');
			$data_save['user_other_company']	=$this->input->post('othercompanynameselect');
			$data_save['user_supply_companyname']		=$this->input->post('supplycompany');
			$data_save['user_other_email']		=$this->input->post('otheremail');
			
		if($data_save['user_type']=="supplier")
		{
		
		  $this->db->trans_start(TRUE);
		  $strSql="SELECT * FROM tbl_user WHERE user_id = ".$this->session->userdata('userid');
		  $rstRowBefore=$this->db->query($strSql)->result_array();
		  $this->common_model->commonUpdate('tbl_user',$data_save ,"user_id",$this->session->userdata('userid'));						  
		  $nEditId=$this->recordEdits(1,$rstRowBefore,$strSql);
		  
		  
		  $this->session->set_userdata(array('usertype' => $data_save['user_type']));
		  if($nEditId>0)
		  	$this->session->set_flashdata('Update_info',"Your information has been successfully submitted. A BEYE representative will review and activate the information shortly. But currently it is not happening");	
		
		}
		else
		{
			 $this->common_model->commonUpdate('tbl_user',$data_save ,"user_id",$this->session->userdata('userid'));
			 $this->session->set_flashdata('Update_info',"Your information has been successfully updated.");	
		}
		   redirect("accountinfo");
		  exit();
		  }
		
	  
}

function recordEdits($nType,$rstRowBefore,$strSql)
{
	 $nId=0;
	  $rstRowAfter=$this->db->query($strSql)->result_array();	  
	  $this->db->trans_rollback();
	  $this->db->trans_complete();
 	  $arrResultAfter=array_diff_assoc($rstRowBefore[0],$rstRowAfter[0]);
	  $arrResultBefore=array_diff_assoc($rstRowAfter[0],$rstRowBefore[0]);	
 	   foreach($arrResultAfter  as $key => $value ) 
	   {  
 			$array="";
			$array['his_field']	 	=  $key ;
			$array['his_before']	= $arrResultAfter[$key];
			$array['his_after']	 	= $arrResultBefore[$key];
			$array['his_user_id']	= $this->session->userdata("userid");
			$array['his_type']		= $nType;	
			$array['his_url']		=current_url();		
			$array['his_date_time'] =date("Y-m-d H:i:s");	 
			$nId=$this->common_model->commonSave('tbl_editshistory',$array);
	  }
	  return $nId;
}
/////////////////////////////updatepassword  user update password/////////////////////////////////////
function updatepassword()

{
	authFront();
	$data['actives']="updatepassword";
	if($this->session->userdata('logged_in')==TRUE){
			$data['active']='active';
			$this->form_validation->set_rules('password_old','current password','trim|required|xss_clean|callback_old_pass_check');
			$this->form_validation->set_rules('cpassword_new','New Password', 'trim|required|xss_clean|min_length[8]|matches[cpassword_conf]');
      		$this->form_validation->set_rules('cpassword_conf','Password Confirmation', 'trim|required|xss_clean');	
			if($this->form_validation->run() == false)
			{		 
			$this->load->view('13-2-Update-Password',$data);
			}else
				{
				 $data_save['user_password'] = $this->input->post('cpassword_new');
				 $data_save['user_passwordm_d5'] = md5($this->input->post('cpassword_new'));
				 $this->common_model->commonUpdate('tbl_user',$data_save ,"user_id",$this->session->userdata('userid'));
				 $this->session->set_flashdata('pass_chnage_msg',"Your Password Changed Successfully");	
				 redirect("update_password");
				}
	}
}
///////////////////////////////////////////update password call back function check old password//////////////////
function old_pass_check($str)
	{
		$str = mysql_real_escape_string($str);
		$where = array('user_id' => $this->session->userdata('userid'), 'user_password' => $str );
		$result = $this->common_model->countQuery('tbl_user','user_id',$where);
		if ($result == 0 )
		{
			$this->form_validation->set_message('old_pass_check', '<span>Your %s is incorrect.</span>');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	
	}
	
	
/////////////////////////////////////add-payment-info/////////////////////////////////////////////////////
	function add_payment_info()
	{
		authFront();
		if($this->input->cookie('paymentpassword')=="")
		{
			redirect("accountinfo");
			exit;
		}
				$data['actives']="payment_info";
				if($this->session->userdata('logged_in')==TRUE)
				{
					$this->form_validation->set_rules('cardName','Card Name','trim|required|xss_clean');
					$this->form_validation->set_rules('cardType','Card Type', 'trim|required|xss_clean');
					$this->form_validation->set_rules('cvc','CVC', 'trim|required|xss_clean|max_length[4]|numeric');	
					$this->form_validation->set_rules('cardNumber','cardNumber', 'trim|required|xss_clean|max_length[16]|numeric|min_length[16]');						
					$this->form_validation->set_rules('cardType','card Type', 'trim|required|xss_clean');
					$this->form_validation->set_rules('exMnth','Expire Month', 'trim|required|xss_clean');
					$this->form_validation->set_rules('exYear','Expire Year', 'trim|required|xss_clean');					
					if($this->form_validation->run() == false)
					{		 
						$this->load->view("13-4-2-Payment-Information-Add More",$data);
					}
					else
					{
						
						$cardNumber = $this->aes_encryption->encrypt($this->input->post('cardNumber'));
						$data_save['pay_name'] = $this->input->post('cardName');
						$data_save['pay_card_type'] = $this->input->post('cardType');
						$data_save['pay_card_num'] = $cardNumber;
						$data_save['pay_exp_date'] = $this->input->post('exMnth').'/'.$this->input->post('exYear');
						$data_save['pay_cvc'] = $this->input->post('cvc');
						$data_save['pay_user_id'] = $this->session->userdata('userid');
						$data_save['pay_datatime'] = date('Y-m-d H:i:s');
						$this->db->insert('tbl_paymentinfo', $data_save);
						redirect("payment-info");
						exit;
					}
				}
				else
				{
					redirect("login");
					exit;
				}
		//
		//13-4-1-Payment-Information
	}
function paymentpassword()
{
	
	$checkpassword=array(
									
									'user_password'=>$this->input->post('testpassword')
									
									);
					$result=$this->common_model->common_where('tbl_user',$checkpassword);
					
					if($result->num_rows()>0)
					{
					  $row = $result->row_array();
					  $this->input->set_cookie('paymentpassword', md5($row['user_password']),300);
					  echo "redirect";
					}else
					{
						echo "Invalid Password";
					}
	
}

//////////////////////////////////////////////list-payment-info/////////////////////////////////////
	function payment_info()
	{
		if($this->input->cookie('paymentpassword')=="")
		{
			redirect("accountinfo");
			exit;
		}
		$data['actives']="payment_info";
		$user_id = array(	
							'pay_user_id'=>$this->session->userdata('userid')
						  );
		
		$result = $this->common_model->common_where('tbl_paymentinfo',$user_id,'pay_id','DESC')->result_array();
		$results = array();
		$counter = 0;
		foreach($result as $key => $val){
			$card_number = '';
			$four_number = '';
			$card_number = $this->aes_encryption->decrypt($val['pay_card_num']);
			$four_number = substr($card_number, strlen($card_number)-4,4);
			$results[$counter] = $val;
			$results[$counter]['pay_card_num'] = $four_number;
			$counter++;
		}
		$data['results'] = $results;
		$this->load->view("13-4-1-Payment-Information",$data);
		
	}
	
//////////////////////////////////////del-payment-info//////////////////////////////////////	
	function payment_info_del($Id)
	{
		if($this->input->cookie('paymentpassword')=="")
		{
			redirect("accountinfo");
			exit;
		}
		$this->common_model->commonDelete('tbl_paymentinfo',"pay_id",$Id);
		$this->session->set_flashdata('msg', 'Payment Info deleted successfully');
 		redirect("payment-info");
	}
	
//////////////////Shipping and Billing address///////////////////////////////////
	
	public function billing_shipping_address()
	{
		authFront();
		$data['actives']="biiling";
		$data['address_data'] = $this->user_model->get_user_address_details($this->session->userdata('userid'));
		$default_address_data = $this->user_model->get_default_user_address_details($this->session->userdata('userid'));
		$data['default_address_data'] = $default_address_data[0]; 		
		$this->load->view('13-3-1-Billing-Shipping-Addresses', $data);
	}
	
	
	public function add_address()
	{
		authFront();
		$data['actives']="biiling";
		$data['rstCountry'] = $this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
		$data['rstState'] = $this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();
		
		$this->form_validation->set_rules('firstName','First Name','trim|required|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('lastName','Last Name','trim|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('phone','Phone No','trim|xss_clean|numeric');
		$this->form_validation->set_rules('addType','Address Type','trim|required|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('addLine1','Address Line 1','trim|required|xss_clean');
		$this->form_validation->set_rules('addLine2','Address Line 2','trim|xss_clean');
		$this->form_validation->set_rules('city','City','trim|required|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('state','State','trim|required|xss_clean');
		$this->form_validation->set_rules('zipCode','Zip Code','trim|xss_clean|alpha_numeric');
		$this->form_validation->set_rules('country','Country','trim|required|xss_clean');
		
		if($this->form_validation->run()===FALSE)
		{
			$this->load->view('13-3-2-Add-Billing-Shipping-Address', $data);	
		}
		else
		{
			
			$table_data['add_user_id'] = $this->session->userdata('userid');
			$table_data['add_first_name'] = $this->input->post('firstName');
			$table_data['add_last_name'] = $this->input->post('lastName');
			$table_data['add_phone'] = $this->input->post('phone');
			$table_data['add_address_type'] = $this->input->post('addType');
			$table_data['add_address_line_1'] = $this->input->post('addLine1');
			$table_data['add_address_line_2'] = $this->input->post('addLine2');
			$table_data['add_city'] = $this->input->post('city');
			$table_data['add_state'] = $this->input->post('state');
			$table_data['add_zipcode'] = $this->input->post('zipCode');
			$table_data['add_country'] = $this->input->post('country');
			$table_data['add_datetime'] = date('Y-m-d H:i:s');
			$table_data['add_status'] = TRUE;
			$result = $this->common_model->commonSave('tbl_address',$table_data);
			
			if($result)
			{
				$this->session->set_flashdata('prog_status',"Your Address Added Successfully");
				redirect(base_url('billing-shipping'));
				exit;
			}
			else
			{
				$this->session->set_flashdata('prog_status',"Your Address Add Failed");
				redirect(base_url('add-billing-shipping'));
				exit;
			}
		}
	}
	
//////////////////Edit Shipping and Billing address///////////////////////////////////
	
	public function edit_address($id)
	{
		authFront();
		$data['actives']="biiling";
		if(is_numeric($id)==false)
		{
			$this->session->set_flashdata('prog_status',"Your Address Update Failed");
			redirect(base_url('billing-shipping'));
			exit;
		}
		
		$data['rstCountry'] = $this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
		$data['rstState'] = $this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();
		$data['address_id'] = $id;
		
		$address_data = $this->common_model->common_where('tbl_address', array('add_id' => $id))->result_array();
		$data['address_data'] = $address_data[0];
			
		$this->form_validation->set_rules('firstName','First Name','trim|required|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('lastName','Last Name','trim|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('phone','Phone No','trim|xss_clean|numeric');
		$this->form_validation->set_rules('addType','Address Type','trim|required|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('addLine1','Address Line 1','trim|required|xss_clean');
		$this->form_validation->set_rules('addLine2','Address Line 2','trim|xss_clean');
		$this->form_validation->set_rules('city','City','trim|required|xss_clean|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('state','State','trim|required|xss_clean');
		$this->form_validation->set_rules('zipCode','Zip Code','trim|xss_clean|alpha_numeric');
		$this->form_validation->set_rules('country','Country','trim|required|xss_clean');
		
		if($this->form_validation->run()===FALSE)
		{
			$this->load->view('13-3-2-Edit-Billing-Shipping-Address', $data);	
		}
		else
		{
			
			$table_data['add_user_id'] = $this->session->userdata('userid');
			$table_data['add_first_name'] = $this->input->post('firstName');
			$table_data['add_last_name'] = $this->input->post('lastName');
			$table_data['add_phone'] = $this->input->post('phone');
			$table_data['add_address_type'] = $this->input->post('addType');
			$table_data['add_address_line_1'] = $this->input->post('addLine1');
			$table_data['add_address_line_2'] = $this->input->post('addLine2');
			$table_data['add_city'] = $this->input->post('city');
			$table_data['add_state'] = $this->input->post('state');
			$table_data['add_zipcode'] = $this->input->post('zipCode');
			$table_data['add_country'] = $this->input->post('country');
			
			$result = $this->common_model->commonUpdate('tbl_address', $table_data, 'add_id', $data['address_id']);
			if($result)
			{
				$this->session->set_flashdata('prog_status',"Your Address Updated Successfully");
				redirect(base_url('billing-shipping'));
				exit;
			}
			else
			{
				$this->session->set_flashdata('prog_status',"Your Address Update Failed");
				redirect(base_url('edit-billing-shipping'));
				exit;
			}
		}
	}
	
///////////////////////////////////delete_Shipping address/////////////////////////////////////////////////

	public function delete_address($id)
	{
		authFront();
		if(is_numeric($id)==false)
		{
			$this->session->set_flashdata('prog_status',"Your Address Delete Failed");
			redirect(base_url('billing-shipping'));
			exit;
		}
		
		$result = $this->common_model->commonDelete('tbl_address', 'add_id', $id);
		if($result)
		{
			$this->session->set_flashdata('prog_status',"Your Address Deleted Successfully");
			redirect(base_url('billing-shipping'));
			exit;
		}
		else
		{
			$this->session->set_flashdata('prog_status',"Your Address Delete Failed");
			redirect(base_url('billing-shipping'));
			exit;
		}
	}
	////////////////////////////Address section ends////////////////////////////////////
	////////////////////////////Company info Start/////////////////////////////////////
	public function company_info()
	{
		authFront();
		$data['actives'] = "company-info";		
		$data['rstCountry'] = $this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
		$data['rstState'] = $this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();
		
		$company_data_info = $this->company_model->company_single_data();
		$data['company_info'] = $company_data_info['company_info'];
		$data['com_additional_info'] = $company_data_info['com_additional_info'];
		if(!empty($data['com_additional_info']))
		{
			$data['com_users_info'] = array_chunk($company_data_info['com_users_info'], 3, TRUE);
		}
		
		
		$this->form_validation->set_rules('company','Company','trim|required|xss_clean|max_length[254]|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('companyWeb','Webite','trim|required|xss_clean|max_length[254]|prep_url');
		$this->form_validation->set_rules('cEmail','Email','trim|required|xss_clean|max_length[254]|valid_email');
		$this->form_validation->set_rules('cPhone','Phone','trim|required|xss_clean|max_length[254]|numeric');
		$this->form_validation->set_rules('caddTitle','Address Title','trim|required|xss_clean|max_length[254]|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('caddLine1','Address Line 1','trim|required|xss_clean|max_length[254]');
		$this->form_validation->set_rules('caddLine2','Address Line 2','trim|xss_clean|max_length[254]');
		$this->form_validation->set_rules('cCity','City','trim|required|xss_clean|max_length[254]|callback_validate_alpha_spaces');
		$this->form_validation->set_rules('cState','State','trim|required|xss_clean|max_length[254]');
		$this->form_validation->set_rules('cZipCOde','Zip Code','trim|required|xss_clean|max_length[254]|alpha_numeric');
		$this->form_validation->set_rules('cCountry','Country','trim|required|xss_clean|max_length[254]');
		
		if(isset($_POST['hEmail']) && $_POST['hEmail']!='')
		{
			for($i=0; $i<=count($_POST['hEmail']); $i++)
			{
				$this->form_validation->set_rules("hEmail[]",'Additional Email '.$i,'trim|required|xss_clean|max_length[254]|valid_email');
				$this->form_validation->set_rules('hPhone[]','Additional Phone '.$i,'trim|required|xss_clean|max_length[254]|numeric');
				$this->form_validation->set_rules('haddTitle[]','Additional Address Title '.$i,'trim|required|xss_clean|max_length[254]callback_validate_alpha_spaces');
				$this->form_validation->set_rules('haddLine1[]','Additional Address Line 1 of '.$i,'trim|required|xss_clean|max_length[254]');
				$this->form_validation->set_rules('haddLine2[]','Additional Address Line 2 of '.$i,'trim|xss_clean|max_length[254]');
				$this->form_validation->set_rules('hCity[]','Additional City '.$i,'trim|required|xss_clean|max_length[254]|callback_validate_alpha_spaces');
				$this->form_validation->set_rules('hState[]','Additional State '.$i,'trim|required|xss_clean|max_length[254]');
				$this->form_validation->set_rules('hzipCode[]','Additional Zip Code '.$i,'trim|required|xss_clean|max_length[254]|numeric');
				$this->form_validation->set_rules('hCountry[]','Additional Country '.$i,'trim|required|xss_clean|max_length[254]');
								
			}
		}
		
		
		$this->form_validation->set_rules('cDescrption','Descrption','trim|required|xss_clean|max_length[254]');
		
		$this->form_validation->set_rules('cFacebook','Facebook','trim|xss_clean|max_length[254]|prep_url');
		$this->form_validation->set_rules('cTwitter','Twitter','trim|xss_clean|max_length[254]|prep_url');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('13-6-Company-Info',$data);
		}
		else
		{
			$array_update_info = array();
			$array_update_info['com_user_id'] = $this->session->userdata('userid');
			//$array_update_info['com_date'] = date('Y-m-d H:i:s');
			$array_update_info['com_name'] = $this->input->post('company');
			$array_update_info['com_phone'] = $this->input->post('cPhone');
			$array_update_info['com_email'] = $this->input->post('cEmail');
			$array_update_info['com_website'] = $this->input->post('companyWeb');
			$array_update_info['com_address_title'] = $this->input->post('caddTitle');
			$array_update_info['com_address1'] = $this->input->post('caddLine1');
			$array_update_info['com_address2'] = $this->input->post('caddLine2');
			$array_update_info['com_city'] = $this->input->post('cCity');
			$array_update_info['com_state'] = $this->input->post('cState');
			$array_update_info['com_zip'] = $this->input->post('cZipCOde');
			$array_update_info['com_country'] = $this->input->post('cCountry');
			$array_update_info['com_description'] = $this->input->post('cDescrption');
			$array_update_info['com_facebook'] = $this->input->post('cFacebook');
			$array_update_info['com_twitter'] = $this->input->post('cTwitter');
			
			if(isset($_FILES['userfile']['error']) && $_FILES['userfile']['error']==0)
			{
				$arr_company_logo = $this->company_model->upload_company_logo();
				
				if(isset($arr_company_logo['error']) && $arr_company_logo['error']!="")
				{
					$this->session->set_flashdata('Update_info', $arr_company_logo['error']);
					redirect(base_url('/company-info'));
					exit;
				}
				
				$array_update_info['com_logo'] = $arr_company_logo['upload_data']['file_name'];
			}
			
			$this->db->trans_start(TRUE);
			$strSql="SELECT * FROM tbl_company WHERE com_id= ".$data['company_info']['com_id'];
			$rstRowBefore=$this->db->query($strSql)->result_array();
			$ninserted_rec_id = $this->common_model->commonUpdate('tbl_company', $array_update_info, 'com_id', $data['company_info']['com_id']);
			$nEditId=$this->recordEdits(2,$rstRowBefore,$strSql);
			
			if($ninserted_rec_id)
			{
				if(isset($_POST['hEmail']) && $_POST['hEmail']!='')
				{
					for($i=0; $i<count($_POST['hEmail']); $i++)
					{
						$arr_additional_info = array();
						$arr_additional_info['addc_email'] = $_POST['hEmail'][$i];
						$arr_additional_info['addc_phone'] = $_POST['hPhone'][$i];
						$arr_additional_info['addc_title'] = $_POST['haddTitle'][$i];
						$arr_additional_info['addc_add_line_1'] = $_POST['haddLine1'][$i];
						$arr_additional_info['addc_add_line_2'] = $_POST['haddLine2'][$i];
						$arr_additional_info['addc_city'] = $_POST['hCity'][$i];
						$arr_additional_info['addc_state'] = $_POST['hState'][$i];
						$arr_additional_info['addc_zipcode'] = $_POST['hzipCode'][$i];
						$arr_additional_info['addc_country'] = $_POST['hCountry'][$i];
						$arr_additional_info['addc_com_id'] = $data['company_info']['com_id'];
						
						if($_POST['addc_id'][$i]!='')
						{
							$this->common_model->commonUpdate('tbl_add_company_info', $arr_additional_info, 'addc_id', $_POST['addc_id'][$i]);
						}
						else
						{
							$this->common_model->commonSave('tbl_add_company_info', $arr_additional_info);
						}
					}			
				}
				
				if($nEditId>0)
					$this->session->set_flashdata('Update_info',"Your information has been successfully submitted. A BEYE representative will review and activate the information shortly. But currently it is not happening");
				
				redirect(base_url('/company-info'));
				exit;
			}
			else
			{
				$this->session->set_flashdata('Update_info','Data Inserting Failed');
				redirect(base_url('/company-info'));
				exit;
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	function lead_gerneration($nId)
	{
		authFront();
		$strWere=" AND prod_vendor_user_id ='".$this->session->userdata('userid')."' AND prod_type=1";	
		$data['actives']="lead";				
		$data['resultsProd']=$this->product_model->getproductleadedit($strWere,$perPage="",$page="",1);				
		$this->load->view('13-11-3-Lead-Generation', $data);	

	}
	function lead_edit($nId)
	{	
		authFront();
		$data['actives']="lead";
		$this->form_validation->set_rules('cta','cta','trim|required|xss_clean');
		if($this->form_validation->run()===FALSE)
		{
			$strWere=" AND prod_vendor_user_id ='".$this->session->userdata('userid')."' AND prod_type=1 AND prod_id='".$nId."'";
			$data['resultsProd']=$this->product_model->getproductleadedit($strWere,$perPage="",$page="",1);
			$data['actives']="biiling";
			$this->load->view('13-11-2-Lead-Generation-Edit', $data);	
		}
		else
		{
			$checklead=array('lead_prod_id' =>$nId);
			$result=$this->common_model->common_where('tbl_lead_gerneration',$checklead);
				if($result->num_rows()>0)
				{
					$data_update['lead_prod_id']=$nId;
					$data_update['lead_cta']=$this->input->post('cta');
					$this->common_model->commonUpdate('tbl_lead_gerneration', $data_update, 'lead_prod_id', $nId);
					$this->session->set_flashdata('msg',"Update Lead Generation");
					redirect(base_url('lead'));
					
				}
				else
				{
					$data_save['lead_prod_id']=$nId;
					$data_save['lead_cta']=$this->input->post('cta');
					$this->common_model->commonSave('tbl_lead_gerneration',$data_save);
					$this->session->set_flashdata('msg',"Update Lead Generation");
					redirect(base_url('lead'));
					
					
				}
		}
					
	}
	function lead($page=0)
	{
		authFront();
		$strWere=" AND prod_vendor_user_id ='".$this->session->userdata('userid')."' AND prod_type=1";
		$total_records= count($this->product_model->getproductlink($strWere,"","",0));
		
		$perPage=10; 
		$last = end($this->uri->segments);
		if($last>0)
		$page = $last;
		else
		$page = 0;
		$data['resultsProd']=$this->product_model->getproductlead($strWere,$perPage,$page,1);
		$data['actives']="lead";
		$data['total_records']=$total_records;
		$mypaing['total_rows'] 	=  $total_records;
		$mypaing['base_url'] 	= base_url()."lead/index/";
		$mypaing['per_page']	=$perPage;
		$mypaing['uri_segment']	=3;	
		$mypaing['full_tag_open']   = '<div class="pagination-rp left">';
		$mypaing['full_tag_close']  = '</div>';
		$mypaing['cur_tag_open']    = '<span>';
		$mypaing['cur_tag_close']   = '</span>';
		$this->pagination->initialize($mypaing);
		$data['paginglink']	=$this->pagination->create_links();
		$this->load->view('13-11-1-Lead-Generation', $data);
	}
	
	
	function send_invite()
	{
		authFront();
		$data['actives']="invite";
		$data_email['f_name']=$this->input->post('sndName');
		$data_email['email']=$this->input->post('sndEmail');
		$emails=$this->inviteemail1($data_email,$data);	
		echo "email sent";	
	}
		
////////////////////////send-invite-email///////////////////		
 	function inviteemail1($data)
 	{	
		$data['title'] ='Invite';
		$subject = '';
		$message = '';
		$message.='Dear'.$data['f_name'];
		$message.='<br />You have been invited by'.$this->session->userdata('userfname'). $this->session->userdata('userlname').'to visit BEYE.com. Please register for an account by using the link below.
<br />';
		$message.=base_url().'register <br />';
		$message.='Thank you <br />';
		$message.='Team BEYE';
		//$site_email = 'sspinelli@bmctoday.com';
		//$site_email = 'dotlogics159@gmail.com';
		$this->load->library('email');
		$this->email->set_mailtype('html');
		$this->email->from('Admin@beye.com');
		$this->email->to($data['email']);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
		//echo  $this->email->print_debugger();
		 $this->email->clear(TRUE);
	}
	
	
	function prod_visibility_stats()
	{
		authFront();
		$data['actives']="prod-visibility-stats";
		$data['visibility']=1;		 
		$data['rstStats']=$this->product_model->prodVisibilityStats(" AND stat_page_type IN(1,2,3) AND prod_vendor_user_id = '".$this->session->userdata('userid')."'");
		//echo $this->db->last_query();
		$this->load->view('13-9-1-Product-Metrics-Visibility', $data);
	}
	
	function prod_visibility_stats_detail($nProdId)
	{
		authFront();
		$data['actives']="";
		$data['visibility']=1;	
		$data['monthStat']="";	
		if($this->input->post('monthStat')!="")
		{
			$dateFrom=date("Y-".$this->input->post('monthStat')."-01");	
			$dateTo=date("Y-".$this->input->post('monthStat')."-31"); 
			$data['monthStat']=$this->input->post('monthStat');
		}
		else
		{			
			$dateFrom=date("Y-m-01");	
			$dateTo=date("Y-m-31"); 
		}
		
		$strWhere=" AND stat_page_type IN(1,2,3) AND prod_vendor_user_id = '".$this->session->userdata('userid')."' AND prod_id = '".$nProdId."'  AND stat_datetime BETWEEN '".$dateFrom."'  AND '".$dateTo."' ";
		$data['rstStats']=$this->product_model->prodVisibilityStatsDetail($strWhere);
		 //echo $this->db->last_query();
		$this->load->view('13-9-2-1-product-metrics-visibility-detail', $data);
	}	
	function traffic()
	{
		authFront();
		$data['actives']="";
		$data['traffic']=1;				 
		$data['rstStats']=$this->product_model->prodTrafficStats(" AND prod_vendor_user_id = '".$this->session->userdata('userid')."'");
		//echo $this->db->last_query();
		$this->load->view('Metrics-Traffic', $data);
	}
	
	function traffic_detail($nProdId)
	{
		authFront();
		$data['actives']="";
		$data['traffic']=1;		
		$data['monthStat']="";	
		if($this->input->post('monthStat')!="")
		{
			$dateFrom=date("Y-".$this->input->post('monthStat')."-01");	
			$dateTo=date("Y-".$this->input->post('monthStat')."-31"); 
			$data['monthStat']=$this->input->post('monthStat');
		}
		else
		{			
			$dateFrom=date("Y-m-01");	
			$dateTo=date("Y-m-31"); 
		}
		
		 $strWhere=" AND prod_vendor_user_id = '".$this->session->userdata('userid')."' AND prod_id = '".$nProdId."'  AND stat_datetime BETWEEN '".$dateFrom."'  AND '".$dateTo."' ";
		 $data['rstStats']=$this->product_model->prodTrafficStats($strWhere);
		 //echo $this->db->last_query();
		$this->load->view('metrics-traffic-detail', $data);
	}
	function location()
	{
		authFront();
		$data['actives']="";
		$data['location']=1;				 
		$data['rstStats']=$this->product_model->prodLocationStats(" AND prod_vendor_user_id = '".$this->session->userdata('userid')."'");
		//echo $this->db->last_query();
		$this->load->view('Metrics-Location', $data);
	}
	function location_detail($nProdId)
	{
		authFront();
		$data['actives']="";
		$data['location']=1;		
		$data['monthStat']="";	
		if($this->input->post('monthStat')!="")
		{
			$dateFrom=date("Y-".$this->input->post('monthStat')."-01");	
			$dateTo=date("Y-".$this->input->post('monthStat')."-31"); 
			$data['monthStat']=$this->input->post('monthStat');
		}
		else
		{			
			$dateFrom=date("Y-m-01");	
			$dateTo=date("Y-m-31"); 
		}
		
		 $strWhere=" AND prod_vendor_user_id = '".$this->session->userdata('userid')."' AND prod_id = '".$nProdId."'  AND stat_datetime BETWEEN '".$dateFrom."'  AND '".$dateTo."' ";
		 $data['rstStats']=$this->product_model->prodLocationDetailStats($strWhere);
		 //echo $this->db->last_query();
		$this->load->view('metrics-location-detail', $data);
	}
	
	
	function demographic()
	{
		authFront();
		$data['actives']="";
		$data['demographic']=1;				 
		$data['rstStats']=$this->product_model->prodLocationStats(" AND prod_vendor_user_id = '".$this->session->userdata('userid')."'");
		 //echo $this->db->last_query();
		$this->load->view('metrics-demographic', $data);
	}
	function demographic_detail($nProdId)
	{
		authFront();
		$data['actives']="";
		$data['demographic']=1;		
		$data['monthStat']="";	
		if($this->input->post('monthStat')!="")
		{
			$dateFrom=date("Y-".$this->input->post('monthStat')."-01");	
			$dateTo=date("Y-".$this->input->post('monthStat')."-31"); 
			$data['monthStat']=$this->input->post('monthStat');
		}
		else
		{			
			$dateFrom=date("Y-m-01");	
			$dateTo=date("Y-m-31"); 
		}
		
		 $strWhere=" AND prod_vendor_user_id = '".$this->session->userdata('userid')."' AND prod_id = '".$nProdId."'  AND stat_datetime BETWEEN '".$dateFrom."'  AND '".$dateTo."' ";
		 $data['rstStats']=$this->product_model->prodDemographicDetailStats($strWhere);
		  //echo $this->db->last_query();
		$this->load->view('metrics-demographic-detail', $data);
	}
	
	function popular()
	{
		authFront();
		$data['actives']="";
		$data['popular']=1;				 
		$data['rstStats']=$this->product_model->prodPopularStats(" AND prod_vendor_user_id = '".$this->session->userdata('userid')."'");
		 //echo $this->db->last_query();
		$this->load->view('Metrics-Most-Popular', $data);
	}
	
	function company_products($page=0)
	{
		authFront();
		$data['actives']="company-product";
		$strWere=" AND prod_vendor_user_id ='".$this->session->userdata('userid')."'";
		$total_records= count($this->product_model->getproductlink($strWere,"","",0));
					
		$perPage=10; 
		$last = end($this->uri->segments);
		if($last>0)
		$page = $last;
		else
		$page = 0;
		$data['resultsProd']=$this->product_model->getproductcompany($strWere,$perPage,$page,1);
		
		$data['total_records']=$total_records;
		$mypaing['total_rows'] 	=  $total_records;
		$mypaing['base_url'] 	= base_url()."company-product/index/";
		$mypaing['per_page']	=$perPage;
		$mypaing['uri_segment']	=3;	
		$mypaing['full_tag_open']   = '<div class="pagination-rp left">';
		$mypaing['full_tag_close']  = '</div>';
		$mypaing['cur_tag_open']    = '<span>';
		$mypaing['cur_tag_close']   = '</span>';
		$this->pagination->initialize($mypaing);
		$data['paginglink']	=$this->pagination->create_links();
		
		
		$this->load->view('13-7-1-Company-Products', $data);
	}
	/////////////////////////////////////company add/////////////////////////////////////////////////////////////////
	function company_products_add()
	{
		authFront();
		$data['actives']="company-product";
		$data['cat_name']="";
		$data['cat_cat_id']="";
		$data['cat_id']=0;
		$data['cat_desc']="";
		$data['rstCatSpec']=0;
		$data['title']="Add Product"; 
 		$this->form_validation->set_rules('channel', 'Channel', 'trim|xss_clean|required');
		$this->form_validation->set_rules('category', 'Sategory', 'trim|xss_clean|required');
		$this->form_validation->set_rules('sub_category', 'Sub-category', 'trim|xss_clean|required');
		$this->form_validation->set_rules('name', 'Product name', 'trim|required|xss_clean|max_length[500]');
			
		if ($this->form_validation->run() == FALSE)
		{	 	
 			$data['resultsChannel']=$this->common_model->common_where('tbl_categories',array('cat_type'=>'0','cat_status'=>'1'));
			$company_data_info = $this->company_model->company_single_data();
			$data['company_info'] = $company_data_info['company_info'];
			$this->load->view('13-7-2-Company-Products', $data);	
		}
		else
		{
			$arrImg=array();
			$myFiles=$_FILES['postfile'];	
			for($nLoop=0;$nLoop < $this->input->post('nTotalShow');$nLoop++)
			{
 				$_FILES['postfile']['name']=$myFiles['name'][$nLoop];
				$_FILES['postfile']['type']=$myFiles['type'][$nLoop];
				$_FILES['postfile']['tmp_name']=$myFiles['tmp_name'][$nLoop];
				$_FILES['postfile']['error']=$myFiles['error'][$nLoop];				
				$_FILES['postfile']['size']=$myFiles['size'][$nLoop];
 				
				$this->load->library('upload');
				$file_Post ='';			 
 			// =======================File Display 			
 						 		
				
				if($_FILES['postfile']['name'] != '')
				{ //If user selected the photo to upload .
						
						$uploaddir_11 					= FCPATH."images/prod_img/";
						$uploaddir_11 					= str_replace(" ","",$uploaddir_11);
						$config_11['upload_path'] 		= $uploaddir_11;
						$config_11['allowed_types'] 	= 'gif|jpg|png|jpeg';
						$config_11['overwrite'] 		= false;
						$config_11['remove_spaces'] 	= true;
						$config_11['file_name'] 		= "img_".rand(1,1000).time();
						$config_11['min_height']  		= '100';					
						
						$this->upload->initialize($config_11);
						if ( ! $this->upload->do_upload('postfile'))
						{ //Print message if file is not uploaded
							
							$this->session->set_flashdata('msg_err', $this->upload->display_errors());
							header("Location: ".base_url()."company-product-add");
							exit();
							
						}
						else
						{
							
							$dataDP = $this->upload->data();
							$file_Post	= $dataDP['file_name'];							
							$arrImg[]=$file_Post; 
							for($n=0;$n<=1;$n++)
							{
								if($n==0)
								{
									$width 	= 95;
									$height = 103;	
									$thumb="_thumb";	
								}
								else
								{
									$width 	= 341;
									$height = 394;
									$thumb="_thumb1";	
								}
								
								$this->load->library('image_lib');
								$configt['image_library'] 		= 'gd2';
								$configt['source_image']		= $dataDP['full_path'];
								$configt['new_image']			= $uploaddir_11;
								$configt['create_thumb'] 		= TRUE;
								$configt['thumb_marker'] 		= $thumb;
								$configt['maintain_ratio'] 		= FALSE;
								$configt['width']	 			= $width;
								$configt['height']				= $height;
								$this->image_lib->initialize($configt);
								$this->image_lib->resize();
								if ( ! $this->image_lib->resize())
								{									
									$this->session->set_flashdata('msg_err', $this->image_lib->display_errors());
									$this->image_lib->clear();
									unset($configt);									
									unlink($dataDP['full_path']);
									header("Location: ".base_url()."company-product-add");
									exit();
								}
								else
								{
									@unlink($uploaddir_11.$this->input->post('saved_post'));
									@unlink($uploaddir_11.GetImageThumb($this->input->post('saved_post')));								
								}
							}//end for
						} 
					}		 
			}
			/*** END of image upload **/
			 
			/*** start of document upload **/		
			$arrDoc=array();
			$myFiles=$_FILES['document'];		
			for($nLoop=0;$nLoop < $this->input->post('nTotalShow2');$nLoop++)
			{
				
				$_FILES['postfile']['name']=$myFiles['name'][$nLoop];
				$_FILES['postfile']['type']=$myFiles['type'][$nLoop];
				$_FILES['postfile']['tmp_name']=$myFiles['tmp_name'][$nLoop];
				$_FILES['postfile']['error']=$myFiles['error'][$nLoop];				
				$_FILES['postfile']['size']=$myFiles['size'][$nLoop];
 				
				$this->load->library('upload');
				$file_Post ='';			 
 				
				if($_FILES['postfile']['name'] != '')
				{ //If user selected the photo to upload.
						
						$uploaddir_11 					= FCPATH."images/prod_img/";
						$uploaddir_11 					= str_replace(" ","",$uploaddir_11);
						$config_11['upload_path'] 		= $uploaddir_11;
						$config_11['allowed_types'] 	= 'doc|docx|xls|xlsx|pdf|gif|jpg|png|jpeg';
						$config_11['overwrite'] 		= false;
						$config_11['remove_spaces'] 	= true;
						$config_11['file_name'] 		= "doc_".rand(1,1000).time();
 				
						
						$this->upload->initialize($config_11);
						if ( ! $this->upload->do_upload('postfile'))
						{ //Print message if file is not uploaded
							
							$this->session->set_flashdata('msg', $this->upload->display_errors());
							header("Location: ".base_url()."company-product-add");
							exit();
							
						}
						else
						{
 							$dataDP = $this->upload->data();
							$file_Post	= $dataDP['file_name'];							
							$arrDoc[]=$file_Post; 
 						} 
					}		 
			}
			
			
		for($n=0;$n<count($_POST['doc_text']);$n++)
		{
			$mydoc[]=$_POST['doc_text'][$n];	
         
		}
			/*** END of document upload **/	
			$array['prod_cat_id_channel'] 	= $this->input->post('channel');
			$array['prod_cat_id'] 			= $this->input->post('category');			
			$array['prod_sub_cat_id'] 		= $this->input->post('sub_category');			 
			$array['prod_name'] 			= $this->input->post('name'); 
			$array['prod_url'] 				= clean(strtolower(trim($this->input->post('name'))));
			$array['prod_com_id'] 			= $this->input->post('prodcomname'); 
			$array['prod_img1'] 			= @$arrImg[0]; 
			$array['prod_img2'] 			= @$arrImg[1]; 
			$array['prod_img3'] 			= @$arrImg[2]; 
			$array['prod_img4'] 			= @$arrImg[3]; 
			$array['prod_desc'] 			= $this->input->post('desc');
			$array['prod_ecommerce'] 		= $this->input->post('enableComm');
			/*$array['prod_price'] 			= $this->input->post('hPrice');*/
			$array['prod_manufacturer'] 	= $this->input->post('manufacturer');
			$array['prod_dealer'] 			= $this->input->post('dealer');
			$array['prod_makeoffer'] 		= $this->input->post('makeoffer');
		/*	$array['prod_shipping'] 			= $this->input->post('pshipping');*/
			
			$array['prod_stock'] 			= $this->input->post('stock');
			$array['prod_price_type'] 		= $this->input->post('price_type');
			$array['prod_facebook'] 		= $this->input->post('facebook');
			$array['prod_twitter'] 			= $this->input->post('twitter');
			$array['prod_status'] 			= $this->input->post('activate');   
			$array['prod_type'] 			= $this->input->post('type');
			$array['prod_doc1'] 			= @$arrDoc[0]; 
			$array['prod_doc2'] 			= @$arrDoc[1]; 
			$array['prod_doc3'] 			= @$arrDoc[2]; 
			$array['prod_doc4'] 			= @$arrDoc[3]; 
			$array['prod_doc5'] 			= @$arrDoc[4];
			$array['prod_vendor_user_id'] 			= $this->session->userdata('userid');
			$array['prod_doc_text1']		=$mydoc['0'];
			$array['prod_doc_text2']		=$mydoc['1'];
			$array['prod_doc_text3']		=$mydoc['2'];
			$array['prod_doc_text4']		=$mydoc['3']; 
			$array['prod_doc_text5']		=$mydoc['4'];
			$array['prod_create_date'] 		= date("Y-m-d");
			$array['prod_datetime'] 		= date("Y-m-d");
			
 			$nProdId=$this->common_model->commonSave('tbl_products',$array);
			
			$results=$this->common_model->common_where('tblcategory_spec',array('cat_spec_cat_id'=>$this->input->post('sub_category')), "cat_spec_table_no, cat_spec_id ","ASC");
			foreach($results->result() as $name_result)
			{
				$array=NULL;
				$array['prod_att_value'] 		= @$_POST['id'.$name_result->cat_spec_id];
				$array['prod_att_cat_spec_id'] 	=$name_result->cat_spec_id;
				$array['prod_att_prod_id'] 		=$nProdId;
 				$this->common_model->commonSave('tbl_products_attributs',$array);
			}
			
			$rstFilter =$this->db->query("SELECT * FROM tbl_category_filter WHERE  filt_cat_id = '".$this->input->post('sub_category')."' order by filt_id ASC")->result_array();
			for ($n=0; $n<count($rstFilter);$n++)
			{ 
				$nCounter=0;
				$arr=explode(",",$rstFilter[$n]['filt_criteria']);
				foreach($arr as $key=>$value)
				{  
					$array=NULL;
					$array['filt_prod_value'] 		= @$_POST['name'.$rstFilter[$n]['filt_id'].str_replace(" ","_",clean($value))];
					$array['filt_prod_label'] 		= @$value;
					$array['filt_prod_filt_id'] 	= $rstFilter[$n]['filt_id'];
					$array['filt_prod_prod_id'] 		=$nProdId;
					$this->common_model->commonSave('tbl_products_filters',$array);
					$nCounter++;
				}
			}
			$this->session->set_flashdata('msg',"add product");
			redirect('company-product');
		}
		
		
	}
///////////////////////////////////////////////////////////////Get category////////////////////////////////////////////////////////////
	function getCategory()
	{
		
		$resultsChannel =$this->common_model->common_where('tbl_categories',array('cat_type'=>'1','cat_status'=>'1',"cat_cat_id"=>$this->input				->post('nId')));
		$str="<option value=''>Select Category</option>";
		foreach($resultsChannel->result() as $name_result)
			$str.="<option value=".$name_result->cat_id.">".$name_result->cat_name."</option>";
		 
		echo $str;
	}
///////////////////////////////////////////////////////////////Get sub category////////////////////////////////////////////////////////////	
	function getSubCategory()
	{
		$resultsChannel =$this->common_model->common_where('tbl_categories',array('cat_type'=>'2','cat_status'=>'1',"cat_cat_id"=>$this->input->post('nId')));
		$str="<option value=''>Select Sub-Category</option>";
		foreach($resultsChannel->result() as $name_result)
			$str.="<option value=".$name_result->cat_id.">".$name_result->cat_name."</option>";
		 
		echo $str;
	}
///////////////////////////////////////////////////////////////Get Prod Spec////////////////////////////////////////////////////////////	
	function getProdSpec()
	{
		authFront();
		$this->load->helper('text');
		//$_POST['nId']=48;																	//ORDER BY  cat_spec_id ASC
  		$data['result'] =$this->common_model->common_where('tblcategory_spec',array('cat_spec_cat_id'=>$this->input->post('nId')),"cat_spec_table_no, cat_spec_id ","ASC");
		//echo $this->db->last_query();
		$data['rstFilter'] =$this->db->query("SELECT * FROM tbl_category_filter WHERE  filt_cat_id = '".$this->input->post('nId')."' order by filt_id ASC")->result_array();
		
		$this->load->view('products_spec_front',$data); 
	}
//////////////////////////////////////////Del Company Products	//////////////////////////////////////////
	function company_product_del($nId)
	{
		authFront();
		$data['actives']="company-product";
		$this->common_model->commonDelete('tbl_products',"prod_id", $nId);
		redirect("company-product");
	}
	
//////////////////////////////////////////Edit Company Products	//////////////////////////////////////////	
	function company_product_edit($nId)
	{
		authFront();
		$data="";
		$data['actives']="company-product";
		$data['cat_name']="";
		$data['cat_cat_id']="";
		$data['cat_id']=0;
		$data['cat_desc']="";
		$data['rstCatSpec']=0;
		$data['title']="Edit Product"; 
 		$this->form_validation->set_rules('channel', 'Channel', 'trim|xss_clean|required');
		$this->form_validation->set_rules('category', 'Sategory', 'trim|xss_clean|required');
		$this->form_validation->set_rules('sub_category', 'Sub-category', 'trim|xss_clean|required');
		$this->form_validation->set_rules('pname', 'Product name', 'trim|required|xss_cleanmax_length[500]');
		if ($this->form_validation->run() == FALSE)
		{	 	
 			$data['rstProd']=$this->common_model->common_where('tbl_products',array('prod_id'=>$nId))->row();
			$data['row']=$this->product_model->getProductSpecs($nId);
			$data['rstFilter']=$this->db->query("SELECT * FROM tbl_category_filter WHERE  filt_cat_id = '".$data['rstProd']->prod_sub_cat_id."'")->result_array();
			
			$data['resultsChannel']=$this->common_model->common_where('tbl_categories',array('cat_type'=>'0','cat_status'=>'1'));
			$data['resultsCategory']=$this->common_model->common_where('tbl_categories',array('cat_type'=>'1','cat_status'=>'1','cat_cat_id'=>$data['rstProd']->prod_cat_id_channel));
			$data['resultsSubCategory']=$this->common_model->common_where('tbl_categories',array('cat_type'=>'2','cat_status'=>'1', 'cat_cat_id'=>$data['rstProd']->prod_cat_id));
			$company_data_info = $this->company_model->company_single_data();
			$data['company_info'] = $company_data_info['company_info'];
			$this->load->view('13-7-2-Company-Products-edit',$data); 
			
		}
		else
		{
			$arrImg=array();
			$myFiles=$_FILES['postfile'];		
			for($nLoop=0;$nLoop < $this->input->post('nTotalShow');$nLoop++)
			{
				@$arrImg[$nLoop]=$_POST['savedImg'][$nLoop]; 
 				$_FILES['postfile']['name']=$myFiles['name'][$nLoop];
				$_FILES['postfile']['type']=$myFiles['type'][$nLoop];
				$_FILES['postfile']['tmp_name']=$myFiles['tmp_name'][$nLoop];
				$_FILES['postfile']['error']=$myFiles['error'][$nLoop];				
				$_FILES['postfile']['size']=$myFiles['size'][$nLoop];
 				
				$this->load->library('upload');
				$file_Post ='';			 
 			// =======================File Display 			
 				$width 	= 95;
				$height = 103;			 		
				
				if($_FILES['postfile']['name'] != '')
				{ //If user selected the photo to upload.
						
						$uploaddir_11 					= FCPATH."images/prod_img/";
						chmod($uploaddir_11,0777);
						$uploaddir_11 					= str_replace(" ","",$uploaddir_11);
						$config_11['upload_path'] 		= $uploaddir_11;
						$config_11['allowed_types'] 	= 'gif|jpg|png|jpeg';
						$config_11['overwrite'] 		= false;
						$config_11['remove_spaces'] 	= true;
						$config_11['file_name'] 		= "img_".rand(1,1000).time();
						$config_11['min_height']  		= '100';					
						
						$this->upload->initialize($config_11);
						if ( ! $this->upload->do_upload('postfile'))
						{ //Print message if file is not uploaded  
							
							$this->session->set_flashdata('msg_err', $this->upload->display_errors());
							header("Location: ".base_url()."company-product-edit/".$nId);
							exit();
							
						}
						else
						{
							
							$dataDP = $this->upload->data();
							$file_Post	= $dataDP['file_name'];							
							$arrImg[$nLoop]=$file_Post; 
							for($n=0;$n<=1;$n++)
							{
								if($n==0)
								{
									$width 	= 95;
									$height = 103;	
									$thumb="_thumb";	
								}
								else
								{
									$width 	= 341;
									$height = 394;
									$thumb="_thumb1";	
								}
								
								$this->load->library('image_lib');
								$configt['image_library'] 		= 'gd2';
								$configt['source_image']		= $dataDP['full_path'];
								$configt['new_image']			= $uploaddir_11;
								$configt['create_thumb'] 		= TRUE;
								$configt['thumb_marker'] 		= $thumb;
								$configt['maintain_ratio'] 		= FALSE;
								$configt['width']	 			= $width;
								$configt['height']				= $height;
								$this->image_lib->initialize($configt);
								$this->image_lib->resize();
								if ( ! $this->image_lib->resize())
								{									
									$this->session->set_flashdata('msg_err', $this->image_lib->display_errors());
									$this->image_lib->clear();
									unset($configt);									
									unlink($dataDP['full_path']);
									header("Location: ".base_url()."company-product-edit/".$nId);
									exit();
								}
								else
								{
									@unlink($uploaddir_11.$this->input->post('saved_post'));
									@unlink($uploaddir_11.GetImageThumb($this->input->post('saved_post')));								
								}
							}//end for
						} 
					}		 
			}
			
			//print_r($arrImg);		exit;
			/*** END of image upload **/
			 
			/*** start of document upload **/		
			 
			$arrDoc=array();
			$myFiles=$_FILES['document'];		
			for($nLoop=0;$nLoop < $this->input->post('nTotalShow2');$nLoop++)
			{
				
				$arrDoc[$nLoop]=$_POST['savedDoc'][$nLoop]; 
				
				$_FILES['postfile']['name']=$myFiles['name'][$nLoop];
				$_FILES['postfile']['type']=$myFiles['type'][$nLoop];
				$_FILES['postfile']['tmp_name']=$myFiles['tmp_name'][$nLoop];
				$_FILES['postfile']['error']=$myFiles['error'][$nLoop];				
				$_FILES['postfile']['size']=$myFiles['size'][$nLoop];
 				
				$this->load->library('upload');
				$file_Post ='';			 
 				
				if($_FILES['postfile']['name'] != '')
				{ //If user selected the photo to upload.
						
						$uploaddir_11 					= FCPATH."images/prod_img/";
						$uploaddir_11 					= str_replace(" ","",$uploaddir_11);
						$config_11['upload_path'] 		= $uploaddir_11;
						$config_11['allowed_types'] 	= 'doc|docx|xls|xlsx|pdf|gif|jpg|png|jpeg';
						$config_11['overwrite'] 		= false;
						$config_11['remove_spaces'] 	= true;
						$config_11['file_name'] 		= "doc_".rand(1,1000).time();
 				
						
						$this->upload->initialize($config_11);
						if ( ! $this->upload->do_upload('postfile'))
						{ //Print message if file is not uploaded
							
							$this->session->set_flashdata('msg', $this->upload->display_errors());
							header("Location: ".base_url()."company-product-edit/".$nId);
							exit();
							
						}
						else
						{
 							$dataDP = $this->upload->data();
							$file_Post	= $dataDP['file_name'];							
							$arrDoc[$nLoop]=$file_Post; 
 						} 
					}		 
			}
				for($n=0;$n<count($_POST['doc_text']);$n++)
		{
			$mydoc[]=$_POST['doc_text'][$n];	
         
		}
			/*** END of document upload **/	
			//exit;			 
 			$array['prod_cat_id_channel'] 	= $this->input->post('channel');
			$array['prod_cat_id'] 			= $this->input->post('category');			
			$array['prod_sub_cat_id'] 		= $this->input->post('sub_category');			 
			$array['prod_name'] 			= $this->input->post('pname');
			$array['prod_url'] 				= clean(strtolower(trim($this->input->post('pname')))); 
			$array['prod_com_id'] 			= $this->input->post('prodcomname'); 
			$array['prod_img1'] 			= @$arrImg[0]; 
			$array['prod_img2'] 			= @$arrImg[1]; 
			$array['prod_img3'] 			= @$arrImg[2]; 
			$array['prod_img4'] 			= @$arrImg[3]; 
			$array['prod_desc'] 			= $this->input->post('desc');
			$array['prod_ecommerce'] 		= $this->input->post('enableComm');	
			if($this->input->post('enableComm')==0)
			{
			/*$array['prod_price'] 			= "";*/
			$array['prod_manufacturer'] 	= "";
			$array['prod_dealer'] 			= "";
			$array['prod_makeoffer'] 		= "";
			/*$array['prod_shipping'] 		="";*/
			$array['prod_stock'] 		="";
			}
			else
			{
			/*$array['prod_price'] 			= $this->input->post('hPrice');
			$array['prod_shipping'] 			= $this->input->post('pshipping');*/
			$array['prod_manufacturer'] 	= $this->input->post('manufacturer');
			$array['prod_dealer'] 			= $this->input->post('dealer');
			$array['prod_makeoffer'] 		= $this->input->post('makeoffer');
			$array['prod_stock'] 		= $this->input->post('stock');
			}
			$array['prod_price_type'] 		= $this->input->post('price_type');
			$array['prod_facebook'] 		= $this->input->post('facebook');
			$array['prod_twitter'] 			= $this->input->post('twitter');
			$array['prod_status'] 			= $this->input->post('activate');   
			$array['prod_type'] 			= $this->input->post('type');
			$array['prod_vendor_user_id'] 	  = $this->session->userdata('userid'); 
			$array['prod_doc1'] 			= @$arrDoc[0]; 
			$array['prod_doc2'] 			= @$arrDoc[1]; 
			$array['prod_doc3'] 			= @$arrDoc[2]; 
			$array['prod_doc4'] 			= @$arrDoc[3]; 
			$array['prod_doc5'] 			= @$arrDoc[4]; 
			$array['prod_doc_text1']		=$mydoc['0'];
			$array['prod_doc_text2']		=$mydoc['1'];
			$array['prod_doc_text3']		=$mydoc['2'];
			$array['prod_doc_text4']		=$mydoc['3']; 
			$array['prod_doc_text5']		=$mydoc['4'];
			$array['prod_datetime'] 		= date("Y-m-d");
 			$this->common_model->commonUpdate('tbl_products',$array,"prod_id",$nId);			
			 
			$results=$this->common_model->common_where('tblcategory_spec',array('cat_spec_cat_id'=>$this->input->post('sub_category')),"cat_spec_table_no, cat_spec_id","ASC");//,"cat_spec_table_no, cat_spec_id ","ASC"
			foreach($results->result() as $name_result)
			{
				$array=NULL;
				$array['prod_att_value'] 		= @$_POST['id'.$name_result->cat_spec_id];
				$array['prod_att_cat_spec_id'] 	=$name_result->cat_spec_id;
				$array['prod_att_prod_id'] 		=$nId;
				$recCount=$this->common_model->countRecord("tbl_products_attributs"," WHERE prod_att_cat_spec_id = '".$name_result->cat_spec_id."'  AND prod_att_prod_id = '".$nId."'");
				if($recCount>0)	
 					$this->common_model->commonUpdate2('tbl_products_attributs',$array, "prod_att_cat_spec_id",$name_result->cat_spec_id,"prod_att_prod_id",$nId);
				else
					$this->common_model->commonSave('tbl_products_attributs',$array);
 			}
			
			$rstFilter =$this->db->query("SELECT * FROM tbl_category_filter WHERE  filt_cat_id = '".$this->input->post('sub_category')."'")->result_array();
			for ($n=0; $n<count($rstFilter);$n++)
			{ 
				$nCounter=0;
				$arr=explode(",",$rstFilter[$n]['filt_criteria']);
				foreach($arr as $key=>$value)
				{
					$array=NULL;
					$array['filt_prod_value'] 		= @$_POST['name'.$rstFilter[$n]['filt_id'].str_replace(" ","_",clean($value))];//$_POST['name'.$rstFilter[$n]['filt_id']][$nCounter];
					$array['filt_prod_label'] 		= $value;
					$array['filt_prod_filt_id'] 	= $rstFilter[$n]['filt_id'];
					$array['filt_prod_prod_id'] 		=$nId;
					
					$recCount=$this->common_model->countRecord("tbl_products_filters"," WHERE filt_prod_prod_id = '".$nId."'  AND filt_prod_filt_id = '".$rstFilter[$n]['filt_id']."' AND filt_prod_label = '".$value."'  ");
					if($recCount>0)
						$this->common_model->commonUpdate3('tbl_products_filters',$array,"filt_prod_prod_id",$nId,"filt_prod_filt_id",$rstFilter[$n]['filt_id'],"filt_prod_label",$value);
					else
						$this->common_model->commonSave('tbl_products_filters',$array);
							
					$nCounter++;
				}
			}
			$this->session->set_flashdata('msg',"Your information has been successfully submitted. A BEYE representative will review and activate the information shortly.");
			redirect('company-product');
		}
		
	}
////////////////////////////company prod status//////////////////////////////////
	function changeStatus()
	{
		authFront();
 		$array['prod_status']	 	= $this->input->post('bStatus');
 		$this->common_model->commonUpdate('tbl_products',$array ,"prod_id", $this->input->post('nId'));
		echo "Status changed successfully";	
	}	
//////////////////////////end controller////////////////////////////////////////

////////////////////////////Show cart///////////////////////////////////////////////
	function showcart()
	{	 
		authFront();
		$this->load->library('cart');		
		
		if($this->cart->total_items() == 0) 
		{
			$data['cart_items']=$this->session->set_flashdata('cart_items',"Your Cart is Empty");			
		}
		
		$data['resultsProd']=$this->product_model->getcartproduct($strWere='',$perPage="",$page="",1);		
		$data['title']       ="My Cart";
		$this->load->view('new-cart-14-1',$data);				 	
	}
	
////////////////////////////////////add cart//////////////////////////////
	function addcart()
	{
		authFront();	
			$nQty='';
			$strWere=array('prod_id' => $this->input->post('p_id'));
			$resultsProdss=$this->common_model->common_where('tbl_products',$strWere);
			
					if($resultsProdss->num_rows()>0)
									 {
										 
			
										  $row = $resultsProdss->row_array();
										  if($row['prod_stock'] <  $this->input->post("qty"))
										  {
											  $nQty=$row['prod_stock'];
											   $this->session->set_flashdata('cart_items',"Avaliable stock ".$row['prod_stock']);
										  }else
										  {
											  $nQty=$this->input->post("qty");
											  
										  }
										  
									 }
 		$this->load->library('cart');
		$p_id = $this->input->post('p_id');
		
		$price=$this->input->post('p_price');
		$total_price = $nQty*$this->input->post('p_price');
		$nStatId=$this->statics($this->input->post('p_id'),5);
 		
 		$data = array(
						'id'      => $this->input->post('p_id'),
					    'qty'      =>  $nQty,
					    'price'      =>$price,
					    'name'      => clean3(substr($this->input->post('p_name',TRUE),0,35)),	
					 	 'option'	=>array('img'=>$this->input->post('p_thumb'),
											'shipping'=>$this->input->post('p_shipping'),
											'abondantcart'=>$nStatId
											) 			   							   
					  );
		
 		$this->cart->insert($data);		 
 		redirect("mycart");
	}
	
	public function removecart($id)
	{
		authFront();
		$this->load->library('cart');
		if($this->cart->total_items() == 0) 
		    {
			$this->session->set_flashdata('cart_items',"Your Cart is Empty");
			redirect("mycart");
			exit();
			}
		
		$data = array(
               'rowid' => $id,
               'qty'   => 0
            );
				
			$this->cart->update($data); 
			$this->session->set_flashdata('cart_items',"Update Cart");
			redirect("mycart");
	}	
	
	function updatecart()
	{
		authFront();
		if($this->cart->total_items() == 0) 
		    {
			$this->session->set_flashdata('cart_items',"Your Cart is Empty");
			redirect("mycart");
			
			}
			
		for($n=0;$n<count($_POST['qty']);$n++)
		{
			
			$strWere=array('prod_id' => $_POST['prod_ids'][$n]);
			$resultsProdss=$this->common_model->common_where('tbl_products',$strWere);
			
					if($resultsProdss->num_rows()>0)
									 {
										 
			
										  $row = $resultsProdss->row_array();
										  if($row['prod_stock'] > $_POST['qty'][$n]) 
										  {
											  	$this->session->set_flashdata('cart_item',"Update Cart");
											  
										  } else{
											 			
												$_POST['qty'][$n]=$row['prod_stock'];
											    $this->session->set_flashdata('cart_items',"Avaliable stock ".$_POST['qty'][$n]);
										         }
									 }
			$data = array(
               				'rowid' => $_POST['rowid'][$n],
               				'qty'   => $_POST['qty'][$n]
                	   	);
						
			$this->cart->update($data);
		}
		
	  	redirect("mycart");
	}	
///////////////////////////////////END cart////////////////////////////////	
///////////////////////////SHIPPING CART/////////////////////////////////
		   
 function shippinginfo()
 {
	 authFront();
	 $data['resultsProd']=$this->product_model->getproductcompany($strWere='',$perPage="",$page="",1);
	if($this->cart->total_items() == 0) 
	{
		$this->session->set_flashdata('cart_items',"Your Cart is Empty");
		redirect("mycart");
		exit();
	}	
	
	$data['rstCountry']=$this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
	$data['rstState']=$this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();	
	$data['rstAddress']=$result=$this->common_model->common_where('tbl_address',array('add_user_id'=>$this->session->userdata('userid')));
	$data['results']=$result=$this->common_model->common_where('tbl_user',array('user_id'=>$this->session->userdata('userid')));	
	$this->form_validation->set_rules('ufname', 'First Name' , 'trim|xss_clean|required');		
	$this->form_validation->set_rules('ulname', 'Last Name' , 'trim|xss_clean |required');	
	$this->form_validation->set_rules('usuffix', 'Suffix' , 'trim|xss_clean|required');
	$this->form_validation->set_rules('uphone', 'Phone' , 'trim|xss_clean|required');
	$this->form_validation->set_rules('uaddtype', 'Address Type' , 'trim|xss_clean|required');
	$this->form_validation->set_rules('uaddline1', 'Address1' , 'trim|xss_clean |required');
	$this->form_validation->set_rules('ucity', 'City' , 'trim|xss_clean|required');
	$this->form_validation->set_rules('uzipcode', 'Zipe' , 'trim|xss_clean|required');
	$this->form_validation->set_rules('ustate', 'State' , 'trim|xss_clean|required');
	$this->form_validation->set_rules('ucountry', 'Country' , 'trim|xss_clean|required');
	
	if($this->form_validation->run() == false)
	{ 	
		$this->load->view('new-cartdetail',$data);		
	}else
	{
		$data_save['sfname']=$this->input->post('ufname');
		$data_save['slname']=$this->input->post('ulname');
		$data_save['ssuffix']=$this->input->post('usuffix');
		$data_save['sphone']=$this->input->post('uphone');
		$data_save['saddtype']=$this->input->post('uaddtype');
		$data_save['saddline1']=$this->input->post('uaddline1');
		$data_save['saddline2']=$this->input->post('uaddline2');
		$data_save['scity']=$this->input->post('ucity');
		$data_save['szipcode']=$this->input->post('uzipcode');
		$data_save['sstate']=$this->input->post('ustate');
		$data_save['scountry']=$this->input->post('ucountry');
		if($this->input->post('ud-bill')=="")
		{
			$data_save['bfname']=$this->input->post('ufname');
			$data_save['blname']=$this->input->post('ulname');
			$data_save['bsuffix']=$this->input->post('usuffix');
			$data_save['bphone']=$this->input->post('uphone');
			$data_save['baddtype']=$this->input->post('uaddtype');
			$data_save['baddline1']=$this->input->post('uaddline1');
			$data_save['baddline2']=$this->input->post('uaddline2');
			$data_save['bcity']=$this->input->post('ucity');
			$data_save['bzipcode']=$this->input->post('uzipcode');
			$data_save['bstate']=$this->input->post('ustate');
			$data_save['bcountry']=$this->input->post('ucountry');
		}else
		{
			$data_save['bfname']=$this->input->post('ufname2');
			$data_save['blname']=$this->input->post('ulname2');
			$data_save['bsuffix']=$this->input->post('usuffix2');
			$data_save['bphone']=$this->input->post('uphone2');
			$data_save['baddtype']=$this->input->post('uaddtype2');
			$data_save['baddline1']=$this->input->post('uaddline12');
			$data_save['baddline2']=$this->input->post('uaddline22');
			$data_save['bcity']=$this->input->post('ucity2');
			$data_save['bzipcode']=$this->input->post('uzipcode2');
			$data_save['bstate']=$this->input->post('ustate2');
			$data_save['bcountry']=$this->input->post('ucountry2');
		}
		
		$shippingdata = array(
											'sfname'  =>$data_save['sfname'],
											'slname'=> $data_save['slname'],
											'ssuffix'=> $data_save['ssuffix'],
											'sphone'=>$data_save['sphone'],
											'saddtype'=>$data_save['saddtype'],
											'saddline1'=>$data_save['saddline1'],
											'saddline2'=>$data_save['saddline2'],
											'scity'=>$data_save['scity'],
											'szipcode'=>$data_save['szipcode'],
											'sstate'=>$data_save['sstate'],
											'scountry'=>$data_save['scountry'],
											
											'bfname'  =>$data_save['bfname'],
											'blname'=> $data_save['blname'],
											'bsuffix'=> $data_save['bsuffix'],
											'bphone'=>$data_save['bphone'],
											'baddtype'=>$data_save['baddtype'],
											'baddline1'=>$data_save['baddline1'],
											'baddline2'=>$data_save['baddline2'],
											'bcity'=>$data_save['bcity'],
											'bzipcode'=>$data_save['bzipcode'],
											'bstate'=>$data_save['bstate'],
											'bcountry'=>$data_save['bcountry'],
											
										  );
						$this->session->set_userdata($shippingdata);
						redirect("payment");
		
	}
		
 }
 
 function payment()
{
	authFront();
	$user_id = array(	
						'pay_user_id'=>$this->session->userdata('userid')
					  );
	$result = $this->common_model->common_where('tbl_paymentinfo',$user_id,'pay_id','DESC')->result_array();
	$results = array();
	$counter = 0;
	foreach($result as $key => $val){
		$card_number = '';
		$four_number = '';
		$card_number = $this->aes_encryption->decrypt($val['pay_card_num']);
		$four_number = substr($card_number, strlen($card_number)-4,4);
		$results[$counter] = $val;
		$results[$counter]['pay_card_num'] = $four_number;
		$counter++;
	}
		$data['results'] = $results;
		$this->form_validation->set_rules('uccard', 'Select Card' , 'trim|xss_clean');
		$this->form_validation->set_rules('unamecard', 'Card Name' , 'trim|xss_clean|required');		
		$this->form_validation->set_rules('uctype', 'Card Type' , 'trim|xss_clean|required');
		$this->form_validation->set_rules('ucardnumber', 'Card Number' , 'trim|xss_clean|required');		
		//$this->form_validation->set_rules('ucmonth', 'Card Month' , 'trim|xss_clean|required');
		$this->form_validation->set_rules('ucyear', 'Card Year' , 'trim|xss_clean|required');
		$this->form_validation->set_rules('ucvc', 'Verification Code' , 'trim|xss_clean|required');
			if($this->form_validation->run() == false)
		{ 	
				$this->load->view('payment',$data);	
		}else
		{
			
			$data['cfname']=$this->input->post('unamecard');
			$data['uctype']=$this->input->post('uctype');
			$data['cnumber']=$this->input->post('ucardnumber');
			$data['cmonth']=$this->input->post('ucmonth');
			$data['cyear']=$this->input->post('ucyear');
			$data['cverification']=$this->input->post('ucvc');
			$datacard = array(
										'cardfirst_name'  =>$data['cfname'],
										'card_type'		   =>$data['uctype'],
										'cnumber'=> $data['cnumber'],
										'cmonth'=>$data['cmonth'].$data['cyear'],
										'cverification_value'=>$data['cverification']
							 );
							 
			$this->session->set_userdata($datacard);
			 
			redirect('cardconnect');
		}
	
}


function cardconnect()
{
		authFront();
	//	echo $this->session->userdata('card_type');
		
		require 'CardConnectRestClient.php';
		//exit("after".$this->session->userdata('card_type'));
		 $totaltamount=number_format($this->session->userdata('totalprices'), 2, '.', '');
		
		// Username
		$user = "testing";
		// Password
		$password = "testing123";
		// Site's REST URL
		$url = 'https://fts.prinpay.com:6443/cardconnect/rest';
		$request = array(
		'merchid'   => "496160873888",
		'accttyppe' => $this->session->userdata('card_type'),
		'account'   => $this->session->userdata('cnumber'),
		'expiry'    => $this->session->userdata('cmonth'),
		'cvv2'      => "123",
		'amount'    => $totaltamount,
		'currency'  => "USD",
		//'orderid'   => "796090",
		'name'      => $this->session->userdata('cardfirst_name'),
		//'street'    => "123 Test St",
		//'city'      => "centor port",
		//'region'    => "New York",
		//'country'   => "US",
		//'postal'    => "11721",
		'tokenize'  => "Y",
	);
	
	$response1 = $this->authTransaction($request);
		$retref=$response1['retref'];
		
			$request2= array(
								'merchid' => "496160873888",
								'amount' => $totaltamount,
								'currency' => "USD",
								'retref' => $retref,
								'ponumber' => "796090",
							/*	'taxamnt' => "007",*/
								/*'shipfromzip' => "11721",
								'shiptozip' => "11111",*/
								'shiptocountry' => "US",
								/*'postal' => "11111",*/
								'authcode' => "0001234",
								/*'invoiceid' => "0123456789",
								'orderdate' => "20141105",*/
								'frtamnt' => "0",
								'dutyamnt' => "0",
							);
		$rstResponce=$this->captureTransaction($request2);
		if($rstResponce['resptext']=='Approval' || $response1['resptext']=='Approval')
			{
				redirect('order-confirmation');
			}//$row = $result->row_array();
			else
			{
				$this->session->set_flashdata('msg', $response1['resptext'].$rstResponce['resptext']);
				redirect('payment');
			}
		
	
}
function authTransaction($request) {
	global $url, $user, $password;
	echo "\nAuthorization Request\n";
	$client = new CardConnectRestClient($url='https://fts.prinpay.com:6443/cardconnect/rest', $user='testing', $password='testing123');
	$response = $client->authorizeTransaction($request);
	//print var_dump($response);
	 return $response; 
	 
}
// Capture transaction example
function captureTransaction($request) {
	global $url, $user, $password;
	echo "\nCapture Transaction Request\n";
	$client = new CardConnectRestClient($url, $user, $password);

	$response = $client->captureTransaction($request);
	return($response);
	//print var_dump($response);
}
 function getothershipping()
 {
	$wherstr =array('add_user_id'=>$this->session->userdata('userid'),'add_id'=>$this->input->post('nId'));
	$data['results']=$this->common_model->common_where('tbl_address',$wherstr);
	$data['rstCountry']=$this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
	$data['rstState']=$this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();
	echo $this->load->view('othershippingaddress',$data,true);

 }
  function getotherbilling()
 {
	$wherstr =array('add_user_id'=>$this->session->userdata('userid'),'add_id'=>$this->input->post('nId'));
	$data['results']=$this->common_model->common_where('tbl_address',$wherstr);
	$data['rstCountry']=$this->common_model->commonselect('tblcountries',"coun_id != ","", 'coun_name', $order="ASC")->result_array();
	$data['rstState']=$this->common_model->commonselect('tblstates',"stat_id != ","", 'stat_name', $order="ASC")->result_array();
	echo $this->load->view('otherbillingaddress',$data,true);

 }
 
  function getpayments()
 {
	 $user_id = array(	
							'pay_user_id'=>$this->session->userdata('userid'),
							'pay_id'=>$this->input->post('nId')
						  );
		$result = $this->common_model->common_where('tbl_paymentinfo',$user_id,'pay_id','DESC')->result_array();
		$results = array();
		$counter = 0;
		foreach($result as $key => $val){
			$card_number = '';
			$four_number = '';
			$card_number = $this->aes_encryption->decrypt($val['pay_card_num']);
			$four_number = $card_number;
			$results[$counter] = $val;
			$results[$counter]['pay_card_num'] = $four_number;
			$date_arr=explode("/", $val['pay_exp_date']);
			$month= $date_arr[0];
			$year= $date_arr[1];
			$results[$counter]['pay_card_moth']=$month;
			$results[$counter]['pay_card_yera']=$year;
			$counter++;
		}
		$data['results'] = $results;
		
		
	
	echo $this->load->view('otherpayment',$data,true);


 }
 function  orderconfirmation()
{
		authFront();
		$data['resultsProd']=$this->product_model->getcartproduct($strWere='',$perPage="",$page="",1);
		$data['cardfirst_name']=$this->session->userdata('cardfirst_name');
		$data['card_type']=$this->session->userdata('card_type');
		$data['cmonth']=$this->session->userdata('cmonth');
		$data['cnumber']=$four_number = substr($this->session->userdata('cnumber'), strlen($this->session->userdata('cnumber'))-4,4);
		$data['cverification_value']=$this->session->userdata('cverification_value');
		$data['saddtype']=$this->session->userdata('saddtype');
		$data['saddline1']=$this->session->userdata('saddline1');
		$data['saddline2']=$this->session->userdata('saddline2');
		$data['scity']=$this->session->userdata('scity');
		$data['szipcode']=$this->session->userdata('szipcode');
		$data['sstate']=$this->session->userdata('sstate');
		$data['scountry']=$this->session->userdata('scountry');
		$data['sphone']=$this->session->userdata('sphone');
		$data['bphone']=$this->session->userdata('bphone');
		$data['baddtype']=$this->session->userdata('baddtype');
		$data['baddline1']=$this->session->userdata('baddline1');
		$data['baddline2']=$this->session->userdata('baddline2');
		$data['bcity']=$this->session->userdata('bcity');
		$data['bzipcode']=$this->session->userdata('bzipcode');
		$data['bstate']=$this->session->userdata('bstate');
		$data['bcountry']=$this->session->userdata('bcountry');
		$data['user_id']=$this->session->userdata('userid');
		$data['user_name']=$this->session->userdata('userfname');
		$data['user_suffix']=$this->session->userdata('usersuffix');
		$data['user_email']=$this->session->userdata('useremail');
		$data['user_phone']=$this->session->userdata('userphone');
		if($this->input->post('confrim')==1)
		{
				$user_data['order_user_id']                = $data['user_id'];
				$user_data['order_bill_phone']             = $data['bphone'];
				$user_data['order_bill_add_type']          = $data['baddtype'];
				$user_data['order_bill_add_line1']         = $data['baddline1'];
				$user_data['order_bill_add_line2']         = $data['baddline2'];
				$user_data['order_bill_city']              = $data['bcity'];
				$user_data['order_bill_stat_id']           = $data['bstate'];
				$user_data['order_datetime']           = date('Y-m-d H:i:s');	
				$user_data['order_bill_zip']               = $data['bzipcode'];
				$user_data['order_bill_coun_id']           = $data['bcountry'];
				$user_data['order_ship_phone']             = $data['bphone'];
				$user_data['order_ship_addt_type']         = $data['saddtype'];
				$user_data['order_ship_add_line1']         = $data['saddline1'];
				$user_data['order_ship_add_line2']         = $data['saddline2'];
				$user_data['order_ship_city']              = $data['scity'];
				$user_data['order_ship_stat_id']           = $data['sstate'];
				$user_data['order_ship_zip']               = $data['szipcode'];
				$user_data['order_ship_coun_id']           = $data['scountry'];
				$user_data['order_pay_cc_name']            = $data['cardfirst_name'];
				$user_data['order_pay_cc_type']            = $data['card_type'];
				$user_data['order_pay_cc_num']             = $four_number = substr($data['cnumber'], strlen($data['cnumber'])-4,4);
				$user_data['order_tax']                    = '';
				$user_data['order_subtotal']               = $this->session->userdata('subtotal');
				$user_data['order_shipping_total']         = $this->session->userdata('totalshipping'); 
				$user_data['order_gtotal']                 = $this->session->userdata('totalprices');
				$user_data['order_status']                 = 'pending';
				$nCuId= $this->common_model->commonSave('tbl_order',$user_data);
				
				foreach ($this->cart->contents() as $items):
					$strWere=array('prod_id' => $items['id']);
					$resultsProdss=$this->common_model->common_where('tbl_products',$strWere);
					
					if($resultsProdss->num_rows()>0)
									 {
										  
										  $row = $resultsProdss->row_array();
										  $data_email['prod_name']= $row['prod_name'];
										  $data_save['prod_stock']=$row['prod_stock']-$items['qty'];
										  if($data_save['prod_stock']<=0)
									 		{
												$strWere=array('user_id' => $row['prod_vendor_user_id']);
												$resultsusers=$this->common_model->common_where('tbl_user',$strWere);
												if($resultsusers->num_rows()>0)
									 			{
										  			 $row = $resultsusers->row_array();
										  			 $data_email['user_fname']=$row['user_fname']; 
													 $data_email['user_lname']=$row['user_lname']; 
													 $data_email['user_email']=	$row['user_email']; 
													 $data_email['prod_name'];
													 $email=$this->stockemail1($data_email);
												}
											}
										 $this->common_model->commonUpdate('tbl_products',$data_save ,"prod_id",$items['id']);
									 }
					$this->common_model->commonDelete('tbl_stats',"stat_id",$items['option']['abondantcart']);
					$order_data['orderd_order_id']      = $nCuId;		
					$order_data['orderd_prod_id']        =  $items['id'];				
					$order_data['orderd_price']           =  $items['price'];			
					$order_data['orderd_shiping_price']   =  $items['option']['shipping'];				
					$order_data['orderd_qty']             =  $items['qty'];
					$this->common_model->commonSave('tbl_orderdetail',$order_data);
				endforeach;
				$datacard = array(
											'cardfirst_name'  =>"",
											'card_type'		   =>"",
											'cnumber'=> "",
											'cmonth'=>"",
											'cverification_value'=>""
										  );
				$shippingdata = array(
											'sfname'  =>"",
											'slname'=> "",
											'ssuffix'=> "",
											'sphone'=>"",
											'saddtype'=>"",
											'saddline1'=>"",
											'saddline2'=>"",
											'scity'=>"",
											'szipcode'=>"",
											'sstate'=>"",
											'scountry'=>"",
											
											'bfname'  =>"",
											'blname'=> "",
											'bsuffix'=> "",
											'bphone'=>"",
											'baddtype'=>"",
											'baddline1'=>"",
											'baddline2'=>"",
											'bcity'=>"",
											'bzipcode'=>"",
											'bstate'=>"",
											'bcountry'=>""
											
				 );
				 $totalamount=array('totalprices'=>"",
									'totalshipping'=>"",
									'subtotal'=="");
				$this->session->unset_userdata($datacard);
				$this->session->unset_userdata($shippingdata);
				$this->session->set_userdata($totalamount);
				$this->cart->destroy();
				redirect('order-thanks');
		}
		else
		{	
			$data['resultsProd']=$this->product_model->getcartproduct($strWere='',$perPage="",$page="",1);
			$this->load->view('orderconfirmation',$data);
		}
}
function order_thanks()
{
	authFront();
	$data['']='';
	$this->load->view('order_thanks',$data);
	
}

 ////////////////////////////////////Stock email//////////////
 	function stockemail1($data)
 	{	
		$data['title'] ='BEYE';
		$subject = 'Stock Email!';
		
		
		$site_email = 'dotlogics159@gmail.com';
		$this->load->library('email');
		$this->email->set_mailtype('html');
		$this->email->from('Admin@beye.com');
		$this->email->to($data['user_email']);
		$message='';
		$message.='Dear'.$data['user_fname'] . $data['user_lname'] ;
		$message.='Your Product Stock is Available is Zero';
		$this->email->subject($subject);
		
		$this->email->message($message);
		$this->email->send();
		//echo  $this->email->print_debugger();
		 $this->email->clear(TRUE);
	}
//////////////////////////////////////////////////////////////////////
///////////////////////////end cart/////////////////////////////////////////////////

	public function premium_product_request($nId)
	{
		
		authFront();
		if(!is_numeric($nId))
		{
			$this->session->set_flashdata('msg','Your request Failed');
			redirect('/company-product');
			exit;
		}
		
		$update_values = array(
								'prod_pre_resquest_status' => 1,
								'prod_pre_request_datetime' => date('Y-m-d H:i:s')
							);
		$result = $this->common_model->commonselect2('tbl_products', 'prod_id',$nId,'prod_type',0);
		
		if($result->num_rows()>0)
		{
			$this->common_model->commonUpdate2('tbl_products',$update_values,'prod_id',$nId,'prod_type !=', 1);
			$this->session->set_flashdata('msg',"Thank you. We've received your request to upgrade to a premium product listing.
												Team BEYE will reach out to you within 3 business days.
												If you have any questions please email ContactUs@Beye.com.");
		}
		else
		{
			$this->session->set_flashdata('msg','Your Product is Already Listed Premium');
		}
		
		redirect('/company-product');
		exit;
	}
	
	
	
	
	public function abandoned_cart()
	{
		authFront();
		$data['actives'] = "abandoned_cart";
		$data['abandoned'] = 1;
		$data['title'] = "Abandoned Cart";
	
		$strWhere = "";
		$data['rstStats'] = $this->product_model->get_abandoned_cart_list(" AND prod_vendor_user_id = '".$this->session->userdata('userid')."'");
		
		$this->load->view('abandoned-cart-list', $data);
	}
	
	public function adandoned_cart_detail($nProdId)
	{
		authFront();
		$data['actives'] = "abandoned_cart";
		$data['abandoned'] = 1;
		$data['title'] = "Abandoned Cart";
		$data['nProdId'] = $nProdId;
		
		$data['monthStat']="";	
		if($this->input->post('monthStat')!="")
		{
			$dateFrom = date("Y-".$this->input->post('monthStat')."-01");	
			$dateTo = date("Y-".$this->input->post('monthStat')."-31"); 
			$data['monthStat'] = $this->input->post('monthStat');
		}
		else
		{			
			$dateFrom = date("Y-m-01");
			$dateTo = date("Y-m-31");
		}
		
		$strWhere=" AND prod_vendor_user_id = '".$this->session->userdata('userid')."' AND prod_id = '".$nProdId."'  AND stat_datetime BETWEEN '".$dateFrom."'  AND '".$dateTo."' ";
 		$data['rstStats'] = $this->product_model->get_abandoned_cart_list($strWhere);
		
		
		$this->load->view('abandon-cart-detail', $data);
	}
	
	public function order_history()
	{
		authFront();
		$data['title'] = "Order History";
		$data['actives'] = "order-history";
		
		$data['str_all_order_rec'] = $this->common_model->common_where('tbl_order',array('order_user_id' => $this->session->userdata('userid')),"order_id","DESC")->result_array();
		
		$this->load->view("13-5-1-Order-History",$data);
	}
	
	public function order_history_details($nOrdId)
	{
		authFront();
		$data['title'] = "Order History";
		$data['actives'] = "order-history";
		$data['nOrdId'] = $nOrdId;
		$this->load->model('order/order_model');
		$data['str_user_rec'] = $this->order_model->get_order_data($nOrdId, $this->session->userdata('userid'));
		$data['order_details'] = $this->order_model->get_order_details($nOrdId, $this->session->userdata('userid'));
		
		$this->load->view("13-5-2-Order-History-Details",$data);
		
	}
	
	
	public function ecommerce_report()
	{
		authFront();
		$data['title'] = 'E Commerce Report';
		$data['actives'] = 'ecommerce-report';
		
		$data['str_ecommerce_report'] = $this->product_model->get_ecommerce_report($this->session->userdata('userid'));
		
		$this->load->view('13-8-1-1-eCommerce-Report-Order-By-Date', $data);
	}
	
	public function ecommerce_report_detail($nOrdId)
	{
		authFront();
		$data['title'] = 'E Commerce Report';
		$data['actives'] = 'ecommerce-report';
		$data['nOrdId'] = $nOrdId;
		$this->form_validation->set_rules('ucarrier', 'Carrier Type', 'trim|xss_clean');	
		if ($this->form_validation->run() == FALSE)
		{
			$data['str_user_rec'] = $this->product_model->get_vendor_order_data($nOrdId);
			$data['order_details'] = $this->product_model->get_vendor_order_details($nOrdId, $this->session->userdata('userid'));
			//echo $this->db->last_query();
			$this->load->view("13-8-1-2-eCommerce-Report-Order-By-Date", $data);
		}
		else
		{
			
 			$array="";
			$array['orderd_shipping_type']	 	= $this->input->post('ucarrier');
			$array['orderd_shipping_number']	 	= $this->input->post('utrackno');
			$array['orderd_shipping_comments']	 	= $this->input->post('uscomment');
			
 			$this->common_model->commonUpdate('tbl_orderdetail',$array ,"orderd_id", $this->input->post('nId'));
			redirect("ecommerce-details/".$nOrdId);
		}	
		
		
		
	}
	
	public function ecommerce_report_products()
	{
		authFront();
		$data['title'] = 'ECommerce Report';
		$data['actives'] = 'ecommerce-report';
		
		$data['str_ecommerce_report'] = $this->product_model-> get_ecreport_by_product($this->session->userdata('userid'));
		
		$this->load->view('13-8-2-1-eCommerce-Report-Products-Sold', $data);
	}
	
	public function ecommerce_products_details($nProId)
	{
		authFront();
		$data['title'] = 'ECommerce Report';
		$data['actives'] = 'ecommerce-report';
		$data['nProId'] = $nProId;
		
		$data['str_ecommerce_report'] = $this->product_model-> get_product_purchaser_details($this->session->userdata('userid'), $nProId);
		
		$this->load->view('13-8-2-2-3eCommerce-Report-Products-Sold-View', $data);
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */