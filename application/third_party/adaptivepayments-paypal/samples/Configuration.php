<?php 
class Configuration
{
	// For a full list of configuration parameters refer in wiki page (https://github.com/paypal/sdk-core-php/wiki/Configuring-the-SDK)
	public static function getConfig()
	{
		$config = array(
				// values: 'sandbox' for testing
				//		   'live' for production
				"mode" => 'sandbox'
				//"mode" => 'live'
				// These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
				// "http.ConnectionTimeOut" => "5000",
				// "http.Retry" => "2",
			);
		return $config;
	}

	// Creates a configuration array containing credentials and other required configuration parameters.
	public static function getAcctAndConfig()
	{
		$config = array(
				// Signature Credential
				/*
				// Developer Credentials
				"acct1.UserName" => "dotlogics5-facilitator_api1.gmail.com",
				"acct1.Password" => "YY9BZ4H5LBG6FP6B",
				"acct1.Signature" => "AnLIMXehgn2YAYJGxpsSrAW62DuPAX9DZT6rlxKL3ZTyCWMOACQXU7ik",
				"acct1.AppId" => "APP-80W284485P519543T"
				*/
				"acct1.UserName" => "eDucki.Help-facilitator_api1.gmail.com",
				"acct1.Password" => "EJVNKNCU22NMHS37",
				"acct1.Signature" => "ADwX3D7wkVQzqaWbYh0USKdsOs5IAiUJMeWnaQgr8MJka.9ehgXlQ2fA",
				"acct1.AppId" => "APP-80W284485P519543T"

				);
		return array_merge($config, self::getConfig());;
	}
}