<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
- Helper created by Haseeb
- Helper for Category Management in admin side.
- Project: eDucki
*/

function getMainCategory(){
	$CI = &get_instance();
	$mainCat = $CI->db->query("
							SELECT *
							FROM tbl_categories
							WHERE cat_parent_id = 0
							AND cat_is_delete = 0
							ORDER BY cat_sort ASC
							");
	if($mainCat->num_rows() > 0){
		return $mainCat->result();
	} else{
		return NULL;
	}
}

function getSubCategory($cat_id = ''){
	$CI = &get_instance();
	$subCat = $CI->db->query("
							SELECT *
							FROM tbl_categories
							WHERE cat_parent_id = ".$cat_id."
							AND cat_is_delete = 0
							ORDER BY cat_sort ASC
							");
	if($subCat->num_rows() > 0){
		return $subCat->result_array();
	} else{
		return NULL;
	}
}

function getMainCategories(){
	$CI = &get_instance();
	$subCat = $CI->db->query("
							SELECT *
							FROM tbl_categories
							WHERE cat_level = 1
							AND cat_is_delete = 0
							ORDER BY cat_sort ASC
							");
	if($subCat->num_rows() > 0){
		return $subCat->result_array();
	} else{
		return NULL;
	}
}

function getSubCategories($cat_id = ''){
	$CI = &get_instance();
	$subCat = $CI->db->query("
							SELECT *
							FROM tbl_categories
							WHERE cat_parent_id = ".$cat_id."
							AND cat_level = 2
							AND cat_is_delete = 0
							ORDER BY cat_sort ASC
							");
	if($subCat->num_rows() > 0){
		return $subCat->result_array();
	} else{
		return NULL;
	}
}

function getSubSubCategories($sub_cat_id = ''){
	$CI = &get_instance();
	$subSubCat = $CI->db->query("
							SELECT *
							FROM tbl_categories
							WHERE cat_parent_id = ".$sub_cat_id."
							AND cat_level = 3
							AND cat_is_delete = 0
							ORDER BY cat_sort ASC
							");
	if($subSubCat->num_rows() > 0){
		return $subSubCat->result_array();
	} else{
		return NULL;
	}
}

function getproductcount($prod_id){
    $CI = &get_instance();
    	$subSubCat = $CI->db->query("SELECT 
                                    COUNT(prod.prod_id) AS prod_count
                                    FROM
                                    tbl_products prod
                                     INNER JOIN tbl_product_category prodCat 
                                    ON prod.prod_id = prodCat.prod_id 
                                    INNER JOIN tbl_categories category 
                                    ON prodCat.category_id = category.cat_id 
                                    LEFT JOIN tbl_categories subCat 
                                    ON prodCat.sub_category_id = subCat.cat_id 
                                    LEFT JOIN tbl_categories subSubCat 
                                    ON prodCat.sub_sub_category_id = subSubCat.cat_id 
                                    INNER JOIN tbl_user users 
                                    ON users.user_id = prod.prod_user_id 
                                    INNER JOIN tbl_stores store 
                                    ON store.user_id = users.user_id 
                                    WHERE prod.prod_cat_id =".$prod_id."
                                    AND prod.prod_status = 1
                                    AND prod.qty > 0
                                    AND store.store_is_hide = 0
                                    AND prod.prod_is_delete = 0 AND user_is_delete = 0 
                                    AND user_status = 1");
	if($subSubCat->num_rows() > 0){
		return $subSubCat->row();
	} else{
		return NULL;
	}
}

function get_main_count($id='')
{
	$sum = 0;
	$sum += getproductcount($id)->prod_count;
	$sub_cat = getSubCategories($id);
	if(count($sub_cat)>0){
		foreach ($sub_cat as $key => $value) {
			$sum += getproductcount($value['cat_id'])->prod_count;
			$sub_sub_cat = getSubSubCategories($value['cat_id']);
			if(count($sub_sub_cat)>0){
				foreach ($sub_sub_cat as $key => $cat_id) {
					$sum += getproductcount($cat_id['cat_id'])->prod_count;
				}
			}			
		}
	}	

	return $sum;
}

function get_sub_count($id='')
{
	$sum = 0;
	$sum += getproductcount($id)->prod_count;
	$sub_sub_cat = getSubSubCategories($id);
	if(count($sub_sub_cat)>0){
		foreach ($sub_sub_cat as $key => $cat_id) {
			$sum += getproductcount($cat_id['cat_id'])->prod_count;
		}
	}	

	return $sum;
}

function getFirstMainCategory(){
	$CI = &get_instance();
	$mainCat = $CI->db->query("
							SELECT *
							FROM tbl_categories
							WHERE cat_parent_id = 0
							AND cat_is_delete = 0
							ORDER BY cat_sort ASC
                                                        LIMIT 1
							");
	if($mainCat->num_rows() > 0){
		return $mainCat->row();
	} else{
		return NULL;
	}
}

function array_stripslash($theArray){
   foreach ( $theArray as &$v ) if ( is_array($v) ) $v = array_stripslash($v); else $v = stripslashes($v);
   return $theArray;
}

?>