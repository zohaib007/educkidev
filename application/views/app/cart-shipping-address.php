<?php echo form_open_multipart('', array('name' => 'shipping_address', 'id' => 'add_shipping_form')); ?>
<div class="wrapper">
    <div class="crt-wrap">
        <div class="cart-pro-con">
            <div class="cart-left-mian-hed">
                <h2>Shopping Cart</h2>
            </div>
            <div class="crt-shp-addr-main">
                <div class="crt-shp-addr-hed">
                    <h3>Shipping Address</h3>
                </div>
                <div class="crt-shp-addr-frm">
                    <div class="crt-shp-addr-frm-rw">
                        <select name="select_address" id="select_address">
                            <option value="">Select</option>
                            <?php foreach ($address_list as $list) { ?>
                                <option value="<?= $list->add_id ?>"><?=($default_address_list->nick_name == $list->nick_name) ? "Default Shipping Address" : $list->nick_name?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="crt-shp-addr-frm-rw">
                        <input type="text"  name="nick_name" id="nick_name" value="<?=@$current->shipping_nick_name?>" placeholder="Nickname *" />
                        
                    </div>

                    <div class="crt-shp-addr-frm-rw">
                        <input type="text" name="first_name" id="first_name" value="<?=@$current->shipping_first_name?>" placeholder="First Name *" />
                        
                    </div>

                    <div class="crt-shp-addr-frm-rw">
                        <input type="text" name="last_name" id="last_name" value="<?=@$current->shipping_last_name?>" placeholder="Last Name *" />
                        
                    </div>

                    <div class="crt-shp-addr-frm-rw">
                        <div class="crt-shp-addr-frm-inr">
                            <select  name="country" id="country">
                                <option value="" >Country *</option>
                                <?php foreach ($allcountries as $country) { ?>
                                <option value="<?= $country['iso'] ?>"<?php echo (@$current->shipping_country =='' && $country['iso']=='US') ? 'selected' : ""; ?> <?php echo (@$current->shipping_country == $country['iso']) ? 'selected' : ""; ?>><?= $country['name'] ?></option>
                                            <?php } ?>
                            </select>
                        </div>
                    </div>



                    <div class="crt-shp-addr-frm-rw usState">
                        <div class="crt-shp-addr-frm-inr">
                            <select  name="state" id="state" >
                                <option value="">State *</option>
                                <?php foreach ($allstate as $state) { ?>
                                <option value="<?= $state['stat_id'] ?>" <?php echo (@$current->shipping_state == $state['stat_id']) ? 'selected' : ""; ?>><?= $state['stat_name'] ?></option>
                                                
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="crt-shp-addr-frm-rw otherState" >
                        <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                            <input type="text" id="other_state" name="other_state" value="<?=@$current->shipping_state_other?>" placeholder="State *" />
                        </div>
                    </div>

                    <div class="crt-shp-addr-frm-rw">
                        <div class="crt-shp-addr-frm-inr">
                            <input type="text" name="street" id="street" value="<?=@$current->shipping_street?>" placeholder="Street *" />
                        </div>
                    </div>
                    <div class="crt-shp-addr-frm-rw">
                        <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                            <input type="text" name="city" id="city" value="<?=@$current->shipping_city?>" placeholder="City *" />
                        </div>
                    </div>
                    <div class="crt-shp-addr-frm-rw">
                        <div class="crt-shp-addr-frm-inr">
                            <input type="text" name="zip_code" id="zip_code" value="<?=@$current->shipping_zip?>" placeholder="Zip *" maxlength="11" />
                        </div>
                    </div>
                    <div class="crt-shp-addr-frm-rw">
                        <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                            <input type="text" name="phone" id="phone" value="<?=@$current->shipping_phone?>" placeholder="Telephone" />
                        </div>
                    </div>
                    <div class="crt-shp-addr-frm-rw">
                        <div class="signed-in-checkbox">
                            <label>
                                <input type="checkbox" id="save_address" name="save_address" class="mycheckbox">
                                <span></span> Save Address
                            </label>
                        </div>
                    </div>
                    <div class="sign-in-btns">
                        <input type="hidden" id="user_id" name="user_id" value="<?= $userid ?>">
                        <input type="hidden" id="default_shipping" name="default_shipping" value="Y">
                        <!-- <input class="submit" type="submit" value="Next"> -->
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="crt-order-detail">
    <div class="wrapper">
        <div class="cart-right-main">
            <div class="boxgrey-rp"></div>
            <div class="cart-right-hed">
                <h6>Order Details</h6>
            </div>
            <div class="cart-right-lowr">
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Subtotal
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($cart_sub_total, 2, '.', '') ?>
                    </div>
                </div>
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Discount
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($discount_amount, 2, '.', '') ?>
                    </div>
                </div>
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Shipping Total
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                    </div>
                </div>
            </div>
            <div class="cart-right-rw">
                <div class="cart-right-rw-lft">
                    <h6>Total</h6>
                </div>
                <div class="cart-right-rw-rgt">
                    <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                </div>
            </div>
            <div class="cart-right-lowr2 center-block">
                <input class="prcd-btn submit" type="submit" value="Next">
            </div>
            <!--<div class="inpt-tp-no">
                        <input type="number" value="1">
                        <a href="#">Remove</a>
                    </div>-->
        </div>
    </div>
</div>

<?php echo form_close(); ?>

