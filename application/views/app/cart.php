
<?php if (count($cart_product_details) > 0) { ?>
<div class="wrapper">
    <div class="crt-wrap">

        <?php
                                        
        $send = 1;
        $i = 1;
        foreach ($cart_product_details as $product)
        {
            $product_details = getproduct_details($product->cart_prod_id);
            if ($product_details != '') {
                // var_dump($product_details);die;
                if ($product_details->subSubCatURL != '') {
                    $category_level = 3;
                } elseif ($product_details->subCatURL != '') {
                    $category_level = 2;
                } elseif ($product_details->catURL != '') {
                    $category_level = 1;
                }
        ?>
        <div class="cart-pro-con <?php if(($i%2) == 0){ echo 'seprate';} ?>" id="row_<?= $product->cart_detail_id ?>">
            <?php if($i == 1) { ?>
            <div class="cart-left-mian-hed">
                <h2>Shopping Cart</h2>
            </div>
            <?php } ?>
            <div class="cart-pro-cvr">
                <div class="cart-left">
                    <img src="<?= base_url() ?>resources/prod_images/<?= $product_details->img_name ?>" alt="image" />
                </div>
                <div class="cart-right">
                    <div class="crt-lft">
                        <h3><?= $product_details->prod_title ?></h3>
                        <div class="stor-nm">
                            <h4>Store Name:</h4>
                            <p><a href="javascript:void" class="store_click" data-id="<?=$product_details->store_id?>" style="text-decoration: none;"><?=$product_details->store_name ?></a></p>
                        </div>
                    </div>
                    <div class="crt-rgt">
                        <div class="cart-price">
                            <h3>Price:<span id="prod_price_<?= $product_details->prod_id ?>">$<?= number_format($product->unit_prod_price, 2, '.', '') ?></span></h3>
                        </div>
                        <div class="inpt-tp-no">
                            <?php
                            if(getCartTotalQty($product_details->prod_id, $product->cart_id) > $product_details->qty){
                                $send = 0;
                            }
                            ?>
                            <input type="number" min=1 class="qtyChange qty<?=$product->cart_detail_id?> qqty<?=$product_details->prod_id?>" value="<?=$product->cart_prod_qnty?>"
                            data-cdid="<?=$product->cart_detail_id ?>" data-prod=<?=$product_details->prod_id?>
                            data-dbqty="<?=$product_details->qty?>" data-old = '<?=$product->cart_prod_qnty?>'
                            data-remqty = "<?=$product_details->qty-getCartTotalQty($product_details->prod_id, $product->cart_id)?>" 
                            data-cartqty="<?=getCartTotalQty($product_details->prod_id, $product->cart_id)?>" />
                            <a class='removeItem' data-prodId="<?=$product_details->prod_id ?>" data-detail_id="<?=$product->cart_detail_id ?>" href="javascript:void(0);">Remove</a>
                        </div>
                        <div class="cart-total"></div>
                        <input type="hidden" id="number_<?= $product_details->prod_id ?>" value="0">
                        <input type="hidden" class="prod_db_qty_<?= $product_details->prod_id ?>" id="prod_db_qty_<?= $product_details->prod_id ?>" value="<?=$product_details->qty?>"/> <!-- <?=$product_details->qty?> -->
                        <input type="hidden" class="prod_price_<?= $product_details->prod_id ?>" value="<?= $product->unit_prod_price ?>"/>
                        <input type="hidden" class="total_price_<?= $product_details->prod_id ?>" value="<?= $product->total_prod_price ?>"/>
                    </div>
                </div>
            </div>
            <div class="cart-pro-btm">
                <?php
                $filters = json_decode($product->cart_details);
                if ($filters) {
                    foreach ($filters as $filter) {
                ?>
                        <h2><span><?=$filter->filterName ?></span>: <?= $filter->filtervalue ?></h2><br/>
                <?php
                    } 
                }
                ?>
                <h2><span>Available Shipping Type:</span> <?= $product_details->shipping?><?=(!empty($product_details->prod_shipping_methods))?' ('.$product_details->prod_shipping_methods.')':''?></h2>
                <h3>Total: <span id="total_price_<?= $product_details->prod_id ?>">$<?= number_format($product->total_prod_price, 2, '.', '') ?></span></h3>
                <div class="cart-des">
                    <?php if ($product_details->shipping == 'Offer free Shipping') { ?>
                        <h2>Ready to ship in <?= $product_details->free_ship_days ?> days.</h2>
                    <?php } elseif ($product_details->shipping == 'Charge for Shipping') { ?>
                        <h2>Ready to ship in <?= $product_details->ship_days ?> days.</h2>
                        <input type="hidden" class="shipping_cost_<?= $product_details->prod_id ?>" value="<?= $product_details->ship_price ?>">
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php
            }
            $i++; 
        }
        ?>

    </div>
</div>
<div class="crt-order-detail">
    <div class="wrapper">
        <div class="cart-right-main">
            <div class="boxgrey-rp"></div>
            <div class="cart-right-hed">
                <h6>Order Details</h6>
            </div>
            <div class="cart-right-lowr">
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Subtotal
                    </div>
                    <div class="cart-right-rw-rgt"  id="cart_sub_total">
                        $<?= number_format($cart_sub_total, 2, '.', '') ?>
                    </div>
                </div>
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Discount
                    </div>
                    <div class="cart-right-rw-rgt" id="discount_amount">
                        $<?= number_format($discount_amount, 2, '.', '') ?>
                    </div>
                </div>
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Shipping Total
                    </div>
                    <div class="cart-right-rw-rgt" id="shipment_sub_total">
                        $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                    </div>
                </div>
            </div>
            <div class="cart-right-rw">
                <div class="cart-right-rw-lft">
                    <h6>Total</h6>
                </div>
                <div class="cart-right-rw-rgt" id="total_shipment_amount">
                    <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                </div>
            </div>
            <div class="cart-right-lowr2 center-block">
                <?php if(!is_numeric($userId)){ ?>
                <a href="javascript:void" class="prcd-btn checkout cart-login">Proceed to Checkout</a>
                <?php } else { ?>
                <a href="cart-shipping-address.html" class="prcd-btn checkout">Proceed to Checkout</a>
                <?php } ?>
                <a href="main-cat-list.html" class="conti-btn">Continue Shopping</a>
            </div>
        </div>
    </div>
</div>
<?php } else { ?>
    <div class="wrapper">
        <div class="crt-wrap">
            <div class="no-item-found">You have no items in your shopping cart.</div>
        </div>
    </div>
<?php } ?>