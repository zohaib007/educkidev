<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Add News</title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
    
    <!-- tinymce configration start here.-->
    <script>
    var base_url='<?php echo base_url();?>';
    var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>dev/resources/tiny_upload';
    </script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
    <!-- tinymce configration end here.-->
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.my-form .add-box').click(function() {
            var n = $('.text-box').length + 1;
            if (5 < n) {
                alert('Stop it!');
                return false;
            }
            var box_html = $('<p class="text-box"><label for="box' + n + '">image <span class="box-number">' + n + '</span></label> <input type="file" name="file[]" value="" id="box' + n + '" /> <a href="#" class="remove-box">Remove</a></p>');
            box_html.hide();
            $('.my-form p.text-box:last').after(box_html);
            box_html.fadeIn('slow');
            return false;
        });
        $('.my-form').on('click', '.remove-box', function() {

            $(this).parent().fadeOut("slow", function() {
                $(this).remove();
                $('.box-number').each(function(index) {
                    $(this).text(index + 1);
                });
            });
            return false;
        });
    });
    </script>
    <script>
    $(document).ready(function() {
        $("#hide").click(function() {
            $('#datepicker-example1').val('');
            $('#timeid').val('');
            $(".show_hide").hide();
        });
        $("#show").click(function() {
            $(".show_hide").show();
        });

    });
    </script>
    <script>
    function hiddenpublishdate() {
        $(document).ready(function() {
            $("#radiohidden").click(function() {
                $('#datepicker-example1').val('');
                $('#timeid').val('');
                $(".show_hide").hide();
            });
        });
    }
    </script>
    <script>
    $(document).ready(function() {
        $("#create_vender").click(function() {
            $("#show_vender_div").show();
        });
    });
    </script>
</head>

<body>
    <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="mu-contnt-wrp">
                <div class="mu-contnt-hdng">
                    <h1>Add News</h1>
                </div>
                <!-- Bread crumbs starts here -->
                <div class="n-crums">
                    <ul>
                        <li>
                            <a href="<?php echo base_url() ?>admin/pages">News &amp; Advice</a>
                        </li>
                        <li>
                            <div class="crms-sep">
                                &gt;
                            </div>
                        </li>
                        <li>
                            <a href="javascript:voide(0);">Add News</a>
                        </li>
                    </ul>
                </div>
                <!-- Bread crumbs ends here -->
                <?php echo form_open_multipart('',array('name'=>'add_blog')); ?>
                <!-- Box along with label section begins here -->
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Write your News</h2>
                        <p>Give your news a name, author, and add your news content.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>News Name*</label>
                                    <input type="text" value="<?php echo set_value('name'); ?>" name="name" placeholder="" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('name')?>
                                </div>
                            </div>                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Author Name*</label>
                                    <input type="text" value="<?php echo set_value('author'); ?>" name="author" placeholder="" />
                                </div>
                                <div class="error"><?php echo form_error('author')?></div>
                            </div>                            

                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>News &amp; Advice Category*</label>
                                    <select name="blog_category">
                                        <option value="">Select</option>
                                        <?php foreach($blog_category as $category) { ?>}
                                        <option value="<?=$category->blog_cat_id?>" <?php echo set_select( 'blog_category', $category->blog_cat_id); ?> ><?=$category->blog_cat_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="error"><?php echo form_error('blog_category')?> </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <label class="lone-set">Description*</label>
                                <textarea class="myeditor" name="description" id="desc"><?php echo set_value('description'); ?></textarea>
                                <div class="error">
                                    <?php echo form_error('description')?>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Image</h2>
                        <p>Upload an image for this news.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <div class="my-form1">
                                        <div class="my-form1">
                                            <div id="testdiv">
                                            </div>
                                            <p class="text-box">
                                                <label for="box1">Image<!--<span class="sub-text x-set">Image size: 715 x 372</span>--></label>
                                                <!--<input type="file" name="image" value="" id="box1" />-->
                                                <div class="fileUp">
                                                    <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="box1-txt" name="hidden_file3" value="Upload File" readonly="true" />
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="error"><?php echo form_error('hidden_file3')?></div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Meta Information</h2>
                        <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta title</label>
                                    <input type="text" value="<?php echo set_value('meta_title'); ?>" name="meta_title" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Keyword</label>
                                    <input type="text" value="<?php echo set_value('meta_keywords'); ?>" name="meta_keywords" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta description </label>
                                    <textarea class="text_meta" name="meta_description"><?php echo set_value('meta_description'); ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>News Status</h2>
                        <p>Manage your news status</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select( 'status', '1'); ?> >Active</option>
                                        <option value="0" <?php echo set_select( 'status', '0'); ?> >Inactive</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('status')?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-fld-sub">
                    <input type="submit" value="Submit" />
                    <a href="<?=base_url() ?>admin/blog">cancel</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</body>

</html>