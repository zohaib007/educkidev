<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Edit
        <?php echo $blog_data->blog_name; ?>
    </title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
    <!-- tinymce configration start here.-->
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <script>
    var base_url='<?php echo base_url();?>';
    var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>dev/resources/tiny_upload';
    </script>
    
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
    <!-- tinymce configration end here.-->
    <script>
    function delete_gall_image(sel) {
        //  alert(sel);
        $(".text-box").hide();
        $(".remove-box").hide();
        $("input[name='image']").val('');
        $.ajax({
            url: base_url + 'admin/blog/delete_blog_img',
            type: 'POST',
            data: { nId: sel, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        }).done(function() {
            console.log("success");
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }

    $('.my-form').on('click', '.remove-box', function() {

        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index) {
                $(this).text(index + 1);
            });
        });
        return false;
    });
    </script>
</head>

<body>
    <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Edit <?=$blog_data->blog_name?></h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/blog">News &amp; Advice</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:void(0);"> Edit <?=$blog_data->blog_name?></a> </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <?php echo form_open_multipart('',array('name'=>'edit_blog')); ?>
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Write your news</h2>
                            <p>Give your news a name, author, and add your news content.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>News Name*</label>
                                        <input type="text" value="<?php echo $blog_data->blog_name; ?>" name="name" />
                                    </div>
                                    <div class="error"><?php echo form_error('name')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Author Name*</label>
                                        <input type="text" value="<?php echo $blog_data->blog_author; ?>" name="author" />
                                    </div>
                                    <div class="error"><?php echo form_error('author')?> </div>
                                </div>                                

                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>News &amp; Advice Category*</label>
                                        <select name="blog_category">
                                            <option value="">Select</option>
                                            <?php foreach($blog_category as $category) { ?>}
                                            <option value="<?=$category->blog_cat_id?>" <?php echo set_select( 'blog_category', $category->blog_cat_id, $blog_data->blog_category_id == $category->blog_cat_id?true:''); ?> ><?=$category->blog_cat_name?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="error"><?php echo form_error('blog_category')?> </div>                                    
                                </div>

                                <div class="mu-flds-wrp">
                                    <label class="lone-set">Description* </label>
                                    <textarea class="myeditor" name="description" id="desc"><?php echo $blog_data->blog_description; ?></textarea>
                                    <div class="error"><?php echo form_error('description')?> </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Image</h2>
                            <p>Upload an image for this news.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <?php                            
                                                if(@$blog_data->blog_image)
                                                {
                                                ?>
                                                <label for="box1">Image <span class="box-number"></span></label>
                                                <p class="text-box my-form">
                                                    <img src="<?= base_url() ?>/resources/blog_image/thumb/<?=$blog_data->blog_image; ?>"></img>
                                                    <input type="hidden" name="image" value="<?=@$blog_data->blog_image; ?>" />
                                                </p>
                                                <a class="remove-box" href="javascript:void(0)" onclick="delete_gall_image(<?php echo @$blog_data->blog_id; ?>);">Remove</a> </div>
                                                <div class="fileUp">
                                                    <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="box1-txt" value="Upload File" readonly="true" />
                                                </div>
                                                <?php
                                                } else {
                                                ?>
                                                <p class="text-box">
                                                    <label for="box1">Image</label>
                                                    <!--<input type="file" name="image" value="" id="box1" />-->
                                                    <div class="fileUp">
                                                        <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                        <label for="box1"></label>
                                                        <input type="text" id="box1-txt" value="Upload File" readonly="true" />
                                                    </div>
                                                </p>
                                                <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Meta Information</h2>
                        <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta title</label>
                                    <input type="text" value="<?php echo $blog_data->meta_title; ?>" name="meta_title" />
                                    
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Keyword</label>
                                    <input type="text" value="<?php echo $blog_data->meta_keywords; ?>" name="meta_keywords" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta description </label>
                                    <textarea class="text_meta" name="meta_description"><?php echo $blog_data->meta_description; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>News Status</h2>
                        <p>Manage your news status</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select( 'status', '1', $blog_data->blog_status == 1?true:''); ?> >Active</option>
                                        <option value="0" <?php echo set_select( 'status', '0',$blog_data->blog_status == 0?true:''); ?> >Inactive</option>
                                    </select>
                                </div>
                                <div class="error"><?php echo form_error('status')?> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-fld-sub">
                    <input type="submit" value="Submit" />
                    <a href="<?php echo base_url() ?>admin/blog">cancel</a>
                </div>
                <!-- Box wihout label section ends here -->
                <?php echo form_close(); ?> </div>
    </div>
    </div>
</body>

</html>