<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>FAQ's</title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?=base_url()?>js/tablesort/style.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
    <script>
    $(function() {
        $('.message').append('<span class="close" title="Dismiss"></span>');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
            function() { $(this).addClass('hover'); },
            function() { $(this).removeClass('hover'); }
        );

        $('.message .close').click(function() {
            $(this).parent().fadeOut('slow', function() { $(this).remove(); });
        });
    });
    </script>
    <style>
    #display_tr {
        display: table-row !important;
    }
    </style>
</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="n-crums">
                    <ul>
                        <li> <a href="<?=base_url("admin/pages ")?>">Content Management</a> </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li>
                            <a href="<?=base_url('admin/pages/sub_page_listing/'.$main_pg->pg_id)?>"><?=$main_pg->pg_title?>
                            </a>
                        </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <?=$sub_pg->sub_pg_title?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <div class="contntTop-row">
                            <div class="actvity-hd">
                                <div class="act-tab b-ad-tb">
                                    <table cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="35%" />
                                            <col width="30%" />
                                            <col width="35%" />
                                        </colgroup>
                                        <tr>
                                            <td class="no-bdr">
                                                <div class="cat-tbl-tp-lf">
                                                    <h1><?=$sub_pg->sub_pg_title?></h1><span class="n-of-cat"><?=count($pageList)?></span>
                                                </div>
                                            </td>
                                            <td id="" class="pr-msg-bx no-bdr"></td>
                                            <td align="right" class="no-bdr">
                                                <a href="<?= base_url("admin/pages/add_sub_sub_page/".$main_pg->pg_id.'/'.$sub_pg->sub_id) ?>" class="pro-addNew">Add New Question</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <?php   
                    if($this->session->flashdata('msg') != ""){
                       echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                    }
                    ?>
                                <div class="b-prd-tbl align-top">
                                    <table border="0" cellspacing="0" id='mytable'>
                                        <colgroup>
                                            <!-- <col width="10%" /> -->
                                            <col width="15%" />
                                            <col width="40%" />
                                            <col width="15%" />
                                            <col width="15%" />
                                            <col width="15%" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th align="center" style="padding-left:13px;">Created Date</th>
                                                <th align="left">Page Title</th>
                                                <th align="center">Update On</th>
                                                <th align="center">Status</th>
                                                <th align="center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                    if(count($pageList) > 0)
                                    {  
                                        foreach($pageList as $page) {
                                    ?>
                                                <tr>
                                                    <td align="center" class="td-content" style="padding-left:13px;">
                                                        <?php echo date('m-d-Y',strtotime($page->date)); ?>
                                                    </td>
                                                    <td class="td-content">
                                                        <h3><?= limited_text($page->sub_sub_pg_title , 40) ?></h3>
                                                        <?=limited_text($page->sub_sub_pg_description, 100); ?>
                                                    </td>
                                                    <td align="center" class="td-content">
                                                        <?=($page->update!='')?date('m-d-Y', strtotime($page->update)):'N/A'?>
                                                    </td>

                                                    <td align="center" class="td-content">
                                                        <?=$page->sub_sub_pg_status==1?'Active':'Inactive'?>
                                                    </td>
                                                    <td align="center">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <colgroup>
                                                                <col width="25%" />
                                                                <col width="25%" />
                                                                <col width="25%" />
                                                                <col width="25%" />
                                                            </colgroup>
                                                            <tr id="display_tr">
                                                            	
                                                                <td>&nbsp;</td>
                                                                
                                                                <td align="center">
                                                                    <a title="Edit" href="<?= base_url("admin/pages/edit_sub_sub_page/".$main_pg->pg_id."/".$sub_pg->sub_id.'/'.$page->sub_sub_id) ?>" class="tik-cross-btns p-edt-btn-n"></a>
                                                                </td>
                                                                
                                                                <!--
                                                                <?php if($page->sub_sub_is_listing){ ?>
                                                                <td align="center">
                                                                    <a title="View Pages" href="<?= base_url() ?>admin/pages/sub_page_listing/<?= $page->pg_id ?>" class="tik-cross-btns dtlbtn-n"></a>
                                                                </td>
                                                                <?php }  else echo "<td align='center'>&nbsp;</td>"; ?>
                                                                -->
                                                                
                                                                <td align="center">
                                                                    <a onclick="return confirm('Are you sure you want to delete the selected item(s)? ');" href="<?php echo base_url("admin/pages/delteSubSubPage/".$main_pg->pg_id."/".$sub_pg->sub_id.'/'.$page->sub_sub_id)?>" class="tik-cross-btns p-del-btn-n"><img src="<?= base_url() ?>images/b-del-icon.png " alt="" /></a>
                                                                </td>
                                                                
                                                                <td>&nbsp;</td>
                                                                
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
                                        }
                                    }else{
                                        echo '<tr><td colspan = 3 align="center" >No data found.</td></tr>';
                                    }
                                    ?>
                                        </tbody>
                                    </table>
                                    <div class="pagination-row" style="width:100%">
                                        <?php //$paginglink ?>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('#mytable').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ],
            "oLanguage": { "sZeroRecords": "No records found." },
        });
    });
    </script>
</body>

</html>