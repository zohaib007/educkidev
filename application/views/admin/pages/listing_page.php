<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Add Listing</title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <!-- tinymce configration start here.-->
    <script>
    var base_url = '<?php echo base_url(); ?>';
    var ser = '<?php echo $_SERVER['
    DOCUMENT_ROOT ']; ?>educki/resources/tiny_upload';
    </script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>tiny/common.js"></script>
    <!-- tinymce configration end here.-->
    <script type="text/javascript">
    function convertToSlug(Text) {
        url = Text
            .toLowerCase()
            .replace(/ /g, '-').replace(/[^\w -]+/g, '');
        document.getElementById("my_url").value = url;
    }

    function convertToSlug2(Text) {
        url = Text
            .toLowerCase()
            .replace(/ /g, '-').replace(/[^\w -]+/g, '');
        document.getElementById("my_url").value = url;
    }
    </script>
</head>

<body>
    <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="mu-contnt-wrp">
                <div class="mu-contnt-hdng">
                    <h1>Add New Listing</h1>
                </div>
                <!-- Bread crumbs starts here -->
                <div class="n-crums">
                    <ul>
                        <li> <a href="<?php echo base_url() ?>admin/pages">Content Management</a> </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li> <a href="<?php echo base_url('admin/pages/sub_page_listing/' . $main_pg->pg_id) ?>"><?= $main_pg->pg_title ?></a> </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li> <a href="<?= base_url() ?>admin/pages/sub_sub_listing/<?= $main_pg->pg_id ?>/<?= $sub_pg->sub_id ?>"><?= $sub_pg->sub_pg_title ?></a> </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li> <a href="javascript:voide(0);"> Add Listing</a> </li>
                    </ul>
                </div>
                <!-- Bread crumbs ends here -->
                <?php echo form_open_multipart('', array('name' => 'add sub page', 'enctype' => 'multipart/form-data')); ?>
                <!-- Box along with label section begins here -->
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Write your Listing</h2>
                        <p>Give your listing a title and image.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <!--<div id="clonediv">-->
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Listing Title*</label>
                                    <input type="text" value="<?php echo set_value('listing_title'); ?>" name="listing_title" placeholder="" onfocusout="convertToSlug2($(this).val())" id="my_title" />
                                </div>
                                <div class="error"><?php echo form_error('listing_title') ?> </div>
                            </div>                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>URL*</label>
                                    <input type="text" value="<?php echo set_value('listing_url'); ?>" name="listing_url" placeholder="" id="my_url" onkeyup="convertToSlug($(this).val())" />
                                </div>
                                <div class="error"><?php echo form_error('listing_url') ?></div>
                            </div>                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <div class="my-form1">
                                        <div class="my-form1">
                                            <div id="testdiv"></div>
                                            <p class="text-box">
                                                <label for="box1">Listing Image*<span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <div class="fileUp">
                                                    <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" name="file1" id="box1-txt" value="Upload File" readonly="true" />
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="error"><?php echo form_error('file1') ?> </div>
                            </div>                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <div class="my-form1">
                                        <div class="my-form1">
                                            <div id="testdiv"></div>
                                            <p class="text-box">
                                                <label for="box2">Listing Hover Image*<span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <div class="fileUp">
                                                    <input type="file" name="hover_image" value="" id="box2" onchange="javascript: document.getElementById('box2-txt').value = this.value" />
                                                    <label for="box2"></label>
                                                    <input type="text" name="file2" id="box2-txt" value="Upload File" readonly="true" />
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="error"><?php echo form_error('file2') ?>
                            </div>                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Write your Page</h2>
                        <p>Give your Page a banner title banner image and description.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Banner Title*</label>
                                    <input type="text" value="<?php echo set_value('banner_title'); ?>" name="banner_title" placeholder="" />
                                </div>
                                <div class="error"><?php echo form_error('banner_title') ?> </div>
                            </div>                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <div class="my-form1">
                                        <div class="my-form1">
                                            <div id="testdiv"></div>
                                            <p class="text-box">
                                                <label for="box1">Banner Image*<span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <!--<input type="file" name="image" value="" id="box1" />-->
                                                <div class="fileUp">
                                                    <input type="file" name="banner_image" value="" id="box1" onchange="javascript: document.getElementById('bannerbox1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="bannerbox1-txt" name="hidden_file3" value="<?=@$listing_pg->lisintg_image; ?>" readonly="true" />
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="error"><?php echo form_error('hidden_file3') ?> </div>
                            </div>                            


                            <div class="mu-flds-wrp">
                                <label class="lone-set">Page Description</label>
                                <textarea class="myeditor" name="pg_description" id="desc"><?php echo set_value('pg_description'); ?></textarea>
                                <div class="error"><?php echo form_error('pg_description')?></div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Meta Information</h2>
                        <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta title</label>
                                    <input type="text" value="<?php echo set_value('meta_title'); ?>" name="meta_title" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Keyword</label>
                                    <input type="text" value="<?php echo set_value('meta_keywords'); ?>" name="meta_keywords" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta description </label>
                                    <textarea class="text_meta" name="meta_description">
                                        <?php echo set_value('meta_description'); ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Listing Status</h2>
                        <p>Manage your listing status.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="listing_status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select( 'listing_status', '1'); ?> >Active</option>
                                        <option value="0" <?php echo set_select( 'listing_status', '0'); ?> >Inactive</option>
                                    </select>
                                </div>
                                <div class="error"><?php echo form_error('listing_status') ?> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-fld-sub">
                    <input type="submit" value="Submit" />
                    <a href="<?= base_url('admin/pages/sub_sub_listing/' . $main_pg->pg_id .'/'.$sub_pg->sub_id) ?>">cancel</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!--        <script>
                $(document).ready(function () {
                    $(function () {
                        $(".clonebutton").on('click', function () {
                            var ele = $(this).closest('#clonediv').clone(true);
                            $(this).closest('#clonediv').after(ele);
                            $(this).remove();
                        });
                    });
                });
        </script>-->
</body>

</html>