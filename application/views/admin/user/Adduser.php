<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php echo $title;?>
    </title>
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/jquery-1.10.1.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.liveaddress.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/jquery.validate.min.js"></script>
</head>

<body>
    <div class="container_p">
        <!-- container -->
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="mu-contnt-wrp">
                <div class="mu-contnt-hdng">
                    <h1<?=$title?></h1>
                        <a href="<?php echo base_url()?>admin/user" class="c-b-s-l" style="display:none;"> Back <img src="<?=base_url()?>images/back0icon.jpg" alt="" /> </a> </div>
                <div class="n-crums">
                    <ul>
                        <li> <a href="<?php echo base_url() ?>admin/user">Manage Users</a> </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li>
                            <a href="#">
                                <?php echo $title;?>
                            </a>
                        </li>
                    </ul>
                </div>
                <?php echo form_open_multipart('', array('id'=>'edit_user')); ?>
                <!-- Box along with label section begins here -->
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Account Details</h2>
                        <p>Manage the registered accounts on your website.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>First Name*</label>
                                    <input type="text" name="user_fname" value="<?php echo set_value('user_fname');?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_fname')?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Last Name*</label>
                                    <input type="text" name="user_lname" value="<?php echo set_value('user_lname')?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_lname')?> </div>
                            </div>
                            <!-- <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                 <label>Profile Type [Seller/Buyer]*</label>
                 <select name="user_type">
                   <option value="Buyer" <?php echo set_value('user_type','Buyer'); ?> >Buyer</option>
                   <option value="Seller" <?php echo set_value('user_type','Seller'); ?> >Seller</option>
                 </select>
               </div>
             </div>
             <div class="error"> <?php echo form_error('user_type')?> </div> -->
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Email*</label>
                                    <input type="text" name="user_email" value="<?php echo set_value('user_email');?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_email')?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Gender*</label>
                                    <select name="user_sex" id="user_sex">
                                        <option value="Male" <?php echo set_select( 'user_sex', 'Male');?>>Male</option>
                                        <option value="Female" <?php echo set_select( 'user_sex', 'Female'); ?>>Female</option>
                                        <option value="Other" <?php echo set_select( 'user_sex', 'Other'); ?>>Other</option>
                                    </select>
                                </div>
                                <div class="error">
                                  <?php echo form_error('user_sex')?> 
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Interest*</label>
                                    <select name="user_interests" id="user_interests">
                                        <option value="">Interests</option>
                                        <option value="Fertility" <?php echo set_select( 'user_interests', 'Fertility'); ?> >Fertility</option>
                                        <option value="Pregnancy" <?php echo set_select( 'user_interests', 'Pregnancy'); ?> >Pregnancy</option>
                                        <option value="Baby names" <?php echo set_select( 'user_interests', 'Baby names'); ?> >Baby Names</option>
                                        <option value="Baby" <?php echo set_select( 'user_interests', 'Baby'); ?> >Baby</option>
                                        <option value="Toddler" <?php echo set_select( 'user_interests', 'Toddler'); ?> >Toddler</option>
                                        <option value="Child" <?php echo set_select( 'user_interests', 'Child'); ?> >Child</option>
                                        <option value="Family" <?php echo set_select( 'user_interests', 'Family'); ?> >Family</option>
                                        <option value="Advice" <?php echo set_select( 'user_interests', 'Advice'); ?>>Advice</option>
                                        <option value="Food" <?php echo set_select( 'user_interests', 'Food'); ?> >Food</option>
                                        <option value="Other" <?php echo set_select( 'user_interests', 'Other'); ?> >Other</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_interests')?> </div>
                            </div>
                            <div class="mu-flds-wrp" id="otherinterest" style="display: none">
                                <div class="mu-frmFlds_long">
                                    <input type="text" name="user_other_interest" value="<?php echo set_value('user_other_interest'); ?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_other_interest')?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Interest*</label>
                                    <select name="user_interests_second" id="user_interests_second">
                                        <option value="">Interests</option>
                                        <option value="Fertility" <?php echo set_select( 'user_interests_second', 'Fertility'); ?> >Fertility</option>
                                        <option value="Pregnancy" <?php echo set_select( 'user_interests_second', 'Pregnancy'); ?> >Pregnancy</option>
                                        <option value="Baby names" <?php echo set_select( 'user_interests_second', 'Baby names'); ?> >Baby Names</option>
                                        <option value="Baby" <?php echo set_select( 'user_interests_second', 'Baby'); ?> >Baby</option>
                                        <option value="Toddler" <?php echo set_select( 'user_interests_second', 'Toddler'); ?> >Toddler</option>
                                        <option value="Child" <?php echo set_select( 'user_interests_second', 'Child'); ?> >Child</option>
                                        <option value="Family" <?php echo set_select( 'user_interests_second', 'Family'); ?> >Family</option>
                                        <option value="Advice" <?php echo set_select( 'user_interests_second', 'Advice'); ?>>Advice</option>
                                        <option value="Food" <?php echo set_select( 'user_interests_second', 'Food'); ?> >Food</option>
                                        <option value="Other" <?php echo set_select( 'user_interests_second', 'Other'); ?> >Other</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_interests_second')?> </div>
                            </div>
                            <div class="mu-flds-wrp" id="otherinterestsecond" style="display: none">
                                <div class="mu-frmFlds_long">
                                    <input type="text" name="user_other_interest_second" value="<?php echo set_value('user_other_interest_second'); ?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_other_interest_second')?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Age*</label>
                                    <select name="user_age">
                                        <option value="">Select Age</option>
                                        <option value="13-17" <?php echo set_select( 'user_age', '13-17'); ?>>13-17</option>
                                        <option value="18-24" <?php echo set_select( 'user_age', '18-24'); ?>>18-24</option>
                                        <option value="25-34" <?php echo set_select( 'user_age', '25-34'); ?>>25-34</option>
                                        <option value="35-44" <?php echo set_select( 'user_age', '35-44'); ?>>35-44</option>
                                        <option value="45-54" <?php echo set_select( 'user_age', '45-54'); ?>>45-54</option>
                                        <option value="55-64" <?php echo set_select( 'user_age', '55-64'); ?>>55-64</option>
                                        <option value="65+" <?php echo set_select( 'user_age', '65+'); ?>>65+</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_age')?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Password*</label>
                                    <input type="password" name="password" value="<?php echo set_value('password'); ?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('password')?> </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label id="zip_postal">Zip Code*</label>
                                    <input type="text" name="user_zip" id="zip" value="<?php echo set_value('user_zip'); ?>" placeholder="Zip Code" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_zip')?> </div>
                            </div>
                            
                            <!-- <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Phone</label>
                                    <input type="text" name="user_phone" value="<?php set_value('user_phone'); ?>" placeholder="Phone" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_phone')?>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label id="zip_postal">Zip Code*</label>
                                    <input type="text" name="user_zip" id="zip" value="<?php set_value('user_zip'); ?>" placeholder="Zip Code" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_zip')?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Country*</label>
                                    <select name="user_country" id="user_country">
                                        <option value="">Select Country</option>
                                        <?php foreach($country as $val) { ?>
                                        <option value="<?php echo $val['iso']; ?>">
                                            <?php echo $val['name'];?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="error">
                                <?php echo form_error('user_country')?> </div>
                            <div class="mu-flds-wrp country-us" id="us-states" style="display: none">
                                <div class="mu-frmFlds_long">
                                    <label>State</label>
                                    <select name="state">
                                        <option value="">Select</option>
                                        <?php foreach($states as $val) { ?>
                                        <option value="<?php echo $val['stat_id']; ?>">
                                            <?php echo $val['stat_name'];?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="error country-us">
                                <?php echo form_error('state')?> </div>
                            <div class="mu-flds-wrp" id="other-states" style="display: none">
                                <div class="mu-frmFlds_long">
                                    <label>State</label>
                                    <input type="text" id="other_state" name="other_state" value="<?php set_value('other_state'); ?>" />
                                </div>
                            </div>
                            <div class="error">
                                <?php echo form_error('other_state'); ?> </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>City*</label>
                                    <input type="text" name="city" value="<?php set_value('city'); ?>" placeholder="City" />
                                </div>
                            </div>
                            <div class="error">
                                <?php echo form_error('city'); ?> </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Transaction Fees*</label>
                                    <input type="text" name="user_fee" value="<?php set_value('user_fee'); ?>" placeholder="Transaction Fees" />
                                </div>
                            </div>
                            <div class="error">
                                <?php echo form_error('user_fee'); ?> </div> -->
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Flagged Status*</label>
                                    <select name="user_flagged_status">
                                        <option value="">Select</option>
                                        <option value="0" <?php echo set_select( 'user_flagged_status', '0'); ?>>Unflagged</option>
                                        <option value="1" <?php echo set_select( 'user_flagged_status', '1'); ?>>Flagged</option>
                                    </select>
                                    <div class="error">
                                        <?php echo form_error('user_flagged_status'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Are you a small business owner?*</label>
                                    <select name="user_business_owner">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select( 'user_business_owner', '1'); ?>>Yes</option>
                                        <option value="0" <?php echo set_select( 'user_business_owner', '0'); ?>>No</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_business_owner'); ?>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="user_status">
                                        <option value="">Select</option>
                                        <option value="0" <?php echo set_select( 'user_status', '0'); ?>>Inactive</option>
                                        <option value="1" <?php echo set_select( 'user_status', '1'); ?>>Active</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_status'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Box along with label section ends here -->
                <!-- Box wihout label section begins here -->
                <div class="mu-fld-sub">
                    <input type="submit" value="Submit" />
                    <a href="<?php echo base_url()?>admin/user">cancel</a> </div>
                <!-- Box wihout label section ends here -->
                <?php echo form_close(); ?> </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#user_country').change(function() {
            if ($('#user_country').val() === 'US') {
                $('#us-states').show("slow");
                $('#other_state').val('');
                $('#other-states').hide("slow");
            } else {
                $('#us-states').hide("slow");
                $('#other-states').show("slow");
            }
        });
    });
    $(document).ready(function() {
        $('#user_interests').change(function() {
            if ($('#user_interests').val() == 'Other') {
                $('#otherinterest').show("slow");
            } else {
                $('#otherinterest').hide("slow");
            }
        });
        $('#user_interests').change();
    });
    $(document).ready(function() {
        $('#user_interests_second').change(function() {
            if ($('#user_interests_second').val() == 'Other') {
                $('#otherinterestsecond').show("slow");
            } else {
                $('#otherinterestsecond').hide("slow");
            }

        });
        $('#user_interests_second').change();
    });
    </script>
</body>

</html>