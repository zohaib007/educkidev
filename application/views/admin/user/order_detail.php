<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title;?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script>
$(function () {
  $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
  //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
  $('.message .close').hover(
    function() { $(this).addClass('hover'); },
    function() { $(this).removeClass('hover'); }
  );
    
  $('.message .close').click(function() {
    $(this).parent().fadeOut('slow', function() { $(this).remove(); });
  });
});
</script>
<style>
th {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
td{
    padding: 8px;
    text-align: left;
}
</style>
</head>

<body>
<div class="container_p">

<!-- Dashboard Left Side Begins Here -->

<div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
</div>
<!-- Dashboard Left Side Ends Here -->
<div class="right-rp">    
    <!-- Top Green Bar Section Begins Here -->    
    <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>    
    <!-- Top Green Bar Section Ends Here -->    
    <div class="mu-contnt-wrp">
        <div class="mu-contnt-hdng">
            <h1><?=$title;?></h1>
        </div>
        <div class="n-crums"><?=$b_crums;?></div>
        <?php //echo form_open('', array('name'=>'editAdmin') ); ?>
        <!-- Box along with label section begins here -->
            
        	<div class="mu-contnt-outer">
            <?php 
			if($this->session->flashdata('msg') != "")
			{
				echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
			?>

            <?php if(count($order_products)>0){
                foreach ($order_products as $o_prod) { ?>
            <div class="mu-contnt-outer">
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">

                    <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                        <colgroup>
                            <col width="6%">
                            <col width="10%">
                            <col width="15%">
                            <col width="12%">
                            <col width="13%">
                            <col width="13%">
                            <col width="10%">
                            <col width="15%">
                            <col width="2%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Title</th>
                                <th>Store Name</th>
                                <th>Shipping Method</th>
                                <th>Shipped Date</th>
                                <th>Tracking #</th>
                                <th align="center">Price + Shipping</th>
                                <th align="center">Qty</th>                                
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?=$o_prod->order_prod_id?></td>
                            <td>
                                <a href="<?= productDetailUrl($o_prod->order_prod_id) ?>/<?= $o_prod->order_prod_url ?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $o_prod->order_prod_image ?>" alt="" /></a>
                            </td>
                            <td>
                                <?=$o_prod->order_prod_name?>
                                <?php $filters = json_decode($o_prod->order_prod_filters);
                                if(count($filters) > 0)
                                foreach ($filters as $i => $filter) {?>
                                    <p><b><?=$filter->filterName?></b>:<?=$filter->filtervalue?></p>
                                <?php }?>
                            </td>
                            <td>
                                <?=$o_prod->order_prod_store_name?>
                            </td>
                            <td>
                                <?=$o_prod->order_prod_shipping?>
                                <?php if($o_prod->order_prod_shipping=='Charge for Shipping'){
                                    echo $o_prod->order_prod_shipping_methods;
                                }?>
                                <?php if($o_prod->order_prod_shipping=='Charge for Shipping'){
                                    echo "( ".$o_prod->order_prod_ship_days." )";
                                }elseif($o_prod->order_prod_shipping=='Local Pick Up'){
                                    echo "( 7 )";
                                }else{
                                    echo "( ".$o_prod->order_prod_free_ship_days." )";
                                }?>
                            </td>
                            <td>
                                <?php 
                                $shipped_date = '';
                                if($o_prod->order_prod_shipping=='Local Pick Up'){
                                    $shipped_date = "Local Pick-Up";
                                }else{
                                    if($o_prod->order_prod_tracking_number!=''){
                                        if(!(date('Y-m-d H:i:s', strtotime($o_prod->order_prod_shipping_date)) == $o_prod->order_prod_shipping_date)){
                                            $shipped_date = "N/A";
                                        }else{
                                            $shipped_date = date('m-d-Y',strtotime($o_prod->order_prod_shipping_date));
                                        }
                                    }else{
                                        $shipped_date = "N/A";
                                    }
                                }
                                ?>
                                <?=$shipped_date?>
                            </td>
                            <td><?php $shipping_p = "";
                                if($o_prod->order_prod_tracking_number!=''){
                                    $shipping_p = $o_prod->order_prod_tracking_number;
                                }
                                else if($o_prod->order_prod_shipping=='Local Pick Up'){
                                    $shipping_p = 'Local Pick-Up';
                                }else{
                                    $shipping_p = 'N/A';
                                }
                                echo $shipping_p;
                                ?>
                                </td>
                            <td align="center">
                                $ <?=$o_prod->order_prod_total_price?> + $
                                <?php $shipping = 0;
                                    if($o_prod->order_prod_shipping=='Charge for Shipping'){
                                        $shipping = number_format($o_prod->order_prod_ship_price,2,'.','');
                                    }else{
                                        $shipping = '0.00';
                                    }
                                    echo $shipping;                                    
                                ?>
                            </td>
                            <td align="center">
                                <?=$o_prod->order_prod_qty?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                        
            </div>
                    
            </div> 
             <?php } 
            } else{?>
            <div class="mu-flds-wrp">
                <div class="mu-cm-frmFlds">
                    <p>No Subscription History available.</p>
                </div>
            </div>
            <?php }?>            
            <div class="mu-contnt-outer">
            <div class="mu-contntBx-wrp mrkt-plc" style="width: 100%;">
                <div class="contntTop-row rdtl-pg">
                    <div class="cont-half">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Order Details</h1>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Order ID:</label>
                                <div class="review-rdtl"><?=$order_details->order_id?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Order Status:</label>
                                <div class="review-rdtl"><?=$order_details->order_status?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Ordered Date:</label>
                                <div class="review-rdtl"><?=$order_details->order_date?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Order Completed Date:</label>
                                <div class="review-rdtl"><?=($order_details->order_completion_date!='')?$order_details->order_completion_date:'N/A'?></div>
                            </div>
                        </div>
                    </div>
                    <div class="cont-half" style="float:right !important">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Buyer Details</h1>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Buyer ID:</label>
                                <div class="review-rdtl"><?=$order_details->buyer_id?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Buyer Name:</label>
                                <div class="review-rdtl"><?=$order_details->buyer_name?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Buyer Email:</label>
                                <div class="review-rdtl"><?=$order_details->buyer_email?></div>
                            </div>
                        </div>
                    </div>
                </div>
                        
            </div>
        </div>
        <div class="mu-contnt-outer">
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">

                    <div class="review-tphdr">
                        <div class="reviews-hd">
                            <h1>Seller(s) Details</h1>
                        </div>
                    </div>

                    <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                        <colgroup>
                            <col width="12%">
                            <col width="20%">
                            <col width="68%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>Store ID</th>
                                <th>Store Name</th>
                                <th>Store Email</th>                         
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($order_sellers as $o_seller) { ?>
                        <tr>
                            <td><?=$o_seller->store_id?></td>
                            <td><?=$o_seller->store_name?></td>
                            <td><?=$o_seller->store_email?></td>
                        </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>                        
            </div>                    
        </div> 
            <div class="mu-contntBx-wrp mrkt-plc" style="width: 100%;">
                <?php $ship_adrs = json_decode($order_details->order_shipping_address);?>
                <div class="contntTop-row rdtl-pg">
                	<div class="cont-half">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Shipping</h1>
                            </div>
                        </div>
                        <?php if($ship_adrs){ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>First Name:</label>
                                <div class="review-rdtl"><?=$ship_adrs->shipping_first_name?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Last Name:</label>
                                <div class="review-rdtl"><?=$ship_adrs->shipping_last_name?></div>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Street:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$ship_adrs->shipping_street?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>City:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$ship_adrs->shipping_city?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>State:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?php if($ship_adrs->shipping_country == 'US'){echo $ship_adrs->shipping_state;}else{echo $ship_adrs->shipping_state_other;}?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Zip:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$ship_adrs->shipping_zip?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Country:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=get_country_name($ship_adrs->shipping_country)?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Telephone:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$ship_adrs->shipping_phone?></div>      
                                </div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <p>No default shipping address available.</p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php $billing_adrs = json_decode($order_details->order_billing_address);?>
                    <div class="cont-half" style="float:right !important">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Billing</h1>
                            </div>
                        </div>
                        <?php if($billing_adrs){ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>First Name:</label>
                                <div class="review-rdtl"><?=$billing_adrs->billing_first_name?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Last Name:</label>
                                <div class="review-rdtl"><?=$billing_adrs->billing_last_name?></div>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Street:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$billing_adrs->billing_street?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>City:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$billing_adrs->billing_city?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>State:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?php if($billing_adrs->billing_country == 'US'){echo $billing_adrs->billing_state;}else{echo $billing_adrs->billing_state_other;}?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Zip:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$billing_adrs->billing_zip?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Country:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=get_country_name($billing_adrs->billing_country)?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Telephone:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$billing_adrs->billing_phone?></div>      
                                </div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <p>No default billing address available.</p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                        
   			</div>
                    
            </div>
                       
            <div class="mu-contnt-outer">
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Payment Information</h1>
                            </div>
                        </div>
                    
                        <div class="cont-half">
                           <strong>Paypal</strong>
                        </div>
                </div>
                        
   			</div>
                    
            </div> 

            <div class="mu-contnt-outer" style="border-bottom: 0px;">
            <div class="mu-contntBx-wrp store-info" style="width: 30%;float: right;margin-right: 15px;">
                    
                <div class="contntTop-row rdtl-pg">
                     <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Order Details</h1>
                            </div>
                        </div>
                        <div class="cont-half" style="width: 100%;">
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Sub Total:</label>
                                    <div class="review-rdtl" style="text-align: right;">$<?=number_format($order_details->order_sub_total, 2, '.', '')?></div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Discount:</label>
                                    <div class="review-rdtl" style="text-align: right;">$<?=number_format($order_details->order_discount, 2, '.', '')?></div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Commission:</label>
                                    <div class="review-rdtl" style="text-align: right;">$<?=number_format($order_details->order_total_commission, 2, '.', '')?></div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Shipping Total:</label>
                                    <div class="review-rdtl" style="text-align: right;">$<?=number_format($order_details->order_shipping_price, 2, '.', '')?></div>
                                </div>
                            </div>
                        </div>
                        <div class="review-tphdr" style="padding-top: 10px;border-top: 1px solid #eeeeee;border-bottom: 0px;">
                            <div class="reviews-hd">
                                <h1>Total:<span style="float: right;">$<?=number_format($order_details->order_grand_total, 2, '.', '')?></span></h1>
                            </div>
                        </div>
                </div>
                        
            </div>
                    
            </div>           
        </div>
        </div>
    </div>
</div>
</body>
</html>