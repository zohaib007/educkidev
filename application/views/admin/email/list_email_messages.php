<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Email Triggers</title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
<script>
$(function () {
	$('.message').append('<span class="close" title="Dismiss"></span>');
	//setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
	$('.message .close').hover(
		function() { $(this).addClass('hover'); },
		function() { $(this).removeClass('hover'); }
	);
		
	$('.message .close').click(function() {
		$(this).parent().fadeOut('slow', function() { $(this).remove(); });
	});
	
});
</script>
</head>

<body>
<div class="container"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
    <!-- Top Green Bar Section Ends Here -->
    <?php
    	if($this->session->flashdata('msg') != "") {
			echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
		}
	?>
    <div class="col-continer">
      <div class="col-3 cat-box"> 
        
        <!-- Crumbs Starts -->
        <div class="n-crums">
          <ul>
            <li> </li>
            <!--
                <li>
                    <div class="crms-sep">></div>
                </li>
                -->
          </ul>
        </div>
        <!-- Crumbs Ends -->
        
        <div class="contntTop-row">
        	
          <!-- Top Nav Starts Here -->
          <?php  include(ADMIN_INCLUDE_PATH."includes/commnuication.php");?>
          <!-- Top Nav Ends Here -->
          
          <div class="b-prd-tbl">
            <table  id='mytable' class='tablesorter' border="0" cellspacing="0" cellpadding="0" >
              <colgroup>
                <col width="15%" />
                <col width="65%" />
                <col width="20%" />
              </colgroup>
              <thead>
                <tr>
                  
                  <th align="left">Date</th>
                  <th align="left">Message Title</th>
                  		      <th align="center">Actions</th>
                      
                </tr>
              </thead>
              
                <?php for($n=0;$n<count($rstCategories);$n++){ ?>
                <tr>
                  
                    <?php $date = date('m-d-Y',strtotime($rstCategories[$n]['date_created']));?>
                    
                    <td align="left"><?=$date?></td>
                    <td align="left"><?=$rstCategories[$n]['e_email_title']?></td>
                    <td align="center"><table border="0" cellspacing="0" cellpadding="0">
                        <colgroup>
                        <col width="28%" />
                        <col width="44%" />
                        <col width="28%" />
                        </colgroup>
                        <tr>
                          <td align="center">&nbsp;</td>
                          
                          <td align="center">
                          <a title="Edit" href="<?=base_url()?>admin/communication/emailtrigger_edit/<?=$rstCategories[$n]['e_email_id']?>" class="tik-cross-btns p-edt-btn-n"><!--<img src="<?=base_url()?>images/b-edt-icon.png" alt="" />--></a>
                          </td>
                          <td align="center">&nbsp;</td>
                        </tr>
                      </table></td>
                  
                </tr>
                <?php
					}
				?>
             
            </table>
            <div class="pagination-row">
              <?php //$paginglink?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
$(document).ready(function(){
    $('#mytable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     false,
        "order": [[ 0, "asc" ]],
        "columnDefs": [
           { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>
</html>