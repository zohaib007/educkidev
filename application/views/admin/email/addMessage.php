<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Trigger</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>


<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!--<script src="<?= base_url() ?>js/charts/highcharts.js"></script>
<script src="<?= base_url() ?>js/charts/modules/exporting.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>js/editor/examples/css/font-awesome.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>js/editor/dist/summernote.css">
<script type="text/javascript" src="<?php echo base_url() ?>js/editor/dist/summernote.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>-->



<script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/summernote.php"); ?>
</head>
<body>
<div class="container_p"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php
    include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
    ?>
    <!-- Top Green Bar Section Ends Here -->
    <div class="mu-contnt-wrp">
      <div class="mu-contnt-hdng">
        <h1> Add Trigger</h1>
      </div>
      <!-- Bread Crumbs Starts -->
      <div class="n-crums">
        <ul>
          <li> <a href="<?php echo base_url() ?>admin/communication/emailtrigger_list"> Email Triggers </a> </li>
          <li>
            <div class="crms-sep">&gt;</div>
          </li>
          <li> <a href="#"> Add Trigger </a> </li>
        </ul>
      </div>
      <!-- Bread Crumbs Ends --> 
      
      <?php echo form_open_multipart(base_url().'admin/communication/emailtrigger_add'); ?> 
      
      <!-- Box along with label section begins here -->
      
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Trigger Details</h2>
          <p>Manage your trigger details.</p>
        </div>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
          	<div class="mu-flds-wrp">
            	<div class="mu-frmFlds_long">
                    <label>Title*</label>
                    <input type="text" value="<?php echo set_value('nl_title'); ?>" name="nl_title" />
                </div>
            </div>
            <div class="error"> <?php echo form_error('nl_title')?> </div>
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                <label>Subject*</label>
                <input type="text" value="<?php echo set_value('nl_subject'); ?>" name="nl_subject" />
              </div>
            </div>
            <div class="error"> <?php echo form_error('nl_subject')?> </div>
            <div id="middlecategoryDiv"> </div>
            <div id="subcategoryDiv"> </div>
            
            <div class="mu-flds-wrp">
                <label class="lone-set">Description*</label>
                <textarea  class="summernote" name="nl_text" id="desc" ><?php echo set_value('nl_text'); ?></textarea>
                <?php //echo ckeditor('nl_text'); ?>
            </div>
            <div class="error"> <?php echo form_error('nl_text')?> </div>
            </div>
            
            
          </div>
        </div>
      </div>
      
      <!-- Box along with label section ends here --> 
      
      <!-- Box wihout label section begins here -->
      
      <div class="mu-fld-sub">
        <input type="submit" value="Submit" />
        <a href="<?=base_url()?>admin/communication/emailtrigger_list">cancel</a> </div>
      
      <!-- Box wihout label section ends here --> 
      
      <?php echo form_close(); ?> </div>
  </div>
</div>
</body>
</html>