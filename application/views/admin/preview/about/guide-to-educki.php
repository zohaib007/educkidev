<!--banner  starts here-->
<section class="whted-bg">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>what-is-educki">About</a></li>
                                <li class="breadcrumb-item active">Guide to eDucki</li>
                            </ol>
                        </div>

                        <h2>Guide To <span>e</span>Ducki</h2>	
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!--Breadcrumbs ends  here-->
</section>
<!--main sec starts here-->
<section class="weht-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 pad-lft-0">
                    <div class="side-nav">
                        <ul>
                            <li>
                                <a href="javascript:void(0);">What is eDucki</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="active">Guide To eDucki</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Community Guidelines</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">eDucki Protect</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">FAQs</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pad-rgt-0">
                    <div class="whted-rgt-sec">
                        <div class="guid-sec">
                            <div class="guid-rw">
                            <?php foreach (array_chunk($list, 2, true) as $newlist) {
                                echo '</div><div class="guid-rw">';
                                $count = 1;
                                foreach ($newlist as $page) {
                                    ?>
                                    <div class="guid-bx-lft <?= ($count % 2 == 0) ? 'mrg-rgt-0' : '' ?>">
                                        <a href="<?= base_url() ?>about-us/guide-to-educki/<?= $page['listing_url'] ?>">
                                            <div class="get-startd">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <img src="<?= base_url() ?>resources/page_image/listing/thumb/<?= $page['lisintg_image'] ?>" alt="" title="" class="mg-fr-hd"/>
                                                            <img src="<?= base_url() ?>resources/page_image/listing/thumb/<?= $page['lisintg_image'] ?>" alt="" title="" class="mg-fr-sh"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="get-txt">
                                                    <p>
                                                        <?= stripcslashes($page['listing_title'])?>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                            <?php $count ++; } } ?></div>
                        </div>
                        <div class="sprt-sec">
                            <p>
                                Still need assistance? Check out our <a href="javascript:void(0);">Support Center</a>.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--main sec  ends here-->