<?php if (empty($banner)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = $banner;
}
?>
<!--banner  starts here-->
<section class="whted-bg" style="background-image: url('<?=$path?>'); !important">
    <div class="frg-layr"></div>
        <!--Breadcrumbs starts here-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                    <div class="auto-container">
                        <div class="brd-whit">
                        <h2><?=stripcslashes($pg_title);?></h2>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!--Breadcrumbs ends  here-->

</section>
<!--banner  starts here-->



<!--main sec starts here-->
<section class="weht-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 pad-lft-0">
                    <div class="side-nav">
                        <ul>
                            <li>
                                <a href="javascript:void(0);">What is eDucki</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Guide To eDucki</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="active"><?=stripcslashes($pg_title);?></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">eDucki Protect</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">FAQs</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Contact</a>
                            </li>
                        </ul>

                    </div>
                </div>

                <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pad-rgt-0">
                    <div class="whted-rgt-sec">
                        <div class="community-sec"><?=stripcslashes($pg_description);?></div>
                        <div class="sprt-sec">
                            <p>
                                Still need assistance? Check out our <a href="javascript:void(0);">Support Center</a>.
                            </p>

                        </div>


                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<!--main sec  ends here-->