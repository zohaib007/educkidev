<?php
 if (empty($image_5)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = $image_5;
}
?>
<!--banner  starts here-->
<section class="whted-bg" style="background-image: url('<?=$path?>'); !important">
	<div class="frg-layr"></div>
    	<!--Breadcrumbs starts here-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                    <div class="auto-container">
                    	<div class="brd-whit">
                        
                        <h2><?=stripcslashes($page_title)?></h2>	
                        </div>
                        
                    </div>
                </div>
            </div>
        
        </div>
        <!--Breadcrumbs ends  here-->

</section>
<!--banner  starts here-->



<!--main sec starts here-->
<section class="weht-main">
	<div class="container">
    	<div class="row">
        	<div class="auto-container">
            	<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 pad-lft-0">
                	<div class="side-nav">
                    	<ul>
                        	<li>
                            	<a href="javascript:void(0);" class="active">What is eDucki</a>
                            </li>
                            <li>
                            	<a href="javascript:void(0);">Guide To eDucki</a>
                            </li>
                            <li>
                            	<a href="javascript:void(0);">Community Guidelines</a>
                            </li>
                            <li>
                            	<a href="javascript:void(0);">eDucki Protect</a>
                            </li>
                            <li>
                            	<a href="javascript:void(0);">FAQs</a>
                            </li>
                            <li>
                            	<a href="javascript:void(0);">Contact</a>
                            </li>
                        </ul>
                
                	</div>
                </div>
                
                <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pad-rgt-0">
                	<div class="whted-rgt-sec">
                    	<div class="whted-rgt-tit">
                        	<h3><?= stripcslashes($title) ?></h3>
                            <span>
                            	<img src="<?=base_url()?>front_resources/images/bdr2.png" alt="" title=""/>
                            </span>
                    	</div>
                        
                        <div class="whted-rgt-bx">
                        	<div class="whted-rgt-bx-lft">
                            	<img src="<?=$image_1?>" alt="" title="" class="img-responsive pull-left"/>
                            
                            </div>
                            
                            <div class="whted-rgt-bx-rgt">
                            	<!--<div class="cvr-upr">
                                	<h5>Create a Covershot</h5>
                                    <a href="#">With just a few clicks</a>
                                </div>-->
                                <div class="cvr-bttom">
                                	<p><?= stripcslashes($img_description_1)?></p>
                                </div>
                            
                            
                            </div>
                        
                        </div>
                        
                        <div class="whted-rgt-bx">
                        	
                            <div class="whted-rgt-bx-rgt mrg-lft-0 mrg-lft-3">
                            	<!--<div class="cvr-upr">
                                	<h5>Introducing eDucki Protect</h5>
                                    <a href="#">Buy with confidence</a>
                                </div>-->
                                <div class="cvr-bttom">
                                	<p><?=stripcslashes($img_description_2)?></p>
                                </div>
                            
                            
                            </div>
                            <div class="whted-rgt-bx-lft">
                            	<img src="<?=$image_2?>" alt="" title="" class="img-responsive pull-right"/>
                            
                            </div>
                        
                        </div>
                        
                        <div class="whted-rgt-bx">
                        	<div class="whted-rgt-bx-lft">
                            	<img src="<?=$image_3?>" alt="" title="" class="img-responsive pull-left"/>
                            </div>
                            
                            <div class="whted-rgt-bx-rgt">
                            	<!--<div class="cvr-upr">
                                	<h5>Enjoy One-Stop Shopping</h5>
                                    <a href="#">New and pre-loved fashion</a>
                                </div>-->
                                <div class="cvr-bttom">
                                	<p><?=stripcslashes($img_description_3)?></p>
                                </div>
                            
                            
                            </div>
                        
                        </div>
                        
                        <div class="whted-rgt-bx bodr-0">
                        	
                            <div class="whted-rgt-bx-rgt mrg-lft-0 mrg-lft-3">
                            	<!--<div class="cvr-upr">
                                	<h5>Discover the eDucki Community</h5>
                                    <a href="#">Connect and share</a>
                                </div>-->
                                <div class="cvr-bttom">
                                	<p><?=stripcslashes($img_description_4)?></p>
                                </div>
                            
                            
                            </div>
                            <div class="whted-rgt-bx-lft">
                            	<img src="<?=$image_4?>" alt="" title="" class="img-responsive pull-right"/>
                            </div>
                        
                        </div>
                        
                        <div class="sprt-sec">
                        	<p>
                            	Still need assistance? Check out our <a href="javascript:void(0);">Support Center</a>.
                            </p>
                        
                        </div>
                    
                    
                    </div>
                </div>
            
            
            </div>
        </div>
    </div>
</section>

<!--main sec  ends here-->