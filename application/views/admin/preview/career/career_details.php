<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>career">Carrers</a></li>
                        <li class="breadcrumb-item active">Details</li>
                    </ol>
                </div>	

                <div class="car-det-wrp">

                    <div class="crd-det-main">
                        <div class="crd-det-upr">
                            <h2><?= $title ?></h2>
                        </div>

                        <div class="crd-det-midd">

                            <h6>Job Description</h6>

                            <p><?= $description ?></p>
                        </div>

                        <div class="crd-det-btm">

                            <div class="jb-frmsec">
                                <div class="jobfrm-up">
                                    <h5>Apply for this Job</h5>
                                    <a href="">
                                        <img src="images/job-lnklnd.png" alt="" title=""/>
                                    </a>
                                    <span>(Optional)</span>
                                </div>
                                <?php echo form_open_multipart('career/apply_career', array('name' => 'apply_career', 'id' => 'applycareer_form', 'enctype' => 'multipart/form-data')); ?>
                                <div class="jobfrm-md">
                                    <div class="jobfrm-md-inr">
                                        <div>
                                            <div class="jobfrm-md-inr-lft">
                                                <h6>First Name<span>*</span></h6>
                                            </div>
                                            <div class="jobfrm-md-inr-rgt">
                                                <input type="text" name="fname" required value="<?php echo set_value('fname'); ?>" />
                                            </div>
                                        </div>
                                        <div>
                                            <div class="jobfrm-md-inr-lft">
                                                <h6>Last Name<span>*</span></h6>

                                            </div>

                                            <div class="jobfrm-md-inr-rgt">
                                                <input type="text" name="lname" required value="<?php echo set_value('lname'); ?>" />
                                            </div>
                                        </div>
                                        <div>
                                            <div class="jobfrm-md-inr-lft">
                                                <h6>Email<span>*</span></h6>

                                            </div>

                                            <div class="jobfrm-md-inr-rgt">
                                                <input type="text" name="email" required value="<?php echo set_value('email'); ?>" >
                                            </div>
                                        </div>
                                        <div>
                                            <div class="jobfrm-md-inr-lft">
                                                <h6>Phone<span>*</span></h6>

                                            </div>

                                            <div class="jobfrm-md-inr-rgt">
                                                <input type="text" name="phone" required value="<?php echo set_value('phone'); ?>" />
                                            </div>
                                        </div>
                                        <div class="jobfrm-md-inr-rgt">
                                            <div class="input-group btn-upload">   
                                                <span class="input-group-btn">
                                                    <div class="btn btn-upload-input">
                                                        <span class="btn-upload-input-title">Upload Resume/CV</span>
                                                        <input type="file" name="resume"/> <!-- rename it -->
                                                    </div>
                                                </span>                
                                            </div>


                                        </div>

                                        <div class="jobfrm-md-inr-rgt">
                                            <div class="input-group btn-upload">   
                                                <span class="input-group-btn">
                                                    <div class="btn btn-upload-input2">
                                                        <span class="btn-upload-input-title2">Upload Cover Letter</span>
                                                        <input type="file" name="cover"/> <!-- rename it -->
                                                    </div>
                                                </span>                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="jobfrm-thrd">

                                    <div class="jobfrm-thrd-inr">
                                        <div class="lnk-prof">
                                            <h6>LinkedIn Profile</h6>
                                            <input type="text" name="linkedin" value="<?php echo set_value('linkedin'); ?>" />
                                        </div>
                                        <div class="lnk-prof">
                                            <h6>Website</h6>
                                            <input type="text" name="website" value="<?php echo set_value('website'); ?>" />
                                        </div>
                                        <div class="lnk-prof">
                                            <h6>How did you hear about this job?</h6>
                                            <input type="text" name="hear" value="<?php echo set_value('hear'); ?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="jobfrm-lst">
                                    <div class="jobfrm-thrd-inr">
                                        <input type="submit" value="Submit Application"/>
                                    </div>

                                </div>
                                <input type="hidden" value="<?= $career_id ?>" name="career_id"/>
                                <?php echo form_close(); ?>

                            </div>
                            <div class="jb-share-sec">
                                <div class="share-sec">
                                    <p>Share this job:</p>
                                    <img src="images/share.png" alt="" title=""/>
                                </div>
                            </div>


                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
    $(document).ready(function () {
        $("#applycareer_form").validate({
            messages: {
                fname: {
                    required: 'Enter First Name!'
                },
                lname: {
                    required: 'Enter Last Name!'
                },
                email: {
                    required: 'Enter Email Address!'
                },
                Phone: {
                    required: 'Enter Phone Number!'
                }
            }
        });

    });
</script>