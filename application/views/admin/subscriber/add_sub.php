<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Dashboard</title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?> 
       
        <script src="<?= base_url() ?>js/charts/highcharts.js"></script>
        <script src="<?= base_url() ?>js/charts/modules/exporting.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>examples/css/bootstrap.min.css" /> 
        <script type="text/javascript" src="<?php echo base_url() ?>examples/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>examples/css/font-awesome.min.css" />  
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/summernote.css">
            <script type="text/javascript" src="<?php echo base_url() ?>dist/summernote.js"></script>
            <script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>



            <script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
            <script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
            <link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
            

            
         

            <script type="text/javascript">
                $(function() {
                    $.noConflict();
                    $('.summernote').summernote({
                        height: 200
                    });

                    $('form').on('submit', function (e) {
                        // alert($('.summernote').code());
                    });
                });
            </script>

            
            
 
            

    </head>
    <body>

        <div class="container_p">

            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">

                <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>

            </div>
            <!-- Dashboard Left Side Ends Here -->

            <div class="right-rp">

                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Add New Customer</h1>
                    </div>

                    <form method="post" name="form" action="<?php echo base_url();?>admin/newsletter/submit_sub" enctype="multipart/form-data">  



                        <!-- Box along with label section begins here -->

                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Product details</h2>
                                <p>
                                    Write a name and description, and provide a type and vendor to categorize this product.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                    <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <label>Subscriber email:</label>
                                    <input type="text" value="<?php echo set_value('sub_email'); ?>" name="sub_email" />
                                </div>
                               
                            </div>
                                       <div class="error">  
                                    <?php echo form_error('sub_email')?>
                                    </div>
                                   
                                  
                                    
                                </div>
                            </div>
                        </div>
                        
                     
                        
                        
                        <!-- Box along with label section ends here -->

                        <!-- Box wihout label section begins here -->

                        <div class="mu-fld-sub">
                            <input type="submit" value="Add Subscriber" />
<!--                            <a href="">cancel</a>-->
                        </div>

                        <!-- Box wihout label section ends here -->

                    </form>
                </div>

            </div>
        </div>
    </body>

</html>