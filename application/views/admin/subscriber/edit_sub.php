<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Dashboard</title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?> 
       
        <script src="<?= base_url() ?>js/charts/highcharts.js"></script>
        <script src="<?= base_url() ?>js/charts/modules/exporting.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>examples/css/bootstrap.min.css" /> 
        <script type="text/javascript" src="<?php echo base_url() ?>examples/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>examples/css/font-awesome.min.css" />  
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/summernote.css">
            <script type="text/javascript" src="<?php echo base_url() ?>dist/summernote.js"></script>
            <script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>



            <script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
            <script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
            <link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
            
            <script type="text/javascript">
jQuery(document).ready(function($){
    $('.my-form .add-box').click(function(){
        var n = $('.text-box').length + 1;
        if( 5 < n ) {
            alert('Stop it!');
            return false;
        }
           var box_html = $('<p class="text-box"><label for="box' + n + '">image <span class="box-number">' + n + '</span></label> <input type="file" name="file[]" value="" id="box' + n + '" /> <a href="#" class="remove-box">Remove</a></p>');
        box_html.hide();
        $('.my-form p.text-box:last').after(box_html);
        box_html.fadeIn('slow');
        return false;
    });
    $('.my-form').on('click', '.remove-box', function(){
       
        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index){
                $(this).text( index + 1 );
            });
        });
        return false;
    });
});
</script>
            
            <script>
                $(document).ready(function(){
                    $("#hide").click(function(){
                        $('#datepicker-example1').val('');
                        $('#timeid').val('');
                        $(".show_hide").hide();
                    });
                    $("#show").click(function(){
                        $(".show_hide").show();
                    });
                });
            </script>

            <script type="text/javascript">
                $(function() {
                    $.noConflict();
                    $('.summernote').summernote({
                        height: 200
                    });

                    $('form').on('submit', function (e) {
                        // alert($('.summernote').code());
                    });
                });
            </script>
         


            

    </head>
    <body>

        <div class="container_p">

            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">

                <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>

            </div>
            <!-- Dashboard Left Side Ends Here -->

            <div class="right-rp">

                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Add New Customer</h1>
                    </div>

                    <form method="post" name="form" action="<?php echo base_url();?>admin/newsletter/submit_editnews" enctype="multipart/form-data">  



                        <!-- Box along with label section begins here -->
<?php
         // print_r($resultsChannel);
                          
                          	for($n=0;$n<count($resultsChannel);$n++)
                                {
                                    ?>
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Product details</h2>
                                <p>
                                    Write a name and description, and provide a type and vendor to categorize this product.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                    <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <label>From:</label>
                                    <input type="text" name="nl_from" value="<?php echo $resultsChannel[$n]['nl_from'] ?>" />
                                    <input type="hidden" name="nl_id" value="<?php echo $resultsChannel[$n]['nl_id'] ?>" />
                                </div>
                               
                            </div>
                                    <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <label>Subject:</label>
                                    <input type="text" value="<?php echo $resultsChannel[$n]['nl_subject'] ?>"  name="nl_subject" />
                                </div>
                               
                            </div>
                                   
                                    <div id="middlecategoryDiv">
                                         
                                    </div>
                                    <div id="subcategoryDiv">
                                        
                                        
                                         
                                    </div>
                                    <div class="mu-flds-wrp">
                                        <label>Description</label>
                                        <textarea  class="summernote" name="nl_text" id="desc" ><?php echo $resultsChannel[$n]['nl_text'] ?></textarea>   
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                     
                        
                        
                        <!-- Box along with label section ends here -->

                        <!-- Box wihout label section begins here -->

                        <div class="mu-fld-sub">
                            <input type="submit" value="Add Subscriber" />
<!--                            <a href="#.">cancel</a>-->
                        </div>

                        <!-- Box wihout label section ends here -->

                    </form>
                </div>

            </div>
        </div>
        <?php } ?>
    </body>

</html>