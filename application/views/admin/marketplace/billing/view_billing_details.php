<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title;?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script>
$(function () {
  $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
  //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
  $('.message .close').hover(
    function() { $(this).addClass('hover'); },
    function() { $(this).removeClass('hover'); }
  );
    
  $('.message .close').click(function() {
    $(this).parent().fadeOut('slow', function() { $(this).remove(); });
  });
});
</script>
</head>

<body>
<div class="container_p">

<!-- Dashboard Left Side Begins Here -->

<div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
</div>

<!-- Dashboard Left Side Ends Here -->

<div class="right-rp">
    
    <!-- Top Green Bar Section Begins Here -->
    
    <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
    
    <!-- Top Green Bar Section Ends Here -->
    
    <div class="mu-contnt-wrp">
        <!--<div class="mu-contnt-hdng">
            <h1><?=$title;?></h1>
        </div>-->
        <div class="n-crums">
            <ul>
                <li>
                    <a href="<?php echo base_url() ?>admin/marketplace/billing">Marketplace Billing</a>
                </li>
                <li>
                    <div class="crms-sep">
                        &gt;
                    </div>
                </li>
                <li>
                    <a href="javascritp:void(0);"><?=$title;?></a>
                </li>
            </ul>
        </div>
        <?php //echo form_open('', array('name'=>'editAdmin') ); ?>
        <!-- Box along with label section begins here -->
        
        	<div class="mu-contnt-outer">
            <?php 
			if($this->session->flashdata('msg') != "")
			{
				echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
			?>
            <div class="mu-contntBx-wrp mrkt-plc" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                	<div class="cont-half">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Billing</h1>
                            </div>
                        </div>
                        <?php if($default_billing){ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Name:</label>
                                <div class="review-rdtl"><?=$default_billing->nick_name?></div>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Phone:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=(!empty($default_billing->phone)?$default_billing->phone:"N/A")?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Created date:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=date_format(date_create($default_billing->created_date),"m-d-Y")?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Billing Street:</label>
                                <div class="review-rdtl"><?=$default_billing->street?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>City:</label>
                                <div class="review-rdtl"><?=$default_billing->city?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>State:</label>
                                <div class="review-rdtl"><?=($default_billing->country == "US" ? get_state_name($default_billing->state) : $default_billing->state_other)?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Country:</label>
                                <div class="review-rdtl"><?=(!empty($default_billing->country)?get_country_name($default_billing->country):"N/A")?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Zip:</label>
                                <div class="review-rdtl"><?=$default_billing->zip?></div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <p>No default billing address available.</p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    
                    <div class="cont-half" style="float:right !important">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Shipping</h1>
                            </div>
                        </div>
                        <?php if($default_shipping){ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Name:</label>
                                <div class="review-rdtl"><?=$default_shipping->nick_name?></div>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Phone:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=(!empty($default_shipping->phone)?$default_shipping->phone:"N/A")?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Created date:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=date_format(date_create($default_shipping->created_date),"m-d-Y")?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Shipping Street:</label>
                                <div class="review-rdtl"><?=$default_shipping->street?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>City:</label>
                                <div class="review-rdtl"><?=$default_shipping->city?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>State:</label>
                                <div class="review-rdtl"><?=($default_shipping->country == "US" ? get_state_name($default_shipping->state) : $default_shipping->state_other)?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Country:</label>
                                <div class="review-rdtl"><?=(!empty($default_shipping->country)?get_country_name($default_shipping->country):"N/A")?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Zip:</label>
                                <div class="review-rdtl"><?=$default_shipping->zip?></div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <p>No default shipping address available.</p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                        
   			</div>
                    
            </div>
            
        	<div class="mu-contnt-outer">

            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Personal Information</h1>
                            </div>
                        </div>
                        
                        <div class="cont-half">
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Name:</label>
                                    <div class="review-rdtl"><?=$personal_information->user_fname." ".$personal_information->user_lname?></div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Email:</label>
                                    <div class="review-rdtl"><?=$personal_information->user_email?></div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Last Log in:</label>
                                    <div class="review-rdtl"><?=date_format(date_create($personal_information->user_last_active),"m-d-Y")?></div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Seller Since:</label>
                                    <div class="review-rdtl"><?=date_format(date_create($personal_information->user_register_date),"m-d-Y")?></div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Account Status:</label>
                                    <div class="review-rdtl">
                                        <div class="review-rdtl"><?php if($personal_information->user_status == 1){echo 'Active';}else{echo 'InActive';}?></div>      
                                    </div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Subscription Tier:</label>
                                    <div class="review-rdtl"><?=getTierName($personal_information->user_id)?></div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of Months left in Subscription:</label>
                                    <?php
                                    $time ='';
                                    if($sub_left>0){
                                        if(($sub_left/30)>1){
                                            $months = round($sub_left/30);
                                            $days = $sub_left%30;
                                            $time = $months." Month(s)";
                                            if($days>0){
                                                $time .= " , ".$days." days";
                                            }
                                        }else{
                                            $time = $sub_left." days";
                                        }
                                    }else{
                                        $time = '0 days';
                                    }
                                    ?>
                                    <div class="review-rdtl"><?=$time?></div>
                                </div>
                            </div>
                        </div>

                        <div class="cont-half" style="float:right !important">
                            
                            <!-- <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of People Referred:</label>
                                    <div class="review-rdtl">0</div>
                                </div>
                            </div> -->

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Paypal Email:</label>
                                    <div class="review-rdtl"><?=$personal_information->store_paypal_id?></div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of times User Reported / Flagged:</label>
                                    <div class="review-rdtl"><?=$user_reported?></div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of times their Product Reported / Flagged:</label>
                                    <div class="review-rdtl"><?=prod_reported($personal_information->user_id)?></div>
                                </div>
                            </div>
                        </div>
                  
                    
                   
                </div>
                        
   			</div>
                    
            </div>
            
            <div class="mu-contnt-outer">
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Store Information</h1>
                            </div>
                        </div>
                        
                        <div class="cont-half">
                        
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Number of Products in Store:</label>
                                    <div class="review-rdtl"><?=$no_of_products?></div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Lifetime Sales GMV:</label>
                                    <div class="review-rdtl">$<?=(!empty($GMV))?number_format($GMV, 2, '.', ''):'00.00'?></div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Lifetime Transaction Value:</label>
                                    <div class="review-rdtl">
                                        <div class="review-rdtl">$<?=(!empty($fees))?number_format($fees, 2, '.', ''):'00.00'?></div>      
                                    </div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Lifetime Subscription Value:</label>
                                    <div class="review-rdtl">
                                        <div class="review-rdtl">$<?=(!empty($sub_revenue))?number_format($sub_revenue, 2, '.', ''):'00.00'?></div>      
                                    </div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Total # of months as Subscriber:</label>
                                    <div class="review-rdtl">
                                        <?php
                                        $time ='';
                                        if($subscribed>0){
                                            if(($subscribed/30)>1){
                                                $months = round($subscribed/30);
                                                $days = $subscribed%30;
                                                $time = $months." Month(s)";
                                                if($days>0){
                                                    $time .= " , ".$days." days";
                                                }
                                            }else{
                                                $time = $subscribed." days";
                                            }
                                        }else{
                                            $time = '0 days';
                                        }
                                        ?>
                                        <div class="review-rdtl"><?=$time?></div>      
                                    </div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Total # of Times Subscribed:</label>
                                    <div class="review-rdtl"><?=count($subscription)?></div>
                                    <!--
                                    <div class="review-rdtl">Product Review</div>
                                    -->
                                </div>
                            </div>
                  
                    	</div>
                   
                </div>
                        
   			</div>
                    
            </div>
            
            <div class="mu-contnt-outer">
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Subscription History</h1>
                            </div>
                        </div>

                        <div class="cont-half">
                            <?php if(count($subscription)>0){
                                foreach ($subscription as $sub) { ?>
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label><?=$sub->tier_name?>:</label>
                                    <div class="review-rdtl"><?=date_format(date_create($sub->sub_start),"m-d-Y")?> <b>to</b> <?=date_format(date_create($sub->sub_ends),"m-d-Y");?></div>
                                </div>
                            </div>
                            <?php } 
                            } else{?>
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <p>No Subscription History available.</p>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                </div>
                        
   			</div>
                    
            </div>

            <div id="myModal" class="modal-2">
                <div class="modal-2-content">
                    <div class="modal-header">
                        <span class="close2">&times;</span>
                        <h2>Edit Comment</h2>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="form-group nw-pop-clas">
                                <label for="v_title" style="width: 15%;">Comment*</label>
                                <textarea id="editcomment" style="width: 80%;"></textarea>
                                <input type="hidden" name="curr_comm" id="curr_comm">
                                <div class="error" id="edit-error" style="width: 100%;padding-left: 95px;padding-top: 5px;"></div>
                            </div>
                            <button type="button" class="btn btn-default btn-new" id="process-edit">Edit</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="mu-contnt-outer">
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg2">
                        <?php if(count($comments)>0){
                            foreach ($comments as $com) {?>
                        <div class="review-tphdr">
                        	<div class="mu-cm-frmFlds" style="width: 100%;">
                                <label style="width: 15%">Comments:</label>
                                <div class="review-rdtl" style="width: 80%"><?=$com->comment?>
                                    <span style="float: right;"><a onclick="return confirm('Are you sure you want to delete the selected item(s)? ');" href="<?php echo base_url("admin/marketplace/delete_comment/".$com->comment_id."/".$personal_information->user_id)?>" class="tik-cross-btns p-del-btn-n"><img src="<?= base_url() ?>images/b-del-icon.png " alt="" /></a></span>
                                    <span style="float: right;margin-right: 10px;"><a title="Edit" href="javascritp:void" data-id="<?=$com->comment_id?>" data-comment="<?=$com->comment?>" data-user="<?=$personal_information->user_id?>" class="tik-cross-btns p-edt-btn-n edit-comment"></a></span>
                                </div>
                            </div>
                        </div>
                        <?php }}?>
                        <?php echo form_open('admin/Marketplace/register_comment')?>
                        <input type="hidden" name="seller_id" value="<?=$personal_information->user_id?>">
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Comments:</label>
                                <div class="review-rdtl" style="width: 60%;"><textarea class="txtarea" name="comment"></textarea><div class="error"><?php if($this->session->flashdata('comment_error') != ""){echo $this->session->flashdata('comment_error');}?></div></div>
                                <div class="mu-fld-sub">        
                                    <input type="submit" value="Submit" />
                                    <a href="<?php echo base_url() ?>admin/marketplace/billing">Cancel</a> </div>                                    
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                </div>
                        
   			</div>
                    
            </div>
            
        </div>
        
        <!-- Box along with label section ends here -->
        <!-- Box wihout label section begins here -->
    </div>
</div>
<script type="text/javascript">
    $(document).on('click','.edit-comment',function(){
        $('#curr_comm').val($(this).attr('data-id'));
        $('#editcomment').text($(this).attr('data-comment'));
        $(".modal-2").show();
    });

    $(document).on('click','#process-edit',function(){
        var comm_id = $('#curr_comm').val();
        var comment = $('#editcomment').val();
        console.log("comment = "+comment);
        if(comment!=''){
            $('#edit-error').html('');
            $.ajax({
            url: base_url + 'admin/marketplace/edit_comment',
            type: 'POST',
            datatype: 'json',
            data: { id: comm_id,comment: comment, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
            }).done(function(result) {
                location.reload();
            }).fail(function() {
                console.log("error");
            });
        }else{
            $('#edit-error').html('This field is required.');
        }
    });

    $('.close2').bind("click", function () {
       $(".modal-2").hide();
    });
</script>
</body>
</html>