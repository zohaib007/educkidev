<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Manage Orders</title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/jquery-ui-1.8.13.custom.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/fullcalendar.css" />
        <link rel="stylesheet" href="<?= base_url() ?>js/tablesort/style.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>

        <script src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>

        <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.daterange.js"></script>

    </head>
    <body>
        <div class="container">

            <div class="left_wrp" id="menu-top">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>

            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp"> 
                <!-- Top Green Bar Section Begins Here -->
                <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-hdng">
                    <h1>Manage Orders</h1>
                </div>
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <div class="contntTop-row"> <!-- col-box1 -->

                            <div class="actvity-hd">
                                <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div class="message info"><p>' . $this->session->flashdata('msg') . '</p></div>';
                                }
                                ?>
                            </div>

                            <!-- Bottom Section Starts -->
                            <div class="nsec-btm">

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    Gross Merchandise Volume (GMV)
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    $<?= (null == $GMV) ? '0.00' : number_format($GMV,2, '.', ' '); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- Section Ends -->

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    Total Number of Transactions Completed on Site
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <?=$transactions_number?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- Section Ends -->

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    Average Order Value
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    $<?= (null == $avg_order_value) ? '0.00' : number_format($avg_order_value,2, '.', ' '); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- Section Ends -->

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    eDucki Commissions Today
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    $<?= number_format($Totalcommission, 2, '.', '') ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- Section Ends -->
                            </div>
                            <!-- Bottom Section Ends -->
                            <div class="actvity-hd">
                            <div class="act-tab b-ad-tb">
                                <table cellspacing="0" border="0">
                                    <colgroup>
                                        <col width="90%" />
                                        <col width="10%" />
                                    </colgroup>
                                    <tr>
                                        <td class="no-bdr">
                                        </td>
                                        <td align="right" class="expo-cell no-bdr">
                                            <span>Export to:</span> 
                                            <form method="post" name="MyFormExport">
                                            <?php echo form_open('',array("name"=>"MyFormExport"));?>
                                            <span>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="<?=base_url()?>admin/order/exportcsv">CSV</a>
                                            <?php echo form_close();?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row">
                                <div class="b-srch-bl-sec">
                                    <table border="0" width="100%" cellspacing="0">
                                        <colgroup>
                                            <col width="10%" />
                                            <col width="15%" />
                                            <col width="20%" />
                                            <col width="10%" />
                                            <col width="45%" />
                                        </colgroup>
                                        <tr>
                                            <?php echo form_open('', array('name' => 'myForm', 'method' => 'get')); ?>
                                            <td align="center"><span>Search by</span></td>
                                            <td align="center">
                                                <select name="srchOp" class="catName">
                                                    
                                                    <option <?php if (@$_GET['srchOp'] == 'status') { ?> selected="selected"
                                                                                                         <?php } ?> value="status">Order Status</option>
                                                    <option <?php if (@$_GET['srchOp'] == 'id') { ?> selected="selected"
                                                                                                         <?php } ?> value="id">Order ID</option>
                                                </select>
                                            </td>
                                            <td align="center">
                                                <input value="<?php
                                                if (isset($_GET['searchText'])) {
                                                    echo $_GET['searchText'];
                                                }
                                                ?>" type="text" name="searchText" class="catName" />
                                            </td>
                                            <td align="center">
                                                <input type="submit" value="" class="catSrch" />
                                            </td>
                                            <?php echo form_close(); ?>
                                            <td align="left">
                                                <div class="ndpdSort" style="width:auto; float:right; margin-right: 274px; width: auto;">
                                                    <button class="pro-addNew" id="nsortBy">Sort By<span class="dn-angl"></span> </button>
                                                    <div class="dropval"></div>
                                                    <div class="ndp-nx" id="nsortDp" style="display:none; width: 135px;">
                                                        <ul>
                                                            <li>
                                                                <div class="usr-srt-dp subDpd"> Select date range <span class="sub-dn-angl" style="top: 10px;"></span> </div>
                                                                <div class="dp-dt-pik" style="display:none;">
                                                                    <?php echo form_open('', array('name' => 'sort_form')); ?>
                                                                    <input name="date_range" style="width: 100%;" placeholder="Select date to view report" id="d" class="form__daterange catName" />
                                                                    <input type="submit" value="Submit" />
                                                                    <?php echo form_close(); ?>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="b-prd-tbl">
                                    <table border="0" cellspacing="0" id='mytable' class='display'>
                                        <colgroup>                                            
                                            <col width="10%" />
                                            <col width="22%" />
                                            <col width="15%" />
                                            <col width="15%" />
                                            <col width="15%" />
                                            <col width="18%" />
                                            <col width="5%" />
                                        </colgroup>
                                        <thead>
                                            <tr>                                                
                                                <th align="center">Order ID</th>
                                                <th align="left">Buyer Name</th>
                                                <th align="center">Order Status</th>
                                                <th align="center">Date Ordered</th>
                                                <th align="center">Date Completed</th>
                                                <th align="center">Total Sale Amount</th>
                                                <th align="center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($list as $page) {
                                                ?>
                                                <tr>                                                    
                                                    <td align="center" >
                                                        <?= $page['order_id'] ?>
                                                    </td>
                                                    <td align="left" >
                                                        <?= $page['buyer_name'] ?>
                                                    </td>
                                                    <td align="center" >
                                                        <?= $page['order_status'] ?>
                                                    </td>
                                                    <td align="center" style="padding-left:13px;">
                                                        <?= date('m-d-Y', strtotime($page['order_date'])) ?>
                                                    </td>
                                                    <td align="center" style="padding-left:13px;">
                                                        <?= ($page['order_completion_date']!='')?date('m-d-Y', strtotime($page['order_completion_date'])):'N/A' ?>
                                                    </td>
                                                    <td align="center" >
                                                        $ <?= number_format($page['order_grand_total'],2,'.','') ?>
                                                    </td>
                                                    <td align="center">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <colgroup>
                                                                <col width="26%" />
                                                                <col width="16%" />
                                                                <col width="16%" />
                                                                <col width="16%" />
                                                                <col width="26%" />
                                                            </colgroup>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td align="center">
                                                                    <a title="View Order" href="<?= base_url() ?>admin/order/view_orders/<?= $page['order_id'] ?>" class="tik-cross-btns p-view-btn-n"></a>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
        <script>
            $(function () {
                $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
                //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
                $('.message .close').hover(
                        function () {
                            $(this).addClass('hover');
                        },
                        function () {
                            $(this).removeClass('hover');
                        }
                );
                $('.message .close').click(function () {
                    $(this).parent().fadeOut('slow', function () {
                        $(this).remove();
                    });
                });
            });

        </script>

        <script type="text/javascript">
            $(function () {
                jQuery.noConflict();
                $("#d").daterange({
                    dateFormat: "yy-mm-dd",
                    maxDate: 0,
                });
            });

            $(document).ready(function () {
                $('#nsortBy').click(function () {
                    $('#nsortDp').slideToggle();
                });

                $('.subDpd').click(function () {
                    $('.dp-dt-pik').slideToggle();
                });

                $('.valclick').click(function () {
                    $('.dropval').html($(this).attr("title"));
                    $('#nsortDp').slideToggle();
                });
            });
        </script>
 <!-- <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>  -->
 <!-- <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script> -->
        <script>
            $(document).ready(function() {
                $('#mytable').DataTable({
                    "paging": true,
                    "ordering": true,
                    "info": false,
                    "lengthChange": false,
                    "searching": false,
                    "order": [
                        [1, "desc"]
                    ],
                    "columnDefs": [
                        { orderable: false, targets: -1 }
                    ],
                    "oLanguage": { "sZeroRecords": "No records found." },
                });
            });
        </script>

        <script src="<?= base_url() ?>/js/jquery.filtertable.min.js"></script>
    </body>
</html>
