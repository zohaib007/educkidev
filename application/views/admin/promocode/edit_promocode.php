<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Promo Code</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
<script>
function hide_next_item(id)
{     
	$(document).ready(function(){
	if(id==0 || id == "")
	{
	
		$('#promo_cat').val('');
		$('#promo_sub_cat').val('');
		$('#promo_prod').val('');                       
		$(".cus-hdn-contaier").hide();
		$("#middlecategoryDiv").hide();
		$("#promo_prod").hide();
	}
	
	
	
	if(id==1)
	{
	
	/*	$('#order_over').val('');
		$("#order_over").hide();*/
		/*$("#specific_order").hide();
		$("#specific_order").val('');*/
		$("#cat_prod_Div").hide();
		$("#customer_group").val('');
		$(".cus-hdn-contaier").show();
	
	}
	});
	
	}  
	function hide_percentage(id)
	{
	
	if(id==0)
	{
		//alert('doller')
		$("#percentage_val").val('');
		$("#doller").show();
		$("#percentage").hide();
	
	}
	if(id==1)
	{

		$("#doller").hide();
		$("#doller_val").val('');
		$("#percentage").show();
	}
	
	}
	$(document).ready(function(){
	/*  $(".cus-sc-outr").click(function(){
	$(".cus-scrl-wrp").toggle();
	});
	*/
	var id = $('#promo_discount_type').val();
	if(id==0)
	{
	//alert('doller')
	
	$("#percentage").val('');
	$("#doller").show();
	$("#percentage").hide();
	
	}
	if(id==1)
	{
	$("#doller").hide();
	$("#doller").val('');
	$("#percentage").show();
	}
	
	var promo_type = $('#promo_type_id').val();
	if(promo_type==1)
	{
	
	$(".cus-hdn-contaier").show();
	
	}
	
	});
	
	$(document).ready(function(){      
	$('#date_check').on('click',function(){
	//alert('asdf');
	var disableField = $(this).attr('dis');
	if(disableField==0){
	$(this).attr('dis',1);
	$("#expiredate").addClass("important");
	$('#expiredate').prop('disabled', true);
	}else{
	$(this).attr('dis',0);
	$('#expiredate').prop('disabled', false);
	}
	}); 
	});
	
	$(document).ready(function(){      
	$('#limit').on('click',function(){
	//alert('asdf');
	var disableField = $(this).attr('dis');
	if(disableField==0){
	$(this).attr('dis',1);
	$('#limit_dis').prop('disabled', true);
	}else{
	$(this).attr('dis',0);
	$('#limit_dis').prop('disabled', false);
	}
	}); 
	});
	
	$(document).ready(function()     
	{
	$('#rand_string').on('click',function(){
	
	$.ajax({        
	type:"post",
	url: base_url + "admin/promocode/randstring",
	data: {<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
	success:function(res)
	{
	$("#rand").attr('value',res);
	}
	});
	});  
	});
	
	$(document).ready(function()     
	{
	$('#keyup').on('keyup',function(){
	var value = $(this).val();
	// alert(value);
	if(value!=="")
	{ 
	$.ajax({        
	type:"post",
	url: base_url + "admin/discounts/getproducts",
	data:{keyword:value},
	success:function(res)
	{
		
	$("#thelist").html('');
	$("#thelist").html(res);
	}
	});
	}
	});  
	});
	$(document).ready(function()     
	{
	$('.getid').on('click',function(){
	
	//alert('asdf');
	
	var text = $(this).text();
	var id = $(this).attr('pid');
	$('#selprod').text('');
	$('#selprod').text(text);
	$('#selprodid').val(id);
	
	});  
	});
 </script>
<!--//////////////////////////select cat/////////////////////////-->
<script>

		function getmiddleCategory(sel)
		{
			$("#customer_group").val('');
			$("#middlecategoryDiv").hide();
			$("#cat_prod_Div").hide();
			$('#promo_prod').val('');                       
			$("#cat_prod_Div").hide();
			
			if(sel == 0)
			{
				$("#customer_group").val('');
				$("#middlecategoryDiv").hide();
				$("#cat_prod_Div").hide();
				$('#promo_prod').val('');                       
				$("#cat_prod_Div").hide();
			}
			else
			{
				$("#middlecategoryDiv").show();
				$("#middlecategoryDiv").load(base_url+'admin/promocode/getmiddleCategory', {"nId":sel,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} );      
			}
			}    
		
			function get_cat_prod(sel)
			{
				if(sel == 0)
				{
					
					$('#promo_prod').val('');
					                       
					$("#cat_prod_Div").hide();
				}
				else
				{
					$("#cat_prod_Div").show();
					$("#cat_prod_Div").load(base_url+'admin/promocode/get_cat_prod', {"nId":sel,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} ); 
				}
		}    
</script>
</head>
<body>
<div class="container_p"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php
                include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
                ?>
    <!-- Top Green Bar Section Ends Here -->
    <div class="mu-contnt-wrp">
      <div class="mu-contnt-hdng">
        <h1>Edit Promo code</h1>
      </div>
      
            <!-- Bread crumbs starts here -->
      <div class="n-crums">
        <ul>
          <li> <a href="<?php echo base_url() ?>admin/promocode">Promo Code Management</a> </li>
          <li>
            <div class="crms-sep">&gt;</div>
          </li>
          <li> <a href="#"> Edit Promo Code</a> </li>
        </ul>
      </div>
      <!-- Bread crumbs ends here --> 
      <?php echo form_open(base_url()."admin/promocode/edit/".$promo_edit_list[0]['promo_id'].""); ?> 
      
      <!-- Box along with label section begins here -->
      
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Generate Promo Code</h2>
          <p> </p>
        </div>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
              
                <div class="mu-flds-wrp">
                  <div class="mu-frmFlds">
                    <label>Date : <span class="last-updateby"><?php echo date('m-d-Y', strtotime($promo_edit_list[0]['promo_created_date']));  ?></span></label>
                  </div>
                  <!--<div class="mu-frmFlds mu-flt-rght">
                    <label>Last Update Date : <span class="last-updateby"><?php echo date('m-d-Y', strtotime($promo_edit_list[0]['promo_created_date'])); ?></span></label>
                  </div>-->
                </div>
                
          
          
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long gcode">
                <label>Promo code*</label>
                <input id="rand" type="text" value="<?php echo $promo_edit_list[0]['promo_code'] ?>" name="promo_code" placeholder="Promo Code" readonly="readonly" class="spaceset-small2" />
                <div class="gcode-txt">
                	<a href="javascript:void(0)" id="rand_string">Generate Code</a>
                </div>
              </div>
            </div>
            <div class="error"> 
              <?php  echo form_error('promo_code')?>
            </div>
           
          </div>
        </div>
        
      </div>
      <?php //echo validation_errors(); ?>
      <!-- ////////////////////////////////////////select product///////////////////////////////////////////-->
      
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Promo Code Scope</h2>
          <p>Select promo code for a Specific product or Entire site.</p>
        </div>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
            <div class="mu-frmFlds_long">
              <label>Code For* <span class="dec-unbold">(Select product name OR Entire Site)</span></label>
              <select onchange="hide_next_item(this.value);" name="promo_type">
                <option value="">Select</option>
                <option <?php if ($promo_edit_list[0]['promo_type'] == 0) { ?> selected <?php } ?> value="0">Entire Site (all orders)</option>
                <option <?php if ($promo_edit_list[0]['promo_type'] == 1) { ?> selected <?php } ?> value="1">Specific Product</option>
              </select>
              <div class="error set"> <?php echo form_error('promo_type')?> </div>
			  <div class="mu-flds-wrp"> </div>
            </div>
            
            <!-- New Div Starts -->
            <div  style="display:<?php if ($promo_edit_list[0]['promo_type'] == 0) { ?> none;<?php } ?>"  class="new-crow cus-hdn-contaier new-crow-set" >
              <div class="mu-frmFlds_long">
                <label>Select category*</label>
                        
                <select id="promo_cat" name="promo_category_id" onchange="getmiddleCategory(this.value);">
                  <option value="">Select </option>
                  <?php for ($i = 0; $i < count($main_cat); $i++) {
                        ?>
                  <option <?php if ($promo_edit_list[0]['promo_category_id'] == $main_cat[$i]['cat_id']) { ?> selected <?php } ?> value="<?= $main_cat[$i]['cat_id'] ?>">
                  <?= $main_cat[$i]['cat_name'] ?>
                  </option>
                  <?php } ?>
                </select>
                <div class="error"> <?php echo form_error('promo_category_id')?> </div>
				<div class="mu-flds-wrp"> </div>
              </div>
            </div>
            <!-- New Div Ends -->
            
            <div id="middlecategoryDiv">
              <?php if ($promo_edit_list[0]['promo_sub_cat_id'] > 0) { ?>
              <div class="new-crow cus-hdn-contaier">
                <div class="mu-frmFlds_long">
                  <label>Select Sub Category*</label>
                  <select id="promo_sub_cat"  name="promo_sub_cat_id" onchange="get_cat_prod(this.value)">
                    <option value="">Select </option>
                    <?php

                         for ($n = 0; $n < count($promo_sub_cat_id); $n++){
                                ?>
                    <option <?php if ($promo_edit_list[0]['promo_sub_cat_id'] == $promo_sub_cat_id[$n]['cat_id']) { ?> selected <?php } ?> value="<?= $promo_sub_cat_id[$n]['cat_id']; ?>">
                    <?= $promo_sub_cat_id[$n]['cat_name']; ?>
                    </option>
                    <?php }
                            ?>
                  </select>
                </div>
              </div>
              <?php } ?>
              <div class="error set"> <?php echo form_error('promo_sub_cat_id')?> </div>
            </div>
             
            <div id="cat_prod_Div">
              <?php if ($promo_edit_list[0]['promo_product_id'] > 0) { ?>
              <div class="new-crow cus-hdn-contaier">
                <div class="mu-frmFlds_long">
                  <label>Select Product*</label>
                  <select id="promo_prod"  name="promo_product_id" >
                    <option value="">Select</option>
                    <?php
                         for ($j = 0; $j < count($cat_prod); $j++){
                                ?>
                    <option <?php if ($promo_edit_list[0]['promo_product_id'] == $cat_prod[$j]['prod_id']) { ?> selected <?php } ?> value="<?= $cat_prod[$j]['prod_id']; ?>">
                    <?= $cat_prod[$j]['prod_name']; ?>
                    </option>
                    <?php }
                            ?>
                  </select>
                </div>
              </div>
              <?php } ?>
              <div class="error set"> <?php echo form_error('promo_product_id')?> </div>
            </div>
             
          </div>
        </div>
      </div>  
      <!--///////////////////////////////////////select product end//////////////////////////////////////--> 
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Discount Type</h2>
          <p>Select promo code discount type.</p>
        </div>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
            <div class="mu-flds-wrp">
              <label class="nlbl-set">Select Type*</label>
              <div class="cus-ad-fld-sel">
                <select id="promo_discount_type" onchange="hide_percentage(this.value)" name="promo_discount_type">
                  <option <?php if ($promo_edit_list[0]['promo_discount_type'] == 0) { ?> selected <?php } ?>  value="0" <?php echo set_select('promo_type', '0'); ?>>$USD</option>
                  <option <?php if ($promo_edit_list[0]['promo_discount_type'] == 1) { ?> selected <?php } ?>  value="1" <?php echo set_select('promo_type', '1'); ?>>%Discount</option>
                </select>
              </div>
              
              <div style="display:<?php if ($promo_edit_list[0]['promo_discount_type'] == 1) { ?> none;<?php } ?>" id="doller"  class="cus-ad-tk-dis">
                <label></label>
                <div class="cus-tk-dis-fld "> <span class="dol-line">$</span>
                  <input type="text" value="<?php if ($promo_edit_list[0]['promo_discount_type'] == 1) {  echo ""; } else { echo $promo_edit_list[0]['promo_price_amount']; }?>" name="promo_price_usd" />
                 
                </div>
                 <div class="error"> <?php echo form_error('promo_price_usd')?> </div>
              </div>
              <div style="display:<?php if ($promo_edit_list[0]['promo_discount_type'] == 0) { ?> none;<?php } ?>" id="percentage" class="cus-ad-tk-dis">
                <label></label>
                <div class="cus-tk-dis-fld">
                  <input  type="text" value="<?php if ($promo_edit_list[0]['promo_discount_type'] == 0) {  echo ""; } else { echo $promo_edit_list[0]['promo_price_amount']; }?>" name="promo_price_percentage" />
                  <span>%</span> </div>
              </div>
            </div>
            <div class="error"> <?php echo form_error('promo_price_percentage')?> </div>
          </div>
          
        </div>
        
      </div>
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Promo Code Validity</h2>
          <p>Set promo code validity range.</p>
        </div>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
          	
            <div class="mu-flds-wrp">
            	
            	<div class="mu-frmFlds">
                    <label id="show">Promo code start date*</label>
                    <?php 

 					$promo_start_date = date("m-d-Y", strtotime($promo_edit_list[0]['promo_start_date']));
					$promo_end_date = date("m-d-Y", strtotime($promo_edit_list[0]['promo_end_date']));
					 ?>
                    <input type="text" value="<?php echo $promo_start_date; ?>"  name="promo_start_date" class="dateformate"  id="datepicker-example7-start" placeholder="From" />
                    <div class="error set"><?php echo form_error('promo_start_date'); echo $this->session->flashdata('msg'); ?></div>
                </div>
                <div class="mu-frmFlds mu-flt-rght">
                    <label id="show">Promo code end date*</label>
                    <input type="text"  value="<?php echo $promo_end_date; ?>" name="promo_end_date" id="datepicker-example7-end"  placeholder="From" />
                    <div class="error set"><?php echo form_error('promo_end_date')?></div>
                </div>  
            </div>
            
          </div>
        </div>
      </div>
      <!-- Box along with label section ends here -->
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Promo Code Status</h2>
          <p>Manage your promo code status.</p>
        </div>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
            <div class="mu-frmFlds_long">
              <label>Status*</label>
              <select   name="promo_status">
              	<option value="">Select</option>
                <option <?php if ($promo_edit_list[0]['promo_status'] == 1) { ?> selected <?php } ?> value="1">Active</option>
                <option <?php if ($promo_edit_list[0]['promo_status'] == 0) { ?> selected <?php } ?> value="0">Expired</option>
              </select>
            </div>
            <div class="error set"><?php echo form_error('promo_status')?></div>
            <!-- New Div Starts --> 
            <div class="mu-flds-wrp"> </div>
          </div>
        </div>
      </div>
      <!-- Box wihout label section begins here -->
      
      <div class="mu-fld-sub">
        <input type="submit" value="Submit" />
        <a href="<?php echo base_url(); ?>admin/promocode">cancel</a> </div>
      
      <!-- Box wihout label section ends here --> 
      
      <?php echo form_close();?> </div>
  </div>
</div>
<script>

$('#datepicker-example7-start').Zebra_DatePicker({

  direction: true,
   
  pair: $('#datepicker-example7-end'),
  
});

$('#datepicker-example7-end').Zebra_DatePicker({
	format: 'Y-m-d',
  direction: 1
  
});
$('.dateformate').Zebra_DatePicker({
 
});


</script>

<style type="text/css" media="all">

#scrollWrapper {
	position:absolute; z-index:1;
	top:32px; bottom:0; left:0;
	width:100%;
	overflow:auto;
}

#scroller {
	position:absolute; z-index:1;
/*	-webkit-touch-callout:none;*/
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	width:100%;
	padding:0;
}

#scroller ul {
	list-style:none;
	padding:0;
	margin:0;
	width:100%;
	text-align:left;
}

#scroller li {
	width:96%;
	height:auto;
	margin:0 2%;
	border-bottom:1px solid #f1f1f1;
}
#scroller li a{
	width:96%;
	padding:0 2%;
	display:block;
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	color:#767676;
	text-decoration:none;
	line-height:30px;
	
	background-color:#FFF;
}
#scroller li a:hover{
	background-color:#52ca63;
	color:#FFF;
}
</style>
<!-- Scroll bar css and js ends here -->
</body>
</html>