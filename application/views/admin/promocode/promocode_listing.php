<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Promotions</title>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
    <script>
    function alertshow() {
        document.getElementById("perminum").style.display = "block";
        setTimeout(function() { document.getElementById("perminum").style.display = "none"; }, 9000);

    }
    </script>
    <script>
    $(function() {
        $('.message').append('<span class="close" title="Dismiss"></span>');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
            function() { $(this).addClass('hover'); },
            function() { $(this).removeClass('hover'); }
        );

        $('.message .close').click(function() {
            $(this).parent().fadeOut('slow', function() { $(this).remove(); });
        });
    });
    </script>
</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php 
             
            include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
         ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="col-continer">
                <?php if($this->session->flashdata('msg') != "")
            {
                echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
            }
            ?>
                <div class="col-3 cat-box">
                    <div class="contntTop-row">
                        <div class="actvity-hd">
                            <div class="act-tab b-ad-tb">
                                <table cellspacing="0" border="0">
                                    <colgroup>
                                        <col width="30%" />
                                        <col width="25%" />
                                        <col width="29%" />
                                        <col width="16%" />
                                    </colgroup>
                                    <tr>
                                        <td class="no-bdr">
                                            <div class="cat-tbl-tp-lf">
                                                <h1>Promotions</h1>
                                                <span class="n-of-cat">
                                            <?=$total_records?>
                                            </span>
                                            </div>
                                        </td>
                                        <td id="message" class="pr-msg-bx">
                                        </td>
                                        <td align="left" class="expo-cell"></td>
                                        <td align="center">
                                            <a href="<?=base_url()?>admin/promocode/add" class="pro-addNew"> Add New Promotion </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="b-prd-tbl">
                            <table border="0" cellspacing="0" id="mytable">
                                <colgroup>
                                    <col width="10%" />
                                    <col width="20%" />
                                    <col width="15%" />
                                    <col width="15%" />
                                    <col width="15%" />
                                    <col width="10%" />
                                    <col width="15%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th align="center">Date</th>
                                        <th align="center">Coupon Code</th>
                                        <th align="center">Discount Amount</th>
                                        <th align="center">Start date</th>
                                        <th align="center">End date</th>
                                        <th align="center">Status</th>
                                        <th align="center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($n=0;$n<count($promo_list);$n++) { ?>
                                    <tr id="row<?= $promo_list[$n]['promo_id'] ?>">
                                        <td align="center">
                                            <?=date('m-d-Y', strtotime($promo_list[$n]['promo_created_date'])); ?>
                                        </td>
                                        <td align="center">
                                            <?=$promo_list[$n]['promo_code']?>
                                        </td>
                                        <td align="center">
                                            <?php if($promo_list[$n]['promo_discount_type'] == 0)
                                        {
                                        echo "$".$promo_list[$n]['promo_price_amount'];
                                        }
                                        else
                                        {
                                            echo $promo_list[$n]['promo_price_amount']."%";
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <?=date('m-d-Y', strtotime($promo_list[$n]['promo_start_date'])); ?>
                                        </td>
                                        <td align="center">
                                            <?=date('m-d-Y', strtotime($promo_list[$n]['promo_end_date'])); ?>
                                        </td>
                                        <td align="center">
                                            <?php if($promo_list[$n]['promo_status'] ==1)
                                        {
                                            echo "Active"; 
                                        }
                                        else
                                        {
                                            echo "Expired";
                                        }
                                        ?>
                                        </td>
                                        <td align="center">
                                            <table cellpadding="0" cellspacing="0" border="0" class="nbdr-new">
                                                <colgroup>
                                                    <col width="20%">
                                                    <col width="30%">
                                                    <col width="30%">
                                                    <col width="20%">
                                                </colgroup>
                                                <tbody>
                                                    <tr id="display_tr">
                                                        <td>&nbsp;</td>
                                                        <td align="center">
                                                            <a title="Edit" href="<?= base_url() ?>admin/promocode/edit/<?= $promo_list[$n]['promo_id'] ?>" class="tik-cross-btns p-edt-btn-n"><!--<img src="<?= base_url() ?>images/b-edt-icon.png" alt="" />--></a>
                                                        </td>
                                                        <td align="center" id="delete_<?= $promo_list[$n]['promo_id'] ?>">
                                                            <a title="Delete" onclick="return confirm('Are you sure you want delete this item(s)?');" href="<?php echo base_url(); ?>admin/promocode/delete/<?php echo $promo_list[$n]['promo_id'] ?>" id="<?=$promo_list[$n]['promo_id'] ?>" class="tik-cross-btns discount p-del-btn-n"><!--<img src="<?= base_url() ?>images/b-del-icon.png " alt="" />--></a>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
$(document).ready(function() {
    $('#mytable').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "order": [
            [0, "desc"]
        ],
        "columnDefs": [
            { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>

</html>