<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Add Career</title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>

        <!-- tinymce configration start here.-->
        <script>
            var base_url = '<?php echo base_url(); ?>';
            var ser = '<?php echo $_SERVER['DOCUMENT_ROOT']; ?>educki/resources/tiny_upload';
        </script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>tiny/common.js"></script>
        <!-- tinymce configration end here.-->
        <script type="text/javascript">

            $(document).ready(function () {

            });
        </script>
    </head>

    <body>
        <div class="container_p">
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp">
                <!-- Top Green Bar Section Begins Here -->
                <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Add New Page</h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li>
                                <a href="<?php echo base_url() ?>admin/career">Career</a>
                            </li>
                            <li>
                                <div class="crms-sep">
                                    &gt;
                                </div>
                            </li>
                            <li>
                                <a href="javascript:voide(0);"> Add Career</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <?php echo form_open_multipart('', array('name' => 'add_career')); ?>
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Career</h2>
                            <p>Manage your career.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Career Title*</label>
                                        <input type="text" value="<?php echo set_value('title'); ?>" name="title" placeholder="" />                        
                                    </div>
                                    <div class="error"><?php echo form_error('title') ?></div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Career Field/Area*</label>
                                        <input type="text" value="<?php echo set_value('career_area'); ?>" name="career_area" placeholder="" />
                                    </div>
                                    <div class="error">
                                        <?php echo form_error('career_area') ?>
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <label class="lone-set">Description*</label>
                                    <textarea class="myeditor" name="career_description" id="desc"><?php echo set_value('career_description'); ?></textarea>
                                    <div class="error">
                                        <?php echo form_error('career_description') ?>
                                    </div>
                                </div>

                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>City*</label>
                                        <input type="text" value="<?php echo set_value('city'); ?>" name="city" placeholder="" />
                                    </div>
                                    <div class="error">
                                        <?php echo form_error('city') ?>
                                    </div>
                                </div>

                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>State*</label>
                                        <select name="state">
                                            <option value="">Select State</option>
                                            <?php foreach ($allstate as $state) { ?>
                                                <option <?=set_select('state',$state['stat_id'])?> value="<?= $state['stat_id'] ?>"><?= $state['stat_name'] ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                    <div class="error"><?= form_error('state') ?></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Meta Information</h2>
                            <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta title</label>
                                        <input type="text" value="<?php echo set_value('meta_title'); ?>" name="meta_title" />
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta Keyword</label>
                                        <input type="text" value="<?php echo set_value('meta_keywords'); ?>" name="meta_keywords" />
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta description </label>
                                        <textarea class="text_meta" name="meta_description"><?php echo set_value('meta_description'); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Career Status</h2>
                            <p>Manage your career status</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Status*</label>
                                        <select name="status">
                                            <option value="">Select</option>
                                            <option value="1" <?php echo set_select('status', '1'); ?> >Active</option>
                                            <option value="0" <?php echo set_select('status', '0'); ?> >Inactive</option>
                                        </select>
                                    </div>
                                    <div class="error">
                                        <?php echo form_error('status') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="mu-fld-sub">
                        <input type="submit" value="Submit" />
                        <a href="<?= base_url() ?>admin/career">Cancel</a>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </body>
</html>