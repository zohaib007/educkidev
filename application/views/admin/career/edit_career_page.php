<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php echo $title; ?>
    </title>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <?php include ADMIN_INCLUDE_PATH."includes/js.php";?>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <!-- tinymce configration start here.-->
    <script>
    var base_url = '<?php echo base_url(); ?>';
    var ser = '<?php echo $_SERVER['DOCUMENT_ROOT']; ?>educki/resources/tiny_upload';
    </script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>js/additional-methods.min.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
    <!-- tinymce configration end here.-->
    <script>
    function delete_gall_image(sel, name, fieldname) {
        //          alert(fieldname);
        $('.' + name).hide();
        $('#' + name).val('');
        $.ajax({
            url: base_url + 'admin/career/delete_page_img',
            type: 'POST',
            data: { nId: sel, fieldname: fieldname, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        }).done(function() {
            console.log("success");
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }

    $('.my-form').on('click', '.remove-box', function() {

        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index) {
                $(this).text(index + 1);
            });
        });
        return false;
    });
    </script>
    <script>
    $(function() {
        $('.message').append('<span class="close" title="Dismiss"></span>');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
            function() { $(this).addClass('hover'); },
            function() { $(this).removeClass('hover'); }
        );

        $('.message .close').click(function() {
            $(this).parent().fadeOut('slow', function() { $(this).remove(); });
        });
    });
    </script>
    <script>
    $(document).ready(function() {
        $(".submit_blog_form").on('click', function(e) {
            var data_attr = $(this).attr('data_attr');
            var sub_url = $(this).attr('submit_url');
            if (data_attr == 1) {
                $('#myForm').attr('action', sub_url);
                $("#myForm").attr('target', '_blank');
                $('#myForm').submit();

            } else {

                $('#myForm').attr('action', sub_url);
                $("#myForm").attr('target', '_self');
                $('#myForm').submit();
            }
        });
    });
    </script>
</head>

<body>
    <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include ADMIN_INCLUDE_PATH . "includes/dash-left.php";?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include ADMIN_INCLUDE_PATH . "includes/top_green_bar.php";?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1><?php echo $title; ?></h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/career">Career</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <?php echo $title; ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <?php if ($this->session->flashdata('msg') != "") {
                        echo '<div id="message" class="message info"><p>' . $this->session->flashdata('msg') . '</p></div>';
                    }?>
                    <?php echo form_open_multipart('', array('id' => 'myForm', 'name' => 'edit career page')); ?>
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Page Details</h2>
                            <p>Manage the content on this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Title*</label>
                                        <input type="text" value="<?php echo $page_data->career_page_title; ?>" name="pg_title" />
                                    </div>
                                    <div class="error" id="pg_title_validate"><?php echo form_error('pg_title') ?></div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <label class="lone-set">Description* </label>
                                    <textarea class="myeditor" name="pg_description" id="desc"><?php echo $page_data->career_page_description; ?></textarea>
                                    <div class="error"><?php echo form_error('pg_description') ?></div>
                                </div>                                
                            </div>
                        </div>
                    </div>

                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Banner Image</h2>
                            <p>Upload an banner image for this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Banner Title*</label>
                                        <input type="text" value="<?php echo $page_data->career_page_banner_title; ?>" name="banner_title" />
                                    </div>
                                    <div class="error"><?php echo form_error('banner_title') ?></div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <label for="box1">Image <span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <?php if (@$page_data->career_page_banner_image) {?>
                                                <p class="text-box my-form banner_image">
                                                    <img style="width: 200px;height: 65px;" src="<?=base_url()?>/resources/career_image/career_page/<?=$page_data->career_page_banner_image;?>"></img>                
                                                </p>
                                                <?php }?>
                                                <div class="fileUp">
                                                    <input type="file" name="banner_image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="box1-txt" value="<?=$page_data->career_page_banner_image;?>" readonly="true" name="b_image" />
                                                    <div class="error" id="banner_image_validate">
                                                        <?=form_error('b_image')?>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>First Page Image</h2>
                            <p>Upload an First image for this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>First Image Title*</label>
                                        <input type="text" value="<?php echo $page_data->career_page_title_first; ?>" name="first_image_title" />
                                    </div>
                                    <div class="error"><?php echo form_error('first_image_title') ?></div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>First Image Description </label>
                                        <textarea class="text_meta" name="first_image_description"><?php echo $page_data->career_page_description_first; ?></textarea>
                                        <div class="error"><?php echo form_error('first_image_description') ?></div>
                                    </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <label for="box1">Image <span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <?php if (@$page_data->career_page_image_first) {?>
                                                <p class="text-box my-form first_image">
                                                    <img src="<?=base_url()?>/resources/career_image/career_page/<?=$page_data->career_page_image_first;?>"></img>
                                                    <!-- <input type="hidden" name="first_image" id="first_image" value="<?=@$page_data->career_page_image_first;?>"  /> -->
                                                </p>
                                                <?php }?>
                                                <div class="fileUp">
                                                    <input type="file" name="first_image" value="" id="box1" onchange="javascript: document.getElementById('first_box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="first_box1-txt" value="<?=$page_data->career_page_image_first;?>" name="fir_name" readonly="true" />
                                                    <div class="error" id="first_image_validate">
                                                        <?=form_error('fir_name')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Second Image</h2>
                            <p>Upload an Second image for this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Second Image Title*</label>
                                        <input type="text" value="<?php echo $page_data->career_page_title_second; ?>" name="second_image_title" />                                        
                                    </div>
                                    <div class="error"><?php echo form_error('second_image_title') ?></div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Second Image Description </label>
                                        <textarea class="text_meta" name="second_image_description"><?php echo $page_data->career_page_description_second; ?></textarea>
                                        <div class="error"><?php echo form_error('second_image_description') ?></div>
                                    </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <label for="box1">Image <span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <?php if (@$page_data->career_page_image_second) {?>
                                                <p class="text-box my-form second_image">
                                                    <img src="<?=base_url()?>/resources/career_image/career_page/<?=$page_data->career_page_image_second;?>"></img>                                                        
                                                </p>
                                                <?php }?>
                                                <div class="fileUp">
                                                    <input type="file" name="second_image" value="" id="box1" onchange="javascript: document.getElementById('second_box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="second_box1-txt" value="<?=$page_data->career_page_image_second;?>" name="sec_image" readonly="true" />
                                                </div>
                                                <div class="error" id="second_image_validate">
                                                    <?=form_error('sec_image')?>
                                                </div>                                                        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Third Image</h2>
                            <p>Upload an third image for this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Third Image Title*</label>
                                        <input type="text" value="<?php echo $page_data->career_page_title_third; ?>" name="third_image_title" />
                                    </div>
                                    <div class="error"><?php echo form_error('third_image_title') ?></div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Third Image Description </label>
                                        <textarea class="text_meta" name="third_image_description"><?php echo $page_data->career_page_description_third; ?></textarea>
                                        <div class="error"><?php echo form_error('third_image_description') ?></div>
                                    </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <label for="box1">Third Image <span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <?php if (@$page_data->career_page_image_third) {?>
                                                <p class="text-box my-form third_image">
                                                    <img src="<?=base_url()?>/resources/career_image/career_page/<?=$page_data->career_page_image_third;?>"></img>
                                                </p>
                                                <?php }?>
                                                <div class="fileUp">
                                                    <input type="file" name="third_image" value="" id="box1" onchange="javascript: document.getElementById('third_box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="third_box1-txt" value="<?=$page_data->career_page_image_third;?>" name="thir_image" readonly="true" />
                                                    <div class="error" id="third_image_validate">
                                                        <?=form_error('thir_image')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Meta Information</h2>
                            <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta title</label>
                                        <input type="text" value="<?php echo $page_data->meta_title; ?>" name="meta_title" />
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta Keyword</label>
                                        <input type="text" value="<?php echo $page_data->meta_keyword; ?>" name="meta_keywords" />
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta description </label>
                                        <textarea class="text_meta" name="meta_description"><?php echo $page_data->meta_description; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-fld-sub">
                    <?php if ($this->session->userdata('admin_status') == 1) {?>
                    <a data_attr="0" submit_url="<?=base_url()?>admin/career/edit_career_page/1" class="submit_blog_form" href="javascript:void(0);" id="asd">Submit</a>
                    <a data_attr="1" submit_url="<?=base_url()?>admin/preview/career_preview" class="submit_blog_form" href="javascript:void(0);" id="preview">Preview</a>
                    <a href="<?=base_url()?>admin/career">cancel</a>
                    <?php }?>
                </div>
                                <!-- Box wihout label section ends here -->
                <?php echo form_close(); ?>
        </div>
    </div>
</body>
</html>