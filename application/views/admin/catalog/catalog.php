<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Catalog</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script>
$(function () {
  $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
  //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
  $('.message .close').hover(
    function() { $(this).addClass('hover'); },
    function() { $(this).removeClass('hover'); }
  );
    
  $('.message .close').click(function() {
    $(this).parent().fadeOut('slow', function() { $(this).remove(); });
  });
});
</script>
</head>

<body>
<div class="container_p">

<!-- Dashboard Left Side Begins Here -->

<div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
</div>

<!-- Dashboard Left Side Ends Here -->

<div class="right-rp">
    
    <!-- Top Green Bar Section Begins Here -->
    
    <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
    
    <!-- Top Green Bar Section Ends Here -->
    
    <div class="mu-contnt-wrp">
        <div class="mu-contnt-hdng">
            <h1>Manage Catalog</h1>
        </div>

        <?php echo form_open('', array('name'=>'') ); ?>
        <!-- Box along with label section begins here -->
        
        <div class="mu-contnt-outer">
            <?php 
			if($this->session->flashdata('msg') != "")
			{
				echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
			?>
            <div class="mu-contntBx-wrp" style="width: 100%;">
                <div class="contntTop-row">
                    
                    <?php include(ADMIN_INCLUDE_PATH."includes/catalog.php"); ?>

                    <!-- Inner Section Starts -->
                    <div class="subsec-wrap catStatistic">

                        <!-- Row Starts -->
                        <div class="subsec-row">
                            <!-- Col Starts -->
                            <div class="ssr-col">
                                <div class="ssr-table">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="74%" /> <!-- 558 of 722 72 -->
                                            <col width="26%" />
                                        </colgroup>
                                        <tr>
                                            <th align="left">
                                                <span class="st-htxt">Top 5 Product Categories</span>
                                                <div class="ssr-hsel">
                                                    <!-- <div class="mu-frmFlds_long">
                                                        <select>
                                                            <option selected="">Filter By</option>
                                                            <option>Option 1</option>
                                                            <option>Option 2</option>
                                                        </select>
                                                    </div> -->
                                                </div>
                                            </th>
                                            <th align="right" class="last">Inventory Count</th>
                                        </tr>
                                        <!-- Space Setting Starts -->
                                        <tr class="nspace">
                                            <td align="left">&nbsp;</td>
                                            <td align="right">&nbsp;</td>
                                        </tr>
                                        <!-- Space Setting Starts -->
                                        <?php if(count($top_cat)>0){
                                            foreach ($top_cat as $cat) {?>
                                        <tr>
                                            <td align="left"><?=$cat->cat_name?></td>
                                            <td align="right">
                                                <strong><?=$cat->total?></strong>
                                            </td>
                                        </tr>
                                        <?php }}else{?>
                                        <tr>                                            
                                            <td align="center" colspan="2"><strong>No Data Found</strong></td>
                                        </tr>
                                        <?php }?>
                                    </table>
                                </div>
                            </div>
                            <!-- Col Ends -->
                        </div>
                        <!-- Row Ends -->

                        <!-- Row Starts -->
                        <div class="subsec-row">
                            <!-- Col Starts -->
                            <div class="ssr-col">
                                <div class="ssr-table">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="74%" /> <!-- 558 of 722 72 -->
                                            <col width="26%" />
                                        </colgroup>
                                        <tr>
                                            <th align="left">
                                                <span class="st-htxt">Total Number of Products Listed</span>
                                                <div class="ssr-hsel">
                                                    <!-- <div class="mu-frmFlds_long">
                                                        <select>
                                                            <option selected="">Filter By</option>
                                                            <option>Option 1</option>
                                                            <option>Option 2</option>
                                                        </select>
                                                    </div> -->
                                                </div>
                                            </th>
                                            <th align="center" class="last">&nbsp;</th>
                                        </tr>
                                        <!-- Space Setting Starts -->
                                        <tr class="nspace">
                                            <td align="left">&nbsp;</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <!-- Space Setting Starts -->
                                        <tr>
                                            <td align="left"># Active Sellers</td>
                                            <td align="center">
                                                <strong><?=$seller_data['active_sellers']->ActiveSellers?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"># Sellers on Vacation</td>
                                            <td align="center">
                                                <strong><?=$seller_data['seller_vacation']->SellersOnVacation?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Average Number of Items Per store</td>
                                            <td align="center">
                                                <strong><?=$seller_data['average_prod']?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"># of Items Left in Shopping Cart</td>
                                            <td align="center">
                                                <strong><?=$seller_data['cart_items'][0]->total?></strong>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- Col Ends -->
                        </div>
                        <!-- Row Ends -->

                    </div>
                    <!-- Inner Section Ends -->

                </div>
            </div>
        </div>
        
        <!-- Box along with label section ends here -->

        <!-- Box wihout label section begins here -->
        
        <div class="mu-fld-sub">

            <!-- <input type="submit" value="Submit" />
            <a href="">Cancel</a> </div> -->
            
            <!-- Box wihout label section ends here -->
            
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</body>
</html>