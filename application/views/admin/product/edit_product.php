<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title;?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>

</head>
<body>
<div class="container_p"> 
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
        
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Edit Product</h1>
            </div>
            <!-- Bread crumbs starts here -->
            <div class="n-crums">
                <ul>
                    <li> <a href="<?php echo base_url() ?>admin/product">Product Management</a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);"> Edit Product</a> </li>
                </ul>
            </div>
            <!-- Bread crumbs ends here --> 
            
            <?php echo form_open_multipart('', array('name'=>"form", 'id'=>"myForm")); //"onsubmit"=>"return confirm('Please confirm your information. Are you ready to submit?')" ?> 
            <!-- Box along with label section begins here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Product Details</h2>
                    <!-- <p>Manage your product Status.</p> -->
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <label>Product ID: <?=$prod_data->prod_id?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Product Name: <?=$prod_data->prod_title?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Product For: <?=$prod_data->looking_for?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Condition: <?=$prod_data->condition?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Quantity: <?=$prod_data->qty?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Short Description: <?=$prod_data->prod_description?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Youtube Video: <?=($prod_data->youtube_links!='')?'<a href="'.$prod_data->youtube_links.'">'.$prod_data->youtube_links.'</a>':'N/A'?></label>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Product Categories & Filters</h2>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Categoy: <?=$prod_data->catName?></label>
                            </div>
                            <?php if($prod_data->subCatName != ''){?>
                            <div class="mu-frmFlds_long">
                                <label>Sub Categoy: <?=$prod_data->subCatName?></label>
                            </div>
                            <?php } ?>
                            <?php if($prod_data->subSubCatName != ''){?>
                            <div class="mu-frmFlds_long">
                                <label>Sub Sub Categoy: <?=$prod_data->subSubCatName?></label>
                            </div>
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Product Images</h2>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Image(s):</label>
                                <p>
                                <?php foreach($prodImg as $img){ ?>
                                <img src="<?=base_url('resources/prod_images/thumb/'.$img->img_name)?>" alt="<?=$img->img_name?>">
                            	<?php } ?>
                            	</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Product Price & Shipping Info</h2>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Price: $<?=number_format($prod_data->prod_price, '2', '.', '')?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>On Sale: <?=($prod_data->prod_onsale==1)?'Yes':'No'?></label>
                            </div>
                        </div>
                        <?php if($prod_data->prod_onsale==1){?>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Sale Price: $<?=number_format($prod_data->sale_price, '2', '.', '')?></label>
                            </div>
                        </div>
                        <?php if($prod_data->prod_start_date != ''){?>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Sale Start Date: <?=date('m-d-Y',strtotime($prod_data->sale_price))?></label>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if($prod_data->prod_end_date != ''){?>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Sale End Date: <?=date('m-d-Y', strtotime($prod_data->sale_price))?></label>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Shipping: <?=$prod_data->shipping?></label>
                            </div>
                        </div>
                        <?php if($prod_data->shipping == "Charge for Shipping") { ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Shipping Price: $<?=$prod_data->ship_price?></label>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Shipping Days: <?=$prod_data->ship_days?></label>
                            </div>
                        </div>
                        <?php } else if($prod_data->shipping == "Offer free Shipping" ){ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Shipping Days: <?=$prod_data->free_ship_days?></label>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Returns: <?=$prod_data->prod_return?></label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Product Status & Feature</h2>
                    <!-- <p>Manage your product status.</p> -->
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Status*</label>
                                <select name="prod_status">
                                    <option value="">Select</option>
                                    <option value="1" <?php if (set_select('prod_status', '1') != '') { echo set_select('prod_status', '1');} else if($prod_data->prod_status == '1' ){echo "selected"; }?> >Active</option>
                                    <option value="0" <?php if (set_select('prod_status', '0') != '') { echo set_select('prod_status', '0');} else if($prod_data->prod_status == '0' ){echo "selected"; }?>>Inactive</option>
                                </select>
                            </div>
                            <div class="error"> <?php echo form_error('prod_status')?> </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Is Featured*</label>
                                <select name="prod_feature">
                                    <option value="">Select</option>
                                    <option value="1" <?php if (set_select('prod_feature', '1') != '') { echo set_select('prod_feature', '1');} else if($prod_data->prod_feature == '1' ){echo "selected"; }?> >Yes</option>
                                    <option value="0" <?php if (set_select('prod_feature', '0') != '') { echo set_select('prod_feature', '0');} else if($prod_data->prod_feature == '0' ){echo "selected"; }?>>No</option>
                                </select>
                            </div>
                            <div class="error"> <?php echo form_error('prod_feature')?> </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Box along with label section ends here --> 
            
            <!-- Box wihout label section begins here -->
            
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?=base_url()?>admin/product">cancel</a>
                <!-- <a target="_blank"> href="<?=base_url()?>admin/product">Preview</a> -->
            </div>
            
            <!-- Box wihout label section ends here --> 
            
            <?php echo form_close(); ?> </div>
    </div>
</div>
</body>
</html>