<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title;?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />



<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>


<!-- COLOR PICKER FILES -- COLOR PICKER FILES -- COLOR PICKER FILES -- COLOR PICKER FILES --

<link rel="stylesheet" href="<?= base_url() ?>css/jquery-ui.css" type="text/css" media="all" />
<link href="<?php echo base_url() ?>css/demo_color_picker.css" rel="stylesheet" />
<link href="<?php echo base_url() ?>css/evol.colorpicker.min.css" rel="stylesheet" />
<script src="<?php echo base_url('js/jquery-ui-min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js/evol.colorpicker.min.js" type="text/javascript"></script>

<!-- COLOR PICKER FILES -- COLOR PICKER FILES -- COLOR PICKER FILES -- COLOR PICKER FILES -->


<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>



<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />


<script type="text/javascript">
	
	/*function hidden_file(n)
	{
		var x = $('#box'+n).val();
		document.getElementById('hidden_file'+ n).value = x;
	}*/
	
	function img_name(img_na, no)
	{
		
		document.getElementById('hidden_file'+no).value = img_na;
		document.getElementById('img'+no).value = img_na;
		
	}
	
	function bimg_name(img_na, no)
	{
		
		document.getElementById('bhidden_file'+no).value = img_na;
		document.getElementById('bimg'+no).value = img_na;
		
	}
	/****************************************************************************************/
		jQuery(document).ready(function($){
			
			
			$('.my-form .add-box').click(function(){
				var n = $('.text-box').length + 1;
				var n2 = $('.btext-box').length + 1;
				if(2 < n)
				{
					$('#add_more_file').css('display', 'none');
				}
				else
				{
					$('#add_more_file').css('display', 'block');
				}
				if( 3 < n )
				{
					//$('#add_more_file').css('display', 'none');
					//alert('Stop it!');
					return false;
				}
				
				document.getElementById('total_files').value = n;
				//alert(n +' - > '+n2)
				var box_html = $('<div class="mu-flds-wrp image_div"><div class="mu-frmFlds"><span class="img_dis"><p class="text-box"><label for="box'+n+'">Front Image <span class="box-number">'+n+'</span><span class="sub-text x-set">Image size: 800 x 800</span></label><div class="fileUp"><input type="file" name="file'+n+'" id="box'+n+'" onchange="img_name(this.value, '+n+')" /><input type="text" id="img'+n+'" value="Upload File" readonly="true" /></div><input type="hidden" name="hidden_file'+n+'" id="hidden_file'+n+'" /><p class="error" id="img_ext_error_'+n+'" style="display:none;">Please upload a valid image.</p><p class="error" id="img_error_'+n+'" style="display:none;">This field is required.</p></p></span></div><div class="mu-frmFlds mu-flt-rght"><span class="bimg_dis"><p class="btext-box"><label for="bbox'+n+'">Back Image <span class="bbox-number">'+n+'</span><span class="sub-text x-set">Image size: 800 x 800</span></label><div class="fileUp"><input type="file" name="bfile'+n+'" value="" id="bbox'+n+'" onchange="bimg_name(this.value, '+n+')" /><input type="text" id="bimg'+n+'" value="Upload File" readonly="true" /></div><input type="hidden" name="bhidden_file'+n+'" value="" id="bhidden_file'+n+'" /><p class="error" id="bimg_ext_error_'+n+'" style="display:none;">Please upload a valid image.</p><p class="error" id="bimg_error_'+n+'" style="display:none;">This field is required.</p></p></span></div><a href="javascript:void(0)" class="remove-box" >Remove</a></div> ');
				
				
				box_html.hide();
				$('.my-form div.image_div:last').after(box_html);
				box_html.fadeIn('slow');
				return false;			
								
								
				/*var box_html = $('<span class="img_dis"><p class="text-box"><label for="box' + n + '">Front Image <span class="box-number">' + n + '</span> <span class="sub-text x-set">Image size: 800 x 800</span></label> <div class="fileUp"> <input type="file" name="file'+ n +'" value="" id="box' + n + '" onchange="img_name(this.value,'+ n +')" /><input type="text" id="img'+n+ '" value="Upload File" readonly="true" /></div> <input type="hidden" name="hidden_file'+ n +'" value="" id="hidden_file'+ n +'" /><a href="#" class="remove-box">Remove  </a> <p class="error" id="img_ext_error_'+ n +'" style="display:none;">Please upload a valid image.</p><p class="error" id="img_error_'+ n +'" style="display:none;">This field is required.</p></p></span>');
				box_html.hide();
				$('.my-form span.img_dis:last').after(box_html);
				box_html.fadeIn('slow');
				
				
				var box_html = $('<span class="bimg_dis"><p class="btext-box"><label for="bbox' + n + '">Back Image <span class="box-number2">' + n + '</span> <span class="sub-text x-set">Image size: 800 x 800</span></label> <div class="fileUp"> <input type="file" name="bfile'+ n +'" value="" id="bbox' + n + '" onchange="bimg_name(this.value,'+ n +')" /><input type="text" id="bimg'+n+ '" value="Upload File" readonly="true" /></div> <input type="hidden" name="bhidden_file'+ n +'" value="" id="bhidden_file'+ n +'" /><a href="javascript:void(0)" class="empty-remove-box remove2_'+ n +'"> &nbsp; </a><p class="error" id="bimg_ext_error_'+ n +'" style="display:none;">Please upload a valid image.</p><p class="error" id="bimg_error_'+ n +'" style="display:none;">This field is required.</p></p></span>');
				box_html.hide();
				$('.my-form span.bimg_dis:last').after(box_html);
				box_html.fadeIn('slow');
				return false;*/
			});
			
			
			
			$('.my-form').on('click', '.remove-box', function(){
				
				var n = document.getElementById('total_files').value;
				document.getElementById('total_files').value  =  document.getElementById('total_files').value - 1;
			    //console.log(this);
				//return false;
				//alert(n);
				$(this).parent().fadeOut("slow", function() {
					$(this).remove();
					$('#add_more_file').css('display', 'block');
					$('.box-number').each(function(index){
						$(this).text( index + 1 );
					});
					$('.bbox-number').each(function(index){
						$(this).text( index + 1 );
					});
					
				});
				
				return false;
				
			});
			
			/*
			$('.my-form').on('click', '.remove-box', function(){
				
				var n = document.getElementById('total_files').value ;
				var n1 = document.getElementById('total_files').value  =  document.getElementById('total_files').value - 1;
			    //console.log(this);
				//return false;
				alert(n);
				
				$('.remove2_'+(n1)).parent().fadeOut("slow", function() {
						$(this).remove();
						$('.box-number2').each(function(index){
						$(this).text( index + 1 );
					});
				});
				
				
				$(this).parent().fadeOut("slow", function() {
					$(this).remove();
					$('#add_more_file').css('display', 'block');
					$('.box-number').each(function(index){
						$(this).text( index + 1 );
					});
				});
				
				
				return false;
			});
		
		*/
		
		/************  [Multi category append]  *************/
		
			$('.add_more_cat').click(function(){
				var n = $('.cat_box').length + 1;
				/*if( 3 < n )
				{
					alert('Stop it!');
					return false;
				}*/
				var box_html = $('<div class="cat_box" ><div class="mu-flds-wrp"><div class="mu-frmFlds_long"><label>Select Navigation*</label><select  name="main_cat[]" onchange="getmiddleCategory(this.value,'+ n +');"><option value="">Select </option><?php foreach($main_cat as $val) { ?><option <?php if(set_value('main_cat') == $val['cat_id']) { ?> selected <?php }?> value="<?=$val['cat_id']?>"><?php echo $val['cat_name'];?></option><?php } ?></select><div class="error"> <?php echo form_error('main_cat[]'); ?> </div></div></div><div id="middlecategoryDiv'+ n +'"></div><a href="javascript:void(0)" class="remove_box_cat">Remove</a></p>');
				box_html.hide();
				$('.cat_box:last').after(box_html);
				box_html.fadeIn('slow');
				return false;
			});
			
			
			$('.cat_form').on('click', '.remove_box_cat', function(){
				$(this).parent().fadeOut("slow", function() {
					$(this).remove();
					/*$('.box-number').each(function(index){
						$(this).text( index + 1 );
					});*/
				});
				return false;
			});
			
			
			
	
			/*** ADD MORE COLOR ***************/
			$('.add_more_color').click(function(){
				
				//var box_html = $('<span class="color_dis"><p class="ctext-box"> <div class="fileUp"> <input type="file" name="file'+ n +'" value="" id="box' + n + '" onchange="img_name(this.value,'+ n +')" /><input type="text" id="img'+n+ '" value="Upload File" readonly="true" /></div> <input type="hidden" name="hidden_file'+ n +'" value="" id="hidden_file'+ n +'" /><a href="#" class="remove-box">Remove  </a> <p class="error" id="img_ext_error_'+ n +'" style="display:none;">Please upload a valid image.</p><p class="error" id="img_error_'+ n +'" style="display:none;">This field is required.</p></p></span>');
				
				//var box_html = $('<span class="color_dis"><p class="ctext-box"><label>Color*</label><input type="text" readonly="readonly" style="width:50%;" class="cpButton" value="<?php echo set_value('prod_color[]'); ?>" name="prod_color[]"  /><div class="sub-label">Select Color</div><a href="#" class="remove-box remove-color">Remove  </a> </p></span>');
				var n = $('.ctext-box').length + 1;
				var box_html = $('<span class="color_dis"><p class="ctext-box"><label>Color <span class="bbox-number">'+n+'</span></label><input type="text" style="width:50%;" name="prod_color[]"  /><a href="#" class="remove-box remove-color">Remove </a> </p></span>');
				//value="<?php echo set_value("prod_color[]"); ?>"
				box_html.hide();
				$('.my-formc span.color_dis:last').after(box_html);
				box_html.fadeIn('slow');
				return false;
				
			});
			
			$('.my-formc').on('click', '.remove-color', function(){
				$(this).parent().fadeOut("slow", function() {
					$(this).remove();
					$('.cbox-number').each(function(index){
						$(this).text( index + 1 );
					});
				});
				return false;
			});
			
			
		
		});
		
		
		function getmiddleCategory(sel, display_div)
		{
			if(sel == '')
			{
				$("#middlecategoryDiv"+display_div).hide();
			}
			else
			{
				$("#middlecategoryDiv"+display_div).show();
			}
			
			$("#middlecategoryDiv"+display_div).load(base_url+'admin/product/getmiddleCategory', {"nId":sel, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} );
		}    
		
	
		/*function getsubCategory(sel)
		{
			$("#subcategoryDiv").load(base_url+'admin/product/get_sub_cat', {"nId":sel, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} );
		}  */  
		
	
	$(document).ready(function(){
		var sale_val = $("select[name='prod_onsale']").val();
		
		if(sale_val == '1')
		{
			$('#on_sale_id').css('display', 'block');
		}
		else
		{
			$('#on_sale_id').css('display', 'none');
		}
		
	});
	
	
	function show_div(n, id)
	{
		if(n==1)
		{
			$('#'+id).css('display', 'block');
		}
		else
		{
			$('#'+id).css('display', 'none');
		}
	}



	
	
	//function related_product_data(one, one_listing)
	function related_product_data(field_name, id_name)
	{
		//alert(field_name+" -->"+id_name)
		var prod1, prod2, prod3, prod4, prod5;
		var prod6 = '';
		//$("input[name='"+field_name+"']").keyup(function(){
			
			if(field_name == 'one')
			{
				prod1 = document.getElementById('one').value;
				prod2 = document.getElementById('two_id').value;
				prod3 = document.getElementById('three_id').value;
				prod4 = document.getElementById('four_id').value;
				prod5 = document.getElementById('five_id').value;
				//prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'two')
			{
				prod1 = document.getElementById('two').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('three_id').value;
				prod4 = document.getElementById('four_id').value;
				prod5 = document.getElementById('five_id').value;
				//prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'three')
			{
				prod1 = document.getElementById('three').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('four_id').value;
				prod5 = document.getElementById('five_id').value;
				//prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'four')
			{
				prod1 = document.getElementById('four').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('three_id').value;
				prod5 = document.getElementById('five_id').value;
				//prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'five')
			{
				prod1 = document.getElementById('five').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('three_id').value;
				prod5 = document.getElementById('four_id').value;
				//prod6 = document.getElementById('six_id').value;
			}
			
			/*
			else if(field_name == 'six')
			{
				prod1 = document.getElementById('six').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('three_id').value;
				prod5 = document.getElementById('four_id').value;
				prod6 = document.getElementById('five_id').value;
			}
			*/
		
			//var input_name = "'"+field_name+"'";
			
			if(prod1.length >= 2)
			{
				$.ajax({
					type: "POST",
					url: base_url + "admin/product/related_products/"+field_name,
					
					data:{prod1:prod1,prod2:prod2, prod3:prod3, prod4:prod4, prod5:prod5, prod6:prod6, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},			
					
					success: function(res)
					{
						//alert(res);
						if(res == '')
						{
							$("."+field_name).hide();
						}
						else
						{
							$("."+field_name).show();
							$("#"+id_name).html(res);
						}
					},
					error: function(){
						
						$("."+field_name).show();
						$("#"+id_name).html('No result found.');
						//alert('failure');
					}
				});
			}
			else if(prod1.length <= 1)
			{
				$("."+field_name).hide();
				$("input[name='"+field_name+"_id']").val('');
			}
		//});
	
	}

	function get_related_product(field_name, prod_id, prod_name)
	{
		//alert(field_name+'-->'+prod_id+'-->'+prod_name);
		$("input[name='"+field_name+"']").val(prod_name);
		$("input[name='"+field_name+"_id']").val(prod_id);
		$("."+field_name).hide();
	}


	function get_prod_name(value)
	{
		//alert(value);
		$("input[name='name']").val(value);	
		$(".dyn-listing").hide();
	}
	
	
	
	
	$("#box1").change(function(){
		var x = $("#box1").val();
		alert (x);
		
	});
	
/*
Sir Ali new code 10-11-2015
$(document).ready(function(){
	
	
	
	$("input[name='fleft']").keyup(function(){
		$(".fleft").show();
	});
	
	$("input[name='fleft']").focusout(function(){
		$(".fleft").hide();
	});
	//first Left end
	
	
});

function get_prod_name(value)
{
	//alert(value);
	$("input[name='name']").val(value);	
	$(".dyn-listing").hide();
}
*/

</script>
</head>
<body>
    <div class="container_p"> 
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        
        <div class="right-rp"> 
            
            <!-- Top Green Bar Section Begins Here -->
            <?php
                include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
                ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="mu-contnt-wrp">
                <div class="mu-contnt-hdng">
                    <h1>Add New Product</h1>
                </div>
                <!-- Bread crumbs starts here -->
                <div class="n-crums">
                    <ul>
                        <li> <a href="<?php echo base_url() ?>admin/product">Product Management</a> </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li> <a href="javascript:void(0)"> Add Product</a> </li>
                    </ul>
                </div>
                <!-- Bread crumbs ends here --> 
                <!--      <form method="post" name="form" action="<?php echo base_url();?>admin/product/prod_add" enctype="multipart/form-data">--> 
                <?php echo form_open_multipart('', array('name'=>"form", 'id'=>"myForm")); ?> 
                <!-- Box along with label section begins here -->
                
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Product Details</h2>
                        <p><!--Provide ID, name, category and other details.-->Manage your product details.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <label>Product ID*</label>
                                    <input type="text" placeholder="" value="<?php echo set_value('prod_sku'); ?>" name="prod_sku" class="spaceset-small" />
                                    <div class="error"> <?php echo form_error('prod_sku')?> </div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Product Name*</label>
                                    <input type="text" value="<?php echo set_value('prod_name'); ?>" name="prod_name" placeholder="" class="spaceset-large" />
                                    <div class="error"> <?php echo form_error('prod_name')?> </div>
                                </div>
                                
                            </div>
                            
                            <div class="cat_form">
                                <div class="cat_box">
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Select Navigation*</label>
                                            <select name="main_cat[]" onchange="getmiddleCategory(this.value,1);"   >
                                                <option value="">Select </option>
                                                <?php foreach($main_cat as $val) { ?>
                                                <option <?php if(set_value('main_cat') == $val['cat_id']) { ?> selected <?php }?> value="<?=$val['cat_id']?>"> <?php echo $val['cat_name'];?> </option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden"  name="category" placeholder="" />
                                            <div class="error"> <?php echo form_error('category'); ?> </div>
                                        </div>
                                    </div>
                                    <div id="middlecategoryDiv1"></div>
                                </div>
                                <!--<div id="subcategoryDiv"></div>--> 
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="nlink-div"> <a href="javascript:void(0)" class="add_more_cat" title="Add More">Add More</a> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <label class="lone-set">Description</label>
                                <textarea class="ckeditor" name="prod_dec" id="desc" ><?php echo set_value('prod_dec'); ?></textarea>
                                <?php echo ckeditor('prod_dec'); ?>
                            </div>
                            <div class="error"> <?php echo form_error('prod_dec'); ?> </div>
                            <!--<div class="mu-flds-wrp">
                                <label class="lone-set">Content Details</label>
                                <textarea  class="ckeditor" name="prod_content" id="desc1" ><?php echo set_value('prod_content'); ?></textarea>
                                <?php echo ckeditor('prod_content'); ?>
                            </div>
                            <div class="error"> <?php echo form_error('prod_content'); ?> </div>
                            <div class="mu-flds-wrp">
                                <label class="lone-set">Shipping Details</label>
                                <textarea  class="ckeditor" name="prod_shipping" id="desc2" ><?php echo set_value('prod_shipping'); ?></textarea>
                                <?php echo ckeditor('prod_shipping'); ?>
                            </div>
                            <div class="error"> <?php echo form_error('prod_shipping'); ?> </div>-->
                        </div>
                    </div>
                </div>
                
                <!-- Images Section Starts -->
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Images</h2>
                        <p>Upload image(s) for this product.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                        	<div class="my-form">
                                <div class="mu-flds-wrp image_div">
                                    <div class="mu-frmFlds">
                                    	<span class='img_dis'>
                                            <p class="text-box">
                                                <label for="box1">Front Image* <span class="box-number">1</span> 
                                                <span class="sub-text x-set">Image size: 800 x 800</span></label>
                                                <div class="fileUp">
                                                <input type="file" name="file1"  value="" id="box1" onchange="img_name(this.value, 1)" />
                                                <input type="text" id="img1" value="Upload File" readonly="true" />
                                                </div>
                                                <input type="hidden" name="hidden_file1" value="" id="hidden_file1" />
                                                <input type="hidden" name="total_files" value="1" id="total_files" />
                                                <p class="error" id="img_ext_error_1" style="display:none;">Please upload a valid image.</p>
                                                <p class="error" id="img_error_1" style="display:none;">This field is required.</p>
                                            </p>
                                        </span>
                                        <div class="error"> <?php echo form_error('hidden_file1')?> </div>
                                        <div class="error"> <?php echo form_error('hidden_file2')?> </div>
                                        <div class="error"> <?php echo form_error('hidden_file3')?> </div>
                                        <?php
                                        if($this->session->flashdata('msg_err0') != "")
                                        {
                                            echo '<div class="error ">'.$this->session->flashdata('msg_err0').'</div>';
                                        }
                                        else if($this->session->flashdata('msg_err1') != "")
                                        {
                                            echo '<div class="error ">'.$this->session->flashdata('msg_err1').'</div>';
                                        }
                                        else if($this->session->flashdata('msg_err2') != "")
                                        {
                                            echo '<div class="error ">'.$this->session->flashdata('msg_err2').'</div>';
                                        }
                                        ?>
                                        <?php if($this->session->flashdata('msg_err') != ''){ echo '<p class="error">'.$this->session->flashdata('msg_err').'</p>'; } ?>
                                    </div>
                                    
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <span class='bimg_dis'>
                                            <p class="btext-box">
                                                <label for="bbox1">Back Image* <span class="bbox-number">1</span> 
                                                <span class="sub-text x-set">Image size: 800 x 800</span></label>
                                                <div class="fileUp">
                                                <input type="file" name="bfile1"  value="" id="bbox1" onchange="bimg_name(this.value, 1)" />
                                                <input type="text" id="bimg1" value="Upload File" readonly="true" />
                                                </div>
                                                <input type="hidden" name="bhidden_file1" value="" id="bhidden_file1" />
                                                <input type="hidden" name="btotal_files" value="1" id="btotal_files" />
                                                <p class="error" id="bimg_ext_error_1" style="display:none;">Please upload a valid image.</p>
                                                <p class="error" id="bimg_error_1" style="display:none;">This field is required.</p>
                                            </p>
                                        </span>
                                        <div class="error"> <?php echo form_error('bhidden_file1')?> </div>
                                        <div class="error"> <?php echo form_error('bhidden_file2')?> </div>
                                        <div class="error"> <?php echo form_error('bhidden_file3')?> </div>
                                        <?php
                                        if($this->session->flashdata('bmsg_err0') != "")
                                        {
                                            echo '<div class="error ">'.$this->session->flashdata('bmsg_err0').'</div>';
                                        }
                                        else if($this->session->flashdata('bmsg_err1') != "")
                                        {
                                            echo '<div class="error ">'.$this->session->flashdata('bmsg_err1').'</div>';
                                        }
                                        else if($this->session->flashdata('bmsg_err2') != "")
                                        {
                                            echo '<div class="error ">'.$this->session->flashdata('bmsg_err2').'</div>';
                                        }
                                        ?>
                                        <?php if($this->session->flashdata('bmsg_err') != ''){ echo '<p class="error">'.$this->session->flashdata('bmsg_err').'</p>'; } ?>
                                            
									</div>
                                    
                                    
                                </div>
                                
                                <p style="clear: both;"><a class="add-box" id="add_more_file" href="javascript:void(0)">Add More</a></p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Images Section Ends -->
                
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Prices &amp; Variants</h2>
                        <p>Manage price, inventory and sale details.</p>
                        <!--<p> Manage inventory, and configure the options for selling this product. </p>-->
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <label>Price*</label>
                                    <input type="text" placeholder="0.00" value="<?php echo set_value('prod_price'); ?>" name="prod_price" class="spaceset-small" />
                                    <div class="error"> <?php echo form_error('prod_price')?> </div>
                                </div>
                                <div class="mu-frmFlds mu-flt-rght">
                                    <label>Inventory*</label>
                                    <input type="text" placeholder="0" value="<?php echo set_value('prod_qty'); ?>" name="prod_qty" class="spaceset-small" />
                                    <div class="error"> <?php echo form_error('prod_qty')?> </div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <label>Sale*</label>
                                    <select name="prod_onsale" id="prod_onsale" onchange="show_div(this.value,'on_sale_id');">
                                        <option value="">Select </option>
                                        <option value="1" <?php echo set_select('prod_onsale', '1'); ?> >Active</option>
                                        <option value="0" <?php echo set_select('prod_onsale', '0'); ?>>Inactive</option>
                                    </select>
                                    <div class="error"> <?php echo form_error('prod_onsale')?> </div>
                                    
                                    <!--<div id="show_vender_div" style="display:block;" class="mu-flds-wrp ntxtbox">-->
                                    <div id="on_sale_id" style="display:none;" class="mu-flds-wrp ntxtbox">
                                        <input type="text" value="<?php echo set_value('prod_sale_price'); ?>" placeholder="Enter Sale Price" name="prod_sale_price" class="spaceset-small" />
                                        <div class="error"> <?php echo form_error('prod_sale_price');?> </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
                
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Shipping Information</h2>
                        <p>Manage USPS shipping details.</p>
                        <!--<p> Manage inventory, and configure the options for selling this product. </p>-->
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <label>Shipping Service*</label>
                                    <select name="prod_ship_box" id="prod_ship_box" >
                                        <option value="">Select </option>
                                        <option value="smallBox" <?php echo set_select('prod_ship_box', 'smallBox'); ?> >Priority Mail 2-Day Small Flat Rate Box</option>
                                        <option value="mediumBox" <?php echo set_select('prod_ship_box', 'mediumBox'); ?>>Priority Mail 2-Day Medium Flat Rate Box</option>
                                        <option value="largeBox" <?php echo set_select('prod_ship_box', 'largeBox'); ?>>Priority Mail 2-Day Large Flat Rate Box</option>
                                    </select>
                                    <div class="error"> <?php echo form_error('prod_ship_box')?> </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
                
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Product Colors</h2>
                        <p>Manage your product color.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds">
                                    <div class="my-formc">
                                    	<span class='color_dis'>
                                            <p class="ctext-box">
                                                <label>Color* <span class="cbox-number">1</span></label>
                                                <input type="text" autocomplete="off" style="width:50%;" value="<?php echo @$_POST['prod_color'][0]; /*set_value("prod_color[0]");*/ ?>" name="prod_color[]" onkeyup="document.getElementById('prod_color1').value = this.value;" />
                                                <input type="hidden" style="width:50%;" value="<?php echo @$_POST['prod_color'][0]; ?>" id="prod_color1" name="prod_color1"  />
                                                <a href="javascript:void(0)" class="empty-remove-box"> </a>
                                            </p>
                                            <div class="error"> <?php echo form_error('prod_color1'); ?> </div>
                                        </span>
                                        <p style="clear: both;"><a class="add_more_color" href="javascript:void(0)">Add More</a> </p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>May We Suggest</h2>
                        <p>Add suggestions for this product.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row"> 
                            
                            <!-- Product 1 Starts -->
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>First Product</label>
                                    <input type="text" name="one" id="one" autocomplete="off"  value=""  onkeyup="related_product_data('one','one_list');" class="spaceset-large" />
                                    <input type="hidden" name="one_id" id="one_id" value="" />
                                    <!-- Drop Starts -->
                                    <div class="dyn-list2 one">
                                        <div class="dyn-scroll">
                                            <ul id="one_list">
                                            
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Drop Ends --> 
                                </div>
                            </div>
                            <!-- Product 1 Ends --> 
                            
                            <!-- Product 2 Starts -->
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Second Product</label>
                                    <input type="text" name="two" id="two" autocomplete="off"  value=""  onkeyup="related_product_data('two','two_list');" class="spaceset-large" />
                                    <input type="hidden" name="two_id" id="two_id" value="" />
                                    <!-- Drop Starts -->
                                    <div class="dyn-list2 two">
                                        <div class="dyn-scroll">
                                            <ul id="two_list">
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Drop Ends --> 
                                </div>
                            </div>
                            <!-- Product 2 Ends --> 
                            
                            <!-- Product 3 Starts -->
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Third Product</label>
                                    <input type="text" name="three" id="three" autocomplete="off"  value=""  onkeyup="related_product_data('three','three_list');" class="spaceset-large" />
                                    <input type="hidden" name="three_id" id="three_id" value="" />
                                    <!-- Drop Starts -->
                                    <div class="dyn-list2 three">
                                        <div class="dyn-scroll">
                                            <ul id="three_list">
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Drop Ends --> 
                                </div>
                            </div>
                            <!--<div class="error"> <?php echo form_error('relp_prod_three_id')?> </div>
                                 Product 3 Ends --> 
                            
                            <!-- Product 4 Starts -->
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Fourth Product</label>
                                    <input type="text" name="four" id="four" autocomplete="off"  value=""  onkeyup="related_product_data('four','four_list');" class="spaceset-large" />
                                    <input type="hidden" name="four_id" id="four_id" value="" />
                                    <!-- Drop Starts -->
                                    <div class="dyn-list2 four">
                                        <div class="dyn-scroll">
                                            <ul id="four_list">
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Drop Ends --> 
                                </div>
                            </div>
                            <!--<div class="error"> <?php echo form_error('relp_prod_four_id')?> </div>
                                 Product 4 Ends --> 
                            
                            <!-- Product 5 Starts -->
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Fifth Product</label>
                                    <input type="text" name="five" id="five" autocomplete="off"  value=""  onkeyup="related_product_data('five','five_list');" class="spaceset-large" />
                                    <input type="hidden" name="five_id" id="five_id" value="" />
                                    <!-- Drop Starts -->
                                    <div class="dyn-list2 five">
                                        <div class="dyn-scroll">
                                            <ul id="five_list">
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Drop Ends --> 
                                </div>
                            </div>
                            <!--<div class="error"> <?php echo form_error('relp_prod_five_id')?> </div>
                                Product 5 Ends --> 
                            
                            <!-- Product 6 Starts -->
                            <!--
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Sixth Product</label>
                                    <input type="text" name="six" id="six" autocomplete="off"  value=""  onkeyup="related_product_data('six','six_list');" class="spaceset-large" />
                                    <input type="hidden" name="six_id" id="six_id" value="" />
                                    <!- Drop Starts ->
                                    <div class="dyn-list2 six">
                                        <div class="dyn-scroll">
                                            <ul id="six_list">
                                            </ul>
                                        </div>
                                    </div>
                                    <!- Drop Ends -> 
                                </div>
                            </div>
                            -->
                            <!-- <div class="error"> <?php echo form_error('relp_prod_six_id')?> </div>
                                 Product 6 Ends --> 
                            
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Meta Information</h2>
                        <p>Set up the meta description. These help define how this product shows up on search engines.</p>
                        <!--<p> Set up the meta title, meta keyword and meta description. These help define how this product shows up on search engines. </p>-->
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Title<!-- 0 of 70 characters used--></label>
                                    <input type="text" value="<?php echo set_value('prod_meta_title');?>" name="prod_meta_title" class="spaceset-large" />
                                    <div class="error"> <?php echo form_error('prod_meta_title')?> </div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Keyword</label>
                                    <input type="text" value="<?php echo set_value('prod_keyword');?>" name="prod_keyword" class="spaceset-large" />
                                    <div class="error"> <?php echo form_error('prod_keyword')?> </div>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Description<!-- 0 of 160 characters used--></label>
                                    <textarea class="text_meta spaceset-large" name="prod_meta_des"><?php echo set_value('prod_meta_des');?></textarea>
                                    <div class="error"> <?php echo form_error('prod_meta_des')?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Product Status</h2>
                        <p>Manage your product status.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="prod_status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select('prod_status', '1'); ?> >Active</option>
                                        <option value="0" <?php echo set_select('prod_status', '0'); ?> >Inactive</option>
                                    </select>
                                </div>
                                <div class="error"> <?php echo form_error('prod_status')?> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-fld-sub">
                    <input type="submit" value="Submit" />
                    <a href="<?=base_url()?>admin/product">cancel</a> 
                </div>
                
                <!-- Box wihout label section ends here --> 
                
                <?php echo form_close(); ?> </div>
        </div>
    </div>
</body>



</html>