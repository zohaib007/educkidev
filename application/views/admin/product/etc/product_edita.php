<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Product Management</title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<script type="text/javascript" src="<?=base_url()?>js/commonAjax.js"></script>
 <script>
function alertshow()
{
	document.getElementById("perminum").style.display="block";
	 setTimeout(function(){document.getElementById("perminum").style.display="none";}, 9000);
					
}
</script>
</head>

<body>
<div class="container">

	<!-- Dashboard Left Side Begins Here -->
	<div class="left_wrp">
    	
    	<?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp">
    	
        <!-- Top Green Bar Section Begins Here -->
    	<?php 
			 
			include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
		 ?>
        <!-- Top Green Bar Section Ends Here -->
        
        <div class="col-continer">
        
            <div class="col-3 cat-box">
                <div class="contntTop-row">
                    <div class="actvity-hd">
                        <div class="act-tab b-ad-tb">
                            <table cellspacing="0" border="0">
                                <colgroup>
                                    <col width="20%" />
                                    <col width="35%" />
                                    <col width="25%" />
                                    <col width="15%" />
                                    <!--<col width="15%" />-->
                                </colgroup>
                                <tr>
                                    <td class="no-bdr">
                                        <div class="cat-tbl-tp-lf">
                                            <h1>Product Management</h1>
                                        </div>
                                    </td>
                                    <td id="message" class="pr-msg-bx"><div class="errorMsg-large" style="display:none;" id="perminum" >This product is already a premium product. Please select another.</div><?php echo $this->session->flashdata('msg'); ?></td>
                                    

                                    <td align="left" class="expo-cell">
                                        <!--<a href="<?=base_url()?>admin/product/add_products">
                                        	Lead Generation
                                            <img src="<?=base_url()?>images/l-gen-icon.png" alt="" />
                                        </a>-->
                                        <span>Export to:</span>
                                        <form method="post" name="MyFormExport">
                                       			&nbsp;&nbsp;&nbsp;<a href="<?=base_url()?>admin/product/exportcsv">CSV</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?=base_url()?>admin/product/exportexcelxml">Excel XML</a>
                                        	<!--<select name="export" id="export">
                                            	<option value="1">CSV</option>
                                                <option value="2">Excel XML</option>
                                                
                                            </select>-->
                                        </form>
                                       <!-- <a href="#."><img src="<?=base_url()?>images/expo-btn.png" alt="" /></a>-->
                                    </td>
                                    <td align="center">
                                         <a href="<?=base_url()?>admin/product/add_products/" class="p-n-a-lnk">
                                        	Add New Product
                                            <img class="p-na-icon-d" src="<?=base_url()?>images/add-icon.png" alt="" />
                                            <img class="p-na-icon-h" src="<?=base_url()?>images/add-icon-hvr.png" alt="" />
                                        </a> 
                                    </td>
                                    <!--<td align="center">
                                        <a href="#">
                                        	Product Metrics
                                            <img src="<?=base_url()?>images/b-p-mrt-icon.png" alt="" />
                                        </a>
                                    </td>-->

                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="b-srch-bl-sec">
                    
                    	<table border="0" width="100%" cellspacing="0">
                        	<colgroup>
                            	<col width="10%" />
                                <col width="15%" />
                                <col width="20%" />
                                <col width="10%" />
                                <col width="45%" />
                            </colgroup>
                            <tr>
                                <form method="post" action="" name="myForm">
                                	<td align="center"><span>Search by</span></td>
                                    <td align="center" >
                                        <select name="srchOp" class="catName">
                                            <option value="">----</option>
                                            <option value="1">Company</option>
                                            <option value="2">Product Category</option>
                                            <option value="3">Premium</option>
                                            <option value="4">Basic</option>
                                             <option value="5">Product Name</option>
                                            <option value="6">Wating for Admin Approval</option>
                                           
                                            
                                        </select>
                                    </td>
                                    
                                    <td align="center">
                                        <input type="text" name="searchText" class="catName" />
                                    </td>  
                                    
                                    <td align="center">
                                    	<input type="submit" value="" class="catSrch" />
                                    </td>
                                    <td align="center"></td>
                                </form>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="b-prd-tbl">
                    <form name="myForm" method="post" action="<?=base_url()?>admin/product/del/">
                   	 <input type="hidden" name="delIds" id="delIds" />  
                    	<table border="0" cellspacing="0">
                        	<colgroup>
                            	<col width="2%" />
                                <col width="17%" />
                            	<col width="15%" />
                                <col width="15%" />
                                <col width="15%" />                                
                                <col width="14%" />                                
                                <col width="12%" />
                                <col width="4%" />
                                <col width="4%" />
                                <col width="4%" />
                               
                            </colgroup>
                            <tr>
                            	<!--<th align="center">ID</th>-->
                                <th align="center"><input type='checkbox'   id='chkall' name='chkAll' onclick="selAll()";></th>
                            	<th align="center">Image</th>
                            	<th align="center">Product</th>
                                <th align="center">Inventory</th>
                                <th align="center">Category</th>                               
                                <th align="center">status</th>
                               
                                <th align="center">Action</th>
                                <th align="center"></th>
                            </tr>
                          <?php
                      // print_r($resultsChannel);
                       //echo $resultsChannel[0]['prod_id'];
                          
                          	for($n=0;$n<count($resultsChannel);$n++)
							{
                                    
                                   $image=$this->product_model->getfirst_image($resultsChannel[$n]['prod_id']); 
                                   //echo $image;
                                    
                                    ?>
						
                            <tr <?php if(($n%2)==0) echo "bgcolor='#ebeef0'";?> id="row<?=$resultsProd[$n]['prod_id']?>">
                            	 
                                  
                                    <td><input type="checkbox" name="chk[]" id="chk[]" value="<?=$resultsProd[$n]['prod_id']?>"  onclick='checkDel()' /></td>                                
                                    <td align="center"><img id="img<?=$resultsChannel[$n]['prod_image_path']?>" src="<?=base_url()?>images/test_img/thumb/<?php echo $image ?>" alt="" /></td>                                    
                                    <td align="center"><?=$resultsChannel[$n]['prod_title']?></td>                                    
                                    <td align="center"><?=$resultsChannel[$n]['prod_quantity']?> in stock</td>                                    
                                   
                                    
                                    <td align="center" id="">
                                        <?=$resultsChannel[0]['cat_name']?>
                                  
                                    </td>
                                      <?php 
										$strImg2="tick-btn.png";
										$status2="InActive";
										$bStats2=1;
										if($resultsProd[$n]['prod_vendor_approval']==1)
										{
											$strImg2="cross-btn.png";
											$status2="Active";
											$bStats2=0;
										}
									?>
                                    
                                    <td align="center" id="">
                                        <a href="#." class="tik-cross-btns"><img id="img<?=$resultsProd[$n]['prod_id']?>" src="<?=base_url()?>images/<?php echo $strImg2;?>" alt="" /></a>
                                    </td>
                                                                      
                                    <td align="center">
                                        <a href="<?=base_url()?>admin/product/editProduct/<?=$resultsProd[$n]['prod_id']?>" class="tik-cross-btns p-edt-btn-n"><!--<img src="<?=base_url()?>images/b-edt-icon.png" alt="" />--></a>
                                    </td>                                    
                                     <td align="center" id="delete_<?=$resultsProd[$n]['prod_id']?>">
                                        <a href="#."  class="tik-cross-btns prodDelete p-del-btn-n"><!--<img src="<?=base_url()?>images/b-del-icon.png " alt="" />--></a>
                                    </td>
                                    
                            </tr>
                           <?php
							}
						   ?> 
                         </table>
                        <ul class="ad-options">
                        	
                            <li id="2"><a href="" class="actionProd">Activate All</a></li>
                            <li><div class="ad-op-sep"></div></li>
                            <li id="1"><a href="" class="actionProd">Deactivate All</a></li>
                            
                            
                            <!--<li><div class="ad-op-sep"></div></li>
                            <li><a href="javascript:actionProd(1);">Premium</a></li>
                            <li><div class="ad-op-sep"></div></li>
                            <li><a href="javascript:actionProd(2);">Basic</a></li>-->
                            
                            
                            
                            <li><div class="ad-op-sep"></div></li>
                            <li id="3"><a href="" class="actionProd">Delete All</a></li>
                        </ul>
                            
                         </form>
                        <div class="pagination-row" style="width:100%">
                        	<?=$paginglink?>
                        </div>
                    </div>
                     
                </div>
               
            </div>
            
        </div>
        
    </div>
</div>

</body>
</html>