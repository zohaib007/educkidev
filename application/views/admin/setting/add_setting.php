<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php echo $title; ?>
    </title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
    <script>
    $(function() {
        $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
            function() { $(this).addClass('hover'); },
            function() { $(this).removeClass('hover'); }
        );

        $('.message .close').click(function() {
            $(this).parent().fadeOut('slow', function() { $(this).remove(); });
        });
    });
    </script>
</head>

<body>
    <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="mu-contnt-wrp">
                <div class="mu-contnt-hdng">
                    <h1>Settings</h1>
                </div>
                <?php
            if($this->session->flashdata('msg') != ""){
                echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
            }
            ?>
                    <?php echo form_open_multipart('', array('name'=>'form')); ?>
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Site Details</h2>
                            <p>Manage the general information on your website.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Site Name</label>
                                        <input type="text" value="<?=$value->name; ?>" name="name" placeholder="Name" />
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Site Email </label>
                                        <input type="text" value="<?=$value->site_email; ?>" placeholder="Site email" name="site_email" />
                                        <div class="error">
                                            <?php echo form_error('site_email')?>
                                        </div>
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label>Site Phone</label>
                                        <input type="text" value="<?=$value->site_phone; ?>" name="site_phone" placeholder="Phone" />
                                        <div class="error">
                                            <?php echo form_error('site_zip')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer" style="display: none;">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Mailing Address</h2>
                            <p>This address will appear on your contact us page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Mailing Address</label>
                                        <input type="text" value="<?=$value->mailing_address; ?>" name="mailing_address" placeholder="Mailing Address" />
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Fax</label>
                                        <input type="text" value="<?=$value->site_fax; ?>" name="site_fax" placeholder="Fax" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>EDucki Commision Fee </h2>
                            <p>Manage your commision fee.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Fee</label>
                                        <input type="text" placeholder="Tax" value="<?=$value->tax; ?>" name="tax" class="tax-percent" />
                                        <span class="perSymbol">%</span> </div>
                                </div>
                                <div class="error">
                                    <?php echo form_error('tax')?>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Social Links</h2>
                            <p>Manage your social links.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Facebook</label>
                                        <input type="text" placeholder="Facebook URL" value="<?=$value->facebook; ?>" name="facebook" />
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label>Pinterest</label>
                                        <input type="text" placeholder="Pinterest URL" value="<?=$value->pinterest; ?>" name="pinterest" />
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Twitter</label>
                                        <input type="text" value="<?=$value->twitter; ?>" placeholder="Twitter URL" name="twitter" />
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label>Linkedin</label>
                                        <input type="text" placeholder="Linkedin URL" value="<?=$value->linkedin; ?>" name="linkedin" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Site Logo</h2>
                    <p>Manage your website logo.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <label for="box1">Image <span class="sub-text x-set">Image size: 215 x 93</span> </label>
                                <img src="<?php echo base_url('resources/company_logo/thumb/'.$value->site_logo) ?>" />
                                <input type="hidden" name="filehidden" value="<?=$value->site_logo; ?>" id="box1-txt" />
                                <input type="file" onchange="javascript: document.getElementById('box1-txt').value = this.value" name="file" value="<?=$value->site_logo; ?>" id="box1" />
                            </div>
                        </div>
                        <div class="error"><?=form_error('filehidden')?><?=$this->session->flashdata('msg_err')?> </div>
                    </div>
                </div>
            </div>
            <!-- Box along with label section ends here -->
            <!-- Box wihout label section begins here -->
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <!--<a href="<?php echo base_url() ?>admin/dashboard">cancel</a> </div>-->
            </div>
            <!-- Box wihout label section ends here -->
            <?php echo form_close();?>
        </div>
    </div>
</body>

</html>