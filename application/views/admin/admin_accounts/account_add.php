<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Create New Admin</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
</head>

<body>
<div class="container_p">
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    <div class="right-rp">
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Create New Admin Account</h1>
            </div>
            <div class="n-crums">
                <ul>
                    <li>
                        <a href="<?php echo base_url() ?>admin/admin_accounts">Admin Accounts</a>
                    </li>
                    <li>
                        <div class="crms-sep">
                            &gt;
                        </div>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Create New Admin</a>
                    </li>
                </ul>
            </div>
            <?php echo form_open(); ?>
            <!-- Box along with label section begins here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Account Details</h2>
                    <p>Manage the admin accounts on your website.</p>
                    
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>First Name*</label>
                                <input type="text" value="<?php echo set_value('fName'); ?>" name="fName" placeholder="First Name" />
                                <div class="error"><?php echo form_error('fName')?></div>
                            </div>
                        </div>                        

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Last Name*</label>
                                <input type="text" value="<?php echo set_value('lName'); ?>" name="lName" placeholder="Last Name" />
                                <div class="error"><?php echo form_error('lName')?></div>
                            </div>
                        </div>                        

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Username*</label>
                                <input type="text" value="<?php echo set_value('username'); ?>" name="username" placeholder="Username" />
                                <div class="error"><?php echo form_error('username')?></div>
                            </div>
                        </div>                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Email*
                                    </span>
                                </label>
                                <input type="text" autocomplete="off" value="<?php echo set_value('email'); ?>" name="email" placeholder="Enter Email" />
                                <div class="error"><?php echo form_error('email')?></div>
                            </div>
                        </div>                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Password*
                                    </span>
                                </label>
                                <input type="password" autocomplete="off" value="<?php echo set_value('password'); ?>" name="password" placeholder="Enter Password" />
                                <div class="error"><?php echo form_error('password')?></div>
                            </div>
                        </div>                        
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <label>Status*</label>
                                <select name="admin_status">
                                    <option value="">Select</option>
                                    <option value="1" <?=set_select('admin_status',1)?>>Active</option>
                                    <option value="0" <?=set_select('admin_status',0)?>>Inactive</option>
                                </select>
                                <div class="error">
                                    <?php echo form_error('admin_status')?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Box along with label section ends here -->
            <!-- Box wihout label section begins here -->
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?php echo base_url()?>admin/admin_accounts">cancel</a>
            </div>
            <!-- Box wihout label section ends here -->
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</body>
</html>