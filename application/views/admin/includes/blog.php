<?php
$blogCat = "";
$blog    = "";

$urlSegeent = $this->uri->segment(3);
if ($urlSegeent == "categories") {
    $blogCat = " active";
} else {
    $blog = "active";
}

?>
    <div class="tnav-hd">
        <div class="tnav-list">
            <a href="<?=base_url()?>admin/blog" class="<?=$blog?>" title="Blog">News &amp; Advice</a>
            <span>|</span>
            <a href="<?=base_url()?>admin/blog/categories" class="<?=$blogCat?>" title="Blog Categories">News &amp; Advice Categories</a>
        </div>
    </div>