<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact Forms</title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>

<body>
<div class="container"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php 
			$strTopGreenText1="Catalog / Add Sub Category";
			$strTopGreenText2="Catalog";
			include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
		 ?>
    <!-- Top Green Bar Section Ends Here -->
    <?php
    	if($this->session->flashdata('msg') != "") {
			echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
		} 
	?>
    <div class="col-continer">
      <div class="col-3 cat-box"> 
        
        <!-- Crumbs Starts -->
        <div class="n-crums">
          <ul>
            <li> </li>
            <!--
                <li>
                    <div class="crms-sep">></div>
                </li>
                -->
          </ul>
        </div>
        <!-- Crumbs Ends -->
        
        <div class="contntTop-row"> <!-- col-box1 --> 
          
          <!-- Top Navigation Starts Here -->
          <?php  include(ADMIN_INCLUDE_PATH."includes/commnuication.php");?>
          <!-- Top Navigation Ends Here --> 
          
          <!-- 01-5-1 Page Starst Here -->
          <div style="display:block">
            <div class="actvity-hd" style="display:none;">
              <div class="act-tab b-ad-tb">
                <table cellspacing="0" border="0">
                  <colgroup>
                  <col width="70%" />
                  <col width="30%" />
                  </colgroup>
                  <tr>
                    <td class="no-bdr"><div class="cat-tbl-tp-lf">
                        <h1>
                          <?=$title?>
                        </h1>
                      </div></td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="b-prd-tbl">
              <table border="0" cellspacing="0" cellpadding="0" id='mytable' class='tablesorter' >
                <colgroup>
                  
                  <col width="15%" />
                  <col width="20%" />
                  <col width="33%" />
                  <col width="17%" />
                  <col width="15%" />
                </colgroup>
                <thead>
                  <tr>
                    
                    <th align="center">Date</th>
                    <th align="left">Name</th>
                    <th align="left">Email</th>
                    <th align="center">Status</th>
                    <th align="center">Actions</th>
                  </tr>
                </thead>
                <?php foreach($results->result() as $rstRow){ ?>
                <tr bgcolor='#fcfdfe'>
                  
                  <td align="center" class="a-sett"><?php echo date('m-d-Y',strtotime($rstRow->date_created));?></td>
                  <td align="left"><p><?php echo $rstRow->fname;?></p></td>
                  <td align="left"><p><?php echo $rstRow->email;?></p></td>
                  <td align="center"><p><?php echo $rstRow->status?></p></td>
                  <td align="center"><table cellpadding="0" cellspacing="0" border="0">
                      <colgroup>
                      <col width="24%" />
                      <col width="26%" />
                      <col width="26%" />
                      <col width="24%" />
                      </colgroup>
                      <tr>
                        <td align="center">&nbsp;</td>
                        <td align="center"><a href="<?=base_url()?>admin/communication/contact_detail/<?php echo $rstRow->c_id;?>" class="tik-cross-btns dtlbtn-n" title="View Details"></a></td>
                        <td align="center">
                        <a title="Delete" href="<?=base_url()?>admin/communication/condelete/<?php echo $rstRow->c_id;?>" onclick="return confirm('Are you sure you want to delete the selected item(s)? ')"  class="tik-cross-btns prodDelete p-del-btn-n"><!--<img src="<?=base_url()?>images/b-del-icon.png " alt="" />--></a>
                        </td>
                        <td align="center">&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
                <?php } ?>
              </table>
            </div>
          </div>
          <!-- 01-5-1 Page Ends Here --> 
          
  <?php echo $paginglink; ?>
          

          
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
$(document).ready(function(){
    $('#mytable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     false,
        "aaSorting": [],
        "columnDefs": [
           { "orderable": false, "targets": [-1,0] }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>
</html>