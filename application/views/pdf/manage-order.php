<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
	<style type="text/css">
		*{
			padding: 0;
			margin: 0;
		}
	</style>
</head>
<body>

	<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="#FFFFFF" align="center" nowrap>
		
		<!-- Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="20px">&nbsp;</td>
		</tr>
		<!-- Row Ends -->

		<!-- Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;">
				<img src="<?= base_url() ?>resources/company_logo/<?=getSiteLogo()?>" alt="" />
			</td>
		</tr>
		<!-- Row Ends -->

		<!-- Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="30px">&nbsp;</td>
		</tr>
		<!-- Row Ends -->

		<!-- Heading Section Starts -->
		<tr>
			<td align="center" valign="middle">
		    	<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
		            <tr>
		                <td align="center" valign="middle" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-weight:bold; font-size:32px; color:#0c97c8; line-height:1.4;">
		                    ORDER DETAIL
		                </td>
		            </tr>
		        </table>
		    </td>
		</tr>
		<!-- Heading Section Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="23px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 2px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="18px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Table Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Order Date:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:16px; color:#46494c; line-height:1.4;">
			            	<?= date('F d, Y', strtotime($results->order_date)) ?>
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="5px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->

			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Order ID:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:16px; color:#46494c; line-height:1.4;">
			            	<?= $results->order_id ?>
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="5px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->

			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Order Total:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:16px; color:#46494c; line-height:1.4;">
			            	$<?= number_format($results->order_grand_total, 2, '.', '') ?>
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="5px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->

			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Order Status:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:16px; color:#46494c; line-height:1.4;">
			            	<?= ucfirst($results->order_status) ?>
			            </td>
			        </tr>

			    </table>
			</td>
		</tr>
		<!-- Table Row Ends -->
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="18px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 2px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="22px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Table Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width="99%" align="left">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Shipping Address
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Nickname:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_shipping_address->shipping_nick_name?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Name:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_shipping_address->shipping_first_name . ' ' . $default_shipping_address->shipping_last_name ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Telephone:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= (!empty($default_shipping_address->shipping_phone))?$default_shipping_address->shipping_phone:'N/A'?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Street:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_shipping_address->shipping_street ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                City:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_shipping_address->shipping_city ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                State:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?php
			                            if ($default_shipping_address->shipping_country == 'US') {
			                                echo get_state_name($default_shipping_address->shipping_state);
			                            } else {
			                                echo $default_shipping_address->shipping_state_other;
			                            }
			                            ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Country:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= get_country_name($default_shipping_address->shipping_country) ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->			        


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Zip:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_shipping_address->shipping_zip ?>
						            </td>
						        </tr>
								
						    </table>
						</td>
						<td>
						    <table cellpadding="0" cellspacing="0" border="0" width="99%" align="right">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Billing Address
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Nickname:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_billing_address->billing_nick_name ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Name:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_billing_address->billing_first_name . ' ' . $default_billing_address->billing_last_name ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Telephone:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= (!empty($default_billing_address->billing_phone))?$default_billing_address->billing_phone:'N/A'?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Street:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_billing_address->billing_street ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                City:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_billing_address->billing_city ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                State:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?php
			                            if ($default_billing_address->billing_country == 'US') {
			                                echo get_state_name($default_billing_address->billing_state);
			                            } else {
			                                echo $default_billing_address->billing_state_other;
			                            }
			                            ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Country:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= get_country_name($default_billing_address->billing_country) ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->			       			        


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="230" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Zip:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?= $default_billing_address->billing_zip ?>
						            </td>
						        </tr>
								
						    </table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- Table Row Ends -->
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="33px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->


		<!-- Table Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr>
						
						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="50%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Payment Method
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<?php if ($results->order_payment_method == 2) { ?>
	                                        Paypal
	                                    <?php } else { ?>
	                                        Credit Card
	                                    <?php } ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->

						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="50%">&nbsp;</td>
						<!-- Col Ends -->
						
					</tr>
				
				</table>
			</td>
		</tr>
		<!-- Table Row Ends -->


    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="25px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 2px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="25px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->



		<!-- REPEATABLE SECTION STARTS -->
		<?php
		if (isset($order_detail_results)) {
		    foreach ($order_detail_results as $data) {
		?>
		<!-- Product Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr>
						
						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="58%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="110">
						            	<img src="<?= base_url() ?>resources/prod_images/thumb/<?= $data['order_prod_image'] ?>" alt="" width="90" />
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<p style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            		<?= $data['order_prod_name'] ?>
						            	</p>
						            	<p style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            		<?php $filters = json_decode($data['order_prod_filters']);
                                            foreach ($filters as $i => $filter) {?>
                                                <p><b><?=$filter->filterName?></b>:<?=$filter->filtervalue?></p>
                                            <?php }?>
						            	</p>
						            	<p>&nbsp;</p>
						            	<p style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            		<?php
                                            if ($data['order_prod_return'] != '') {
                                                echo $data['order_prod_return'];
                                            }
                                            ?><br />
						            		Qty: <?= $data['order_prod_qty'] ?>
						            	</p>
						            </td>
						            <td align="center" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:12px; color:#46494c; line-height:1.4;">						            
										<?php if ($data['order_prod_completion_date'] != NULL) { ?>
                                            Acknowledged<br />
                                            On<br />
                                            <?php echo date("F j, Y", strtotime($data['order_prod_completion_date'])); ?>
                                        <?php } ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="4" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->

						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="42%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Shipping Method
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								<?php if($data['order_prod_shipping']=='Charge for Shipping'){?>
								<tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                <?php $shipping='';
	                                        if($data['order_prod_shipping']=='Charge for Shipping'){
	                                            $shipping = $data['order_prod_shipping_methods'];
	                                        }else{
	                                            $shipping = '';
	                                        }
	                                        echo (!empty($shipping))?$shipping:''?>
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	&nbsp;
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								<?php }?>

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                <?php $days='';
                                            if($data['order_prod_shipping']=='Charge for Shipping'){
                                                $days = $data['order_prod_ship_days']+$dayz;
                                            }elseif($data['order_prod_shipping']=='Offer free Shipping'){
                                                $days = $data['order_prod_free_ship_days']+$dayz;
                                            }else{
                                                $days = +$dayz;
                                            }
                                            echo $data['order_prod_shipping']?> <?php if($data['order_prod_status']!='Delivered' && $data['order_prod_status']!='In Process' && $data['order_prod_status']!='completed' && $data['order_prod_status']!='cancel'){ echo '( '.$days.' )';} ?>
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	&nbsp;
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Item Price:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$<?= $data['order_prod_total_price'] ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Shipping & Handing:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$<?= number_format($data['order_prod_ship_price'], 2, '.', '') ?>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->
						
					</tr>
				
				</table>
			</td>
		</tr>
		<!-- Product Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="14px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 1px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="14px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- REPEATABLE SECTION ENDS -->


	<?php } ?>


	<!-- Spacing Row Starts -->
	<tr>
		<td align="center" valign="top" style="font-size: 1px;" height="10px">&nbsp;</td>
	</tr>
	<!-- Spacing Row Ends -->

	<!-- Table Row Starts -->
	<tr>
		<td align="center" valign="middle">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
				<tr>
					
					<!-- Col Starts -->
					<td align="left" valign="top" bgcolor="#FFF" width="57%">&nbsp;</td> <!-- 50 -->
					<!-- Col Ends -->

					<!-- Col Starts -->
					<td align="left" valign="top" bgcolor="#FFF" width="43%"> <!-- 50 -->
						<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
							
					        <tr>
					        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
					            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
					                Order Summary
					            </td>
					        </tr>

					        <!-- Spacing Row Starts -->
							<tr>
								<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
							</tr>
							<!-- Spacing Row Ends -->

					        <tr>
					        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
					            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
					                Order Sub Total:
					            </td>
					            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
					            	$<?= number_format($results->order_sub_total, 2, '.', '') ?>
					            </td>
					        </tr>

					        <!-- Spacing Row Starts -->
							<tr>
								<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
							</tr>
							<!-- Spacing Row Ends -->


					        <tr>
					        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
					            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
					                Order Discount:
					            </td>
					            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
					            	$<?= number_format($results->order_discount, 2, '.', '') ?>
					            </td>
					        </tr>

					        <!-- Spacing Row Starts -->
							<tr>
								<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
							</tr>
							<!-- Spacing Row Ends -->


					        <tr>
					        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
					            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
					                Shipping & Handing:
					            </td>
					            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
					            	$<?= number_format($results->order_shipping_price, 2, '.', '') ?>
					            </td>
					        </tr>

					        <!-- Spacing Row Starts -->
							<tr>
								<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="30px">&nbsp;</td>
							</tr>
							<!-- Spacing Row Ends -->


					        <tr>
					        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
					            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
					                Order Total
					            </td>
					            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
					            	$<?= number_format($results->order_grand_total, 2, '.', '') ?>
					            </td>
					        </tr>

					        <!-- Spacing Row Starts -->
							<tr>
								<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
							</tr>
							<!-- Spacing Row Ends -->
							
					    </table>
					</td>
					<!-- Col Ends -->
					
				</tr>
			
			</table>
		</td>
	</tr>
	<!-- Table Row Ends -->

<?php } ?>

		

	</table>

</body>
</html>