<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-address'); ?>"> Address Book </a> </li>
                        <li class="breadcrumb-item active">  Add New Address </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="my-account-right-head pad-btm-0">
                            <h5>Add New Address</h5>
                        </div>
                        <?php echo form_open_multipart('', array('name' => 'add_address', 'id' => 'add_address_form')); ?>  
                        <div class="store-profile">
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Nickname <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="nick_name" id="nick_name" placeholder="" value="<?php echo set_value('nick_name')?>" />
                                    <div class="error" id="nick_name_validate"><?php echo form_error('nick_name') ?></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>First Name <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="first_name" id="first_name" placeholder="" value="<?php echo set_value('first_name')?>"/>
                                    <div class="error" id="first_name_validate"><?php echo form_error('first_name') ?></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Last Name <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="last_name" id="last_name" placeholder="" value="<?php echo set_value('last_name')?>"/>
                                    <div class="error" id="last_name_validate"><?php echo form_error('last_name') ?></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Street <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="street" id="street" placeholder="" value="<?php echo set_value('street')?>"/>
                                    <div class="error" id="street_validate"><?php echo form_error('street') ?></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>City <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="city" id="city" placeholder="" value="<?php echo set_value('city')?>"/>
                                    <div class="error" id="city_validate"><?php echo form_error('city') ?></div>
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Country  <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="country" id="country">
                                        <option value="0" <?= set_select('country', '0') ?>>Select Country</option>
                                        <?php foreach ($allcountries as $country) { ?>
                                            <option value="<?= $country['iso'] ?>" <?=($country['iso']=='US')?'selected':''?>><?= $country['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="error" id="country_validate"><?php echo form_error('country') ?></div>
                                </div>
                            </div>

                            <div class="store-profile-row" id="us-states" style="display: block;">
                                <div class="store-profile-row-lft">
                                    <h4>State <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt" >
                                    <select name="state" id="state">
                                        <option value="0" <?= set_select('state', '0') ?>>Select State</option>
                                        <?php foreach ($allstate as $state) { ?>
                                            <option value="<?= $state['stat_id'] ?>"><?= $state['stat_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="error" id="state_validate"><?php echo form_error('state') ?></div>
                                </div>

                            </div>
                            <div class="store-profile-row"  id="other-states" style="display: none">
                                <div class="store-profile-row-lft">
                                    <h4>State <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="other_state" name="other_state" value="<?php echo set_value('other_state')?>"/>
                                    <div class="error" id="other_state_validate"><?php echo form_error('other_state') ?></div>
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Zip <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="zip_code" id="zip_code" placeholder="" />
                                    <div class="error" id="zip_code_validate"><?php echo form_error('zip_code') ?></div>
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Telephone </h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="phone" id="phone" placeholder="" />
                                    <!--<div class="error" id="phone_validate"><?php echo form_error('phone') ?></div>-->
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                </div>
                                <div class="store-profile-row-rgt uty-add-addr-1">
                                    <div class="chng-paswrd pad-tp-0">
                                        <input type="checkbox" id="cp1" value="Y" name="default_billing">
                                        <label for="cp1"><span></span><p>Use as my default billing address</p></label>
                                    </div>
                                    <div class="chng-paswrd pad-tp-4">
                                        <!--<input type="checkbox" id="cp2" name="cc" checked>-->
                                        <input type="checkbox" id="cp2" value="Y" name="default_shipping">
                                        <label for="cp2"><span></span><p>Use as my default shipping address</p></label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="user_id" value="<?=$userid?>" >
                            <div class="store-profile-row">
                                <div class="sv-bck-sec">
                                    <input class="upld-btn" type="submit" value="save"/>
                                    <a href="<?=base_url('my-address'); ?>" class="bck-btn">Back</a>
                                </div>
                            </div>
                            
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>
    $(function () {
        $('#country').change(function () {
            if ($('#country').val() === 'US') {
                $('#us-states').show("slow");
                $('#other_state').val('');
                $('#other-states').hide("slow");
            } else {
                $('#us-states').hide("slow");
                $('#other-states').show("slow");
            }
        });
    });
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    nick_name: {
                        minlength: 2,
                        maxlength: 20,
                        required: true
                    },
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    street: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    other_state: {
                        required: true
                    },
                    zip_code: {
                        required: true
                    },
                    state: {valueNotEquals: "0"},
                    country: {valueNotEquals: "0"}
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };

            $('#add_address_form').validate(rules);
        });
    });
</script>