<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">Message Center</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">

                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?> 
                <!--Left section starts here-->


                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">

                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">
                            <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message"><a href="javascript:void" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'.$this->session->flashdata('msg').'</div>';

                            }?>
                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad">
                                <div class="msg-hd-inrow">

                                    <h5>Message Center</h5>

                                </div>
                                <!-- <p>Sample Text Here.</p> -->
                            </div>
                            <!-- Heading Section Ends -->

                            <!-- Bottom Section Starts -->
                            <div class="msg-center-btm">

                                <!-- Top Message Rely Section Starts -->
                                <div class="msgreply-top">
                                    <div class="msgreply-box">

                                        <div class="msgreply-inner">
                                                <div class="msgreplyFrm">
                                                    <div class="msgreply-row">
                                                        <label>Message</label>
                                                        <div class="msgreply-txtarea">
                                                            <textarea placeholder="Message" id="txtarea"></textarea>
                                                            <div class="error" id="txtarea_error"></div>
                                                        </div>
                                                    </div>

                                                    <div class="msgreply-row">
                                                        <div class="msgreply-btnsec">
                                                            <div class="msgreply-btn">
                                                                <a href="<?=base_url('messages')?>" title="Cancel">Cancel</a>
                                                            </div>
                                                            <div class="msgreply-btn">
                                                                <input type="button" value="REPLY" title="Reply" class="send_message"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="con_id" value="<?=$conversation_id?>">
                                                    <?php $chat_data = get_conversation('tbl_chat_conversations','conversation_id',$conversation_id);
                                                    if($chat_data->buyer==$this->session->userdata('userid')){
                                                       echo '<input type="hidden" id="reply" name="reply" value="'.$chat_data->seller.'">';
                                                       echo '<input type="hidden" id="reply_email" name="reply_email" value="'.get_email($chat_data->seller).'">';
                                                    }else{
                                                        echo '<input type="hidden" id="reply" name="reply" value="'.$chat_data->buyer.'">';
                                                        echo '<input type="hidden" id="reply_email" name="reply_email" value="'.get_email($chat_data->buyer).'">';
                                                    }
                                                    ?>
                                                    <input type="hidden" id="prod-id" name="prod-id" value="<?=$prod_id?>">

                                                </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- Top Message Rely Section Ends -->

                                <!-- Bottom Message Section Starts -->
                                <div class="msgreply-btm">
                                    <div class="msgreply-tbl">
                                        <table cellpadding="0" cellspacing="0" border="0">

                                            <tr>
                                                <th valign="top" align="left" class="firstcol msgcol1">Date</th>
                                                <th valign="top" align="left" class="msgcol2">Sender</th>
                                                <th valign="top" align="left" class="msgcol3">Product ID</th>
                                                <th valign="top" align="left" class="msgcol4">Message</th>
                                            </tr>
                                            <?php
                                            if (count($conversation) > 0) {
                                                $count = 0;
                                                foreach ($conversation as $msg) {
                                                    $count++;
                                                    ?>
                                                    <tr <?php
                                                    if ($count % 2 == 0) {
                                                        echo 'class=even';
                                                    }
                                                    ?>>
                                                        <td valign="top" align="left" class="firstcol">
                                                            <?= date("m-d-Y", strtotime($msg->timestamp)); ?>
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <?php
                                                            /*echo $msg->sendername . ' ' . $msg->sender_lname;*/
                                                            if ($msg->sender == $this->session->userdata('userid')) {
                                                                echo "Me";
                                                            } else {
                                                                echo $msg->sendername . ' ' . $msg->sender_lname;
                                                            }
                                                            ?>
                                                        </td>
                                                        <td valign="top" align="left"><?=($prod_id==0)?'N/A':$prod_id?></td>
                                                        <td valign="top" align="left"><?= $msg->message; ?></td>
                                                    </tr>
                                            <?php
                                            }
                                        }
                                        ?>
                                        </table>
                                    </div>
                                </div>
                                <!-- Bottom Message Section Ends -->

                            </div>
                            <!-- Bottom Section Ends -->

                        </div>
                        <!-- Inner Section Ends -->


                    </div>
                </div>
                <!-- Right Pannel Starts Here -->


            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(document).on('click', '.send_message', function () {
            var seller_id = $('#reply').val();
            var prod_id = $('#prod-id').val();
            var message = $('#txtarea').val();
            var email = $('#reply_email').val();
            var error = 0;
            /*var msg = '';
             msg = msg + 'Seller : '+seller_id;
             msg = msg + 'Product ID : '+prod_id;
             msg = msg + 'Message : '+message;*/
            if (message != '') {
                error = 0;                                 
            } else {
                error = 1;
                $('#txtarea_error').removeClass('success');
                $('#txtarea_error').addClass('error');
                $('#txtarea_error').html('This field is required');
            }
            if (error == 0) {
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url() ?>submit-message',
                    data: {email: email,seller_id: seller_id, prod_id: prod_id, message: message,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    success: function (res) {
                        if (res == '200') {                            
                            $('#txtarea_error').removeClass('error');
                            $('#txtarea_error').addClass('success');
                            $('#txtarea').val('');
                            $('#txtarea_error').html('');
                            location.reload();
                        }
                    },
                    error: function () {
                        console.log('Network error.');
                        $('#txtarea_error').removeClass('success');
                        $('#txtarea_error').addClass('error');
                        $('#txtarea_error').html('Somethig went wrong');
                    }
                });
            }
        });
    });
</script>


<!--Blog main ends  here-->