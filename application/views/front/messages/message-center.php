<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
                      <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                      <li class="breadcrumb-item active">Message Center</li>

                    </ol>
                </div>
            </div>
        </div>
    </div>

</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">

                <!--Left section starts here-->
                  <?php //include("includes/left-navsec2.php") ?>
                  <?php // $this->load->view('front/includes/left-navsec2');?>
                  <?php $this->load->view('front/includes/seller-left-nav'); ?> 
                <!--Left section starts here-->


                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">

                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">
                            <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('msg') . '</div>';
                            }?>

                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad">
                                <div class="msg-hd-inrow uty-message-1">

                                    <h5>Message Center</h5>

                                    <div class="del-btnright">
                                        <div class="custombtn-large">
                                            <a href="javascript:" id="delete_click">DELETE MESSAGES</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- <p>Sample Text Here.</p> -->
                            </div>
                            <!-- Heading Section Ends -->

                            <!-- Bottom Section Starts -->
                            <div class="msg-center-btm">

                                <!-- Top Grey Bar Starts -->
                                <div class="topGrey-bar">
                                    <div class="topGrey-inner">

                                        <!-- Search By Form Starts -->
                                        <div class="searchby-wrap">
                                            <!-- <form name="searchby" method="post"> -->
                                                <div class="searchby-row">
                                                    <label>Search by:</label>
                                                    <div class="sselect-rp">
                                                        <select id="searchType">
                                                            <option value="0">Select</option>
                                                            <option value="1">First Name</option>
                                                            <option value="2">Product ID</option>
                                                        </select>
                                                        <div class="error" id="search_value_validate"></div>
                                                    </div>
                                                    <div class="stextbox-rp">
                                                        <input type="text" id="utxtbox" value=""/>
                                                        <div class="error" id="utxtbox_validate"></div>
                                                    </div>
                                                    <div class="search-btn">
                                                        <div class="custombtn-large">
                                                            <input type="button" value="SEARCH" title="SEARCH" id="search"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- </form> -->
                                        </div>
                                        <!-- Search By Form Ends -->

                                    </div>
                                </div>
                                <!-- Top Grey Bar Ends -->                                
                                <!-- Message Inner Bottom Listing Starts -->
                                <div class="msg-secbtm-rp">

                                    <?php echo form_open_multipart('delete-chat',array('id'=>'delete_chat')); ?>
                                    <!-- Table Section Starts -->
                                    <div class="msg-sectbl">
                                        <table cellpadding="0" cellspacing="0" border="0" id='my_table'>
                                            <tr>
                                                <th class="colOne">&nbsp;</th>
                                                <th class="colTwo">Date</th>
                                                <th class="colThree">Name</th>
                                                <th class="colFour">Product ID</th>
                                                <th class="colFive">Message</th>
                                                <th class="colSix">Actions</th>
                                            </tr>
                                            <?php if (count($Allconversation) > 0) {
                                            $class = '';
                                            foreach ($Allconversation as $chat) {                                                
                                                    if($chat->buyer==$this->session->userdata('userid')){
                                                        if ($chat->buyer_unread > 0) {$class = 'class=unread';} else { $class = '';}
                                                    }else{
                                                        if ($chat->seller_unread > 0) {$class = 'class=unread';} else { $class = '';}
                                                    }
                                                    ?>
                                            <tr <?=$class;?>>
                                                <td><input type="checkbox" name="ulist1[]" value="<?=$chat->conversation_id?>" /></td>
                                                <td><?=date("m-d-Y", strtotime($chat->timestamp));?></td>
                                                <td><?php if ($chat->buyer == $this->session->userdata('userid')) {echo $chat->sellername . ' ' . $chat->seller_lname[0];} else {echo $chat->buyername . ' ' . $chat->buyer_lname[0];}?></td>
                                                <td><?=($chat->product_id==0)?'N/A':$chat->product_id?></td>
                                                <td><?=limited_text(get_latest_message($chat->conversation_id),80);?></td>
                                                <td class="msgaction">
                                                    <div class="msg-actionrp">
                                                        <ul>
                                                            <li>
                                                                <div class="viewmsg-btn">
                                                                    <?php if ($chat->buyer != $this->session->userdata('userid')) {
                                                                        if ($chat->seller_unread > 0) {?>
                                                                            <div class="viewBubble"><?=$chat->seller_unread;?></div>
                                                                    <?php }}else{
                                                                        if ($chat->buyer_unread > 0) {?>
                                                                            <div class="viewBubble"><?=$chat->buyer_unread;?></div>
                                                                    <?php }}?>
                                                                    <a href="<?=base_url()?>messages/<?=$chat->conversation_id?>" title="View Messages"></a>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="delmsg-btn">
                                                                    <a href="<?=base_url()?>delete-conversation/<?=$chat->conversation_id?>/<?=$this->session->userdata('userid')?>" title="Delete Message"></a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }} else {?>
                                            <tr class="unread" align="center"><td colspan="6">No data found.</td></tr>
                                            <?php }?>
                                            <?php if(!empty($paginglink)) {?>
                                            <tr>
                                                <td colspan="6">
                                                <!-- <td colspan="6" style="padding: 1px 0px;" -->
                                                    <div class="msb-lastfrow">

                                                        <!-- Left Section Starts 
                                                        <div class="showing-rp">
                                                            1 - 10 of 14
                                                        </div>
                                                        <! Left Section Ends -->

                                                        <!-- Right Section Starts -->
                                                        <div class="msg-bright">
                                                            <!-- Pagination Section Starts -->
                                                            <div class="pagination-rp2">
                                                               <?php echo $paginglink;?>
                                                        
                                                            </div>
                                                            <!-- Pagination Section Ends -->
                                                        </div>
                                                        <!-- Right Section Ends -->

                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </table>
                                    </div>
                                    <!-- Table Section Ends -->

                                    <?php echo form_close(); ?>

                                </div>
                                <!-- Message Inner Bottom Listing Ends -->

                            </div>
                            <!-- Bottom Section Ends -->

                        </div>
                        <!-- Inner Section Ends -->


                    </div>
                </div>
                <!-- Right Pannel Starts Here -->


            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->

<script>
$(document).ready(function(){
    $("#search").click(function(){
        if(($('#searchType').val()==1 || $('#searchType').val()==2) && $('#utxtbox').val()!=''){
            var input, filter, table, tr, td, i;
            input = document.getElementById("utxtbox");
            filter = input.value.toUpperCase();
            table = document.getElementById("my_table");
            column=0;
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                if($('#searchType').val()==1){column=2;}else{column=3;}
                  td = tr[i].getElementsByTagName("td")[column];
                  if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                      tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
            $('#utxtbox_validate').html('');
            $('#search_value_validate').html('');
        }
    else if($('#utxtbox').val()!=''){
        $('#search_value_validate').html('This field is required.');
        $('#utxtbox_validate').html('');
    }else if($('#searchType').val()==1 || $('#searchType').val()==2){        
        $('#utxtbox_validate').html('This field is required.');
        $('#search_value_validate').html('');
    }else{
        $('#search_value_validate').html('This field is required.');
        $('#utxtbox_validate').html('This field is required.');
    }
    });
});
$('.message .close').click(function () {
    $(this).parent().fadeOut('slow', function () {
        $(this).remove();
    });
});
$('#delete_click').click(function(){
    var checkedNum = $('input[name="ulist1[]"]:checked').length;
    if (!checkedNum) {
        alert('Please select at least one conversation')
    }else{
        $('#delete_chat').submit();
    }
});
</script>