<!--Breadcrumbs starts here-->
<link href="<?= base_url() ?>front_resources/css/pgwslideshow.css" rel="stylesheet" type="text/css" />
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="shop-tit mrg-top-0 pad-10">
                    <h2>Shop</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<section class="shop-details-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="shop-detail-sec">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="cro-slid">
                            <ul class="pgwSlideshow">
<!--                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>
                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>
                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>
                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>
                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>
                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>
                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>
                                <li><img src="<?= base_url() ?>front_resources/images/product-slide-mg.jpg"/></li>-->
                                <?php foreach($img as $image){ ?>
                                <li><img src="<?= base_url() ?>resources/prod_images/<?=$image?>"/></li>
                                <?php  } ?>
                            </ul>
                            
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="shop-det-rgt">
                            <div class="shop-det-rgt-upr">
                                <h4><?=stripslashes($prod_title)?></h4>
                                <p><?php echo $seller_data->user_fname. " " .$seller_data->user_lname;?><span> (Visit their Shop)</span></p>
                            </div>
                            <div class="shop-det-rgt-lowr-1">
                                <h5>

                                    <?php
                                    $productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($sale_start_date));
                                    $eDate = date('Y-m-d', strtotime($sale_end_date));
                                    $tDate = date('Y-m-d');
                                    if ($prod_onsale == 1) {
                                        if ($sale_start_date != '' && $sale_end_date != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '$' . number_format($prod_price, 2, '.', '') . ''; //</span>
                                            }
                                        } else if ($sale_start_date != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '$' . number_format($prod_price, 2, '.', '') . ''; //</span>
                                            }
                                        } else if ($sale_end_date != '') {
                                            if ($tDate <= $eDate) {
                                                echo 'Price:<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                echo 'Price:<span class="sp1">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            }
                                        } else {
                                            $productPrice = number_format($prod_price, 2, '.', '');
                                            echo '<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                        }
                                    } else {
                                        $productPrice = number_format($prod_price, 2, '.', '');
                                        echo '$' . number_format($prod_price, 2, '.', '');
                                    }
                                    ?>



                                    <!-- <?php
                                    /*$productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($sale_start_date));
                                    $eDate = date('Y-m-d', strtotime($sale_end_date));
                                    $tDate = date('Y-m-d');
                                    if ($prod_onsale == 1) {
                                        if ($sale_start_date != '' && $sale_end_date != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '$' . number_format($prod_price, 2, '.', '') . ''; //</span>
                                            }
                                        } else if ($sale_start_date != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod_price, 2, '.', '');
                                                echo '$' . number_format($prod_price, 2, '.', '') . ''; //</span>
                                            }
                                        } else if ($sale_end_date != '') {
                                            if ($tDate <= $eDate) {
                                                echo 'Price:<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                echo 'Price:<span class="sp1">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                            }
                                        } else {
                                            $productPrice = number_format($prod_price, 2, '.', '');
                                            echo '<span class="sp1">$' . number_format($sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_price, 2, '.', '') . '</span>';
                                        }
                                    } else {
                                        $productPrice = number_format($prod_price, 2, '.', '');
                                        echo '$' . number_format($prod_price, 2, '.', '');
                                    }*/
                                    ?> -->
                                </h5>
                                <p style="margin-top:-6px;">Available Quantity: <?=$qty?></p>
                                <p>
                                    <?php if($shipping=='Charge for Shipping'){
                                        $shipping_type =  '+ $ '.$ship_price.'.00 Shipping'.' ('.$ship_method.')';
                                    }elseif($shipping=='Offer free Shipping'){
                                        $shipping_type = "+ Free Shipping";
                                    }elseif($shipping=='Local Pick Up'){
                                        $shipping_type = "+ Local Pickup";
                                    }
                                    echo $shipping_type;
                                    ?>
                                </p>
                            </div>
                            <div class="shop-det-rgt-lowr-2">
                                <a href="javascript:void(0);" data-price = "<?= $productPrice ?>" class="adcrt">Add to cart</a>
                                <a href="javascript:void(0);" class="wish adtowishlist" title="Add to Wishlist">Add to Wishlist</a>
                            </div>
                            <div class="shop-det-rgt-lowr-3">
                                <span class="reprt-btn"><a href="javascript:void">Report</a></span>
                                <button class="addcompr-btn">Add to Compare</button>
                                <?php if(!empty($youtube_links)){?>
                                    <a href="javascript:void" class="watchvideo">Watch Video</a>
                                <?php }?>
                            </div>
                            <div class="shop-det-rgt-lowr-4">
                                <div class="shr-img">
                                    <ul>
                                        <li><a href="javascript:void" class="shr-fb"></a></li>
                                        <li><a href="javascript:void" class="shr-fb2"></a></li>
                                        <li><a href="javascript:void" class="shr-fb3"></a></li>
                                        <li><a href="javascript:void" class="shr-fb4"></a></li>
                                        <li><a href="javascript:void" class="shr-fb5"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="shop-det-rgt-lowr-5">
                                <?php if($shipping!='Local Pick Up'){ ?>
                                <h6>Product Ships within <?=$ship_days; ?> business days</h6>
                                <?php } ?>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <p><?=stripslashes($prod_description)?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= base_url() ?>front_resources/js/pgwslideshow.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    $('.pgwSlideshow').pgwSlideshow(4);
});
</script>