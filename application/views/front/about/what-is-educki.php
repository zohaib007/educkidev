
<?php if (empty($work_data->work_banner)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = base_url('resources/work_page_images/'.$work_data->work_banner);
}
?>
<!--banner  starts here-->
<section class="whted-bg" style="background-image: url(<?= $path ?>); !important">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url() ?><?= $pageurl->sub_pg_url?>">About</a></li>
                                <li class="breadcrumb-item active"><?= $work_data->work_page_title ?></li>
                            </ol>
                        </div>

                        <h2><?=$work_data->work_page_title;?></h2>	
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!--Breadcrumbs ends  here-->

</section>
<!--banner  starts here-->



<!--main sec starts here-->
<?php $this->load->view('front/about/about_side_nav'); ?>

<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 pad-rgt-0">
    <div class="whted-rgt-sec">
        <div class="whted-rgt-tit">
            <h3><?= $work_data->work_title ?></h3>
            <span>
                <img src="<?= base_url() ?>front_resources/images/bdr2.png" alt="" title=""/>
            </span>
        </div>

        <div class="whted-rgt-bx">
            <div class="whted-rgt-bx-lft">
                <img src="<?= base_url() ?>resources/work_page_images/<?= $work_data->work_image_1 ?>" alt="" title="" class="img-responsive pull-left"/>

            </div>

            <div class="whted-rgt-bx-rgt">
                <!--<div class="cvr-upr">
                        <h5>Create a Covershot</h5>
                    <a href="#">With just a few clicks</a>
                </div>-->
                <div class="cvr-bttom">
                    <p><?= clean_string($work_data->work_desc_1); ?></p>
                </div>


            </div>

        </div>

        <div class="whted-rgt-bx">

            <div class="whted-rgt-bx-rgt mrg-lft-0 mrg-lft-3">
                <!--<div class="cvr-upr">
                        <h5>Introducing eDucki Protect</h5>
                    <a href="#">Buy with confidence</a>
                </div>-->
                <div class="cvr-bttom">
                    <p><?= clean_string($work_data->work_desc_2); ?></p>
                </div>


            </div>
            <div class="whted-rgt-bx-lft">
                <img src="<?= base_url() ?>resources/work_page_images/<?= $work_data->work_image_2 ?>" alt="" title="" class="img-responsive pull-right"/>

            </div>

        </div>

        <div class="whted-rgt-bx">
            <div class="whted-rgt-bx-lft">
                <img src="<?= base_url() ?>resources/work_page_images/<?= $work_data->work_image_3 ?>" alt="" title="" class="img-responsive pull-left"/>
            </div>

            <div class="whted-rgt-bx-rgt">
                <!--<div class="cvr-upr">
                        <h5>Enjoy One-Stop Shopping</h5>
                    <a href="#">New and pre-loved fashion</a>
                </div>-->
                <div class="cvr-bttom">
                    <p><?= clean_string($work_data->work_desc_3); ?></p>
                </div>


            </div>

        </div>

        <div class="whted-rgt-bx bodr-0">

            <div class="whted-rgt-bx-rgt mrg-lft-0 mrg-lft-3">
                <!--<div class="cvr-upr">
                        <h5>Discover the eDucki Community</h5>
                    <a href="#">Connect and share</a>
                </div>-->
                <div class="cvr-bttom">
                    <p><?= clean_string($work_data->work_desc_4); ?></p>
                </div>


            </div>
            <div class="whted-rgt-bx-lft">
                <img src="<?= base_url() ?>resources/work_page_images/<?= $work_data->work_image_4 ?>" alt="" title="" class="img-responsive pull-right"/>
            </div>

        </div>

        <div class="sprt-sec">
            <p>
                Still need assistance? Check out our <a href="<?=base_url()?><?=get_page_url(15,'tbl_pages_sub','sub_id')->sub_pg_url;?>">Support Center</a>.
            </p>

        </div>


    </div>
</div>


</div>
</div>
</div>
</section>

<!--main sec  ends here-->