<!--main sec starts here-->
<section class="weht-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 pad-lft-0">
                    <div class="side-nav">
                        <ul>
                            <?php
                                $footermenu = about_side_menu();
                                foreach ($footermenu as $items) {
                                    if($pagedata->sub_pg_url == $items['sub_pg_url']){
                                        $active = 'active'; 
                                    }else {
                                        $active = '';
                                    }
                                    if($items['sub_id']==5 || $items['sub_id']==6 || $items['sub_id']==9 || $items['sub_id']==10 || $items['sub_id']==14 || $items['sub_id']==15){
                                    ?>
                                    <li>
                                        <a class="<?=$active?>" href="<?= base_url() ?><?= $items['sub_pg_url'] ?>"><?= $items['sub_pg_title'] ?></a>
                                    </li>
                                <?php } else{?>
                                    <li>
                                        <a class="<?=$active?>" href="<?= base_url() ?>about/<?= $items['sub_pg_url'] ?>"><?= $items['sub_pg_title'] ?></a>
                                    </li>
                                    <?php }
                                 } ?>
                        </ul>
                    </div>
                </div>
