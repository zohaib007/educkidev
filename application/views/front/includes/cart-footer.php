<!--footer starts here-->
<footer>
    <section class="footr-mid-sec">
        <div class="container">
            <div class="row">
                <div class="footr-wrp-2">
                    <div class="col-md-3">
                        <div class="ftr-comp-tit">
                            <h2>Company</h2>
                        </div>

                        <div class="fotr-lnk-1">
                            <ul>
                                <?php
                                $footermenu = footer_about_menu();
                                foreach ($footermenu as $items) {
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?><?= $items['sub_pg_url'] ?>"><?= $items['sub_pg_name'] ?></a>
                                    </li>
                                <?php } ?>
                            </ul>


                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="ftr-comp-tit">
                            <h2>Help</h2>
                        </div>

                        <div class="fotr-lnk-1">
                            <ul>
                                <!-- <li>
                                    <a href="">How it Works</a>
                                </li> -->
                                <li>
                                    <a href="<?= base_url() ?><?= get_page_url(6, 'tbl_pages_sub', 'sub_id')->sub_pg_url; ?>">Help Center</a>
                                </li>
                                <li>
                                    <a href="<?= base_url() ?><?= get_page_url(10, 'tbl_pages_sub', 'sub_id')->sub_pg_url; ?>/<?= get_page_url(7, 'tbl_pages_listing', 'listing_id')->listing_url; ?>">Getting Started</a>
                                </li>
                                <li>
                                    <a href="<?= base_url() ?><?= get_page_url(9, 'tbl_pages_sub', 'sub_id')->sub_pg_url; ?>"><?= get_page_url(9, 'tbl_pages_sub', 'sub_id')->sub_pg_title; ?></a>
                                </li>
                                <li>
                                    <a href="<?= base_url() ?>blog">Blog</a>
                                </li>
                            </ul>


                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="ftr-comp-tit">

                        </div>

                        <div class="fotr-lnk-1 pad-tp-ft">
                            <ul>
                                <?php
                                $footer_parent_menu = footer_parent_menu();
                                foreach ($footer_parent_menu as $items) {
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?><?= $items['pg_url'] ?>"><?= $items['pg_title'] ?></a>
                                    </li>
                                <?php } ?>
                            </ul>


                        </div>
                    </div>

                    <div class="col-md-3 pad-lft-0 pad-rgt-0">
                        <div class="ftr-comp-tit">
                            <h2>Connect With Us</h2>
                        </div>

                        <div class="fotr-lnk-2">
                            <ul>
                                <li>
                                    <a href="<?php echo get_social_media_links()->facebook; ?>" class="footr-fb" target="_blank">Facebook</a>
                                </li>
                                <li class="mrg-rgt-0">
                                    <a href="<?php echo get_social_media_links()->twitter; ?>" class="footr-twt" target="_blank">Twitter</a>
                                </li>
                                <li>
                                    <a href="<?php echo get_social_media_links()->pinterest; ?>" class="footr-pin" target="_blank">Pinterest</a>
                                </li>
                                <li class="mrg-rgt-0">
                                    <a href="<?php echo get_social_media_links()->linkedin; ?>" class="footr-lnklnd" target="_blank">Linkedin</a>
                                </li>
                            </ul>


                        </div>
                        <div class="ftr-lnk-3">	
                            <img src="<?= base_url() ?>front_resources/images/master-card.png" alt="" title=""/>
                            <img src="<?= base_url() ?>front_resources/images/paypal.png" alt="" title=""/>
                            <img src="<?= base_url() ?>front_resources/images/visa.png" alt="" title=""/>
                            <img src="<?= base_url() ?>front_resources/images/wu.png" alt="" title=""/>


                        </div>
                    </div>
                </div>        
            </div>    
        </div>
    </section>

    <section class="footr-bottm-sec">
        <div class="container">
            <div class="row">
                <div class="footr-wrp">
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                        <div class="fot-cpy">
                            <p>
                                &copy; Copyright <?php echo get_social_media_links()->name; ?> <?= Date('Y'); ?>. All Rights Reserved.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                        <div class="fot-selct">
                            <select>
                                <option selected hidden="hidden">Select language</option>
                                <option>English</option>
                                <option>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</footer>
<!--footer ends here-->