<!--Bootstrap and custom css start-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/main-dev2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/web-fonts/ufonts.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/animate.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/bootstrap-dropdownhover.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/amazonmenu.css" />


<!--Bootstrap and custom css end-->


<!-- Google Chrome Frame for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" /> -->


<!-- mobile meta (hooray!) -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" /> 
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no" />

<!--Bootstrap and custom js start-->
<script type="text/javascript" src="<?php echo base_url(); ?>front_resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front_resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front_resources/js/bootstrap-dropdownhover.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front_resources/js/amazonmenu.js"></script>


<!--Bootstrap and custom js start-->



