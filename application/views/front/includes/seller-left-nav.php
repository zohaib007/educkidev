<div class="col-sm-3 col-xs-12 pad-lft-0">
                	
                    <div class="my-account-left seller-nav">
                    	<h2>My Account</h2>
                        
                        <?php if($this->session->userdata('usertype') == "Seller" ) { ?>
                         <!-- Left Nav Section Starts -->
                                <div class="dashbrd-lnk">
                                    <h5>eDucki Market</h5>
                                    <ul>
                                        <li><a class="<?php if($this->uri->segment(1)=="seller-dashboard"){echo "active";}?>" href="<?=base_url('seller-dashboard');?>">Seller Dashboard</a></li>
                                        <li><a class="<?php if($this->uri->segment(1)=="edit-store"){echo "active";}?>" href="<?=base_url('edit-store');?>">My Store</a></li>
                                        <li><a class="<?php if($this->uri->segment(1)=="manage-products" || $this->uri->segment(1)=="create-new-product" || $this->uri->segment(1)=="edit-product"){echo "active";}?>" href="<?=base_url('manage-products');?>">Manage Products</a></li>
                                        <li><a class="<?php if($this->uri->segment(1)=="manage-order" || $this->uri->segment(1)=="order-waiting" || $this->uri->segment(1)=="order-history" || $this->uri->segment(1)=="search_order" || $this->uri->segment(1)=="order-history-details" || $this->uri->segment(1)=="order-details" || $this->uri->segment(1)=="order-pickup" || $this->uri->segment(1)=="order-pickup-details"){echo "active";}?>" href="<?=base_url('manage-order');?>">Manage Orders</a></li>
                                        <!-- 
                                        <li><a class="<?php if($this->uri->segment(1)=="facebook-store"){echo "active";}?>" href="<?=base_url('facebook-store');?>">Facebook Store</a></li>
                                         -->
                                        <li><a class="<?php if($this->uri->segment(1)=="seller-fans"){echo "active";}?>" href="<?=base_url('seller-fans');?>">My Fans</a></li>
                                        <li><a class="<?php if($this->uri->segment(1)=="seller-store-settings"){echo "active";}?>" href="<?=base_url('seller-store-settings');?>">Store Settings</a></li>
                                        <li><a href="mailto:info@educki.com">Ask eDucki?</a></li>
                                    </ul>
                                </div>
                                <!-- Left Nav Section Ends -->                                
                        <?php } ?>
                        <?php if($this->session->userdata('usertype')== "Buyer" && $this->uri->segment(1)!="become-a-seller" ) { ?>
                    	<a href="<?=base_url('become-a-seller');?>" class="bcom-sel">Become a Seller</a>
                        <?php } ?>
                        <!-- Left Nav Section Starts -->
                        <div class="dashbrd-lnk">
                            <h5>Message Center</h5>
                            <ul>
                                <li><a class="<?php if($this->uri->segment(1)=="messages"){echo "active";}?>" href="<?= base_url()?>messages">Inbox<?=((get_total_msg()>0)?'<div class="notify-bubble">'.get_total_msg().'</div>':'')?></a></li>
                            </ul>
                        </div>
                        <!-- Left Nav Section Ends -->
                        <div class="dashbrd-lnk">
                        	<h5>eDucki Market</h5>
                        	<ul>
                            	<li><a class="<?php if($this->uri->segment(1)=="my-dashboard"){echo "active";}?>" href="<?=base_url('my-dashboard');?>">My Dashboard</a></li>
                                <li><a class="<?php if($this->uri->segment(1)=="my-profile"){echo "active";}?>" href="<?=base_url('my-profile');?>" >My Profile</a></li>
                                <li><a class="<?php if($this->uri->segment(1)=="my-orders" || $this->uri->segment(1)=="my-order"){echo "active";}?>" href="<?=base_url('my-orders');?>" >My Orders</a></li>
                                <li><a class="<?php if($this->uri->segment(1)=="my-address" || $this->uri->segment(1)=="edit-address" || $this->uri->segment(1)=="add-address"){echo "active";}?>" href="<?=base_url('my-address');?>" >Address Book</a></li>
                                <li><a class="<?php if($this->uri->segment(1)=="my-wishlist"){echo "active";}?>" href="<?=base_url('my-wishlist');?>" >My Wishlist</a></li>
                            </ul>
                        </div>
                        <div class="tp-sellng-del large-view-seller">
                        	<h6>Deal Of The Day</h6>
                            <?php
                        $today_deal = getDealOfTheDay();
                        if (!empty($today_deal)) {
                            ?>
                            <?php foreach ($today_deal as $key => $deal) {
                                $url = productDetailUrl($deal['prod_id'])."/".$deal['prod_url'];
                            ?>                            
                        	<div class="mst-vied-deal">
                                        <div class="mst-vied-deal-lft">
                                            <a href="<?=$url?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $deal['img_name'] ?>" alt="" title="" style="max-width: 50px;"/></a>
                                        </div>    
                                        <div class="mst-vied-deal-rgt">
                                            <a href="<?=$url?>"><?= $deal['prod_title'] ?></a>
                                            <p class="sp1"><?= 100-(round(($deal['sale_price']/$deal['prod_price'])*100))?>% Off</p>
                                            <p>
                                            <?php
//                                            echo "<pre>";print_r($deal);die;
                                            $productPrice = 0.0;
                                            $sDate = date('Y-m-d', strtotime($deal['sale_start_date']));
                                            $eDate = date('Y-m-d', strtotime($deal['sale_end_date']));
                                            $tDate = date('Y-m-d');
                                            
                                            if ($deal['prod_onsale'] == 1) {
                                                if ($deal['sale_start_date'] != '' && $deal['sale_end_date'] != '') {
                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($deal['sale_start_date'] != '') {
                                                    if ($tDate >= $sDate) {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($deal['sale_end_date'] != '') {
                                                    if ($tDate <= $eDate) {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else {
                                                    $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                    echo '<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                }
                                            } else {
                                                $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                echo '$' . number_format($deal['prod_price'], 2, '.', '');
                                            }
                                            ?>         
                                                <!--
                                                <?php if ($deal['prod_onsale'] == 0) { ?>
                                                    $<?= number_format($deal['prod_price'], 2, '.', '') ?>
                                                <?php } else { ?>                                                    
                                                    <span class="sp1">$<?= number_format($deal['sale_price'], 2, '.', '') ?></span>
                                                    <span class="sp2">$<?= number_format($deal['prod_price'], 2, '.', '') ?></span>
                                                <?php } ?>
                                                    -->
                                            </p>
                                        </div>
                                    </div>
                            <?php }?>

                        <?php }else{?>
                            <p>No Deals are Available.</p>
                        <?php } ?>
                        </div>
                        <div class="tp-sellng-del large-view-seller">
                        	<h6>Top Selling Deals</h6>
                            <?php
                        $top_selling_deals = getTopSellingsDeals();
                        if (!empty($top_selling_deals)) {
                            ?>
                            <?php foreach ($top_selling_deals as $key => $selling_deal) {
                                $url = productDetailUrl($selling_deal['prod_id'])."/".$selling_deal['prod_url'];?>
                                <div class="mst-vied-deal">
                                        <div class="mst-vied-deal-lft">
                                            <a href="<?=$url?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $selling_deal['img_name'] ?>" alt="" title="" style="max-width: 50px;"/></a>
                                        </div>    
                                        <div class="mst-vied-deal-rgt">
                                            <a href="<?=$url?>"><?= $selling_deal['prod_title'] ?></a>
                                            <p class="sp1"><?= 100-(round(($selling_deal['sale_price']/$selling_deal['prod_price'])*100))?>% Off</p>
                                            <p>
                                            <?php
//                                            echo "<pre>";print_r($selling_deal);die;
                                            $productPrice = 0.0;
                                            $sDate = date('Y-m-d', strtotime($selling_deal['sale_start_date']));
                                            $eDate = date('Y-m-d', strtotime($selling_deal['sale_end_date']));
                                            $tDate = date('Y-m-d');
                                            
                                            if ($selling_deal['prod_onsale'] == 1) {
                                                if ($selling_deal['sale_start_date'] != '' && $selling_deal['sale_end_date'] != '') {
                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($selling_deal['sale_start_date'] != '') {
                                                    if ($tDate >= $sDate) {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($selling_deal['sale_end_date'] != '') {
                                                    if ($tDate <= $eDate) {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else {
                                                    $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                    echo '<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                }
                                            } else {
                                                $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                echo '$' . number_format($selling_deal['prod_price'], 2, '.', '');
                                            }
                                            ?>    
                                                <!--
                                                <?php if ($selling_deal['prod_onsale'] == 0) { ?>
                                                    $<?= number_format($selling_deal['prod_price'], 2, '.', '') ?>
                                                <?php } else { ?>                                                    
                                                    $<?= number_format($selling_deal['sale_price'], 2, '.', '') ?>
                                                    <span class="sp2">$<?= number_format($selling_deal['prod_price'], 2, '.', '') ?></span>
                                                <?php } ?>
                                                    -->
                                            </p>
                                        </div>
                                    </div>
                           <?php }?>

                        <?php }else{?>
                        	<p>No Deals are Available.</p>
                        <?php } ?>
                        </div>
                        
                            <div class="tp-sellng-del large-view-seller">
                                <h6>Most Viewed Deals</h6>
                                <?php
                            $most_viewed_product = getMostViewedDeals();
                        if (!empty($most_viewed_product)) {
                            ?>
                                <?php foreach ($most_viewed_product as $data) { 
                                    $url = productDetailUrl($data['prod_id'])."/".$data['prod_url']; ?>
                                    <div class="mst-vied-deal">
                                        <div class="mst-vied-deal-lft">
                                            <a href="<?=$url?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $data['img_name'] ?>" alt="" title="" /></a>
                                        </div>    
                                        <div class="mst-vied-deal-rgt">
                                            <a href="<?=$url?>"><?= $data['prod_title'] ?></a>
                                            <p class="sp1"><?= 100-(round(($data['sale_price']/$data['prod_price'])*100))?>% Off</p>
                                            <p>
                                            <?php
//                                            echo "<pre>";print_r($data);die;
                                            $productPrice = 0.0;
                                            $sDate = date('Y-m-d', strtotime($data['sale_start_date']));
                                            $eDate = date('Y-m-d', strtotime($data['sale_end_date']));
                                            $tDate = date('Y-m-d');
                                            
                                            if ($data['prod_onsale'] == 1) {
                                                if ($data['sale_start_date'] != '' && $data['sale_end_date'] != '') {
                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($data['sale_start_date'] != '') {
                                                    if ($tDate >= $sDate) {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($data['sale_end_date'] != '') {
                                                    if ($tDate <= $eDate) {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else {
                                                    $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                    echo '<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                }
                                            } else {
                                                $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                echo '$' . number_format($data['prod_price'], 2, '.', '');
                                            }
                                            ?>  
<!--                                                
                                                <?php if ($data['prod_onsale'] == 0) { ?>
                                                    $<?= number_format($data['prod_price'], 2, '.', '') ?>
                                                <?php } else { ?>
                                                    $<?= number_format($data['sale_price'], 2, '.', '') ?>                                                    
                                                    <span class="sp2">$<?= number_format($data['prod_price'], 2, '.', '') ?></span>
                                                <?php } ?>
-->
                                            </p>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php }else{ ?>
                                <p>No Deals are Available.</p>
                                <?php } ?>
                            </div>
                    </div>
                </div>