<div class="col-sm-3 col-xs-12 pad-lft-0 mobile-view-seller">
                	
                    <div class="my-account-left seller-nav">
                        
                        <div class="tp-sellng-del">
                        	<h6>Deal Of The Day</h6>
                            <?php
                        $today_deal = getDealOfTheDay();
                        if (!empty($today_deal)) {
                            ?>
                            <?php foreach ($today_deal as $key => $deal) {
                                $url = productDetailUrl($deal['prod_id'])."/".$deal['prod_url'];
                            ?>                            
                        	<div class="mst-vied-deal">
                                        <div class="mst-vied-deal-lft">
                                            <a href="<?=$url?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $deal['img_name'] ?>" alt="" title="" style="max-width: 50px;"/></a>
                                        </div>    
                                        <div class="mst-vied-deal-rgt">
                                            <a href="<?=$url?>"><?= $deal['prod_title'] ?></a>
                                            <p class="sp1">20% Off</p>
                                            <p>
                                            <?php
//                                            echo "<pre>";print_r($deal);die;
                                            $productPrice = 0.0;
                                            $sDate = date('Y-m-d', strtotime($deal['sale_start_date']));
                                            $eDate = date('Y-m-d', strtotime($deal['sale_end_date']));
                                            $tDate = date('Y-m-d');
                                            
                                            if ($deal['prod_onsale'] == 1) {
                                                if ($deal['sale_start_date'] != '' && $deal['sale_end_date'] != '') {
                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($deal['sale_start_date'] != '') {
                                                    if ($tDate >= $sDate) {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($deal['sale_end_date'] != '') {
                                                    if ($tDate <= $eDate) {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else {
                                                    $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                    echo '<span class="blacktxt">$' . number_format($deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($deal['prod_price'], 2, '.', '') . '</span>';
                                                }
                                            } else {
                                                $productPrice = number_format($deal['prod_price'], 2, '.', '');
                                                echo '$' . number_format($deal['prod_price'], 2, '.', '');
                                            }
                                            ?>         
                                                <!--
                                                <?php if ($deal['prod_onsale'] == 0) { ?>
                                                    $<?= number_format($deal['prod_price'], 2, '.', '') ?>
                                                <?php } else { ?>                                                    
                                                    <span class="sp1">$<?= number_format($deal['sale_price'], 2, '.', '') ?></span>
                                                    <span class="sp2">$<?= number_format($deal['prod_price'], 2, '.', '') ?></span>
                                                <?php } ?>
                                                    -->
                                            </p>
                                        </div>
                                    </div>
                            <?php }?>

                        <?php }else{?>
                            <p>No Deals are Available.</p>
                        <?php } ?>
                        </div>
                        <div class="tp-sellng-del">
                        	<h6>Top Selling Deals</h6>
                            <?php
                        $top_selling_deals = getTopSellingsDeals();
                        if (!empty($top_selling_deals)) {
                            ?>
                            <?php foreach ($top_selling_deals as $key => $selling_deal) {
                                $url = productDetailUrl($selling_deal['prod_id'])."/".$selling_deal['prod_url'];?>
                                <div class="mst-vied-deal">
                                        <div class="mst-vied-deal-lft">
                                            <a href="<?=$url?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $selling_deal['img_name'] ?>" alt="" title="" style="max-width: 50px;"/></a>
                                        </div>    
                                        <div class="mst-vied-deal-rgt">
                                            <a href="<?=$url?>"><?= $selling_deal['prod_title'] ?></a>
                                            <p class="sp1">20% Off</p>
                                            <p>
                                            <?php
//                                            echo "<pre>";print_r($selling_deal);die;
                                            $productPrice = 0.0;
                                            $sDate = date('Y-m-d', strtotime($selling_deal['sale_start_date']));
                                            $eDate = date('Y-m-d', strtotime($selling_deal['sale_end_date']));
                                            $tDate = date('Y-m-d');
                                            
                                            if ($selling_deal['prod_onsale'] == 1) {
                                                if ($selling_deal['sale_start_date'] != '' && $selling_deal['sale_end_date'] != '') {
                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($selling_deal['sale_start_date'] != '') {
                                                    if ($tDate >= $sDate) {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($selling_deal['sale_end_date'] != '') {
                                                    if ($tDate <= $eDate) {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else {
                                                    $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                    echo '<span class="blacktxt">$' . number_format($selling_deal['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($selling_deal['prod_price'], 2, '.', '') . '</span>';
                                                }
                                            } else {
                                                $productPrice = number_format($selling_deal['prod_price'], 2, '.', '');
                                                echo '$' . number_format($selling_deal['prod_price'], 2, '.', '');
                                            }
                                            ?>    
                                                <!--
                                                <?php if ($selling_deal['prod_onsale'] == 0) { ?>
                                                    $<?= number_format($selling_deal['prod_price'], 2, '.', '') ?>
                                                <?php } else { ?>                                                    
                                                    $<?= number_format($selling_deal['sale_price'], 2, '.', '') ?>
                                                    <span class="sp2">$<?= number_format($selling_deal['prod_price'], 2, '.', '') ?></span>
                                                <?php } ?>
                                                    -->
                                            </p>
                                        </div>
                                    </div>
                           <?php }?>

                        <?php }else{?>
                        	<p>No Deals are Available.</p>
                        <?php } ?>
                        </div>
                        
                            <div class="tp-sellng-del">
                                <h6>Most Viewed Deals</h6>
                                <?php
                            $most_viewed_product = getMostViewedDeals();
                        if (!empty($most_viewed_product)) {
                            ?>
                                <?php foreach ($most_viewed_product as $data) { 
                                    $url = productDetailUrl($data['prod_id'])."/".$data['prod_url']; ?>
                                    <div class="mst-vied-deal">
                                        <div class="mst-vied-deal-lft">
                                            <a href="<?=$url?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $data['img_name'] ?>" alt="" title="" /></a>
                                        </div>    
                                        <div class="mst-vied-deal-rgt">
                                            <a href="<?=$url?>"><?= $data['prod_title'] ?></a>
                                            <p class="sp1">20% Off</p>
                                            <p>
                                            <?php
//                                            echo "<pre>";print_r($data);die;
                                            $productPrice = 0.0;
                                            $sDate = date('Y-m-d', strtotime($data['sale_start_date']));
                                            $eDate = date('Y-m-d', strtotime($data['sale_end_date']));
                                            $tDate = date('Y-m-d');
                                            
                                            if ($data['prod_onsale'] == 1) {
                                                if ($data['sale_start_date'] != '' && $data['sale_end_date'] != '') {
                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($data['sale_start_date'] != '') {
                                                    if ($tDate >= $sDate) {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                        echo '$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else if ($data['sale_end_date'] != '') {
                                                    if ($tDate <= $eDate) {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    } else {
                                                        echo 'Price:<span class="blacktxt">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                    }
                                                } else {
                                                    $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                    echo '<span class="blacktxt">$' . number_format($data['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($data['prod_price'], 2, '.', '') . '</span>';
                                                }
                                            } else {
                                                $productPrice = number_format($data['prod_price'], 2, '.', '');
                                                echo '$' . number_format($data['prod_price'], 2, '.', '');
                                            }
                                            ?>  
<!--                                                
                                                <?php if ($data['prod_onsale'] == 0) { ?>
                                                    $<?= number_format($data['prod_price'], 2, '.', '') ?>
                                                <?php } else { ?>
                                                    $<?= number_format($data['sale_price'], 2, '.', '') ?>                                                    
                                                    <span class="sp2">$<?= number_format($data['prod_price'], 2, '.', '') ?></span>
                                                <?php } ?>
-->
                                            </p>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php }else{ ?>
                                <p>No Deals are Available.</p>
                                <?php } ?>
                            </div>
                    </div>
                </div>