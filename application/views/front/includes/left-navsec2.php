<div class="col-sm-3 col-xs-12 pad-lft-0">

    <div class="my-account-left seller-nav">
        <h2>My Account</h2>

        <!-- Left Nav Section Starts -->
        <div class="dashbrd-lnk">
            <h5>eDucki Market</h5>
            <ul>
                <li><a href="<?= base_url(); ?>my-dashboard/">Seller Dashboard</a></li>
                <li><a href="#">My Store</a></li>
                <li><a href="#">Manage Products</a></li>
                <li><a href="#">Manage Orders</a></li>
                <li><a href="#">Facebook Store</a></li>
                <li><a href="#">My Fans</a></li>
                <li><a href="#">Store Settings</a></li>
                <li><a href="#">Ask eDucki?</a></li>
            </ul>
        </div>
        <!-- Left Nav Section Ends -->

        <!-- Left Nav Section Starts -->
        <div class="dashbrd-lnk">
            <h5>Message Center</h5>
            <ul>
                <li><a href="<?= base_url() ?>messages">Inbox</a></li>
            </ul>
        </div>
        <!-- Left Nav Section Ends -->

        <!-- Left Nav Section Starts -->
        <div class="dashbrd-lnk">
            <h5>eDucki Market</h5>
            <ul>
                <li><a href="<?= base_url(); ?>my-dashboard/">My Dashboard</a></li>
                <li><a href="#">My Profile</a></li>
                <li><a href="#">My Orders</a></li>
                <li><a href="#">Address Book</a></li>
                <li><a href="#">My Wishlist</a></li>
            </ul>
        </div>
        <!-- Left Nav Section Ends -->

        <div class="tp-sellng-del">
            <h6>Deal Of The Day</h6>
            <div class="mst-vied-deal">
                <div class="mst-vied-deal-lft">
                    <img src="<?= base_url() ?>front_resources/images/most-viewed.jpg" alt="" title="" />
                </div>

                <div class="mst-vied-deal-rgt">
                    <a href="#">Casual Black Watch for Men</a>
                    <p class="sp1">20% Off</p>
                    <p>$80.00<span class="sp2">$100.00</span></p>
                </div>

            </div>
        </div>

        <div class="tp-sellng-del">
            <h6>Top Selling Deals</h6>
            <p>Currently no deals available</p>
        </div>

        <div class="tp-sellng-del">
            <h6>Most Viewed Deals</h6>
            <div class="mst-vied-deal">
                <div class="mst-vied-deal-lft">
                    <img src="<?= base_url() ?>front_resources/images/most-viewed.jpg" alt="" title="" />
                </div>

                <div class="mst-vied-deal-rgt">
                    <a href="#">Sample Text Here Sample Text</a>
                    <p class="sp1">20% Off</p>
                    <p>$80.00<span class="sp2">$100.00</span></p>
                </div>

            </div>
            <div class="mst-vied-deal">
                <div class="mst-vied-deal-lft">
                    <img src="<?= base_url() ?>front_resources/images/most-viewed.jpg" alt="" title="" />
                </div>

                <div class="mst-vied-deal-rgt">
                    <a href="#">Sample Text Here Sample Text</a>
                    <p class="sp1">20% Off</p>
                    <p>$80.00<span class="sp2">$100.00</span></p>
                </div>

            </div>
            <div class="mst-vied-deal">
                <div class="mst-vied-deal-lft">
                    <img src="<?= base_url() ?>front_resources/images/most-viewed.jpg" alt="" title="" />
                </div>

                <div class="mst-vied-deal-rgt">
                    <a href="#">Sample Text Here Sample Text</a>
                    <p class="sp1">20% Off</p>
                    <p>$80.00<span class="sp2">$100.00</span></p>
                </div>

            </div>
        </div>
    </div>
</div>