<!--header starts here-->
<div class="hdr-new-rw"> 
    <div class="container">
        <div class="row">
            <div class="auto-container-hed2">
                <div class="crt-logo">
                    <a class="img-responsive" href="<?= base_url() ?>"><img src="<?= base_url() ?>resources/company_logo/<?=getSiteLogo()?>"/></a>
                </div>
                <div class="crt-no">
                    <span></span>
                    <h3><?= getSiteSettings("site_phone") ?></h3>
                </div>	

            </div>
        </div>
    </div>
    <!--lower header starts here-->   
</div>
<!--login popup starts here-->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-3" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sign In to eDucki</h5>
                <div class="pp-fr-mg">
                    <a href="<?php  echo $this->facebook->login_url(); ?>">
                        <img src="<?= base_url() ?>front_resources/images/popup-fb.png" alt="" title=""/>
                    </a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="pop-bdr">
                <p>
                    or
                </p>
            </div>
            <div class="modal-body fro-frg">
                <input type="text" id="loginEmail" placeholder="Email" />
                <div class="error"></div>
                <div class="only-reltve">
                    <input type="password" id="loginPassword" placeholder="Password" name="password">
                    <a href="<?=base_url('forgot-password')?>" class="frg-pas-2">I forgot</a>
                    <div class="error loginPasswordError"></div>
                </div>
                <div class="chk-bx-lg">
                    <input type="checkbox" id="remember_me" />
                    <p>Keep me signed in</p>
                </div>
                <input type="button" data-dismiss="modal" aria-label="Close" value="Nevermind" class="nvrmnd" />

                <input type="button" class="loginbtn" value="Sign in" />
            </div>
        </div>
    </div>
</div>
<!--login popup ends here-->







