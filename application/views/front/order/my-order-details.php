<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a> </li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-orders'); ?>">My Orders</a></li>
                        <li class="breadcrumb-item active">Order Detail</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends here-->
<!--Blog main starts here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view("front/includes/seller-left-nav") ?>
                <!--Left section starts here-->
                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">
                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad" style="display:none;">
                                <div class="msg-hd-inrow">
                                    <h5>Order Detail</h5>
                                </div>
                                <!-- <p>Sample Text Here.</p> -->
                            </div>
                            <div id="message" class="alert message hide"></div>
                            <!-- Heading Section Ends -->
                            <!-- Bottom Section Starts -->
                            <div class="store-btmWrap">
                                <!--<form name="track-frm" method="post">-->
                                <!-- Store Section Starts -->
                                <div class="store-mainsec">
                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">
                                            <!-- Search By Form Starts -->
                                            <div class="store-tophdr">
                                                <!-- Left Heading Starts -->
                                                <div class="store-th">
                                                    <h3>Order Detail</h3>
                                                </div>
                                                <!-- Left Heading Ends -->
                                                <div class="store-comp">
                                                    <a id="re_order" href="javascript:void(0);" onclick="reorder(<?= $order_details->order_id ?>)">Reorder</a>
                                                    <span>|</span>
                                                    
                                                    <a href="<?= base_url() ?>print_my_order/<?= $order_details->order_id ?>" class="prt-btn" data-toggle="tooltip" data-placement="bottom" title="Print Order in PDF format" target="_blank">Print Order</a>
                                                </div>
                                            </div>
                                            <!-- Search By Form Ends -->
                                        </div>
                                    </div>
                                    <!-- Top Grey Bar Ends -->

                                    <!-- Bottom Grey Section Starts -->
                                    <div class="btm-boxwrap">
                                        <div class="btm-boxinner">
                                            <!-- Page Section Starts -->
                                            <div class="btm-odrdtl-wrap">
                                                <!-- Border Row Starts -->
                                                <div class="btm-odbdr">
                                                    <div class="order-viewdtl">
                                                        <!-- Track Section Starts -->
                                                        <div class="banything-wrapper">
                                                            <!-- Row Starts -->
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Order Date:</div>
                                                                <div class="banything-dtl"><?= date('F d, Y', strtotime($order_details->order_date)) ?></div>
                                                            </div>
                                                            <!-- Row Starts -->
                                                            <!-- Row Starts -->
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Order ID:</div>
                                                                <div class="banything-dtl"><?= $order_details->order_id ?></div>
                                                            </div>
                                                            <!-- Row Starts -->
                                                            <!-- Row Starts -->
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Order Total:</div>
                                                                <div class="banything-dtl">$<?= number_format($order_details->order_grand_total, 2, '.', '') ?></div>
                                                            </div>
                                                            <!--<div class="banything-row">
                                                                <div class="banything-lbl">
                                                                    <span class="tracknum">Tracking #</span>
                                                                </div>
                                                                <div class="banything-dtl">
                                                                    <span class="tracknum"><?= $order_details->traking_number ?></span>
                                                                </div>
                                                            </div>-->
                                                            <?php $status = '';
                                                                if($order_details->order_status == 'cancel'){
                                                                    $status = 'Cancel';
                                                                }elseif($order_details->order_status == 'Awaiting Tracking' || $order_details->order_status == 'Awaiting Pickup'){
                                                                    $status = 'Processing';
                                                                }elseif($order_details->order_status == 'completed'){
                                                                    $status = 'Completed';
                                                                }elseif($order_details->order_status == 'In Process'){
                                                                    $status = 'Processing';
                                                                }
                                                                
                                                                ?>
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Status:</div>
                                                                <div class="banything-dtl"><?= $status ?></div>
                                                            </div>
                                                            <!-- Row Starts -->
                                                        </div>
                                                        <!-- Track Section Ends -->
                                                    </div>
                                                </div>
                                                <!-- Border Row Ends -->
                                                <!-- Border Row Starts -->
                                                <div class="btm-odbdr btm-odbdr-new">
                                                    <div class="shipbil-view">
                                                        <?php $ship_adrs = json_decode($order_details->order_shipping_address);
                                                        $bill_adrs = json_decode($order_details->order_billing_address); ?>
                                                        <!-- Main Row Starts -->
                                                        <div class="shipbil-vrow">
                                                            <!-- Col Starts -->
                                                            <div class="shipbil-lcol">
                                                                <!-- Track Section Starts -->
                                                                <div class="inany-wrapper">
                                                                    <!-- Row Starts -->
                                                                    <div class="inany-row">
                                                                        <h3>Shipping Address</h3>
                                                                    </div>
                                                                    <!-- <div class="inany-row">
                                                                        <div class="inany-lbl">Nickname:</div>
                                                                        <div class="inany-dtl"><?= $ship_adrs->shipping_nick_name ?></div>
                                                                    </div> -->
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Name:</div>
                                                                        <div class="inany-dtl"><?= $ship_adrs->shipping_first_name ?> <?= $ship_adrs->shipping_last_name ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Telephone:</div>
                                                                        <div class="inany-dtl"><?= (!empty($ship_adrs->shipping_phone))?$ship_adrs->shipping_phone:'N/A'?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Street:</div>
                                                                        <div class="inany-dtl"><?= $ship_adrs->shipping_street ?></div>
                                                                    </div>                
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">City:</div>
                                                                        <div class="inany-dtl"><?= $ship_adrs->shipping_city ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">State:</div>
                                                                        <div class="inany-dtl"><?php
                                                                            if ($ship_adrs->shipping_country == 'US') {
                                                                                echo get_state_name($ship_adrs->shipping_state);
                                                                            } else {
                                                                                echo $ship_adrs->shipping_state_other;
                                                                            }
                                                                            ?></div>
                                                                    </div>                                               
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Country:</div>
                                                                        <div class="inany-dtl"><?= get_country_name($ship_adrs->shipping_country) ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Zip:</div>
                                                                        <div class="inany-dtl"><?= $ship_adrs->shipping_zip ?></div>
                                                                    </div>
                                                                    <!-- Row Starts -->
                                                                </div>
                                                                <!-- Track Section Ends -->
                                                            </div>
                                                            <div class="shipbil-lcol">
                                                                <!-- Track Section Starts -->
                                                                <div class="inany-wrapper">
                                                                    <!-- Row Starts -->
                                                                    <div class="inany-row">
                                                                        <h3>Billing Address</h3>
                                                                    </div>
                                                                    <!-- <div class="inany-row">
                                                                        <div class="inany-lbl">Nickname:</div>
                                                                        <div class="inany-dtl"><?= $bill_adrs->billing_nick_name ?></div>
                                                                    </div> -->
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Name:</div>
                                                                        <div class="inany-dtl"><?= $bill_adrs->billing_first_name ?> <?= $bill_adrs->billing_last_name ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Telephone:</div>
                                                                        <div class="inany-dtl"><?= (!empty($bill_adrs->billing_phone))?$bill_adrs->billing_phone:'N/A'?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Street:</div>
                                                                        <div class="inany-dtl"><?= $bill_adrs->billing_street ?></div>
                                                                    </div>                
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">City:</div>
                                                                        <div class="inany-dtl"><?= $bill_adrs->billing_city ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">State:</div>
                                                                        <div class="inany-dtl"><?php
                                                                            if ($bill_adrs->billing_country == 'US') {
                                                                                echo get_state_name($bill_adrs->billing_state);
                                                                            } else {
                                                                                echo $bill_adrs->billing_state_other;
                                                                            }
                                                                            ?></div>
                                                                    </div>                                               
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Country:</div>
                                                                        <div class="inany-dtl"><?= get_country_name($bill_adrs->billing_country) ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Zip:</div>
                                                                        <div class="inany-dtl"><?= $bill_adrs->billing_zip ?></div>
                                                                    </div>
                                                                    <!-- Row Starts -->
                                                                </div>
                                                                <!-- Track Section Ends -->
                                                            </div>
                                                            <!-- Col Ends -->
                                                        </div>
                                                        <!-- Main Row Ends -->
                                                    </div>
                                                    <div class="banything-row-new">
                                                        <div class="banything-row">
                                                            <div class="banything-lbl">Order Summary</div>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <!-- Border Row Ends -->
                                                <!-- Border Row Starts -->
                                                <div class="btm-odbdr myfan-suny">
                                                    <div class="odrList-wrap">
                                                        <!-- Bottom My Fan Section Starts -->
                                                        <div class="myfan-listwrap">
                                                            <?php foreach ($order_products as $key => $o_prod) { ?> 
                                                                <!-- List Row Starts -->
                                                                <div class="myfan-listrow <?php
                                                                if ($total_prods == $key) {
                                                                    echo 'lastbdr';
                                                                }
                                                                ?>">
                                                                    <!-- Image Section Starts -->
                                                                    <div class="myfan-img">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <a href="<?= productDetailUrl($o_prod->order_prod_id) ?>/<?= $o_prod->order_prod_url ?>">
                                                                                        <img src="<?= base_url() ?>resources/prod_images/thumb/<?= $o_prod->order_prod_image ?>" alt="" />
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <!-- Image Section Ends -->
                                                                    <!-- Right Section Starts -->
                                                                    <div class="myfan-right">
                                                                        <div class="myfan-rinner">
                                                                            <!-- Order Content Section Starts -->
                                                                            <div class="myfan-rtoptxt sec01">
                                                                                <p>
                                                                                    <a href="<?= productDetailUrl($o_prod->order_prod_id) ?>/<?= $o_prod->order_prod_url ?>"><?= $o_prod->order_prod_name ?></a>
                                                                                </p>
                                                                                <p>
                                                                                    <?php $filters = json_decode($o_prod->order_prod_filters);
                                                                                    if(count($filters) > 0)
                                                                                    foreach ($filters as $i => $filter) {?>
                                                                                        <p><b><?=$filter->filterName?></b>:<?=$filter->filtervalue?></p>
                                                                                    <?php }?>
                                                                                    <?= $o_prod->order_prod_return ?><br />
                                                                                    Qty: <?= $o_prod->order_prod_qty ?>
                                                                                </p>
                                                                                <?php if($o_prod->order_prod_shipping!='Local Pick Up'){?>
                                                                                <p>
                                                                                    Tracking #: <?= (!empty($o_prod->order_prod_tracking_number))?$o_prod->order_prod_tracking_number:'Awaiting Tracking'?>
                                                                                </p>
                                                                                <?php }?>
                                                                                <p>
                                                                                    <?php 
                                                                                    $prod_status = '';
                                                                                    if($o_prod->order_prod_status=='In Process'){
                                                                                        $prod_status = 'Shipped';
                                                                                    }elseif(($o_prod->order_prod_status=='Awaiting Tracking' || $o_prod->order_prod_status=='Awaiting Pickup') && $o_prod->order_prod_shipping!='Local Pick Up'){
                                                                                        $prod_status = 'Processing';
                                                                                    }elseif(($o_prod->order_prod_status=='Awaiting Tracking' || $o_prod->order_prod_status=='Awaiting Pickup') && $o_prod->order_prod_shipping=='Local Pick Up'){
                                                                                        $prod_status = 'Awaiting Pickup';
                                                                                    }elseif($o_prod->order_prod_status=='completed'){
                                                                                        $prod_status = 'Delivered';
                                                                                    }elseif($o_prod->order_prod_status=='cancel'){
                                                                                        $prod_status = 'Canceled';
                                                                                    }?>
                                                                                    Status: <?= $prod_status?>
                                                                                </p>
                                                                                <?php if($this->session->userdata('userid')!=$o_prod->seller_id){?>
                                                                                    <a href="javascript:void(0)" class="mesg-toselr msg-seller" data-id ="<?= $o_prod->order_prod_id ?>">Message Seller</a>
                                                                                <?php }?>
                                                                            </div>
                                                                            <!-- Order Content Section Ends -->
                                                                            <!-- Acknowledge Section Starts -->
                                                                            <?php if(!empty($o_prod->order_prod_completion_date)){?>
                                                                            <div class="myfan-rtoptxt ack-txt sec02">
                                                                                    Acknowledged<br>
                                                                                    On<br>
                                                                                    <?=date("F j, Y", strtotime($o_prod->order_prod_completion_date)); ?>
                                                                            </div>
                                                                            <?php }?>
                                                                            <!-- Acknowledge Section Ends -->
                                                                            <!-- Ship Method Section Starts -->
                                                                            <div class="shipbil-rcol sec03">
                                                                                <!-- Track Section Starts -->
                                                                                <div class="inany-wrapper">
                                                                                    <!-- Row Starts -->
                                                                                    <div class="inany-row">
                                                                                        <h3>Shipping Method</h3>
                                                                                    </div>
                                                                                    <!-- Row Starts -->
                                                                                    <!-- Row Starts -->
                                                                                    <div class="inany-row">
                                                                                        <?php $shipping='';
                                                                                        if($o_prod->order_prod_shipping=='Charge for Shipping'){
                                                                                            $shipping = $o_prod->order_prod_shipping_methods;
                                                                                        }else{
                                                                                            $shipping = '';
                                                                                        }
                                                                                        echo (!empty($shipping))?$shipping:''?>
                                                                                    </div>
                                                                                    <div class="inany-row">
                                                                                        <?php $days='';
                                                                                        if($o_prod->order_prod_shipping=='Charge for Shipping'){
                                                                                            $days = $o_prod->order_prod_ship_days+$dayz;
                                                                                        }elseif($o_prod->order_prod_shipping=='Offer free Shipping'){
                                                                                            $days = $o_prod->order_prod_free_ship_days+$dayz;
                                                                                        }else{
                                                                                            $days = $dayz;
                                                                                        }
                                                                                        echo $o_prod->order_prod_shipping?> <?php if($o_prod->order_prod_status!='Delivered' && $o_prod->order_prod_status!='In Process' && $o_prod->order_prod_status!='completed' && $o_prod->order_prod_status!='cancel'){ echo '( '.$days.' day(s) )';} ?>
                                                                                    </div>
                                                                                    <!-- Row Starts -->
                                                                                </div>
                                                                                <!-- Track Section Ends -->
                                                                                <!-- New Section Starts -->
                                                                                <div class="fan-con">
                                                                                    <div class="shipbil-rcol">
                                                                                        <!-- Track Section Starts -->
                                                                                        <div class="inany-wrapper odrsummary">
                                                                                            <!-- Row Starts -->
                                                                                            <div class="inany-row">
                                                                                                <div class="inany-lbl">Item Price:</div>
                                                                                                <div class="inany-dtl">$<?= $o_prod->order_prod_total_price ?></div>
                                                                                            </div>
                                                                                            <!-- Row Starts -->

                                                                                            <!-- Row Starts -->
                                                                                            <div class="inany-row">
                                                                                                <div class="inany-lbl">Shipping &amp; Handing:</div>
                                                                                                <div class="inany-dtl">$<?= number_format($o_prod->order_prod_ship_price, 2, '.', '') ?></div>
                                                                                            </div>
                                                                                            <!-- Row Starts -->
                                                                                        </div>
                                                                                        <!-- Track Section Ends -->
                                                                                    </div>
                                                                                </div>
                                                                                <!-- New Section Ends -->
                                                                            </div>
                                                                            <!-- Ship Method Section Ends -->

                                                                            <div class="nw-frm-txtarea" id="nw-txtarea<?= $o_prod->order_prod_id ?>" style="display:none;">
                                                                                <textarea placeholder="Write a Text" id="txtarea<?= $o_prod->order_prod_id ?>"></textarea>
                                                                                <input type="button" value="Send" class="send_message" data-sellermail="<?=$o_prod->seller_email?>" data-prod="<?= $o_prod->order_prod_id ?>" data-seller="<?= $o_prod->seller_id ?>"/>
                                                                                <div class="error" id="txtarea_error<?= $o_prod->order_prod_id ?>">test error</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Right Section Ends -->
                                                                </div>
                                                                <!-- List Row Ends -->
                                                            <?php } ?> 
                                                            <div class="fan-con">
                                                                <div class="shipbil-rcol">
                                                                    <!-- Track Section Starts -->
                                                                    <div class="inany-wrapper odrsummary">
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Sub Total:</div>
                                                                            <div class="inany-dtl">$<?= number_format($order_details->order_sub_total, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Shipping &amp; Handing:</div>
                                                                            <div class="inany-dtl">$<?= number_format($order_details->order_shipping_price, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Discount:</div>
                                                                            <div class="inany-dtl">$<?= number_format($order_details->order_discount, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                    </div>
                                                                    <!-- Track Section Ends -->
                                                                </div>

                                                                <div class="shipbil-rcol">
                                                                    <!-- Track Section Starts -->
                                                                    <div class="inany-wrapper odrsummary">
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl odrtotal ptop">Order Total</div>
                                                                            <div class="inany-dtl odrtotal ptop">$<?= number_format($order_details->order_grand_total, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                    </div>
                                                                    <!-- Track Section Ends -->
                                                                </div>
                                                            </div>
                                                            <?php
                                                            /*foreach ($order_products as $o_prod) {
                                                                if ($o_prod->order_prod_tracking_number == '' && $o_prod->order_prod_status != 'cancel') {
                                                                    if ($o_prod->order_prod_shipping == 'Local Pick Up' && $o_prod->order_prod_status != 'completed') {
                                                                        $cancel_flag = 1;
                                                                    }
                                                                }
                                                            }*/
                                                            if ($cancel_flag != 0 && $order_details->order_status != 'cancel') { ?>
                                                                <div class="fan-con-btn">
                                                                    <input type="submit" id="cancel_order" value="Cancel Order" >
                                                                </div>
                                                            <?php } ?>
                                                            <!-- Back To Order Section Starts -->
                                                            <div class="btorder-row">
                                                                <div class="btorder-lk">
                                                                    <a href="<?= base_url() ?>my-orders" title="Back to Orders">&lt;&lt; Back to Orders</a>
                                                                </div>
                                                            </div>
                                                            <!-- Back To Order Section Ends -->
                                                        </div>
                                                        <!-- Bottom My Fan Section Ends -->
                                                    </div>
                                                </div>
                                                <!-- Border Row Ends -->
                                            </div>
                                            <!-- Page Section Ends -->
                                        </div>
                                    </div>
                                    <!-- Bottom Grey Section Ends -->
                                </div>
                                <!-- Store Section Ends -->
                                <!--</form>-->
                            </div>
                            <!-- Bottom Section Ends -->
                        </div>
                        <!-- Inner Section Ends -->
                    </div>
                </div>
                <!-- Right Pannel Starts Here -->
            </div>
        </div>
    </div>
</section>



<!-- Popup Starts Here -->
<a data-toggle="modal" data-target="#myModal4" id="myAnchor" style="display: none;">Click HERE</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">

        <div class="modal-content">
            <div class="modal-header pad-0">        
                <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body pad-0">
                <div class="frgt-sec oneline">
                    <div class="fgt-tit">
                        <p class="mrg-botm-0" id="wishlist-text"></p>
                    </div>
                </div>
            </div>

        </div>

        <!-- Ended 2 divs below -->
    </div>
</div>
<!-- Popup Ends Here -->

<!--Blog main ends  here-->
<script>
    $(document).ready(function () {
        $(document).on('click', '.send_message', function () {
            var seller_id = $(this).attr('data-seller');
            var prod_id = $(this).attr('data-prod');
            var message = $('#txtarea' + prod_id).val();
            var email = $(this).attr('data-sellermail');
            var error = 0;
            if (message != '') {
                error = 0;
                /*var msg = '';
                 msg = msg + 'Seller : '+seller_id;
                 msg = msg + 'Product ID : '+prod_id;
                 msg = msg + 'Message : '+message;
                 alert(msg);*/
            } else {
                error = 1;
                $('#txtarea_error' + prod_id).removeClass('success');
                $('#txtarea_error' + prod_id).addClass('error');
                $('#txtarea_error' + prod_id).html('This field is required.');
            }
            if (error == 0) {

                /*$.ajax({
                 type: 'POST',
                 url: "<?= base_url('submit-message') ?>",
                 data:{<?= $this->security->get_csrf_token_name() ?>:'<?= $this->security->get_csrf_hash() ?>'},
                 success:function (res){
                 console.log(res);
                 },
                 error:function(){
                 console.log('Network Error');
                 }
                 });*/

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url() ?>submit-message',
                    data: {email: email, seller_id: seller_id, prod_id: prod_id, message: message,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    success: function (res) {
                        //myObj = JSON.parse(res);
                        if (res == '200') {
                            /*window.location = "<?= base_url() ?>";*/
                            $('#txtarea_error' + prod_id).removeClass('error');
                            $('#txtarea_error' + prod_id).addClass('success');
                            $('#txtarea' + prod_id).val('');
                            $('#txtarea_error' + prod_id).html('Message sent Successfully.');
                            console.log('OK');
                        }
                    },
                    error: function () {
                        console.log('Network error.');
                    }
                });
            }
        });
    });
    
    $(document).ready(function () {
        $('.msg-seller').click(function () {
            var id = $(this).attr('data-id');
            $(".msg-seller").each(function () {
                var abc = $(this).attr('data-id');
                if (abc != id) {
                    $('#nw-txtarea' + abc).slideUp(500);
                    $('#txtarea' + abc).val('');
                    $('#txtarea_error' + abc).html('');
                }
            });
            $('#nw-txtarea' + id).slideToggle(500);
            $('#txtarea' + id).val('');
            $('#txtarea_error' + id).html('');
        });
//        cancel_order
        $('#cancel_order').click(function (e) {
            var order_id = '<?=$order_id?>';
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '<?= base_url() . "cancelorder" ?>',
                data: {order_id: order_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    data = JSON.parse(data);
                    setTimeout(function () {
                        $('#message').html('');
                        $('#message').removeClass("alert-success");
                        $('#message').addClass("hide");
                    }, 5000);
                    if (data.status == 'Available') {
                        $('#message').html("Order has been canceled successfully.");
                        $('#message').addClass("alert-success");
                        $('#message').removeClass("hide");
                    }
                },
                error: function () {
                    console.log('Network error.');
                }
            });
        });
    });
</script>
<script type="text/javascript">

    function reorder(order_id) {
        var remove = 0;
        var items = 0;
        
        var total = 0.00;
        $.ajax({
            type: 'POST',
            url: '<?= base_url() . "addToCart" ?>',
            data: {order_id: order_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            async: false,
            success: function (res) {
                console.log(res);
                myObj = JSON.parse(res);
                //console.log(myObj);
                items = parseInt(myObj.cart_items);
                total = myObj.cart_sub_total;
                if (myObj.status == 'not-ok') {
                    //$("#alert_msg").addClass('alert-danger');
                    //$('#alert_msg').html('Something went wrong.');
                    $("#wishlist-text").text('Item out of stock.');
                    // $('#myAnchor').trigger('click');

                } else if (myObj.status == 'ready-ok') {
                    $("#wishlist-text").text('');
                    $("#wishlist-text").html(myObj.message);
                    remove = 1;


                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);

                }  else if (myObj.status == "limit-ok") {
                    $("#wishlist-text").text('');
                    $("#wishlist-text").html(myObj.message);
                    //$('#myAnchor').trigger('click');
                    remove = 1;


                } else {
                    $("#wishlist-text").text('');
                    $("#wishlist-text").html(myObj.message);
                    //$('#myAnchor').trigger('click');
                    remove = 1;

                    
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);
                }
            },
            error: function () {
                console.log('Network error.');
            }
        });

        $('#cart_items').html(items+ " item");
        $('#cart_sub_total').html(total);
        if(items > 0){
            $('.compl').attr('href', '<?= base_url('cart') ?>');
            $('.compl').removeAttr('data-target')
            if (items > 1) {
                $('#cart_items').html(items + " items");
            } else {
                $('#cart_items').html(items + " item");
            }
            
        }
        $.ajax({
            url: '<?=base_url("whichProdNotAddCart")?>',
            type: 'POST',
            data: {order_id: order_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
        })
        .done(function(res) {
            if(remove == 0){
                $("#wishlist-text").text('');
            }

            $("#wishlist-text").append(res);
            $('#myAnchor').trigger('click');
            //console.log("success");
        })
        .fail(function() {
            //$("#wishlist-text").text('Something went wrong.');
            $('#myAnchor').trigger('click');
            console.log("error");
        });
        
    }

</script>