<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item">Shop</li>
                        <li class="breadcrumb-item"><a href="<?= base_url() ?><?php echo getFirstMainCategory()->cat_url ?>">Clothes & Accessories</a></li>
                        <li class="breadcrumb-item active">Compare</li>
                    </ol>
                </div>
                <div class="shop-tit mrg-top-0 pad-10">
                    <h2>Compare</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Shop Filters starts here-->
<section class="compre-det-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="compre-detal-main">
                    <div class="compre-detal-tbl">

                        <table class="table" id="table">
                            <thead>
                                <tr>  
                                    <th style="width:16.5%;"></th>
                                    <th style="width:27.8%;"></th>
                                    <th style="width:27.8%;"></th>
                                    <th style="width:27.8%;"></th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                <tr>
                                    <td class="brd-top-0"></td>
                                    <?php
                                    foreach ($prods as $prod) {
                                        $prod_details = getproduct_details($prod);
                                        ?>
                                        <td class="brd-right-2 brd-top-0 prod-td-<?= trim($prod) ?>">
                                            <div class="compre-img-bx">
                                                <img src="<?= base_url() . 'resources/prod_images/' . $prod_details->img_name ?>" alt="" title="" class="img-responsive pull-right"/>
                                                <button class="remove-prod" data-id="<?= trim($prod) ?>"></button>
                                            </div>
                                        </td>
                                    <?php } ?>
                                </tr>


                                <tr class="clr-gry">
                                    <td class="brd-right-2  brd-top-0 spec-new">Specifications</td>
                                    <?php foreach ($prods as $prod) { ?>
                                        <td class="brd-right-2 brd-top-0 prod-td-<?= trim($prod) ?>"></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td class="brd-right-2 fnt-stylng brd-top-0">Name</td>
                                    <?php
                                    foreach ($prods as $prod) {
                                        $prod_details = getproduct_details($prod);
                                        ?>
                                        <td class="brd-right-2 brd-top-0 prod-td-<?= trim($prod) ?>"><?= $prod_details->prod_title ?></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td class="brd-right-2 fnt-stylng">Description</td>
                                    <?php
                                    foreach ($prods as $prod) {
                                        $prod_details = getproduct_details($prod);
                                        ?>
                                        <td class="brd-right-2 prod-td-<?= trim($prod) ?>"><?= $prod_details->prod_description ?></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td class="brd-right-2 fnt-stylng">Color</td>
                                    <?php
                                    foreach ($prods as $prod) {
                                        $prod_details = getProduct1lvlFilter($prod, "Color");
                                        $filtersvalue = array();
                                        foreach ($prod_details as $filterpush) {
                                            array_push($filtersvalue, $filterpush['filter_value']);
                                        }
//                                        echo implode(",",$filtersvalue);
////                                        var_dump($filtersvalue);
//                                        echo "<pre>";
//                                        var_dump($prod_details);
//                                        die;
                                        ?>
                                        <td class="brd-right-2 prod-td-<?= trim($prod) ?>">
                                            <?php
                                            if (empty($filtersvalue)) {
                                                echo 'N/A';
                                            } else {
                                                echo implode(",", $filtersvalue);
                                            }
//                                            if ($prod_details['filter_value']) {
//                                                echo implode(",",$filtersvalue);
//                                            } else {
//                                                echo 'N/A';
//                                            }
                                            ?>
                                            <input type="hidden" value="<?= implode(",", $filtersvalue) ?>" class="existing_filters" id="color_value" >
                                        </td>
                                    <?php } ?>
                                </tr>


                                <tr>
                                    <td class="brd-right-2 fnt-stylng">Condition</td>
                                    <?php
                                    foreach ($prods as $prod) {
                                        $prod_details = getproduct_details($prod);
                                        ?>
                                        <td class="brd-right-2 prod-td-<?= trim($prod) ?>"><?= $prod_details->condition ?></td>
                                    <?php } ?>
                                </tr>

                                <tr>
                                    <td class="brd-right-2 fnt-stylng">Size</td>
                                    <?php
                                    foreach ($prods as $prod) {
                                        $prod_details = getProduct1lvlFilter($prod, "Size");
                                        $filtersvalue = array();
                                        foreach ($prod_details as $filterpush) {
                                            array_push($filtersvalue, $filterpush['filter_value']);
                                        }
                                        ?>
                                        <td class="brd-right-2 prod-td-<?= trim($prod) ?>">
                                            <?php
                                            if (empty($filtersvalue)) {
                                                echo 'N/A';
                                            } else {
                                                echo implode(",", $filtersvalue);
                                            }
//                                            if ($prod_details->filter_value) {
//                                                echo $prod_details->filter_value;
//                                            } else {
//                                                echo 'N/A';
//                                            }
                                            ?>
                                            <input type="hidden" value="<?= implode(",", $filtersvalue) ?>" class="existing_filters" id="size_value" >
                                        </td>
                                    <?php } ?>
                                </tr>

                                <tr>
                                    <td class="brd-right-2"></td>
                                    <?php
                                    foreach ($prods as $prod) {
                                        $prod_detail = getProductPriceById($prod);
                                        $prod_details = getproduct_details($prod);
                                        ?>
                                        <td class="brd-right-2 text-center prod-td-<?= trim($prod) ?>">
                                            <!--<span class="clr-red"><?= $prod_details->unit_prod_sale ?> $<?= $prod_details->unit_prod_price ?></span>-->
                                            <!-- <span class="clr-red"> $<?= $prod_detail->unit_prod_price ?></span> -->




                                            <?php
                                            $productPrice = 0.0;
                                            $sDate = date('Y-m-d', strtotime($prod_details->sale_start_date));
                                            $eDate = date('Y-m-d', strtotime($prod_details->sale_end_date));
                                            $tDate = date('Y-m-d');
                                            if ($prod_details->prod_onsale == 1) {
                                                if ($prod_details->sale_start_date != '' && $prod_details->sale_end_date != '') {
                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                        $productPrice = number_format($prod_details->prod_price, 2, '.', '');
                                                        //echo '<span class="sp1">$' . number_format($prod_details->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details->prod_price, 2, '.', '') . '</span>';
                                                        echo '<span class="clr-red">On Sale: $'.$prod_detail->unit_prod_price.'</span>';
                                                    } else {
                                                        $productPrice = number_format($prod_details->prod_price, 2, '.', '');
                                                        echo '<span class="clr-red">$'.$prod_detail->unit_prod_price.'</span>';
                                                    }
                                                } else if ($prod_details->sale_start_date != '') {
                                                    if ($tDate >= $sDate) {
                                                        $productPrice = number_format($prod_details->prod_price, 2, '.', '');
                                                        //echo '<span class="sp1">$' . number_format($prod_details->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details->prod_price, 2, '.', '') . '</span>';
                                                        echo '<span class="clr-red">On Sale: $'.$prod_detail->unit_prod_price.'</span>';
                                                    } else {
                                                        $productPrice = number_format($prod_details->prod_price, 2, '.', '');
                                                        echo '<span class="clr-red">$'.$prod_detail->unit_prod_price.'</span>';
                                                    }
                                                } else if ($prod_details->sale_end_date != '') {
                                                    if ($tDate <= $eDate) {
                                                        // echo 'Price:<span class="sp1">$' . number_format($prod_details->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details->prod_price, 2, '.', '') . '</span>';
                                                        echo '<span class="clr-red">On Sale: $'.$prod_detail->unit_prod_price.'</span>';
                                                    } else {
                                                        // echo 'Price:<span class="sp1">$' . number_format($prod_details->prod_price, 2, '.', '') . '</span>';
                                                        echo '<span class="clr-red">$'.$prod_detail->unit_prod_price.'</span>';
                                                    }
                                                } else {
                                                    $productPrice = number_format($prod_details->prod_price, 2, '.', '');
                                                    // echo '<span class="sp1">$' . number_format($prod_details->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details->prod_price, 2, '.', '') . '</span>';
                                                    echo '<span class="clr-red">On Sale: $'.$prod_detail->unit_prod_price.'</span>';
                                                }
                                            } else {
                                                $productPrice = number_format($prod_details->prod_price, 2, '.', '');
                                                // echo '$' . number_format($prod_details->prod_price, 2, '.', '');
                                                echo '<span class="clr-red">$'.$prod_detail->unit_prod_price.'</span>';
                                            }
                                            ?>



                                            <?php if ($prod_details->qty > 0) { ?>
                                                <a href="javascript:void(0);" id="Modalcompare" data-unique=<?=$prod_details->is_unique_filter?> data-price = "<?= $prod_details->unit_prod_price ?>" data-product="<?= trim($prod) ?>" class="ad-crt-btn cartpopup" data-toggle="modal" data-target="#myModalcompare">Add to cart</a>
                                                <!--<a class="ad-crt-btn adcrt" href="javascript:void(0);" data-price="<?= $prod_details->unit_prod_price ?>" data-product="<?= trim($prod) ?>">Add to cart</a>-->
                                            <?php } else { ?>
                                                <div class="out-of-stock">Out of stock.</div>
                                            <?php } ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="overlay" id="overlay_popup">
    <div class="content">
        <div id="close">x</div>
        <div class="shop-det-rgt">
            <div id="comparefilters">
            </div>
        </div>
    </div>
</div>



<!-- Popup Starts Here -->
<a data-toggle="modal" data-target="#myModal4" id="myAnchor" style="display: none;">Click HERE</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">

        <div class="modal-content">
            <div class="modal-header pad-0">        
                <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body pad-0">
                <div class="frgt-sec oneline">
                    <div class="fgt-tit">
                        <p class="mrg-botm-0" id="wishlist-text"></p>
                    </div>
                </div>
            </div>

        </div>

        <!-- Ended 2 divs below -->
    </div>
</div>
<!-- Popup Ends Here -->

<style>
    .compre-detal-tbl table.singleItem tr th, 
    .compre-detal-tbl table.singleItem tr td {
        border-left: none;
        border-right: none;
        margin: 0px;
    }
    .buttons {
        text-align: center; 
    }
    .overlay .row {
        margin-right: 0px;
        margin-left: 0px; 
    }
    .overlay .shop-det-rgt-lowr-1 {
        text-align: center;
    }
    .buttons a {
        display: inline-block;
        background: #e74a54;
        color: #fff;
        padding: 10px 40px;
        font-size: 15px;
        text-decoration: none;
        cursor: pointer;
    }
    .overlay {
        background: rgba(0, 0, 0, 0.54);
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        width: 100%;
        height: 100%;
        opacity: 0;
        visibility: hidden;
        -webkit-transition: all 0.2s ease-in-out;
        -o-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
        display: inline-block;
        z-index:9999;
    }
    .overlay.is-on {
        opacity: 1;
        visibility: visible;
        top: 0;
    }
    .overlay .shop-det-rgt-upr h5 {
        padding: 0px 0px 18px;
        margin: 0px;
        text-align: center;
        font-size: 16px;
        font-weight: normal;
        font-family: 'source_sans_prosemibold';
        color: #ff0000;
        text-transform: capitalize;
    }
    .overlay .shop-det-rgt {
        width: 100%;
        height: 100%;
        float: left;
        padding: 1%; /* 0% 5%; */
        overflow: auto;
        margin: 0px;
        padding-top: 31px;
    }
    .overlay.is-on .content {
        opacity: 1;
        visibility: visible;
        top: 0;
    }
    .content {
        background: #fff;
        position: absolute;
        top: -50%;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        width: 37%; /* 32 */
        height: 350px; /* 310 320  */
        opacity: 0;
        visibility: hidden;
        -webkit-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        overflow: auto;
    }
    #close {
        /*
        position: absolute;
        right: 13px;
        top: 0px;
        color: #008ac5;
        cursor: pointer;
        font-family: 'source_sans_prosemibold';
        font-size: 30px;
        */
        position: absolute;
        right: 0px;
        top: 0px;
        color: #008ac5;
        cursor: pointer;
        font-family: 'source_sans_prosemibold';
        background-color: #FFF;
        font-size: 30px;
        padding: 0px 5px;
        line-height: 1;
    }

</style>
<script>
    function filtervalue(prod_id, filter_slug) {
        var prod_id = prod_id;
        var filter_slug = filter_slug;
        var select_value = $("#" + filter_slug).val();
        $("#" + prod_id + "_" + filter_slug).val(select_value);
    }
    function cart(prodid) {
        var v = [];
        var filters = $(".required_filters");
        for (var i = 0; i < filters.length; i++) {
            var neww = $(filters[i]).attr('data-slug');
            //v.push({slug: $(filters[i]).attr('data-slug'), value: $(filters[i]).val()});
            var name = $(filters[i]).attr('data-name');
                v.push({name: name, slug : name, value : $(filters[i]).val()});
            //v.push({name: $(filters[i]).attr('data-name'), slug : $(filters[i]).attr('data-name'), value : $(filters[i]).val()});
        }
        $.ajax({
            type: 'POST',
            url: '<?= base_url() . "addToCart" ?>',
            data: {prod_id: prodid, filter_data: v,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            dataType: "json",
            success: function (data) {
                var newjson = JSON.stringify(data);
                myObj = JSON.parse(newjson);


                $('#close').click();
                if (myObj.status == 'not-ok') {
                    $("#wishlist-text").text('Something went wrong.');
                    $('#myAnchor').trigger('click');

                } else if (myObj.status == 'ready-ok') {
                    $("#wishlist-text").text('Item updated to cart successfully.');
                    $('#myAnchor').trigger('click');
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);

                }  else if (myObj.status == "limit-ok") {
                    $("#wishlist-text").text('Item out of stock.');
                    $('#myAnchor').trigger('click');
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);

                } else {
                    $("#wishlist-text").text('Item added to cart successfully.');
                    $('#myAnchor').trigger('click');
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);
                }

                /*
                if (data.status == 'not-ok') {
                    // $("#alert_msg").addClass('alert-danger');
                    // $('#alert_msg').html('Something went wrong.');
                } else {
                    // $("#alert_msg").addClass('alert-success');
                    // $("#add_to_cart_msg").show();
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target');
                    if (data.cart_items > 1) {
                        $('#cart_items').html(data.cart_items + " items");
                    } else {
                        $('#cart_items').html(data.cart_items + " item");
                    }
                    $('#cart_sub_total').html(data.cart_sub_total);
                    $('#close').click();

                }*/
            },
            error: function () {
                console.log('Network error.');
            }
        });
    }
    function cart_withoutilters(prodid, is_unique) {
        var v = [];
        if(!is_unique){
            var color = $("#color_value").val();
            // v.push({slug: 'color', value: color});
            v.push({slug: 'Color', value: color});
            var size = $("#size_value").val();
            // v.push({slug: 'size', value: size});
            v.push({slug: 'Size', value: size});
        }


        $.ajax({
            type: 'POST',
            url: '<?= base_url() . "addToCart" ?>',
            data: {prod_id: prodid, filter_data: v,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            dataType: "json",
            success: function (data) {
                var newjson = JSON.stringify(data);
                myObj = JSON.parse(newjson);
                $('#close').click();
                if (myObj.status == 'not-ok') {
                    $("#wishlist-text").text('Something went wrong.');
                    $('#myAnchor').trigger('click');

                } else if (myObj.status == 'ready-ok') {
                    $("#wishlist-text").text('Item updated to cart successfully.');
                    $('#myAnchor').trigger('click');
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);

                }  else if (myObj.status == "limit-ok") {
                    $("#wishlist-text").text('Item out of stock.');
                    $('#myAnchor').trigger('click');
                    

                } else {
                    $("#wishlist-text").text('Item added to cart successfully.');
                    $('#myAnchor').trigger('click');
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);
                }


                /*if (data.status == 'not-ok') {
                    // $("#alert_msg").addClass('alert-danger');
                    // $('#alert_msg').html('Something went wrong.');
                } else {
                    // $("#alert_msg").addClass('alert-success');
                    // $("#add_to_cart_msg").show();
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target');
                    if (data.cart_items > 1) {
                        $('#cart_items').html(data.cart_items + " items");
                    } else {
                        $('#cart_items').html(data.cart_items + " item");
                    }
                    $('#cart_sub_total').html(data.cart_sub_total);
                    $('#close').click();

                }*/
            },
            error: function () {
                console.log('Network error.');
            }
        });
    }
</script>
<script>
    $(".mymodel").on("click", function () {
        $(".overlay").addClass("is-on");
    });

    $("#close").on("click", function () {
        $(".overlay").removeClass("is-on");
    });
</script>
<!--Shop Filters ends here-->
<script>
    $(document).ready(function () {
        $('.remove-prod').click(function (e) {
            e.preventDefault();
            $prod_id = $(this).attr('data-id');
            $.ajax({
                type: 'POST',
                url: '<?= base_url() . "remove-compare-prod" ?>',
                data: {prod_id: $prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.status == 200) {
                        if (data.total_prods >= 1) {
                            if (data.total_prods == 1) {
                                $('.prod-td-' + $prod_id).remove();
                                $('.fnt-stylng').removeClass('brd-right-2');
                                $('.spec-new').removeClass('brd-right-2');
                                $('#table').addClass('singleItem');
                            } else {
                                $('.prod-td-' + $prod_id).remove();
                            }
                        } else {
                            window.location.assign("<?= base_url() ?>");
                        }
                    }
                },
                error: function () {
                    console.log('Network error.');
                }
            });
        });
        //
        //
        $('.cartpopup').click(function (e) {
            e.preventDefault();
            $prod_id = $(this).attr('data-product');
            var is_unique = $(this).attr('data-unique');
            $.ajax({
                type: 'POST',
                url: '<?= base_url() . "compare-popup" ?>',
                data: {prod_id: $prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.html != "") {
                        $("#comparefilters").html(data.html);
                        jQuery('.overlay').addClass('is-on');
                        $('.required-filters').change();
                    } else {
                        cart_withoutilters($prod_id, is_unique);
                    }
                },
                error: function () {
                    console.log('Network error.');
                }
            });
        });

    });
</script>