<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>blog">News & Advice</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url()?>blog/<?=$blog_cat->blog_cat_url?>"><?=$blog_cat->blog_cat_name?></a></li>
                        <li class="breadcrumb-item"><?= $blog_name ?></li>
                    </ol>
                </div>	
            </div>
        </div>
    </div>
</div>      <!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="blog-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-12 pad-lft-0">
                    <div class="blog-aside">
                        <h3>News & Trends</h3>
                        <ul>
                            <?php if (isset($blog_category) && $blog_category != '') { ?>
                                <?php foreach ($blog_category as $category) { ?>
                                    <li><a class="<?php if($blog_cat->blog_cat_name==$category->blog_cat_name){echo "active";}?>" href="<?= base_url() ?>blog/<?=$category->blog_cat_url?>"><?=$category->blog_cat_name?></a></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                        <div class="discus-sec">
                            <a href="<?=base_url()?>news-advice">Discussions &amp; Forum </a>
                        </div>
                    </div>
                </div>  
                <div class="col-md-10 col-lg-10 col-sm-9 col-xs-12">
                    <div class="blog-main">
                        <div class="blog-list-bx">
                            <div class="blog-list-inner pad-tp-0">
                                <div class="blog-lst-wrp">
                                    <div class="blog-list-tit">
                                        <h2><?= $blog_name ?></h2>
                                    </div>
                                    <div class="blog-list-date">
                                        <p>By <span><?= $blog_author ?></span> on <?= date('F d, Y', strtotime($blog_created_date)) ?></p>
                                    </div>
                                </div>
                                <div class="blog-lst-img">
                                    <img src="<?= base_url() ?>resources/blog_image/<?= $blog_image ?>" alt="" title="" class="img-responsive"/> 
                                </div>
                                <div class="blog-lst-wrp2">
                                    <div class="blog-detail-txt">
                                        <p><?= clean_string($blog_description); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="blog-detail-comment">
                            
                            <div class="share-sec">
                                <p>Share this Blog:</p>
                                <div class="shr-img">
                                    <ul>
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url()?>blog-details/<?=$blog_url?>" class="shr-fb" target="_blank"></a></li>
                                        <li><a href="https://twitter.com/home?status=<?=$blog_name;?>%20%20<?=base_url()?>blog-details/<?=$blog_url?>" class="shr-fb2" target="_blank"></a></li>
                                        <li><a href="https://pinterest.com/pin/create/button/?url=<?=base_url()?>blog-details/<?=$blog_url?>&media=<?=$share_image;?>&description=<?=$blog_name;?>" class="shr-fb3" target="_blank"></a></li>
                                        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=base_url()?>blog-details/<?=$blog_url?>&title=<?=@$share_title?>&summary=<?= $meta_description ?>" class="shr-fb4" target="_blank"></a></li>
                                        <li><a href="mailto:?Subject=<?=$blog_name?>&body=<?=base_url()?>blog-details/<?=$blog_url?>" class="shr-fb5"></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="blw-commt">   
                                <div id="disqus_thread"></div>
                                <!--<img src="<?= base_url() ?>front_resources/images/comment-img.jpg" class="img-responsive"/>-->
                            </div>

                        </div>

                    </div>

                </div>  

            </div>
        </div>
    </div>
</section>
<script id="dsq-count-scr" src="//www-educki-com.disqus.com/count.js" async></script>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://www-educki-com.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<!-- 
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
 -->
