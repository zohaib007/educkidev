<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">My Fans</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">

                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->


                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">

                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">

                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad">
                                <div class="msg-hd-inrow">

                                    <h5>My Fans</h5>

                                </div>
                                <!-- <p>Sample Text Here.</p> -->
                            </div>
                            <!-- Heading Section Ends -->


                            <!-- Bottom Section Starts -->
                            <div class="store-btmWrap">

                                <!-- Store Section Starts -->
                                <div class="store-mainsec">

                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">

                                            <!-- Search By Form Starts -->
                                            <div class="store-tophdr">

                                                <!-- Left Heading Starts -->
                                                <div class="store-th">
                                                    <h3>Total Fans</h3>
                                                </div>
                                                <!-- Left Heading Ends -->

                                            </div>
                                            <!-- Search By Form Ends -->

                                        </div>
                                    </div>
                                    <!-- Top Grey Bar Ends -->

                                    <!-- Bottom Grey Section Starts -->
                                    <div class="btm-boxwrap">
                                        <div class="btm-boxinner">

                                            <!-- Section Starts -->
                                            <div class="btm-colswrap">

                                                <!-- Column Starts -->
                                                <div class="btm-fcol">
                                                    <div class="btm-fcinner">
                                                        <h3><?=$last_weeks->lastweek;?></h3>
                                                        <span class="wfilter">Last Week</span>
                                                    </div>
                                                </div>
                                                <!-- Column Ends -->

                                                <!-- Column Starts -->
                                                <div class="btm-fcol">
                                                    <div class="btm-fcinner">
                                                        <h3><?=$one_month->month;?></h3>
                                                        <span class="wfilter">Last Month</span>
                                                    </div>
                                                </div>
                                                <!-- Column Ends -->

                                                <!-- Column Starts -->
                                                <div class="btm-fcol">
                                                    <div class="btm-fcinner">
                                                        <h3><?=$six_month->sixmonths;?></h3>
                                                        <span class="wfilter">Six Months</span>
                                                    </div>
                                                </div>
                                                <!-- Column Ends -->

                                                <!-- Column Starts -->
                                                <div class="btm-fcol">
                                                    <div class="btm-fcinner">
                                                        <h3><?=$year->year?></h3>
                                                        <span class="wfilter">Last Year</span>
                                                    </div>
                                                </div>
                                                <!-- Column Ends -->

                                                <!-- Column Starts -->
                                                <div class="btm-fcol">
                                                    <div class="btm-fcinner">
                                                        <h3><?=$total?></h3>
                                                        <span class="wfilter">Total</span>
                                                    </div>
                                                </div>
                                                <!-- Column Ends -->


                                            </div>
                                            <!-- Section Ends -->

                                        </div>
                                    </div>
                                    <!-- Bottom Grey Section Ends -->

                                </div>
                                <!-- Store Section Ends -->

                                <!-- Store Section Starts -->
                                <div class="store-mainsec">

                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">

                                            <!-- Search By Form Starts -->
                                            <div class="searchby-wrap myfan-search">
                                                <!-- <form name="searchby" method="post"> -->
                                                    <?php 
                                                    $attributes = array('name' => 'searchby', 'id' => 'searchby','method'=>'get');
                                                        echo form_open('', $attributes)?>
                                                    <div class="searchby-row">
                                                        <div class="stextbox-rp">
                                                            <input type="text" name="utxtbox" placeholder="Search" value="" />
                                                        </div>
                                                        <div class="search-btn">
                                                            <div class="custombtn-large">
                                                                <input type="submit" value="SEARCH" title="SEARCH" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!-- </form> -->
                                                <?php echo form_close(); ?>
                                            </div>
                                            <!-- Search By Form Ends -->

                                        </div>
                                    </div>
                                    <!-- Top Grey Bar Ends -->

                                    <!-- Bottom My Fan Section Starts -->
                                    <div class="myfan-listwrap">
                                        <?php if(count($Allfans)>0) { foreach ($Allfans as $fan) { ?>
                                        <!-- List Row Starts -->
                                        <div class="myfan-listrow">

                                            <!-- Image Section Starts -->
                                            <div class="myfan-img">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <a href="<?php if($fan->store_is_hide==0 AND $fan->store_url!=''){ echo base_url()."store/".$fan->store_url; }else{ echo "javascript:void(0)"; } ?>"><img src="<?=base_url()?><?php if($fan->store_logo_image!=''){ echo "resources/seller_account_image/logo/thumb/".$fan->store_logo_image; }else{if($fan->user_profile_picture!=''){echo "resources/user_profile_image/thumb/".$fan->user_profile_picture; }else{echo "front_resources/images/myfan-thumb01.jpg";}} ?>" alt="" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!-- Image Section Ends -->

                                            <!-- Right Section Starts -->
                                            <div class="myfan-right">
                                                <div class="myfan-rinner">
                                                    <a href="<?php if($fan->store_is_hide==0 AND $fan->store_url!=''){ echo base_url()."store/".$fan->store_url; }else{ echo "javascript:void(0)"; } ?>">
                                                        <!-- Top Content Section Starts -->
                                                        <div class="myfan-rtoptxt">
                                                            <h4><?php if($fan->store_name!=''){ echo $fan->store_name; }else{ echo $fan->user_fname.@$fan->user_lname[0]; } ?></h4>
                                                        </div>
                                                        <!-- Top Content Section Ends -->

                                                        <!-- Bottom Info Section Starts -->
                                                        <div class="myfan-binfo">

                                                            <!-- Sec Starts -->
                                                            <div class="binfo-rp">
                                                                <div class="infoico"></div>
                                                                <div class="infotxt"><?php if($fan->store_name!='') { echo $fan->store_city;}else{echo $fan->user_city;} if($fan->store_state!='') { echo ", ".$fan->store_state;}elseif($fan->user_state!=''){echo ", ".$fan->user_state;}?></div>
                                                            </div>
                                                            <!-- Sec Ends -->

                                                            <!-- Sec Starts -->
                                                            <div class="binfo-rp">
                                                                <div class="infotxt">Member Since <?php echo date('Y',strtotime($fan->user_register_date));?></div>
                                                            </div>
                                                            <!-- Sec Ends -->

                                                        </div>
                                                        <!-- Bottom Info Section Ends -->
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Right Section Ends -->
                                            </a>
                                        </div>
                                        <!-- List Row Ends -->
                                        <?php } }else{?>
                                        <div class="myfan-rtoptxt">
                                            <h4>No data found.</h4>
                                        </div>
                                        <?php }?>

                                        <?php if(count($Allfans)!=0){?>

                                        <div class="msg-bright">
                                            <!-- Pagination Section Starts -->
                                            <div class="pagination-rp2 fanpage">
                                               <?php if (isset($paginglink)) {echo $paginglink;}?>
                                        
                                            </div>
                                            <!-- Pagination Section Ends -->
                                        </div>

                                        <?php }?>

                                    </div>
                                    <!-- Bottom My Fan Section Ends -->

                                </div>
                                <!-- Store Section Ends -->

                            </div>
                            <!-- Bottom Section Ends -->

                        </div>
                        <!-- Inner Section Ends -->


                    </div>
                </div>  
                <!-- Right Pannel Starts Here -->


            </div>
        </div>
    </div>
</section>


<!--Blog main ends  here-->
