<section class="frg-pas-bg">
    <div class="frg-layr"></div>
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="frgt-pasd-wrp">
                    <div class="frgt-sec">
                        <?php echo form_open('', array('name' => 'forgotpassword', 'id' => 'forgotpassword_form',)); ?>
                        <div class="fgt-tit">
                            <h5>I forgot my password!</h5>
                            <p>
                               To reset your password, please enter the email you used while registering with eDucki. We will send you an email with instructions to reset your password.
                            </p>
                            <input type="text" name="email" value="<?= set_value('email') ?>" id="email" placeholder="What's your email?" />
                            <div class="error" id="email_validate" style="text-align: left;"><?php echo form_error('email')?></div>

                            <?php if($this->session->flashdata('forgotEmail2')!=''){?>

                            <div class="alert alert-danger alert-set">
                                <?=$this->session->flashdata('forgotEmail2')?>
                            </div>
                            <?php }elseif($this->session->flashdata('forgotEmail1')!=''){ ?>

                            <div class="alert alert-success alert-set">
                                <?=$this->session->flashdata('forgotEmail1')?>
                            </div>
                            <?php }?>

                            <div class="fgt-btn">
                                <a href="<?=base_url()?>">
                                    <input type="button" class="frg-lft" value="cancel" />
                                </a>
                                <input type="submit" class="frg-rgt" value="Reset my Password" />
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function() {

    $(function validate() {
        // body...
        var rules = {
            rules: {
                email: {
                    minlength: 2,
                    maxlength: 50,
                    required: true,
                    email: true
                }
            },
            errorPlacement: function(error, element) {
                var name = $(element).attr("name");
                error.appendTo($("#" + name + "_validate"));
            },
        };
        $('#forgotpassword_form').validate(rules);
    });
});
</script>