<!--Breadcrumbs starts here-->
<div class="container">
	<div class="row">
    	<div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
    		<div class="auto-container">
            	<div class="brd-sec">
                	<ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                      <li class="breadcrumb-item active"><?= $pagedata->page_title; ?></li>
                    </ol>
                </div>	
            </div>
    	</div>
    </div>

</div>
<!--Breadcrumbs ends  here-->

<!--Pricing & Benefits starts  here-->
<section class="blog-sec">
	<div class="container">
		<div class="row">
        	<div class="auto-container">
        	<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="banfit-wrp">
                	
                    <div class="banfit-upr">
                    	<div class="banfit-upr-tit">
                        	<h2><?= $pagedata->page_title; ?></h2>
                        </div>
                        
                        <div class="banfit-upr-txt"><?= clean_string($pagedata->page_description); ?></div>
                    </div>
                	<div class="banfit-frst-row">
                        	<div class="banfit-low-bx">
                            	<table>
                                	<tr>
                                    	<td>
                                        	<img src="<?=base_url()?>resources/pricing_benefits/<?=$pagedata->image_1?>" alt="" title=""/>
                                        </td>
                                    </tr>
                                </table>
                            	
                                <?= $pagedata->img_description_1;?>
                            
                            </div>
                            
                            <div class="banfit-low-bx">
                            	<table>
                                	<tr>
                                    	<td>
                                        	<img src="<?=base_url()?>resources/pricing_benefits/<?=$pagedata->image_2?>" alt="" title="" />
                                        </td>
                                    </tr>
                                </table>
                            	
                                <?= $pagedata->img_description_2;?>       
                            
                            </div>
                            
                            <div class="banfit-low-bx mrg-rgt-0">
                            	<table>
                                	<tr>
                                    	<td>
                                        	<img src="<?=base_url()?>resources/pricing_benefits/<?=$pagedata->image_3?>" alt="" title="" />
                                        </td>
                                    </tr>
                                </table>
                            	
                                <?= $pagedata->img_description_3;?>

                            </div>
                        
                        </div>
                        
                  	<div class="get-frnd">
                    	<p><?= $pagedata->offer_line;?></p>
                    </div> 
                    
                    <div class="plan-sec">
                    	
                        <div class="plan-sec-inner">
                        	<div class="plan-free">
                            	<div class="plan-free-hed">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?= $tierdata[0]->tier_name; ?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature">
                                	<h4><?= $tierdata[0]->tier_title; ?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	
                                    <p>
                                        <?=$tierdata[0]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[0]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[0]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[0]->tier_months>0) {?>
                                            <?= $tierdata[0]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                    	<?php if($tierdata[0]->tier_amount>0){?>
                                           <h5>$<?=$tierdata[0]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                    </div>
                                    
                                    <div class="plan-lst">
                                        <?php if(!$this->session->userdata('logged_in')){?>
                                        <a href="javascript:void(0)" class="close_click tier_set" data-log="0" data-value="<?=$tierdata[0]->tier_id?>" data-toggle="modal" data-target="#myModal">Sign up!</a>
                                        <?php }else {?>
                                        <a href="javascript:void(0)" class="tier_set" data-log="1" data-value="<?=$tierdata[0]->tier_id?>">Sign up!</a>
                                        <?php }?>
                                    </div>
                                
                                </div>
                        
                        	</div>
                            
                            <div class="plan-free border-lft">
                            	<div class="plan-free-bacs">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?= $tierdata[1]->tier_name; ?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature">
                                	<h4><?= $tierdata[1]->tier_name; ?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	<p>
                                        <?=$tierdata[1]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[1]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[1]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[1]->tier_months>0) {?>
                                            <?= $tierdata[1]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                        <?php if($tierdata[1]->tier_amount>0){?>
                                    	   <h5>$<?=$tierdata[1]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                    </div>
                                    
                                    <div class="plan-lst">
                                        <?php if(!$this->session->userdata('logged_in')){?>
                                        <a href="javascript:void(0)" class="close_click tier_set" data-log="0" data-value="<?=$tierdata[1]->tier_id?>" data-toggle="modal" data-target="#myModal">Sign up!</a>
                                        <?php }else {?>
                                        <a href="javascript:void(0)" class="tier_set" data-log="1" data-value="<?=$tierdata[1]->tier_id?>">Sign up!</a>
                                        <?php }?>
                                    </div>
                                
                                </div>
                        
                        	</div>
                            
                            <div class="plan-free border-lft">
                            	<div class="plan-free-pro">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?= $tierdata[2]->tier_name; ?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature pad-tp-0">
                                	<h4><?= $tierdata[2]->tier_title; ?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	<p>
                                        <?=$tierdata[2]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[2]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[2]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[2]->tier_months>0) {?>
                                            <?= $tierdata[2]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                        <?php if($tierdata[2]->tier_amount>0){?>
                                           <h5>$<?=$tierdata[2]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                        <?php $saves = ((($base_value*$tierdata[2]->tier_months)-($tierdata[2]->tier_amount*$tierdata[2]->tier_months))/($base_value*$tierdata[2]->tier_months))*100 ?>
                                        <h6>Save <?= round($saves)?>%</h6>
                                    </div>
                                    
                                    <div class="plan-lst">
                                        <?php if(!$this->session->userdata('logged_in')){?>
                                        <a href="javascript:void(0)" class="close_click tier_set" data-log="0" data-value="<?=$tierdata[2]->tier_id?>" data-toggle="modal" data-target="#myModal">Sign up!</a>
                                        <?php }else {?>
                                        <a href="javascript:void(0)" class="tier_set" data-log="1" data-value="<?=$tierdata[2]->tier_id?>">Sign up!</a>
                                        <?php }?>
                                    </div>
                                
                                </div>
                        
                        	</div>
                            
                            <div class="plan-free border-lft">
                            	<div class="plan-free-delx">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?= $tierdata[3]->tier_name; ?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature">
                                	<h4><?= $tierdata[3]->tier_title; ?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	<p>
                                    	<?=$tierdata[3]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[3]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[3]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[3]->tier_months>0) {?>
                                            <?= $tierdata[3]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                    	<?php if($tierdata[3]->tier_amount>0){?>
                                           <h5>$<?=$tierdata[3]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                        <?php $saves = ((($base_value*$tierdata[3]->tier_months)-($tierdata[3]->tier_amount*$tierdata[3]->tier_months))/($base_value*$tierdata[3]->tier_months))*100 ?>
                                        <h6>Save <?= round($saves)?>%</h6>
                                    </div>
                                    
                                    <div class="plan-lst">
                                        <?php if(!$this->session->userdata('logged_in')){?>
                                    	<a href="javascript:void(0)" class="close_click tier_set" data-log="0" data-value="<?=$tierdata[3]->tier_id?>" data-toggle="modal" data-target="#myModal">Sign up!</a>
                                        <?php }else {?>
                                        <a href="javascript:void(0)" class="tier_set" data-log="1" data-value="<?=$tierdata[3]->tier_id?>">Sign up!</a>
                                        <?php }?>
                                    </div>
                                
                                </div>
                        
                        	</div>
                        </div>
                    </div>  
                    
                    <div class="evry-plan">
                        <div><?= $pagedata->benefits_description;?></div>
                    </div>   
                
                </div>
            </div>
            </div>
		</div>
    </div>
</section>
<script>
    $('.tier_set').on('click',function(){
        var base_url = '<?=base_url()?>';
        var logged_in = $(this).attr('data-log');
        var tier = $(this).attr('data-value');
        $.ajax({
            type: 'POST',
            url: '<?= base_url() ?>set-session',
            async: false,
            data: {tier: tier,logged_in:logged_in,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (result) {
                if(result['status']=200){
                    console.log('Success');
                }
            },
            error: function () {
            }
        });
        if(logged_in==1){
            window.location.href = base_url+'edit-tier';
        }
    });
</script>


<!--Pricing & Benefits ends  here-->