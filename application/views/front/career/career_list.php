<!--banner  starts here-->
<section class="carer-bg" style="background-image:url('<?= base_url() ?>resources/career_image/career_page/<?=$page_data->career_page_banner_image?>') !important ;">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="breadcrumb-item active">Careers</li>
                            </ol>
                        </div>
                        <h2><?=$page_data->career_page_banner_title?></h2>	
                    </div>
                </div>
            </div>
        </div>
    </div>    <!--Breadcrumbs ends  here-->
</section>
<!--banner  starts here-->
<!--main sec starts here-->
<section class="carrr-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="carer-wrp">
                    <div class="carr-tit">
                        <h3><?=$page_data->career_page_title?></h3>
                        <p><?=clean_string($page_data->career_page_description)?></p>
                    </div>
                    <div class="carr-bx">
                        <img src="<?= base_url() ?>resources/career_image/career_page/<?=$page_data->career_page_image_first?>" alt="" title="" class="img-responsive"/>
                        <h5><?=$page_data->career_page_title_first?></h5>
                        <div class="carr-bx-lwr">
                            <p><?=clean_string($page_data->career_page_description_first)?></p>
                        </div>
                    </div>
                    <div class="carr-bx">
                        <img src="<?= base_url() ?>resources/career_image/career_page/<?=$page_data->career_page_image_second?>" alt="" title="" class="img-responsive"/>
                        <h5><?=$page_data->career_page_title_second?></h5>
                        <div class="carr-bx-lwr">
                            <p><?=clean_string($page_data->career_page_description_second)?></p>
                        </div>
                    </div>
                    <div class="carr-bx mrg-rgt-0">
                        <img src="<?= base_url() ?>resources/career_image/career_page/<?=$page_data->career_page_image_third?>" alt="" title="" class="img-responsive"/>
                        <h5><?=$page_data->career_page_title_third?></h5>
                        <div class="carr-bx-lwr">
                            <p><?=clean_string($page_data->career_page_description_third)?></p>
                        </div>
                    </div>
                    <div class="prk-sec">
                        <h5>The Perks</h5>
                        <div class="prk-rw">
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Work Alongside World-Class Talent
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Fully Sponsored Health Care and Benefit Plan
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Culture of Learning and Innovation
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx mrg-rgt-0">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Opportunities for Career Growth with Training and Support
                                </p>
                            
                            </div>
                        </div>
                        <div class="prk-rw">
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Flexible Vacation / Paid Time Off Policy
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Parental Leave
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Healthy and Exciting Lunches and Snacks Offered Daily
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx mrg-rgt-0">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Personal Style Encouraged (or not, whatever you’re into)
                                </p>
                            
                            </div>
                        </div>
                    
                    </div>
                    <div class="crr-wrp-2">
                        <div class="opng-sec">
                            <h5>Current Openings</h5>
                        </div>
                        <?php if(count($alphabetical_list)>0){?>
                        <div class="op-lnk">
                            <?php foreach ($alphabetical_list as $page) { ?>
                            <div class="op-lnk-rw">
                                <a href="career_details/<?= $page['career_url'] ?>"><?= $page['career_title'] ?></a>
                                <p><?= $page['career_city'] ?>, <?= $page['stat_name']?></p>
                            </div>
                            <?php } ?>
                        </div>
                        <?php }else{?>
                        <div class="op-lnk text-center"><b>Currently we do not have any job openings.</b></div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
