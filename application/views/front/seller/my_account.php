<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url('my-dashboard');?>">My Account</a></li>
                        <li class="breadcrumb-item active">Become a Seller</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>  
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="my-account-right-head pad-btm-0">
                            <h5>My Store Profile</h5>
                        </div>
                        <?php echo form_open_multipart('seller/myaccount', array('name' => 'myaccount', 'id' => 'myaccount_form')); ?>
                        <div class="store-profile">
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Store Name</h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="store_name" readonly id="store_name" placeholder="Name Here" value="<?php echo $page_data->store_name; ?>" />
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Street Address</h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="store_address" id="store_address" value="<?php echo (isset($page_data->store_address) ? $page_data->store_address : ""); ?>"/>
                                    <h6>Recommended if you have a physical store</h6>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>City <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="store_city" id="store_city" value="<?php echo (isset($page_data->store_city) ? $page_data->store_city : "");?>" />
                                    <div class="error" id="store_city_validate"></div>
                                </div>
                            </div>
                            
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Country <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="store_country" id="store_country">
                                        <option value="0" <?= set_select('store_country', '0') ?>>Select Country</option>
                                        <?php foreach ($allcountries as $country) { ?>
                                            <option value="<?= $country['iso'] ?>" <?php echo set_select( 'store_country', $country['iso'], (isset($page_data->store_country) ? $page_data->store_country == $country['iso'] ? true:'' : ($country['iso']=='US')?true:'')); ?> ><?= $country['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="error" id="store_country_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row" id ="us-states" style="display: block">
                                <div class="store-profile-row-lft">
                                    <h4>State <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="store_state">
                                        <option value="0" <?= set_select('store_state', '0') ?>>Select State</option>
                                        <?php foreach ($allstate as $state) { ?>
                                            <option value="<?= $state['stat_id'] ?>" <?php echo set_select( 'store_state', $state['stat_id'], (isset($page_data->store_state) ? $page_data->store_state == $state['stat_id'] ? true:'': "")); ?>><?= $state['stat_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="error" id="store_state_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row" id="other-states" style="display: none">
                                <div class="store-profile-row-lft">
                                    <h4>State <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="other_state" name="other_state" value="<?php echo (isset($page_data->store_state)?$page_data->store_state:""); ?>" />
                                    <div class="error" id="other_state_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Zip <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="store_zip" id="store_zip" onblur="myMap()" value="<?php echo (isset($page_data->store_zip)?$page_data->store_zip:""); ?>" />
                                    <div class="error" id="store_zip_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Contact Number</h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="store_number" name="store_number" placeholder="" value="<?php echo (isset($page_data->store_number)?$page_data->store_number : ""); ?>" onkeypress="return isNumber(event)" />
                                    <h6>Will be displayed publically. Recommended if you have a physical store.</h6>
                                </div>
                            </div>
                            <div class="store-profile-uplad">
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft"></div>
                                    <div class="store-profile-row-rgt">
                                        <div class="store-logo">
                                            <h6>Store Logo or a picture of yourself</h6>
                                            <input type='file' id="logo_img" name="logo_img" multiple/>
                                            <h6>Upload image size 150 x 110</h6>
                                            <div class="error" id="logo_img_validate"></div>
                                            <!-- <div id="imgList">
                                                <button class="cross-btn"></button>
                                            </div> -->
                                        </div>
                                        <div class="store-banner">
                                            <h6>Store Banner</h6>
                                            <input type='file' id="store_banner_img" name="store_banner_img" multiple/>
                                            <h6 class="mrg-botm-0">Upload image size 1100 x 234</h6>
                                            <div class="error" id="store_banner_img_validate"></div>
                                            <!-- <div id="imgTwo">
                                                <button class="cross-btn2"></button>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Store Description</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <textarea name="store_description" id="store_description" placeholder="Great items with a lot of love attached"><?php echo (isset($page_data->store_description) ? $page_data->store_description : ""); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="gve-othr">
                                <h5>Give others the opprtunity to connect with you</h5>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Facebook Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_facebook_url" id="store_facebook_url" value="<?php echo (isset($page_data->store_facebook_url)?$page_data->store_facebook_url :""); ?>" placeholder="https://www.facebook.com/example" />
                                        <h6 class="mrg-botm-5">Ex: https://www.facebook.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Twitter Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_twitter_url" id="store_twitter_url" value="<?php echo (isset($page_data->store_twitter_url)?$page_data->store_twitter_url:""); ?>" placeholder="https://www.twitter.com/example" />
                                        <h6 class="mrg-botm-5">Ex: https://www.twitter.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Instagram Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_instagram_url" id="store_instagram_url" value="<?php echo (isset($page_data->store_instagram_url) ?$page_data->store_instagram_url : ""); ?>" placeholder="https://www.instagram.com/example" />
                                        <h6 class="mrg-botm-5">Ex: https://www.instagram.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Pintrest Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_pintrest_url" id="store_pintrest_url" value="<?php echo (isset($page_data->store_pintrest_url)?$page_data->store_pintrest_url : ""); ?>" placeholder="https://www.pintrest.com/example" />
                                        <h6>Ex: https://www.pintrest.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-rgt">
                                        <h4>Click Submit to complete store set up</h4>
                                        <input type="submit" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="store_id" name="store_id" value="<?=$store_id?>"/>
                            <input type="hidden" name="store_longitude" id="store_longitude" />
                            <input type="hidden" name="store_latitude" id="store_latitude" />
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>


<!--Blog main ends  here-->
<script>


    function readURLm(input) {
        if (input.files.length > 0) {
            var reader = new FileReader();
            $.each(input.files, function (key, val) {
                {

                    reader.onload = function (e) {
                        $('<img>').attr('src', e.target.result).appendTo('#imgList');
                        $('.cross-btn').css('display', 'block')
                    }
                    reader.readAsDataURL(val);
                }
            });
        }
    }

    function readURL(input) {
        if (input.files.length > 0) {
            var reader = new FileReader();
            $.each(input.files, function (key, val) {
                {

                    reader.onload = function (e) {
                        $('<img>').attr('src', e.target.result).appendTo('#imgTwo');
                        $('.cross-btn2').css('display', 'block')
                    }
                    reader.readAsDataURL(val);
                }
            });
        }
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    $("#logo_img").change(function () {
        readURLm(this);
    });

    $("#store_banner_img").change(function () {
        readURL(this);
    });
    
     $(function() {
            $('#store_country').change(function(){
                if($('#store_country').val() === 'US') {
                    $('#us-states').show("slow"); 
                    $('#other_state').val('');
                    $('#other-states').hide("slow"); 
                } else {
                    $('#us-states').hide("slow");
                    $('#other-states').show("slow");
                } 
            });
         });
</script>
<script>
    function myMap() {
        var zipcode = $("#store_zip").val();
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'address': zipcode}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#store_latitude").val(latitude);
                $("#store_longitude").val(longitude);
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAscDVsx1aPq_cm_9DaZAmj6zOIPjhLDpo&callback=myMap"></script>

<script>
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    store_city: {
                        required: true
                    },
                    store_state: {valueNotEquals: "0"},
                    other_state: {
                        required: true
                    },
                    store_zip: {
                        required: true
                    },
                    store_country: {valueNotEquals: "0"}
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };
            $('#myaccount_form').validate(rules);
        });
    });
    /*logo_img: {
        required: true
    },
    store_banner_img: {
        required: true
    },*/
</script>