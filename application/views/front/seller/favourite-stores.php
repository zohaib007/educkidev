<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-btm-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url()?>store/<?=$user_data->store_url;?>"><?=$user_data->store_name;?></a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url()?>story/<?=$user_data->store_url;?>">My Story</a></li>
                        <li class="breadcrumb-item active">Favorite Shops</li>
                    </ol>
                </div>
                <!--my story starts here-->
                <div class="my-stry-list-main">
                    <div class="shop-right-upr">
                        <?php echo form_open('', array('name' => 'f_shop', 'method' => 'get', 'id' => 'f_shop')); ?>
                        <div class="show-itm">
                            <div class="ext-fav">
                                <h5><?php if ($total_rows > 0) {echo $per_page;} else {echo $total_rows;}?> Item(s)</h5>
                                <h6>Show:</h6>
                                <select name="per_page" onchange="javascript: $('#f_shop').submit();">
                                    <option value="10" <?php if ($per_page==10) {echo "selected='selected'";}?>>10</option>
                                    <option value="20" <?php if ($per_page==20) {echo "selected='selected'";}?>>20</option>
                                    <option value="40" <?php if ($per_page==40) {echo "selected='selected'";}?>>40</option>
                                    <option value="60" <?php if ($per_page==60) {echo "selected='selected'";}?>>60</option>
                                    <option value="80" <?php if ($per_page==80) {echo "selected='selected'";}?>>80</option>
                                    <option value="100" <?php if ($per_page==100) {echo "selected='selected'";}?>>100</option>
                                </select>
                            </div>
                            <?php if($logged_user==$this->session->userdata('userid')) { ?>
                            <a href="javascript:void(0)" class="unfavrt-btn">UnFavorite</a>
                            <?php }?>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <div class="my-stry-centr brd-top-0">
                        <div class="my-stry-centr-left">
                            <h5>Favorite Shops</h5>
                            
                        </div>
                        <div class="my-stry-centr-right">
                            <div class="cloths">
                                <?php if (count($stores) > 0) {
                                    $divStart = 0;
                                    $count    = 0;
                                    foreach ($stores as $store) {
                                        if ($divStart == 0) {
                                            echo '<div class="cloths-row">';
                                            $divStart = 1;
                                        }?>
                                        <div class="cloths-bx <?php if( (($count+1)%3) == 0 ) { echo 'mrg-rgt-0';}?>">
                                            <div class="cloths-bx-img-sec">
                                                <img src="<?=base_url()?>resources/seller_account_image/banner/<?=$store['store_banner_image']?>" alt="" title="" class="img-responsive">
                                                <!-- <span class="favrt-shp-abslt"></span> -->
                                            </div>
                                            <div class="elzbt-sec">
                                                <div class="elzbt-sec-left">
                                                    <img src="<?=base_url()?>resources/seller_account_image/logo/<?=$store['store_logo_image']?>" alt="" title="" class="img-responsive" />
                                                </div>
                                                <div class="elzbt-sec-right">
                                                    <div class="storeNname">
                                                        <a href="<?=base_url()?>store/<?=$store['store_url']?>">
                                                            <?=$store['store_name']?>
                                                        </a>
                                                    </div>
                                                    <p class="membr-cty-gry">
                                                        <?=$store['store_city']?>,
                                                            <?=$store['store_state']?>
                                                    </p>
                                                </div>
                                                <?php if($logged_user==$this->session->userdata('userid')) { ?>
                                                <div class="fav-chk">
                                                    <input type="checkbox" id="c<?=$count?>" value="<?=$store['store_id']?>" name="cc">
                                                    <label for="c<?=$count?>"><span></span></label>
                                                </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                        <?php if ((($count + 1) % 3) == 0) {
                                            echo '</div>';
                                            $divStart = 0;
                                        }
                                        $count++;}
                                        if(count($stores)%3!=0){
                                            echo "</div>";
                                        }
                                        /*if ($divStart == 1) {echo "<div>";}*/
                                        ?>
                                    <?php } else {
                                        echo '<div class="cloths-row">No item found.</div>';
                                    }?>
                            </div>
                            <?php if ($total_rows > 0){?>
                            <div class="shop-right-third mrg-top-0">
                                <div class="shop-right-third-lft">
                                    <p>
                                        <?php if ($total_rows > 0) {echo $to + 1;} else {echo $to;}?> -
                                        <?php if ($from > $total_rows) {echo $total_rows;} else {echo $from;}?> of
                                        <?=$total_rows?>
                                    </p>
                                </div>
                                <div class="shop-right-third-rgt">
                                    <ul>
                                        <?=$paginglink?>
                                    </ul>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <!--my story ends here-->
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here -->
<script>
    $(function(){
      $('.unfavrt-btn').click(function(){
        var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });

        if(val.length != 0){
            $.ajax({
                url: '<?=base_url('delete-favorite-shops')?>',
                type: 'POST',
                data: { val: val, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
            })
            .done(function(result) {
                if (result == 1) {
                    //save succesfully
                    window.location.href = "<?=base_url()?>favorite-shops/<?=$this->uri->segment(2)?>";
                } else {
                    error = 1;
                }
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        }else{
            alert("Please select atleast one checkbox.");
        }

      });
    });
</script>