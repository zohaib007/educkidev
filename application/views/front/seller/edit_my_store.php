<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">My Store</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>  
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <?php echo form_open_multipart('', array('name' => 'editstore', 'id' => 'editstore_form')); ?>
                    <div class="my-account-right">
                        <div class="my-account-right-head">
                            <h5>My Store Profile</h5>
                            <p>Store Name <span>*</span></p>
                        </div>
                        <div class="become-seller-sec">
                            <div class="chk-avalb">
                                <div class="chk-avalb-lft">
                                    <input type="text" name="store_name" id="store_name" onblur="check()" value="<?php echo $page_data->store_name; ?>" placeholder="Store name here"/>
                                    <span id="Available" class="g1 hide">Available</span>
                                    <span id="not_available" class="rd1 hide">Not Available</span>
                                </div>
                                <div class="chk-avalb-rgt">
                                    <input type="submit" id="check_availability" value="Check Availability"/>
                                </div>
                                <p>Your shop name will appear in your shop and next to each of your listings throughout eDucki.</p>

                                <div class="optionl-struct">
                                    <p id="rnd_store_name"></p>
                                </div>
                                <div class="error" id="store_name_validate"><?php echo form_error('store_name')?></div>
                            </div>
                            <div class="best-descb">
                                <div class="best-descb-lft">
                                    <h5>Which of these best describes you? <span>*</span></h5>
                                </div>
                                <div class="best-descb-rgt">
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r01" name="describe" <?php echo ($page_data->store_describe == 1 ? "checked" : ""); ?> value="1">
                                        <label for="r01"><span></span><p>Selling is my full-time job</p></label>
                                    </div>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r02" name="describe" <?php echo ($page_data->store_describe == 2 ? "checked" : ""); ?> value="2">
                                        <label for="r02"><span></span><p>I sell part-time but hope to sell full-time</p></label>
                                    </div>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r03" name="describe" <?php echo ($page_data->store_describe == 3 ? "checked" : ""); ?> value="3">
                                        <label for="r03"><span></span><p>I sell part-time and that’s how I like it</p></label>
                                    </div>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r04" name="describe" <?php echo ($page_data->store_describe == 4 ? "checked" : ""); ?> value="4">
                                        <label for="r04"><span></span><p>Other</p></label>
                                    </div>
                                </div>
                            </div>

                            <div class="best-descb">
                                <div class="change-edit">
                                    <a href="<?=base_url()?>edit-tier">Change</a>
                                </div>
                                <div class="best-descb-lft">
                                    <h5>Your current plan <span>*</span></h5>
                                </div>
                                <div class="best-descb-rgt">
                                    <input type="hidden" name="isTireChange" value = 0 id="isTireChange" />
                                    <input type="hidden" name="tirePrice" value = 0 id="tirePrice" />
                                    <?php foreach($tiers as $i => $tir) { ?>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r_<?=$i?>" disabled class="tiresVal" data-month = "<?=$tir->tier_months?>" data-price="<?=number_format($tir->tier_amount,2,'.','')?>" name="tiersId" value="<?=$tir->tier_id?>" <?=$tir->tier_id == $page_data->store_tier_id?'checked':''?>>
                                        <label for="r_<?=$i?>"><span></span><p>Tier <?=$i?>  <?=($tir->tier_months > 0)?'('.$tir->tier_months.' months)':'Free'?> – <?=($tir->tier_amount > 0)?'$'.number_format($tir->tier_amount,2,'.','').'/month,':''?> <?=$tir->tier_fee?>% transaction fee</p>
                                            <?php if($i == 2){?>
                                            <div class="mst-poplr"><h6>Most Popular</h6></div>
                                            <?php } ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="get-paid-sec">
                                <div class="get-paid-sec-hed">
                                    <!-- <h4>How you’ll get paid</h4> -->
                                    <h4 style="margin-bottom: 0px;">Accepted</h4>
                                    <!--
                                    <span class="protected">Protected</span>
                                    <a href="https://www.paypal.com" target="_blank">
                                        <img src="<?= base_url() ?>front_resources/images/paypal3.png" alt="" title="" class="img-responsive"/>
                                    </a>
                                    -->
                                </div>
                                <div class="get-paid-sec-lowr">
                                    <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png" alt="Credit Card Badges">
                                    <p>
                                        eDucki Payment believes simple is better . We use Paypal for all your online sales so you can manage all of your accounts in
                                        one place. Don’t have a Paypal account? Don’t wory, you can create one here .<a href="https://www.paypal.com/us/home" target="_blank"> Learn more</a>
                                    </p>
                                </div>
                                <div class="get-paid-sec-proces-paymnt">
                                    <h6>Process Payment via Paypal Account<span>*</span></h6>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r005" name="cc3" checked>
                                        <label for="r005"><span></span><p>Paypal Email</p></label>
                                    </div>
                                    <input type="text" id="paypal_id" name="paypal_id" placeholder="sample@gmail.com" value="<?= $page_data->store_paypal_id ?>" onblur="pay_check()"/>
                                    <p class="disclaim-ntxt">Provide the email address associated with your current PayPal account. If you do not have a PayPal account, the email you provide will be used to create a PayPal account for you.</p>
                                    <div class="error" id="paypal_id_validate"><?php echo form_error('paypal_id')?></div>
                                    <div id="error-paypal-id" class="error" hidden>Invalid Email Address</div>
                                </div>
                            </div>
                        </div>
                        <div class="onepx"></div>
                        <div class="store-profile">
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Street Address</h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="store_address" id="store_address" value="<?php echo (isset($page_data->store_address) ? $page_data->store_address : ""); ?>"/>
                                    <h6>Recommended if you have a physical store</h6>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>City <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text"  onblur="myMap()"  name="store_city" id="store_city" value="<?php echo (isset($page_data->store_city) ? $page_data->store_city : ""); ?>" />
                                    <div class="error" id="store_city_validate"> <?php echo form_error('store_city') ?> </div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Country <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="store_country" id="store_country"  onblur="myMap()" >
                                        <option value="0" <?= set_select('store_country', '0') ?>>Select Country</option>
                                        <?php foreach ($allcountries as $country) { ?>
                                            <option value="<?= $country['iso'] ?>" <?php echo (isset($page_data->store_country) ? $page_data->store_country == $country['iso'] ? 'selected' : '' : ""); ?> ><?= $country['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="error" id="store_country_validate"> <?php echo form_error('store_country') ?> </div>
                                </div>
                            </div>
                            <div class="store-profile-row" id="us-states" <?= ($page_data->store_country == "US" && $page_data->store_country != '') ? 'style="display: block"' : 'style="display: none"' ?>>
                                <div class="store-profile-row-lft">
                                    <h4>State <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="store_state" id="storeStates"  onblur="myMap()" >
                                        <option value="0" <?= set_select('store_state', '0') ?>>Select State</option>
                                        <?php foreach ($allstate as $state) { ?>
                                            <option value="<?= $state['stat_id'] ?>" <?php echo (isset($page_data->store_state) ? $page_data->store_state == $state['stat_id'] ? 'selected' : '' : ""); ?>><?= $state['stat_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="error" id="store_state_validate"> </div>
                                </div>
                            </div>
                            <div class="store-profile-row" id="other-states" <?= ($page_data->store_country != "US" && $page_data->store_country != '') ? 'style="display: block"' : 'style="display: none"' ?> >
                                <div class="store-profile-row-lft">
                                    <h4>State <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="other_state"  onblur="myMap()"  name="other_state" value="<?php if($page_data->store_state!=''){echo $page_data->store_state;}else{echo "";}?>" />
                                    <div class="error" id="other_state_validate"> </div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Zip <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" name="store_zip" onblur="myMap()" id="store_zip" value="<?php echo (isset($page_data->store_zip) ? $page_data->store_zip : ""); ?>" />
                                    <div class="error" id="store_zip_validate"> <?php echo form_error('store_zip') ?> </div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Contact Number</h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="store_number" name="store_number" placeholder="" value="<?php echo (isset($page_data->store_number) ? $page_data->store_number : ""); ?>" onkeypress="return isNumber(event)" />
                                    <h6>Will be displayed publically.</h6>
                                    <!-- Recommended if you have a physical store. -->
                                </div>
                            </div>
                            <div class="store-profile-uplad">
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <div class="store-logo">
                                            <h6>Store Logo or a picture of yourself</h6>
                                            <input type='file' id="imgInpM" name="logo_img" accept="image/*" class="imgList" />
                                            <h6>Upload image size 150 x 110</h6>
                                            <div id="imgList">
                                                <?php if(@$page_data->store_logo_image != '') { ?>
                                                <!-- <a class="cross-btn" href="javascript:void(0);" style="display: block;" onclick="delete_gall_image(<?php echo @$page_data->store_id; ?>, 'imgList', 'store_logo_image', 'logo_image');"></a> -->
                                                <img src="<?= base_url() ?>/resources/seller_account_image/logo/<?= $page_data->store_logo_image ?>" class="img-responsive" alt="" />
                                                <?php  } ?>
                                                <input type="hidden" name="logo_img" id="logo_image" value="<?= @$page_data->store_logo_image; ?>"  />
                                            </div>
                                            <div class="newlogoimage">
                                                <button class="cross-btn logo"></button>
                                            </div>
                                            <div class="error" id="logo_img_validate"> <?php echo form_error('store_zip') ?> </div>
                                        </div>
                                        <div class="store-banner">
                                            <h6>Store Banner</h6>
                                            <input type='file' id="imgInpN" name="store_banner_img" class="imgTwo" />
                                            <h6 class="mrg-botm-0">Upload image size 1100 x 234</h6>
                                            <div id="imgTwo">
                                                <?php if(@$page_data->store_banner_image != '') { ?>
                                                <!-- <a href="javascript:void(0);" class="cross-btn" style="display: block;" onclick="delete_gall_image(<?php echo @$page_data->store_id; ?>, 'imgTwo', 'store_banner_image', 'banner_image');"></a> -->
                                                <img src="<?= base_url() ?>/resources/seller_account_image/banner/<?= $page_data->store_banner_image ?>" class="img-responsive" alt="" />
                                                <?php } ?>
                                                <input type="hidden" name="store_banner_img" id="banner_image" value="<?= @$page_data->store_banner_image; ?>"  />
                                            </div>
                                            <div class="newbannerimage">
                                                <button class="cross-btn banner"></button>
                                            </div>
                                            <div class="error" id="store_banner_img_validate"> <?php echo form_error('store_zip') ?> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Store Description</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <textarea name="store_description" id="store_description" placeholder="Great items with a lot of love attached"><?php echo (isset($page_data->store_description) ? $page_data->store_description : ""); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="gve-othr">
                                <h5>Give others the opportunity to connect with you</h5>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Facebook Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_facebook_url" id="store_facebook_url" value="<?php echo (isset($page_data->store_facebook_url) ? $page_data->store_facebook_url : ""); ?>" placeholder="https://www.facebook.com/example" />
                                        <h6 class="mrg-botm-5">Ex: https://www.facebook.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Twitter Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_twitter_url" id="store_twitter_url" value="<?php echo (isset($page_data->store_twitter_url) ? $page_data->store_twitter_url : ""); ?>" placeholder="https://www.twitter.com/example" />
                                        <h6 class="mrg-botm-5">Ex: https://www.twitter.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Instagram Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_instagram_url" id="store_instagram_url" value="<?php echo (isset($page_data->store_instagram_url) ? $page_data->store_instagram_url : ""); ?>" placeholder="https://www.instagram.com/example" />
                                        <h6 class="mrg-botm-5">Ex: https://www.instagram.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Pintrest Page Url</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="store_pintrest_url" id="store_pintrest_url" value="<?php echo (isset($page_data->store_pintrest_url) ? $page_data->store_pintrest_url : ""); ?>" placeholder="https://www.pintrest.com/example" />
                                        <h6>Ex: https://www.pintrest.com/example</h6>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <div class="store-profile-row-rgt">
                                        <h4>Click Submit to complete store set up</h4>
                                        <input type="submit" value="Submit" class="saveData" />
                                        <input type="submit" value="Pay With Paypal" class="changeTire" style="display: none;" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="store_id" name="store_id" value="<?= $page_data->store_id ?>"/>
                            <input type="hidden" name="user_id" value="<?= $userid ?>"/>
                            <!--<input type="hidden" name="user_id" value="1"/>-->
                            <input type="hidden" name="store_longitude" id="store_longitude" />
                            <input type="hidden" name="store_latitude" id="store_latitude" />
                            <input type="hidden" id="old_store" value="<?php echo $page_data->store_name; ?>" />
                            <input type="hidden" id="old_paypal_id" value="<?php echo $page_data->store_paypal_id; ?>" />
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>              <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->

<!--register info popup starts here-->
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="exampleModalLabel">Tier Change Request</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>
                    A new billing rate will be effective on your next billing
                    cycle, and the new subscription will begin at month
                    1 of 12
                </p>
            </div>
        </div>
    </div>
</div>

<!--register info popup ends here-->
<script>

/*$("#paypal_id").on('keyup', function() {
        var email = $("#paypal_id").val();
        var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(email);
        console.log(re);
        if(!re) {
            $('#error-paypal-id').show();
        } else {
            $('#error-paypal-id').hide();
        }
    });*/

$('.tiresVal').click(function(){
    var tVal = parseInt($(this).val());
    var oldVal = '<?=$page_data->store_tier_id?>';
    oldVal = parseInt(oldVal);
    if(tVal == oldVal){
        $('#isTireChange').val(0);
        $('.changeTire').hide();
        $('.saveData').show();
    } else {
        if(tVal == 1){
            $('#isTireChange').val(0);
            $('.changeTire').hide();
            $('.saveData').show();
        } else {
            $('#isTireChange').val(1);
            $('.saveData').hide();
            $('.changeTire').show();
        }
    }
});

    /*
    function readURLm(input) {
        if (input.files.length > 0) {
            var reader = new FileReader();
            $.each(input.files, function (key, val) {
                {
                    reader.onload = function (e) {
                        $('<img>').attr('src', e.target.result).appendTo('.newlogoimage');
                        $('.logo').css('display', 'block')
                    }
                    reader.readAsDataURL(val);
                }
            });
        }
    }

    function readURL(input) {
        if (input.files.length > 0) {
            var reader = new FileReader();
            $.each(input.files, function (key, val) {
                {
                    reader.onload = function (e) {
                        $('<img>').attr('src', e.target.result).appendTo('.newbannerimage');
                        $('.banner').css('display', 'block');
                    }
                    reader.readAsDataURL(val);
                }
            });
        }
    }
    */

    $("#imgInpM").change(function () {
        readURLm(this);
    });
    $("#imgInpN").change(function () {
        readURL(this);
    });
</script>
<script>
    var ajaxLoading = false;
    var base_url = '<?php echo base_url(); ?>';
    function check() {
        $("#check_availability").click();
    }
    function delete_gall_image(sel, hide, fieldname, name) {
        $('.' + hide).removeClass("hide");
        $('#' + hide).hide();
        $('#' + name).val('');

        $.ajax({
            url: base_url + 'delete-store-image',
            type: 'POST',
            data: {nId: sel, fieldname: fieldname, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
        }).done(function (res) {
            console.log(res);
            //return false;
        }).fail(function () {
            console.log("error");
        }).always(function () {
            console.log("complete");
        });
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    $(function () {
        $('#store_country').change(function () {
            if ($('#store_country').val() === 'US') {
                $('#us-states').show("slow");
                $('#other_state').val('');
                $('#other-states').hide("slow");
            } else {
                $('#us-states').hide("slow");
                $('#other-states').show("slow");
            }
        });
    });

    $(document).ready(function () {

        $("#check_availability").click(function (e) {
            e.preventDefault();
            var value = $('#store_name').val();
            var old_value = $('#old_store').val();
            if (!ajaxLoading) {
                if(value!=''){
                    if (value != old_value) {
                        var url = "<?= base_url() ?>check_availability";
                        var value = $('#store_name').val();
                        $.ajax({
                            url: url,
                            type: "POST",
                            dataType: "json",
                            data: {"store_name": value,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function (data) {
                                if (data.status == 'Available') {
                                    $("#Available").removeClass("hide");
                                    $("#not_available").addClass("hide");
                                    $('#store_name_validate').html('');
                                } else {
                                    $("#not_available").removeClass("hide");
                                    $("#rnd_store_name").html("Store Name Not Available try this " + data.new_name);
                                    $('#store_name_validate').html('');
                                }
                                ajaxLoading = false;
                            }
                        });
                    }
                }else{
                    $('#store_name_validate').html('This field is required');
                    $("#Available").addClass("hide");
                    $("#not_available").addClass("hide");
                }
            }
        });
    });
</script>
<script>
    function myMap() {
        var zipcode = $("#store_zip").val();
        var city = $("#store_city").val();
        var country = $('#store_country').val();
        var state = $('#other_state').val();
        if(country == 'US'){
            state = $('#storeStates').val();
        }
        var address = city+','+state+','+country+','+zipcode;
        var address = city+','+state+','+country;
        console.log(address);
        var geocoder = new google.maps.Geocoder();
        //1600+Amphitheatre+Parkway,+Mountain+View,+CA
        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                //console.log(latitude+' -> '+longitude);
                $("#store_latitude").val(latitude);
                $("#store_longitude").val(longitude);
            }
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAscDVsx1aPq_cm_9DaZAmj6zOIPjhLDpo&callback=myMap"></script>

<script>
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body..
            var rules = {
                rules: {
                    store_name: {
                        required: true
                    },
                    paypal_id: {
                        email:true,
                        required: true
                    },
                    store_city: {
                        required: true
                    },
                    store_state: {valueNotEquals: "0"},
                    store_zip: {
                        required: true
                    },
                    other_state: {
                        required: true
                    },
                    store_country: {valueNotEquals: "0"}
                    /*logo_img: {
                      extension: "jpg|jpeg|png",
                      required: true
                    },
                    store_banner_img: {
                      extension: "jpg|jpeg|png",
                      required: true
                    }*/
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    $("#" + name + "_validate").html('');
                    $("#" + name + "_validate").html(error);
                    //error.appendTo($("#" + name + "_validate"));
                },
            };
            $('#editstore_form').validate(rules);
        });
    });
</script>
<script>
    var ajaxLoading = false;
    var base_url = '<?php echo base_url(); ?>';

    function pay_check() {
        var value = $('#paypal_id').val();
        var old_value = $('#old_paypal_id').val();
        if (!ajaxLoading) {
            if(value!=''){
                if(value != old_value){
                    var url = "<?= base_url() ?>check_availability";
                    var value = $('#paypal_id').val();
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: {"paypal_id": value,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                        success: function (data) {
                            if (data.status == 'Available') {
                                $('#paypal_id_validate').html('');
                            } else {
                                $('#paypal_id_validate').html('Your Paypal ID should be unique.');
                            }
                            ajaxLoading = false;
                        }
                    });
                }
            }else{
                $('#paypal_id_validate').html('This field is required');
            }
        }
    }
</script>