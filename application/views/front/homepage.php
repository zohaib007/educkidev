<!--Main banner Slider starts here-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDswE9MLqvjoCNwwl2vkideo5i6pYKHmYs&libraries=places"></script>

 <script type="text/javascript" src="<?=base_url()?>front_resources/js/markerclusterer.js"></script>

<style>
  #map-container {
        padding: 6px;
        border-width: 1px;
        border-style: solid;
        border-color: #ccc #ccc #999 #ccc;
        -webkit-box-shadow: rgba(64, 64, 64, 0.5) 0 2px 5px;
        -moz-box-shadow: rgba(64, 64, 64, 0.5) 0 2px 5px;
        box-shadow: rgba(64, 64, 64, 0.1) 0 2px 5px;
        width: 600px;
      }
      #map {
        width: 100%; /* 600px */
        height: 400px;
      }

</style>


 <script>
var data = { "stores": [
<?php  $count = 0;
foreach($home_store as $store) {
if($store->store_longitude!=''&& $store->store_latitude!=''){ ?>
{"store_id": <?= $store->store_id ?>, "store_url": "<?= base_url() ?>store/<?= $store->store_url ?>", "longitude": <?= $store->store_longitude ?>, "latitude": <?= $store->store_latitude ?>} <?php echo ($count!=(count($home_store)-1))?',':''?>
 <?php } $count++;} ?>
]};

console.log(data);    

      function initialize() {
        var center = new google.maps.LatLng(41.450328, -95.894242);

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 3,
          center: center,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var markers = [];


            var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });


$("#searchloc").click(function(){

     //   searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
           /* var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));
*/
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });

            map.fitBounds(bounds);
            map.setZoom(6);
        });



        for (var i = 0; i < data.stores.length; i++) {
          var dataStore = data.stores[i];
          var latLng = new google.maps.LatLng(dataStore.latitude,
              dataStore.longitude);
          var marker = new google.maps.Marker({
            position: latLng,
            url: dataStore.store_url
          });
            console.log(marker.url);

          google.maps.event.addListener(marker, 'click', function() {
              console.log(this.url);
      window.location.href =this.url;
    });
          markers.push(marker);
        }
        var markerCluster = new MarkerClusterer(map, markers, {imagePath: '<?= base_url() ?>front_resources/images/m'});
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>

<div class="main-bannr">
    <ul class="bxslider">
        <?php
        if(count($home_banner) > 0){
            foreach($home_banner as $banner){
        ?>
        <li><img src="<?= base_url() ?>resources/home_image/<?=$banner->banner_image?>"  alt="" />
            <div class="auto-container">
                <div class="wdth-50">
                    <p>
                        Destination for Clothes & Kid's Accessories    
                    </p>
                    <ul>
                        <li>
                            <a href="javascript:void(0);">Buy</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Sell</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Trade</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Donate</a>
                        </li>
                    </ul>
                    <?php if(!$this->session->userdata('logged_in')){?>
                    <div class="bnr-sec">
                        <a href="<?php  echo $this->facebook->login_url(); ?>" class="bnr-fb"></a>
                        <a href="#" data-toggle="modal" data-target="#myModal" class="bnr-eml close_click"></a>
                    </div>
                    <?php }?>
                </div>
            </div>
        </li>
        <?php
            } 
        } else {
        ?>
        <li><img src="<?= base_url() ?>resources/home_image/no-banner.jpg"  alt="" />
            <div class="auto-container">
                <div class="wdth-50">
                    <p>
                        Destination for Clothes & Kid's Accessories    
                    </p>
                    <ul>
                        <li>
                            <a href="javascript:void(0);">Buy</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Sell</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Trade</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Donate</a>
                        </li>
                    </ul>
                    <div class="bnr-sec">
                        <a href="<?php  echo $this->facebook->login_url(); ?>" class="bnr-fb"></a>
                        <a href="#" data-toggle="modal" data-target="#myModal" class="bnr-eml close_click"></a>
                    
                    </div>
                </div>
            </div>
        </li>
        <?php
        }
        ?>
    </ul>
</div>    
<!--Main banner Slider ends here-->
<?php if(count($home_category ) > 0 ){?>
<!--home catgry starts here-->
<section class="catgr-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="cgt-tit">
                    <h4><?=$home_data->category_title?></h4>    
                </div>
                <div class="slk-sec">
                    <div class="slider-for">
                        <?php foreach($home_category as $category) { ?>
                        <div>                            
                            <a href="<?=base_url()?><?=$category->cat_url?>" class="accr-bt"><img src="<?= base_url() ?>resources/cat_image/<?=$category->cat_image?>" alt="" title="" class="img-responsive"/><?=$category->cat_name?></a>
                        </div>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--home catgry ends here-->
<?php } ?>
<!--Featured Sellers starts here-->
<?php if(count($featured_sellers) > 0 ){?>
<section class="hm-sel-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="feat-tit">
                    <h4><?=$home_data->feaSeller?></h4>
                    <span class="brd-lne">
                        <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                    </span>
                </div>
                <div class="featrd-row">
                <?php foreach($featured_sellers as $i => $seller){$image= '';?>
                    <?php if ($i > 0 && $i % 4 == 0) {?>
                        </div><div class="featrd-row">
                    <?php } if(!empty($seller->store_logo_image)){$image = $seller->store_logo_image;}else{$image= 'D-2.jpg';}?>
                    <div class="featrd-bx <?php if ($i > 0 && $i == 3 || $i == 7) {echo ' mrg-rgt-0';}?>">                        
                        <a href="<?=base_url().'store/'.$seller->store_url?>"><img src="<?= base_url() ?>resources/seller_account_image/logo/<?=$image?>" alt="" title="" class="img-responsive"/><span><?=$seller->store_name?></span></a>
                    </div>
                <?php if($i==7)break;} ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<!--Featured Sellers ends here-->
<!--Map sec starts here-->
<section class="hm-mp-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="srch-itm-ar">
                        <div class="itm-ar-tit">
                            <h5><?=$home_data->searchtitle?></h5>
                            <span class="brd-lne">
                                <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                            </span>
                        </div>
                        <div class="itm-ar-txt">
                            <?=$home_data->searchtext?>
                            <input type="text" placeholder="City, State" id="pac-input" class="controls" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City, State, Zip'" />
                            
                            <input type="button"  id="searchloc" value="<?=$home_data->searchButton?>"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div id="map-container"><div id="map"></div></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Map sec ends here-->
<!--Getting starts here-->
<section class="hm-sel-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="feat-tit">
                    <h4><?=$home_data->gettingStartTitle?></h4>
                    <span class="brd-lne">
                        <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                    </span>
                </div>
                <div class="grt-strt-sec">
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('resources/home_image/'.$home_data->start_image1) ?>" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <?=$home_data->startTitle1?>
                            
                        </div>
                    </div>
                    <div class="stp-bx-2">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/step-right-arrow.png" alt="" title="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('resources/home_image/'.$home_data->start_image2) ?>" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <?=$home_data->startTitle2?>
                        </div>
                    </div>
                    <div class="stp-bx-2">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/step-right-arrow.png" alt="" title="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('resources/home_image/'.$home_data->start_image3) ?>" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <?=$home_data->startTitle3?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(!$this->session->userdata('logged_in')){?>
        <div class="row">
            <div class="join-btn">
                <a href="javascript:void(0)" class="close_click" data-toggle="modal" data-target="#myModal">Join Now</a>
            </div>
        </div>
        <?php }?>
    </div>
</section>
<!--Getting starts ends here-->
<?php if(count($home_member) > 0) { ?>
<!--Save and average sec starts here-->
<section class="sve-arg-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="sve-tit">
                    <h5><?=$home_data->membertitle?></h5>
                </div>
                <div class="hm-avrg-sec">
                    <?php 
                    $rowStart = 0;
                    $div = 1;
                    foreach($home_member as $mem ) { 
                    ?>
                    <?php
                    if( $rowStart== 0 ){?> 
                    <div class="hm-avrg-sec-row">
                    <?php
                        $rowStart = 1;
                    } ?>
                        <div class="<?=(($div%2) != 0 )?'only-for-lft':'only-for-rgt'?>">
                            <ul>
                                <li>
                                    <span><img src="<?=base_url('resources/member_image/thumb/thumb_'.$mem->mem_image)?>" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                <?=clean_string($mem->mem_description)?>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    <?php
                    if($div%2 == 0){
                    ?>
                    </div>
                    <?php
                    $rowStart = 0;
                    } 
                    ?>
                    <?php } ?>
                    <?php if($rowStart == 1){ echo "</div"; }?>
                </div>
                <div class="hm-avrg-sec-row">
                  <!-- <div class="newtag-line"><?=clean_string($home_data->memberdesc)?></div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--Save and average sec ends here-->
<?php } ?>