<!--Cart main section starts here-->
<div class="ovr-flw-hid">
    <div class="container">
        <div class="row">
            <div class="auto-container-hed2">
                <div class="col-md-8 cart-col-md-8">
                    <div class="cart-left-mian">
                        <div class="cart-left-mian-hed">
                            <h2>Shopping Cart</h2>
                        </div>
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a data-id="1" href="javascript:;" class="breadcrumb-step <?php
                                    if ($this->uri->segment(1) == "cart") {
                                        echo "active";
                                    }
                                    ?>">Cart</a></li>
                                <li class="breadcrumb-item"><a data-id="2" class="breadcrumb-step" href="javascript:void(0);" >Shipping Address</a></li>
                                <li class="breadcrumb-item"><a data-id="3" class="breadcrumb-step" href="javascript:void(0);"> Payment &amp; Billing</a></li>
                                <li class="breadcrumb-item"><a data-id="4" class="breadcrumb-step" href="javascript:void(0);">Confirm Order</a></li>
                            </ol>
                        </div>

                        <div class="cart-tab-sec">
                            <?php if (count($cart_product_details) > 0) { ?>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:14%"></th>
                                            <th style="width:58%"></th>
                                            <th style="width:11%" class="text-right">Price</th>
                                            <th style="width:8%" class="text-right">Qty</th>
                                            <th class="pad-rgt-0 text-right" style="width:9%">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        
                                        $send = 1;
                                        foreach ($cart_product_details as $product) {
                                            $product_details = getproduct_details($product->cart_prod_id);
                                            if ($product_details != '') {
//                                        var_dump($product_details);die;
                                                if ($product_details->subSubCatURL != '') {
                                                    $category_level = 3;
                                                } elseif ($product_details->subCatURL != '') {
                                                    $category_level = 2;
                                                } elseif ($product_details->catURL != '') {
                                                    $category_level = 1;
                                                }
                                                ?>
                                                <tr id="row_<?= $product->cart_detail_id ?>">
                                                    <td class="pad-lft-0">
                                                        <img src="<?= base_url() ?>resources/prod_images/<?= $product_details->img_name ?>" class=" img-responsive"/>
                                                    </td>
                                                    <td>
                                                        <div class="crt-tab-txt">
                                                            <div class="crt-tab-txt-upr">
                                                                <?php if ($category_level == 3) { ?>
                                                                    <a href="<?= base_url() ?><?= $product_details->catURL ?>/<?= $product_details->subCatURL ?>/<?= $product_details->subSubCatURL ?>/<?= $product_details->prod_url ?>">
                                                                        <?= $product_details->prod_title ?>
                                                                    </a>
                                                                <?php } if ($category_level == 2) { ?>
                                                                    <a href="<?= base_url() ?><?= $product_details->catURL ?>/<?= $product_details->subCatURL ?>/<?= $product_details->prod_url ?>">
                                                                        <?= $product_details->prod_title ?>
                                                                    </a>
                                                                <?php } if ($category_level == 1) { ?>
                                                                    <a href="<?= base_url() ?><?= $product_details->catURL ?>/<?= $product_details->prod_url ?>">
                                                                        <?= $product_details->prod_title ?>
                                                                    </a>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="crt-tab-txt-lowr">
                                                                <h4>Store Name: <span><a href="<?=base_url()?>store/<?=$product_details->store_url?>"><?=$product_details->store_name ?></a></span></h4>
                                                            </div>
                                                            <?php
                                                            $filters = json_decode($product->cart_details);
                                                            /*echo "<pre>";
                                                            print_r($filters);
                                                            exit;*/                                                            
                                                            if ($filters) {
                                                                foreach ($filters as $filter) {
                                                                    ?>
                                                                    <div class="crt-tab-txt-lowr2">
                                                                        <p><?=$filter->filterName ?>:</p>
                                                                        <span><?= $filter->filtervalue ?></span>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <div class="crt-tab-txt-lowr2">
                                                                <p>
                                                                    Available Shipping Type:
                                                                </p>
                                                                <span><?= $product_details->shipping?><?=(!empty($product_details->prod_shipping_methods))?' ('.$product_details->prod_shipping_methods.')':''?></span>
                                                            </div>
                                                            <div class="crt-tab-txt-lowr3">
                                                                <?php if ($product_details->shipping == 'Offer free Shipping') { ?>
                                                                    <h6>
                                                                        Ready to ship in <?= $product_details->free_ship_days ?> days.
                                                                    </h6>
                                                                <?php } elseif ($product_details->shipping == 'Charge for Shipping') { ?>
                                                                    <h6>
                                                                        Ready to ship in <?= $product_details->ship_days ?> days.
                                                                    </h6>
                                                                    <input type="hidden" class="shipping_cost_<?= $product_details->prod_id ?>" value="<?= $product_details->ship_price ?>">
                                                                <?php } ?>
                                                            </div>
                                                            <div class="crt-tab-txt-lowr3">
                                                                <div id="cart_msg_message_<?=$product->cart_detail_id?>" class="alert alert-danger message hide">
                                                                    <a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                                                                    Product out of Stock
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-right" id="prod_price_<?= $product_details->prod_id ?>">$<?= number_format($product->unit_prod_price, 2, '.', '') ?></td>
                                                    <td class="text-right">
                                                        <div class="inpt-tp-no">
                                                            <?php if(getCartTotalQty($product_details->prod_id, $product->cart_id) > $product_details->qty){
                                                                $send = 0;
                                                            }
                                                            ?>
                                                                <input type="number" min=1 class="qtyChange qty<?=$product->cart_detail_id?> qqty<?=$product_details->prod_id?>" value="<?=$product->cart_prod_qnty?>"
                                                                data-cdid="<?=$product->cart_detail_id ?>" data-prod=<?=$product_details->prod_id?>
                                                                data-dbqty="<?=$product_details->qty?>" data-old = '<?=$product->cart_prod_qnty?>'
                                                                data-remqty = "<?=$product_details->qty-getCartTotalQty($product_details->prod_id, $product->cart_id)?>" 
                                                                data-cartqty="<?=getCartTotalQty($product_details->prod_id, $product->cart_id)?>" />
                                                                <input type="hidden" id="number_<?= $product_details->prod_id ?>" value="0">
                                                            <!--  <?php  if ($product_details->qty >= $product->cart_prod_qnty) { ?> -->
                                                            <!-- <?php } else { ?>
                                                                <input type="number" min=1 class="number_<?= $product_details->prod_id ?>" data-cart="<?=getCartTotalQty($product_details->prod_id, $product->cart_id)?>" id="<?= $product_details->prod_id ?>" onchange="price(<?= $product_details->prod_id ?>)" value="<?= $product_details->qty ?>"/>
                                                                <input type="hidden" id="number_<?= $product_details->prod_id ?>" value="0" >
                                                            <?php } ?> -->
                                                            <input type="hidden" class="prod_db_qty_<?= $product_details->prod_id ?>" id="prod_db_qty_<?= $product_details->prod_id ?>" value="<?=$product_details->qty?>"/> <!-- <?=$product_details->qty?> -->
                                                            <input type="hidden" class="prod_price_<?= $product_details->prod_id ?>" value="<?= $product->unit_prod_price ?>"/>
                                                            <input type="hidden" class="total_price_<?= $product_details->prod_id ?>" value="<?= $product->total_prod_price ?>"/>
                                                            <a href="javascript:;" onclick="removeitem(<?= $product_details->prod_id ?>,<?=$product->cart_detail_id ?>)">Remove</a>
                                                        </div>
                                                    </td>
                                                    <td class="pad-rgt-0 text-right" id="total_price_<?= $product_details->prod_id ?>">$<?= number_format($product->total_prod_price, 2, '.', '') ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="ualert-row">
                                    <div id="message" class="alert alert-danger message " style="text-align: center;">
                                        You have no items in your shopping cart.
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 cart-col-md-4 uty-cart-1">
                    <div class="cart-right-main uty-cart-1">
                        <div class="boxgrey-rp"></div><!-- Grey Cover -->
                        <div class="cart-right-hed">
                            <h6>Order Details</h6>
                        </div>
                        <div class="cart-right-lowr">
                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Subtotal
                                </div>
                                <div class="cart-right-rw-rgt" id="cart_sub_total">
                                    $<?= number_format($cart_sub_total, 2, '.', '') ?>
                                </div>
                            </div>
                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Discount
                                </div>
                                <div class="cart-right-rw-rgt" id="discount_amount">
                                    $<?= number_format($discount_amount, 2, '.', '') ?>
                                </div>
                            </div>
                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Shipping Total
                                </div>
                                <div class="cart-right-rw-rgt" id="shipment_sub_total">
                                    $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                                </div>
                            </div>
                        </div>
                        <div class="cart-right-rw">
                            <div class="cart-right-rw-lft">
                                <h6>Total</h6>
                            </div>
                            <div class="cart-right-rw-rgt" id="total_shipment_amount">
                                <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                            </div>
                        </div>

                        <div class="cart-right-lowr2 center-block">
                            <?php if ($this->session->userdata('logged_in')) { ?>
                                <a href="<?= base_url() ?>cart-shipping" class="prcd-btn checkout">Proceed to Checkout</a>
                            <?php } else { ?>
                                <a href="javascript:void(0);" class="hdr-loginbtn prcd-btn" data-toggle="modal" data-target="#myModal3" >Proceed to Checkout</a>
                            <?php } ?>
                            <a href="<?=$_SESSION['refererUrl']?>" class="conti-btn">Continue Shopping</a>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Cart main section ends here-->
<script>
    $("#cat-code").change(function () {
        var value = this.value;
        if (value == "1") {
            $(".clk-shp-sho").show();
        }
        else {
            $(".clk-shp-sho").hide();
        }
    });
</script>
<script>
    $(document).ready(function() { 
        $(document).on('click keyup','.qtyChange', function() {
            $('.checkout').removeAttr('href');
            var qty = parseInt($(this).val());
            var oldVal = parseInt($(this).attr('data-old'));
            var cart_detail_id = $(this).attr('data-cdid');
            var cart_qty = parseInt($(this).attr('data-cartqty'));
            var prod_id = $(this).attr('data-prod');
            var prod_price = $(this).attr('data-prodprice');
            var total_price = qty * prod_price;
            var db_qty = parseInt($(this).attr('data-dbqty'));
            var cartId = <?= $product->cart_id ?>;
            $(this).attr('disabled', true);

            // console.log('qty: '+qty+' -> db_qty: '+db_qty+' -> cart_qty: '+cart_qty);
            // if (qty > db_qty ) {
            //     $("#number_" + prod_id).val('1');
            // } else {
            //     $("#number_" + prod_id).val('0');
            // }
           
            // if(cart_qty > db_qty){
            //     $("#number_" + prod_id).val('1');
            // }

            var remQty = $(this).attr('data-remqty');
            // if((remQty) > 0){
            //     $(this).attr('data-remqty', (parseInt(remQty)-1) )
            //     $("#number_" + prod_id).val('0');

            // }else{
            //     $("#number_" + prod_id).val('1');
            // }
            var setval = 0;
            if((cart_qty+(qty-oldVal)) > db_qty){
                $("#number_" + prod_id).val('1');
            }else{
                $("#number_" + prod_id).val('0');
                setval = (cart_qty+(qty-oldVal));
            }
            

            var hiddennum = $("#number_"+prod_id).val();
            if (qty >= 1) {
                if ((hiddennum == '0')) {
                    // if (qty <= db_qty) {
                        $.ajax({
                            url: "<?=base_url()?>changeitems",
                            type: "POST",
                            dataType: "json",
                            async: false,
                            data: {oldQty: oldVal, cartId: cartId, cart_detail_id: cart_detail_id, qty: qty, prodId: prod_id, <?=$this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function(data){
                                if (data.status == 'Avaliable') {
                                    $("#total_price_"+prod_id).html("$" + data.total_price);
                                    $("#cart_sub_total").html("$"+data.cart_sub_total);
                                    $("#shipment_sub_total").html("$" + data.shipment_sub_total);
                                    $("#discount_amount").html("$" + data.discount_amount);
                                    $("#total_shipment_amount").html("$" + data.total_shipment_amount);
                                }
                                $('.qty'+cart_detail_id).attr('data-old' , parseInt(qty));
                                $('.qqty'+prod_id).attr('data-cartqty' , parseInt(setval));
                                $('.qty'+cart_detail_id).removeAttr('disabled');
                            }
                        });
                    // }
                } else {
                    setTimeout(function () {
                        $('.qty'+cart_detail_id).val(parseInt(oldVal));                    
                        $('#cart_msg_message_'+cart_detail_id).addClass('hide'); //hide();
                        $('.qty'+cart_detail_id).removeAttr('disabled');
                    }, 2000);

                    $('#cart_msg_message_'+cart_detail_id).removeClass('hide');
                }
            } else {
                $('.qty'+cart_detail_id).val(1);
            }
            $('.checkout').attr('href', '<?= base_url() ?>cart-shipping');
            

        });
    });

    // function price(id) {
    //     var cartId = <?= $product->cart_id ?>;
    //     var qty = parseInt($("#" + id).val());
    //     //var Cartqty = parseInt($("#" + id).attr('data-cart'));
    //     var Cartqty = parseInt($(this).attr('data-cart'));

    //     var prod_price = $(".prod_price_" + id).val();
    //     var total_price = qty * prod_price;
    //     var url = "<?= base_url() ?>changeitems";
    //     //var db_qty = parseInt($("#prod_db_qty_"+id).val());  
    //     var db_qty = parseInt($(".prod_db_qty_"+id).val());  
    //     console.log(Cartqty + ' -> QTY:' + qty + ' -> DB-QTY: '+ db_qty +' -> '+ (qty > db_qty && qty >= Cartqty)  );
    //     if (qty > db_qty ) {
    //         $("#number_" + id).val('1');
    //     } else {
    //         $("#number_" + id).val('0');
    //     }
    //     // Code By HASEEB
    //     if(qty >= Cartqty){
    //         $("#number_" + id).val('1');
    //     }
    //     var hiddennum = $("#number_"+id).val();
    //     if (qty >= 1) {
    //         if ((hiddennum == '0')) {
    //             if (qty <= db_qty) {
    //                 $.ajax({
    //                     url: url,
    //                     type: "POST",
    //                     dataType: "json",
    //                     async: false,
    //                     data: {"cartId": cartId, qty: qty, prodId: id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
    //                     success: function(data){
    //                         var values = data;
    //                         if (values.status == 'Avaliable') {
    //                             $("#total_price_"+id).html("$" + values.total_price);
    //                             $("#shipment_sub_total").html("$" + values.shipment_sub_total);
    //                             $("#cart_sub_total").html("$" + values.cart_sub_total);
    //                             $("#discount_amount").html("$" + values.discount_amount);
    //                             $("#total_shipment_amount").html("$" + values.total_shipment_amount);
    //                         }
    //                     }
    //                 });
    //             }
    //         } else {
    //             setTimeout(function () {
    //                 $("#"+id).val(parseInt(qty-1));
    //                 $('#cart_msg_message_'+id).addClass('hide'); //hide();
    //             }, 3000);
    //             $('#cart_msg_message_'+id).removeClass('hide');
    //         }
    //     } else {
    //         $("#"+id).val(1);
    //     }
    // }
    function removeitem(id,detail_id) {
        var cartId = <?= $product->cart_id ?>;
        var qty = <?= $product->cart_prod_qnty ?>;
        var url = "<?= base_url() ?>removeItems";
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: {"cartId": cartId, qty: qty, prodId: id,detail_id:detail_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                if (data.status == 'Avaliable') {
                    //console.log(data);
                    if (data.quantity >= 1) {
                        location.reload();

                        // $("#row_" + detail_id).hide();
                        // $("#shipment_sub_total").html("$" + data.shipment_sub_total);
                        // $("#cart_sub_total").html("$" + data.cart_sub_total);
                        // $("#discount_amount").html("$" + data.discount_amount);
                        // $("#total_shipment_amount").html("$" + data.total_shipment_amount);
                    } else {
                        window.location.assign("<?= base_url() ?>");
                    }
                }
            }
        });
    }
</script>