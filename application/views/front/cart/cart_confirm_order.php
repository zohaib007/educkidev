<!--Cart main section starts here-->
<script src="https://www.paypalobjects.com/js/external/apdg.js" type="text/javascript"></script>
<div class="ovr-flw-hid">
    <div class="container">
        <div class="row">
            <div class="auto-container-hed2">
                <div class="col-md-8">
                    <div class="cart-left-mian">
                        <?php if ($this->session->flashdata('paypal_error') != '') { ?>
                            <div class="alert alert-danger fade in alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <?php echo $this->session->flashdata('paypal_error'); ?>
                            </div>
                        <?php } ?>
                        <div class="cart-left-mian-hed">
                            <h2>Shopping Cart</h2>
                        </div>
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo ($this->uri->segment(1) == "cart") ? "javascript:;" : base_url() . "cart"; ?>">Cart</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo ($this->uri->segment(1) == "cart-shipping") ? "javascript:;" : base_url() . "cart-shipping"; ?>">Shipping Address</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo ($this->uri->segment(1) == "cart-payment") ? "javascript:;" : base_url() . "cart-payment"; ?>">Payment &amp; Billing</a></li>
                                <li class="breadcrumb-item"><a class="<?php
                                    if ($this->uri->segment(1) == "cart-confirm-order") {
                                        echo "active";
                                    }
                                    ?>" href="<?php if ($this->uri->segment(1) == "cart-confirm-order") { echo "javascript:;"; } ?>">Confirm Order</a></li>
                            </ol>
                        </div>
                        <?php //echo form_open_multipart('', array('name' => 'confirm_order', 'id' => 'confirm_order_form'));   ?>
                        <div class="crt-order-main">
                            <div class="crt-order-main-hed">
                                <h3>Your Shopping Cart</h3>
                                <a href="<?= base_url() ?>cart">Edit</a>
                            </div>
                            <div class="crt-order-frst">
                                <?php
                                foreach ($cart_product_details as $product) {
                                    $product_details = getproduct_details($product->cart_prod_id);
                                    if ($product_details != '') {
                                        ?>
                                        <div class="crt-order-frst-rw" id="row_<?= $product_details->prod_id ?>">

                                            <div class="crt-order-frst-bx">
                                                <img src="<?= base_url() ?>resources/prod_images/<?= $product_details->img_name ?>" alt="" title="" class="img-responsive"/>
                                            </div>

                                            <div class="crt-order-frst-bx2">
                                                <div class="crt-order-frst-bx2-rw">
                                                    <div class="crt-order-frst-bx2-rw-left">
                                                        <h5>Title:</h5>
                                                    </div>

                                                    <div class="crt-order-frst-bx2-rw-right">
                                                        <p><?= $product_details->prod_title ?></p>
                                                    </div>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw">
                                                    <div class="crt-order-frst-bx2-rw-left">
                                                        <h5>Store Name:</h5>
                                                    </div>

                                                    <div class="crt-order-frst-bx2-rw-right">
                                                        <p><?= $product_details->store_name ?></p>
                                                    </div>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw">
                                                    <div class="crt-order-frst-bx2-rw-left">
                                                        <h5>Price:</h5>
                                                    </div>

                                                    <div class="crt-order-frst-bx2-rw-right">
                                                        <p>$<?= number_format($product->unit_prod_price, 2, '.', '') ?></p>
                                                    </div>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw">
                                                    <div class="crt-order-frst-bx2-rw-left">
                                                        <h5>Qty:</h5>
                                                    </div>

                                                    <div class="crt-order-frst-bx2-rw-right">
                                                        <p><?= $product->cart_prod_qnty ?></p>
                                                    </div>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw">
                                                    <div class="crt-order-frst-bx2-rw-left">
                                                        <h5>Shipping Type:</h5>
                                                    </div>

                                                    <div class="crt-order-frst-bx2-rw-right">
                                                        <span><?= $product_details->shipping?><?=(!empty($product_details->prod_shipping_methods))?' ('.$product_details->prod_shipping_methods.')':''?></span>
                                                    </div>
                                                </div>
                                                <?php if ($product_details->shipping == 'Offer free Shipping') { ?>
                                                    <div class="crt-order-frst-bx2-rw">
                                                        <div class="crt-order-frst-bx2-rw-left">
                                                            <h5>Ready to ship in:</h5>
                                                        </div>

                                                        <div class="crt-order-frst-bx2-rw-right">
                                                            <p><?= $product_details->free_ship_days ?> days.</p>
                                                        </div>
                                                    </div>
                                                <?php } elseif ($product_details->shipping == 'Charge for Shipping') { ?>
                                                    <div class="crt-order-frst-bx2-rw">
                                                        <div class="crt-order-frst-bx2-rw-left">
                                                            <h5>Ready to ship in:</h5>
                                                        </div>

                                                        <div class="crt-order-frst-bx2-rw-right">
                                                            <p><?= $product_details->ship_days ?> days.</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php
                                                $filters = json_decode($product->cart_details);
                                                if ($filters) {
                                                    foreach ($filters as $filter) {
                                                        ?>
                                                        <div class="crt-order-frst-bx2-rw">
                                                            <div class="crt-order-frst-bx2-rw-left">
                                                                <h5><?= $filter->filterName ?>:</h5>
                                                            </div>

                                                            <div class="crt-order-frst-bx2-rw-right">
                                                                <p><?= $filter->filtervalue ?></p>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>

                            <div class="crt-order-secnd">
                                <div class="crt-order-secnd-left">
                                    <div class="crt-order-secnd-left-hed">
                                        <h4>Shipping</h4>
                                        <a href="<?= base_url() ?>cart-shipping">Edit</a>
                                    </div>

                                    <div class="crt-order-secnd-left-btm">
                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Nickname:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_shipping_address->shipping_nick_name ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>First Name:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_shipping_address->shipping_first_name ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Last Name:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_shipping_address->shipping_last_name ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Telephone:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= (!empty($default_shipping_address->shipping_phone))?$default_shipping_address->shipping_phone:"N/A" ?></p>
                                            </div>
                                        </div>                                        

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Street:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_shipping_address->shipping_street ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>City:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_shipping_address->shipping_city ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>State:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p>
                                                    <?php
                                                    if ($default_shipping_address->shipping_country == 'US') {
                                                        echo get_state_name($default_shipping_address->shipping_state);
                                                    } else {
                                                        echo $default_shipping_address->shipping_state_other;
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Country:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= get_country_name($default_shipping_address->shipping_country); ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Zip:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_shipping_address->shipping_zip ?></p>
                                            </div>
                                        </div>                                        

                                    </div>
                                </div>

                                <div class="crt-order-secnd-left mrg-rgt-0">
                                    <div class="crt-order-secnd-left-hed">
                                        <h4>Billing</h4>
                                        <a href="<?= base_url() ?>cart-payment">Edit</a>
                                    </div>

                                    <div class="crt-order-secnd-left-btm">

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Nickname:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_billing_address->billing_nick_name ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>First Name:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_billing_address->billing_first_name ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Last Name:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_billing_address->billing_last_name ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Telephone:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= (!empty($default_billing_address->billing_phone))?$default_billing_address->billing_phone:"N/A" ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Street:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_billing_address->billing_street ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>City:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_billing_address->billing_city ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>State:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p>
                                                    <?php
                                                    if ($default_billing_address->billing_country == 'US') {
                                                        echo get_state_name($default_billing_address->billing_state);
                                                    } else {
                                                        echo $default_billing_address->billing_state_other;
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Country:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= get_country_name($default_billing_address->billing_country) ?></p>
                                            </div>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Zip:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $default_billing_address->billing_zip ?></p>
                                            </div>
                                        </div>
                                                                            

                                    </div>
                                </div>

                            </div>

                            <div class="crt-order-thrd">
                                <?php if ($this->session->userdata('payment_method') == 1) { ?>
                                    <div class="crt-order-secnd-left-hed">
                                        <h4>Payment Information</h4>
                                        <a href="<?= base_url() ?>cart-payment">Edit</a>
                                    </div>
                                <?php } ?>
                                <div class="crt-order-secnd-left">
                                    <div class="crt-order-secnd-left-btm">
                                        <?php if ($this->session->userdata('payment_method') == 1) { ?>
                                            <div class="crt-order-frst-bx2-rw">
                                                <div class="crt-order-frst-bx2-rw-left">
                                                    <h5>Name on Card:</h5>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw-right">
                                                    <p>Visa</p>
                                                </div>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw">
                                                <div class="crt-order-frst-bx2-rw-left">
                                                    <h5>Card Number:</h5>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw-right">
                                                    <p>123456789</p>
                                                </div>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw">
                                                <div class="crt-order-frst-bx2-rw-left">
                                                    <h5>Expiration Date:</h5>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw-right">
                                                    <p>23-10-2017</p>
                                                </div>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw">
                                                <div class="crt-order-frst-bx2-rw-left">
                                                    <h5>Security Code:</h5>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw-right">
                                                    <p>12345</p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="crt-order-frst-bx2-rw">
                                            <?php 
                                            if (filter_var($this->session->userdata('payPalURL')['payPalURL'], FILTER_VALIDATE_URL) === FALSE) {
                                                $buyData['payPalURL'] = "javascript:void"; ?>
                                                <div class="alert alert-danger"><?=$this->session->userdata('payPalURL')['payPalURL']?></div>
                                            <?php }else{
                                                $buyData = $this->session->userdata('payPalURL');
                                            }
                                            /*print_r($buyData);
                                            exit;*/
                                            ?>
                                            <a href="<?=$buyData['payPalURL']?>" id="confirm_order" class="crt-cnfrm-btn">CONFIRM ORDER</a>

                                            <!--<a href="https://sandbox.paypal.com/webapps/adaptivepayment/flow/expresscheckoutincontextremembermeflow?execution=e1s2&_eventId_submit" onClick="$('#paypalForm').submit();" class="paypal-btn">Pay Now</a>-->

                                            <!--<form action="https://sandbox.paypal.com/webapps/adaptivepayment/flow/pay" target="PPDGFrame" class="standard">-->
                                          <!--  <form action="<?= $buyData['payPalURL'] ?>" target="PPDGFrame" class="standard">
                                                <input type="button" id="submitBtn" value="CONFIRM ORDER" class="crt-cnfrm-btn">
                                                <input id="type" type="hidden" name="expType" value="mini">
                                                <input id="paykey" type="hidden" name="paykey" value="<?= $buyData['payKey'] ?>">      
                                            </form>

                                            <script type="text/javascript"> 
                                              var dgFlowMini;
                                               var returnFromPayPal = function(){
                                                   alert("Returned from PayPal");
                                                    //window.location.href = "<?= base_url('save-order') ?>";
                                                   // Here you would need to pass on the payKey to your server side handle to call the PaymentDetails API to make sure Payment has been successful or not
                                                  // based on the payment status- redirect to your success or cancel/failed urls
                                                }
                                              $(function(){
                                                dgFlowMini = new PAYPAL.apps.DGFlowMini({ trigger: 'submitBtn', callbackFunction: 'returnFromPayPal' });
                                              }); 
                                            </script> -->

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <?php //echo form_close();    ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="cart-right-main">
                        <div class="boxgrey-rp"></div><!-- Grey Cover -->
                        <div class="cart-right-hed">
                            <h6>Order Details</h6>
                        </div>
                        <div class="cart-right-lowr">
                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Subtotal
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($cart_sub_total, 2, '.', '') ?>
                                </div>
                            </div>

                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Discount
                                </div>
                                <div class="cart-right-rw-rgt">
                                    <?php //$discount_amount//number_format($discount_amount, 2, '.', '')   ?>
                                    <?= ($discount_amount != '') ? "$" . $discount_amount : "$0.00" ?>
                                </div>
                            </div>

                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Shipping Total
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                                </div>
                            </div>
                        </div>
                        <div class="cart-right-rw">
                            <div class="cart-right-rw-lft">
                                <h6>Total</h6>
                            </div>
                            <div class="cart-right-rw-rgt">
                                <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Cart main section ends here-->
<script>

    /* $(document).ready(function () { 
     $('#confirm_order').click(function () {
     $('#confirm_order_form').submit();
     });
     });*/
</script>
