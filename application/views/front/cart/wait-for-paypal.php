
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Thank you</title>

<meta name="Description" content="educki" />
<meta name="Keywords" content="educki" />
<meta name="author" content="Dotlogics" />

<!--header starts here-->
<?php $this->load->view("front/includes/js") ?>
<!--header ends here-->

<!-- Thankyou CSS Starts -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/thankyou.css" />
<!-- Thankyou CSS Ends -->



</head>

<body style="background-color: #fff !important">
	
<!-- Thankyou Structure Starts -->
<div class="thankyou-bg wait-pro">
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<span class="topicon"><div class="loading">
  <img class="loading-gif-2" src="<?=base_url()?>front_resources/images/preloader-2.gif" alt="preloader" />
</div>
</span> <!--Top Tick Icon -->
				<p>
					Please wait your order is being processed.
				</p>
				
			</td>
		</tr>
	</table>
</div>
<!-- Thankyou Structure Ends -->


</body>
<script>
	$(document).ready(function(){
		setTimeout(function(){
	        window.location.href = "<?=base_url('save-order')?>";
	    }, 10000);
	});
</script>
</html>