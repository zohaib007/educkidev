<!--Breadcrumbs starts here-->
<div class="container">
	<div class="row">
    	<div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
    		<div class="auto-container">
            	<div class="brd-sec">
                	<ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                      <li class="breadcrumb-item active"><?= $pagedata->pg_title; ?></li>
                    </ol>
                </div>	
                
                <div class="prvc-tit">
                	<h2><?= $pagedata->pg_title; ?></h2>
                </div>
                
                <div class="prvcy-txt"><?= clean_string($pagedata->pg_description); ?></div>
            </div>
    	</div>
    </div>

</div>
<!--Breadcrumbs ends  here-->