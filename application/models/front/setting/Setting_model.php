<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
  
    function hide_store(){
//        $store_hide = $this->input->post('store_hide');
        $data_save['store_is_hide'] = $this->input->post('store_hide');
        $user_id = $this->input->post('user_id');
        $response = $this->common_model->commonUpdate('tbl_stores', $data_save, 'user_id', $user_id);
        if ($response == NULL) {
            return 0;
        } else {
            return 1;
        }
    }
}

?>