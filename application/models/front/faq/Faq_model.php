<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Faq_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function view_list($page_id) {
        $where = "sub_pg_id = " .$page_id." AND sub_sub_pg_status = 1";
        $results = $this->common_model->commonselect_array('tbl_pages_sub_sub', $where ,'sub_sub_id')->result_array();
        return $results;
    }

    function view_search_list($page_id) {

        $query = "  SELECT sub_pages_sub.* FROM tbl_pages_sub pages_sub
                    INNER JOIN tbl_pages page ON
                    page.pg_id = pages_sub.sub_pg_id
                    INNER JOIN tbl_pages_sub_sub  sub_pages_sub
                    ON sub_pages_sub.sub_pg_id = pages_sub.sub_id
                    WHERE page.pg_id = " . $page_id . "
                    AND sub_pages_sub.sub_sub_pg_status = 1
                    AND (sub_pages_sub.sub_sub_pg_title LIKE '%" . $this->input->post('search') . "%'
                   OR sub_pages_sub.sub_sub_pg_description LIKE '%" . $this->input->post('search') . "%') ";
//        var_dump($query);
        $results = $this->db->query($query);
        
        return $results->result_array();
    }

    function view_questions($page_id,$search) {
        if($search!=''){
            $where = "sub_pg_id = " .$page_id." AND sub_sub_pg_status = 1 AND sub_sub_pg_title LIKE '%".$search."%' OR sub_sub_pg_description LIKE '%".$search."%' ";
        }else{
            $where = "sub_pg_id = " .$page_id." AND sub_sub_pg_status = 1 ";
        }
        $results = $this->common_model->commonselect_array('tbl_pages_sub_sub', $where ,'sub_sub_id')->result_array();
        return $results;
    }
}

?>