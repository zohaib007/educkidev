<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Blog_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function view_details($blog_url)
    {

        $where         = 'tbl_blog.blog_url = "' . $blog_url . '"';
        $jointable     = '';
        $joincondition = '';
        $jointype      = '';
        $results       = $this->career_applied_user_listing("tbl_blog", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, '', $order = 'ASC')->result_array();
        return $results;
    }

    public function career_applied_user_listing($tabel_name, $where = '', $jointable = '', $joincondition = '', $jointype = '', $page = 0, $per_page = 0, $is_count = 0, $orderby = '', $order = 'ASC')
    {

        $query = "SELECT *
                  FROM " . $tabel_name . "
                 ";
        if ($jointable != '') {

            $query .= $jointype . " JOIN " . $jointable . " ON " . $joincondition;
        }
        if ($where != '') {

            $query .= " WHERE " . $where;
        }

        if ($orderby != '') {
            $query .= " ORDER BY " . $orderby . " " . $order;
        }
        if ($is_count > 0) {
            $query .= " LIMIT " . (int) $page . ", " . (int) $per_page;
        }

        $result = $this->db->query($query);
        return $result;
    }

    public function get_blog_data($limit, $start)
    {
        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->join('tbl_blog_category', 'tbl_blog_category.blog_cat_id = tbl_blog.blog_category_id');
        $this->db->where("tbl_blog_category.blog_cat_status", 1);
        $this->db->where("blog_status", 1);
        $this->db->limit($limit, $start);
        $this->db->order_by("blog_id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function get_blog_count()
    {
        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->join('tbl_blog_category', 'tbl_blog_category.blog_cat_id = tbl_blog.blog_category_id');
        $this->db->where("tbl_blog_category.blog_cat_status", 1);
        $this->db->where("blog_status", 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function view_category_blog_details($blog_url)
    {
        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->join('tbl_blog_category', 'tbl_blog_category.blog_cat_id = tbl_blog.blog_category_id');
        $this->db->where("tbl_blog_category.blog_cat_status", 1);
        $this->db->where("tbl_blog_category.blog_cat_url", $blog_url);
        $this->db->where("tbl_blog.blog_status", 1);
        $this->db->order_by("tbl_blog.blog_id", "desc");
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_category_blog_data($blog_url, $limit, $start)
    {
        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->join('tbl_blog_category', 'tbl_blog_category.blog_cat_id = tbl_blog.blog_category_id');
        $this->db->where("tbl_blog_category.blog_cat_status", 1);
        $this->db->where("tbl_blog_category.blog_cat_url", $blog_url);
        $this->db->where("tbl_blog.blog_status", 1);
        $this->db->limit($limit, $start);
        $this->db->order_by("tbl_blog.blog_id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

}
