<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function view_product_detail($user_id) {
        $this->db->select('*');
        $this->db->from('tbl_products');
//        $this->db->join('tbl_product_images', 'tbl_products.prod_id = tbl_product_images.img_prod_id');
        $this->db->join('tbl_stores', 'tbl_stores.store_id = tbl_products.prod_store_id');
        $this->db->where('tbl_products.prod_user_id', $user_id);
        $this->db->where('tbl_products.prod_is_delete', '0');
//        $this->db->where('tbl_stores.store_is_hide', '0');
        $this->db->group_by('prod_id');
        $this->db->order_by("prod_id", "desc");
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_product_detail($user_id, $limit, $start) {
        $this->db->select('*');
        $this->db->from('tbl_products');
//        $this->db->join('tbl_product_images', 'tbl_products.prod_id = tbl_product_images.img_prod_id');
        $this->db->join('tbl_stores', 'tbl_stores.store_id = tbl_products.prod_store_id');
        $this->db->where('tbl_products.prod_user_id', $user_id);
        $this->db->where('tbl_products.prod_is_delete', '0');
//        $this->db->where('tbl_stores.store_is_hide', '0');
        $this->db->group_by('prod_id');
        $this->db->order_by("prod_id", "desc");
        $this->db->limit($limit, $start);
        $result = $this->db->get();
//                echo $this->db->last_query();die;
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function search_product($user_id, $limit, $start, $is_limit = 0, $search = '', $select_value='' ) {

        
      //  $user_id = $this->input->post('user_id');
        if($search == ''){
            $search = $this->input->post('search_value');
        }

        if($select_value == ''){
            $select_value = $this->input->post('select_search');
        }
        $this->db->select('*');
        $this->db->from('tbl_products');
//        $this->db->join('tbl_product_images', 'tbl_products.prod_id = tbl_product_images.img_prod_id');
        $this->db->join('tbl_stores', 'tbl_stores.store_id = tbl_products.prod_store_id');

        
        $this->db->where('tbl_products.prod_user_id', $user_id);
        $this->db->where('tbl_products.prod_is_delete', '0');
//        $this->db->where('tbl_stores.store_is_hide', '0');
        if ($select_value == '1') {
            $this->db->like('tbl_products.prod_id', $search);
//            $query = "AND tbl_products.prod_id LIKE '%" . $search . "%'";
        } elseif ($select_value == '2') {
            $this->db->like('LOWER(tbl_products.prod_title)', strtolower($search));
//            $query = "AND tbl_products.prod_title LIKE '%" . $search . "%'";
        } elseif ($select_value == '3') {
            $this->db->where('tbl_products.qty', $search);
//            $query = "AND tbl_products.qty LIKE '%" . $search . "%'";
        } elseif ($select_value == '4') {
            $this->db->where("CASE WHEN tbl_products.prod_onsale = 1 THEN tbl_products.sale_price = ".$search." ELSE tbl_products.prod_price = ".$search." END", NULL, FALSE);
            //$this->db->like('tbl_products.prod_price', $search);
//            $query = "AND tbl_products.prod_price LIKE '%" . $search . "%'";
        }
        $this->db->group_by('prod_id');
        $this->db->order_by("prod_id", "desc");
        if ($is_limit == 1) {
            if($start == 0){
                $this->db->limit( $limit,$start);
            }else{
                $this->db->limit( $start, $limit);
            }
        }

        $result = $this->db->get();
//                echo $this->db->last_query();die;

        /*echo "<pre>";
        print_r($result->result());
        echo $this->db->last_query();
        exit;*/
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function product_delete($slug) {
        $data['pagedata'] = $this->common_model->commonselect('tbl_products', 'prod_url', $slug)->row();
        return $data['pagedata']->prod_id;
        $this->image_delete($data['pagedata']->prod_id);
        $this->common_model->commonDelete('tbl_products', 'prod_url', $slug);
        return 1;
    }

    public function product_bluk_delete() {
        $id = $this->input->post('delete_value');
        $this->db->select('*');
        $this->db->from('tbl_products');
        $this->db->where_in('prod_id', $id);
        $result = $this->db->get();
        /*echo "<pre>";
        print_r($result->result());
        exit;*/
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                //$this->image_delete($row->prod_id);
                //$this->common_model->commonDelete('tbl_products', 'prod_url', $row->prod_url);
                $update_data['prod_is_delete'] = 1;
                $this->common_model->commonUpdate('tbl_products', $update_data, 'prod_url', $row->prod_url);
            }
        }
        return 1;
    }

    public function image_delete($id) {
        $select = "SELECT img_name FROM tbl_product_images WHERE img_prod_id=" . $id . "";
        $image = $this->db->query($select);

        $rootpath = base_url('resources/prod_images/');
        $rootpaththumb = base_url('resources/prod_images/thumb/');
        if ($image->num_rows() > 0) {
            foreach ($image->result() as $row) {
                $image = $row->img_name;
                $rootpath . $image;
                @unlink($rootpath . $image);
                @unlink($rootpaththumb . $image);
            }
            $this->db->where('img_prod_id', $id);
            $this->db->delete('tbl_product_images');
        }
    }

    public function product_download_csv() {
        
        $userid = $this->session->userdata('userid');
        
        /*
        $query = "SELECT 
                    prod_id,prod_title,prod_price,shipping,qty,prod_status 
                    FROM
                    tbl_products 
                    RIGHT JOIN tbl_product_images 
                    ON tbl_products.prod_id = tbl_product_images.img_prod_id 
                    AND tbl_products.prod_user_id = $userid
                    ORDER BY prod_id DESC";
        $query = $this->db->query($query);
        */
        $this->db->select('tbl_products.*');
        
        //$this->db->select('tbl_products.prod_id,tbl_products.prod_title,tbl_products.prod_price,tbl_products.shipping,tbl_products.qty,tbl_products.prod_status');
        $this->db->from('tbl_products');
//        $this->db->join('tbl_product_images', 'tbl_products.prod_id = tbl_product_images.img_prod_id');
        $this->db->join('tbl_stores', 'tbl_stores.store_id = tbl_products.prod_store_id');
        $this->db->where('tbl_products.prod_user_id', $userid);
        $this->db->where('tbl_products.prod_is_delete', '0');
//        $this->db->where('tbl_stores.store_is_hide', '0');
        $this->db->group_by('prod_id');
        $this->db->order_by("prod_id", "desc");
//        $this->db->limit($limit, $start);
        $result = $this->db->get();
        
        if ($result->num_rows() > 0) {
//            return $query->result_array();
            return $result->result_array();
        }
        return NULL;
    }

    public function product_bluk_copy() {
        $id = $this->input->post('copy_value');
        $this->db->select('*');
        $this->db->from('tbl_products');
//        $this->db->join('tbl_product_images', 'tbl_products.prod_id = tbl_product_images.img_prod_id');
        $this->db->where_in('prod_id', $id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $this->bluk_insert($row);
            }
        }
        return 1;
    }

    public function bluk_insert($row) {

        $product_info['prodData'] = $this->common_model->commonselect('tbl_products', 'prod_id', $row->prod_id)->row();
        if (count($product_info['prodData']) <= 0) {
            redirect(base_url('manage-products'));
            exit;
        }
        $prodData['looking_for'] = $product_info['prodData']->looking_for;
        $prodData['condition'] = $product_info['prodData']->condition;
        $prodData['qty'] = $product_info['prodData']->qty;
        $prodData['prod_title'] = $product_info['prodData']->prod_title;
        $prodData['prod_url'] = $this->common_model->create_slug($product_info['prodData']->prod_title, 'tbl_products', 'prod_url');
        $prodData['prod_sku'] = $product_info['prodData']->prod_sku;
        $prodData['prod_description'] = $product_info['prodData']->prod_description;
        $prodData['youtube_links'] = $product_info['prodData']->youtube_links;
        $prodData['prod_price'] = $product_info['prodData']->prod_price;
        $prodData['prod_onsale'] = $product_info['prodData']->prod_onsale;
        if ($prodData['prod_onsale'] == 1) {
            $prodData['sale_price'] = $product_info['prodData']->sale_price;
            $prodData['sale_start_date'] = $product_info['prodData']->sale_start_date;
            $prodData['sale_end_date'] = $product_info['prodData']->sale_end_date;
        }
        $prodData['shipping'] = $product_info['prodData']->shipping;
        if ($prodData['shipping'] == "Charge for Shipping") {
            $prodData['ship_price'] = $product_info['prodData']->ship_price;
            $prodData['ship_days'] = $product_info['prodData']->ship_days;
        } else if ($prodData['shipping'] == "Offer free Shipping") {
            $prodData['free_ship_days'] = $product_info['prodData']->free_ship_days;
        }
        $prodData['prod_status'] = 0;
        $prodData['prod_return'] = $product_info['prodData']->prod_return;
        $prodData['prod_cat_id'] = $product_info['prodData']->prod_cat_id;
        $prodData['prod_created_date'] = date('Y-m-d H:i:s');
        $prodData['prod_user_id'] = $product_info['prodData']->prod_user_id;
        $prodData['prod_store_id'] = $product_info['prodData']->prod_store_id;
        $prodData['mata_title'] = $product_info['prodData']->mata_title;
        $prodData['meta_keyword'] = $product_info['prodData']->meta_keyword;
        $prodData['meta_description'] = $product_info['prodData']->meta_description;

        // Insert Product Data
        $prod_id = $this->common_model->commonSave('tbl_products', $prodData);
        $category_info['prodCat'] = $this->common_model->commonselect('tbl_product_category', 'prod_id', $row->prod_id)->row();

        $catData['category_id'] = $category_info['prodCat']->category_id;
        $catData['sub_category_id'] = $category_info['prodCat']->sub_category_id;
        $catData['sub_sub_category_id'] = $category_info['prodCat']->sub_sub_category_id;
        $catData['prod_id'] = $prod_id;

        // Insert Categories
        $prod_cat_id = $this->common_model->commonSave('tbl_product_category', $catData);
        // Insert image Data
//        $data['prodImg'] = $this->common_model->commonselect('tbl_product_images', 'img_prod_id', $row->prod_id)->result();
//        foreach ($data['prodImg'] as $img) {
//            $dest_file_name = $prod_id . "_" . $img->img_name;
//            $image_to_copy = $_SERVER['DOCUMENT_ROOT'] . "/resources/prod_images/" .$img->img_name;
//            $thumb_image_to_copy = $_SERVER['DOCUMENT_ROOT'] . "/resources/prod_images/thumb/".$img->img_name;
//            $dest_file = $_SERVER['DOCUMENT_ROOT'] . "/resources/prod_images/";
//            $thumb_dest_file = $_SERVER['DOCUMENT_ROOT'] . "/resources/prod_images/thumb/";
//            /*var_dump($image_to_copy);
//            var_dump(file_exists($image_to_copy));
//            var_dump(is_file($image_to_copy));
//            die;*/
//            if (file_exists($image_to_copy)) {
//                if (is_file($image_to_copy)) {
//                    copy($image_to_copy, $dest_file . $dest_file_name);
//                    copy($thumb_image_to_copy, $thumb_dest_file . $dest_file_name);
//                }
//            }
//            $imgData['img_name'] = $dest_file_name;
//            $imgData['img_prod_id'] = $prod_id;
//            $this->common_model->commonSave('tbl_product_images', $imgData);
//        }
//        
        // Add unique filters
        $sub_sub_cat_id = $catData['sub_sub_category_id'];
        if ($sub_sub_cat_id != '') {
            $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
            foreach ($filters as $filter_row) {
                $filtersData['prod_id'] = $prod_id;
                $filtersData['category_id'] = $sub_sub_cat_id;
                $filtersData['filter_id'] = $filter_row->filter_title_id;
                $filtersData['filter_slug'] = $filter_row->filter_slug;
                $filtersData['filter_value'] = $filter_row->filter_slug;
                $filtersData['is_unique'] = 1;
                $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);
                // If this filter is conditional get the detail value and save it.
                $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_row->filter_slug)->row();
                if ($row->cat_filter_is_conditional == 1 && $this->input->post($filter_dtil->filter_slug) != '') {
                    $filtersDetailData['prod_filter_id'] = $filterId;
                    $filtersDetailData['prod_id'] = $prod_id;
                    $filtersDetailData['category_id'] = $sub_sub_cat_id;
                    $filtersDetailData['filter_id'] = $filter_row->filter_title_id;
                    $filtersDetailData['filter_detail_id'] = $filter_row->filter_slug;
                    $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                    $filtersDetailData['filter_value'] = $this->input->post($filter_dtil->filter_slug);
                    $filtersDetailData['filter_is_unique'] = 1;
                    $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                }
            }
        }
        return 1;
    }
    
    public function product_report(){
        
        $prod_id = $this->input->post('prodId');
        $product['productdata'] = $this->common_model->commonselect('tbl_products', 'prod_id', $prod_id)->row();
        $report_count = $product['productdata']->prod_report + 1;
        $update_data = array('prod_report' => $report_count);
        $this->common_model->commonUpdate('tbl_products', $update_data, 'prod_id', $prod_id);
        $history['flag_prod_id'] = $prod_id;
        $history['flag_date'] = date('Y-m-d H:i:s');
        $this->common_model->commonSave('tbl_item_flag_history', $history);
        $user['user_flagged_status'] = 1;
        $this->common_model->commonUpdate('tbl_user', $user,'user_id',$product['productdata']->prod_user_id);
        return 1;
    }

}

?>
