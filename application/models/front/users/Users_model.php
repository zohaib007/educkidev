<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function edit_profile() {
        $user_id = $this->input->post('user_id');
        $data_save['user_fname'] = $this->input->post('first_name');
        $data_save['user_lname'] = $this->input->post('last_name');
//        $data_save['user_email'] = $this->input->post('previous_email');
        $data_save['user_zip'] = $this->input->post('zip_code');
        $data_save['user_age'] = $this->input->post('user_age');
        $data_save['user_sex'] = $this->input->post('user_sex');
        $data_save['user_interests'] = $this->input->post('user_interests');
        $data_save['user_interests_second'] = $this->input->post('user_interests_second');
        $data_save['user_business_owner'] = $this->input->post('user_business_owner');
        $data_save['user_other_interest'] = ($this->input->post('user_interests') == "Other") ? $this->input->post('user_other_interest') : '';
        $data_save['user_other_interest_second'] = ($this->input->post('user_interests_second') == "Other") ? $this->input->post('user_other_interest_second') : '';
        $userImage = $_FILES['user_image'];
        if ($this->input->post('new_password') != '') {
            $data_save['user_password'] = $this->input->post('new_password');
            $data_save['user_password_encryp'] = md5($this->input->post('new_password'));
        }
        $userimage = '';
        $upload_path = FCPATH . 'resources/user_profile_image/';
        if ($userImage['name'] !== "") {
            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }
            $config = array(
                'allowed_types' => 'jpg|jpeg|png|txt',
                'upload_path' => $upload_path,
                'file_name' => 'user_' . date('Y_m_d_h_i_s'),
                'maintain_ratio' => false,
                'width' => 150,
                'height' => 110,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('user_image')) { //Print message if file is not uploaded
                $bannererror = array('profileimageerror' => $this->upload->display_errors(), 'content_view' => 'member/profile');
                $this->load->view("front/custom_template", $bannererror);
            } else {
                if (!file_exists($upload_path . 'thumb')) {
                    mkdir($upload_path . 'thumb', 0777, true);
                }
                $userdataDP = $this->upload->data();
                $userimage = $userdataDP['file_name'];
                $this->load->library('image_lib');
                $configt['image_library'] = 'gd2';
                $configt['source_image'] = $userdataDP['full_path'];
                $configt['new_image'] = FCPATH . "resources/user_profile_image/thumb/";
                $configt['maintain_ratio'] = false;
                $configt['width'] = 80;
                $configt['height'] = 80;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();
            }
            $data_save['user_profile_picture'] = $userimage;
        }
        else{
            $data_save['user_profile_picture'] = $this->input->post('user_image');
        }

        $this->common_model->commonUpdate('tbl_user', $data_save, 'user_id', $user_id);
        $email = $this->input->post('previous_email');
        require_once(ABSPATH.'wp-load.php');
        $userdata = get_user_by('email', $email); //get user wordpress Table id by email         
        $wp_data_save['ID'] = $userdata->ID; //user wordpress table id
        $wp_data_save['first_name'] = $this->input->post('first_name');
        $wp_data_save['last_name'] = $this->input->post('last_name');
        $wp_data_save['user_email'] = $this->input->post('previous_email');
        if ($userimage != '') {
            $wp_data_save['profile_image'] = $userimage;
        }
        if ($this->input->post('new_password')) {
            $wp_data_save['user_pass'] = $this->input->post('new_password');
            wp_set_password($wp_data_save['user_pass'], $wp_data_save['ID']);
        }
        wp_update_user($wp_data_save);  //wordpress user update function
        return TRUE;
    }

    function check_current_password() {
        $password = $this->input->post('password');
        $user_id = $this->input->post('user_id');
        $query = "SELECT 
                  user_password 
                  FROM
                  tbl_user 
                  WHERE user_password = '" . $password . "' 
                  AND user_id = " . $user_id;
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function change_password() {
        $data_save['user_password'] = $this->input->post('new_password');
        $data_save['user_password_encryp'] = md5($this->input->post('new_password'));
        $user_id = $this->input->post('user_id');
        $response = $this->common_model->commonUpdate('tbl_user', $data_save, 'user_id', $user_id);
        return $response;
    }

    public function get_prod_list($store_id, $sort = '', $to = 0, $from = '', $is_limit = 0,$price=0) {

        $price1 = 0;
        $price2 = 0;
        if($price!=0){
            $price1 = $price['price_range_one'];
            $price2 = $price['price_range_two'];

            $price_sql = " CASE WHEN prod.`prod_onsale` = 1 THEN prod.`sale_price` >= ".$price1." AND prod.`sale_price` <= ".$price2." ELSE prod.`prod_price` >= ".$price1." AND prod.`prod_price` <= ".$price2." END AND ";
        }else{
            $price_sql = "";
        }

        $query = "
                SELECT 
                prod.*, store.store_name, store.store_url, images.img_name, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
                FROM tbl_products prod
                INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                INNER JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
                INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id

                WHERE ".$price_sql."
                prod_is_delete = 0
                AND
                prod_status = 1
                AND
                prod.prod_store_id = " . $store_id . "
                GROUP BY prod.prod_id
                ORDER BY $sort
                ";
        #ORDER BY prod.prod_price ".$sort."

        if ($is_limit == 1) {
            $query .= " LIMIT " . $to . "," . $from;
        }

        $result = $this->db->query($query);
        return $result;
    }

    public function get_prod_list_app($store_id, $sort = '', $to = 0, $from = '',$is_limit, $range = '') {
        
        $range = explode(',',$range);
        
        $price1 = 0;
        $price2 = 0;
        
        if(count($range)>0){
            $price1 = $range[0];
            $price2 = $range[1];

            $price_sql = " CASE WHEN prod.`prod_onsale` = 1 THEN prod.`sale_price` >= ".$price1." AND prod.`sale_price` <= ".$price2." ELSE prod.`prod_price` >= ".$price1." AND prod.`prod_price` <= ".$price2." END AND ";
        }else{
            $price_sql = "";
        }
        
        $query = "
                SELECT 
                prod.*, store.store_name, store.store_url, images.img_name, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
                FROM tbl_products prod
                INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                INNER JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
                INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
                WHERE ".$price_sql." 
                prod_is_delete = 0 AND prod_status = 1 AND prod.prod_store_id = " . $store_id . "
                GROUP BY prod.prod_id 
                ORDER BY $sort
                ";
        //

        if ($is_limit == 1) {
            $query .= " LIMIT " . $to . "," . $from;
        }

        $result = $this->db->query($query);
        return $result;
    }

}

?>