<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Career_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->load->library('upload');
    }

    function view_details($career_url) {
        
        
        $where = 'career_is_deleted = 0 AND career_is_active = 1 AND tbl_career.career_url = "' . $career_url . '"';
        $jointable = 'tbl_states';
        $joincondition = 'tbl_states.stat_id = tbl_career.career_state';
        $jointype = 'inner';
        $results = $this->career_applied_user_listing("tbl_career", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, '', $order = 'ASC')->result_array();
        return $results;
    }

    function career_applied_user_listing($tabel_name, $where = '', $jointable = '', $joincondition = '', $jointype = '', $page = 0, $per_page = 0, $is_count = 0, $orderby = '', $order = 'ASC') {

        $query = "SELECT * 
                  FROM " . $tabel_name . "
                 ";
        if ($jointable != '') {

            $query .= $jointype . " JOIN " . $jointable . " ON " . $joincondition;
        }
        if ($where != '') {

            $query .= " WHERE " . $where;
        }

        if ($orderby != '') {
            $query .= " ORDER BY " . $orderby . " " . $order;
        }
        if ($is_count > 0) {
            $query .= " LIMIT " . (int) $page . ", " . (int) $per_page;
        }
        
        $result = $this->db->query($query);
        return $result;
    }

    function add_applied_user() {
        $data['career_id'] = $this->input->post('career_id');
        $data['career_user_first_name'] = $this->input->post('fname');
        $data['career_user_last_name'] = $this->input->post('lname');
        $data['career_user_email'] = $this->input->post('email');
        $data['career_user_phone'] = $this->input->post('phone');
        $data['career_user_website'] = $this->input->post('website');
        $data['career_user_linkedin_profile'] = $this->input->post('linkedin');
        $data['career_user_hear_from'] = $this->input->post('hear');
        $data['career_user_applied_date'] = date('Y-m-d h:i:s');
        $resumeFiles1 = $_FILES['resume'];
        $coverFiles1 = $_FILES['cover'];

        $resumefile = '';
        $coverfile = '';
        if ($resumeFiles1['name'] !== "") {
            $config = array(
                'allowed_types' => "docx|doc|pdf|ppt|txt",
                'upload_path' => FCPATH . 'resources/applied_user_career_images/resume/',
                'file_name' => 'resume_' . date('Y_m_d_h_i_s'),
            );

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('resume')) {
                echo "Not uploaded resume";
                print_r($this->upload->display_errors());
                exit;
            } else {
                $dataDP = $this->upload->data();
                $resumefile = $dataDP['file_name'];
            }
        }
        if ($coverFiles1['name'] !== "") {
            $confignew = array(
                'allowed_types' => 'doc|docx|ppt|txt|pdf',
                'upload_path' => FCPATH . 'resources/applied_user_career_images/cover/',
                'file_name' => 'cover_' . date('Y_m_d_h_i_s'),
            );
            $this->upload->initialize($confignew);
            if (!$this->upload->do_upload('cover')) {
            } else {
                $coverdataDP = $this->upload->data();
                $coverfile = $coverdataDP['file_name'];
            }
        }
        $data['career_user_resume'] = $resumefile;
        $data['career_user_cover'] = $coverfile;
        $career_id = $this->common_model->commonSave('tbl_careers_applied_user', $data);
        if ($career_id != '') {
            $career_data = $this->common_model->commonselect('tbl_career', 'career_id', $this->input->post('career_id'))->row();
            $name = $data['career_user_first_name'] . ' ' . $data['career_user_last_name'];
            $career_name = $career_data->career_title;
            $Resume_Link = base_url() . 'resources/applied_user_career_images/resume/' . $resumefile;
            $cover_Link = base_url() . 'resources/applied_user_career_images/cover/' . $coverfile;
            $email_data['subject'] = 'Applied User Details';
            $email_msg = '<b>' . $name . '</b> has applied for a post of <b>' . $career_name . '</b>.<br>';
            $email_msg .= '<b>Email : </b>' . $data['career_user_email'].'<br>';
            if($data['career_user_linkedin_profile']!=''){
                $email_msg .= '<b>Linkedin Profile :</b>' . $data['career_user_linkedin_profile'].'<br>';
            }
            else{
                $email_msg .= '<b>Linkedin Profile :</b> N/A'.'<br>';
            }
            if($data['career_user_website']!=''){
                $email_msg .= '<b>Website :</b>' . $data['career_user_website'].'<br>';
            }
            else{
                $email_msg .= '<b>Website :</b> N/A'.'<br>';
            }
            if($data['career_user_hear_from']!=''){
                $email_msg .= '<b>How did you hear about this job? : </b>' . $data['career_user_hear_from'].'<br>';
            }
            else{
                $email_msg .= '<b>How did you hear about this job? : </b> N/A'.'<br>';
            }
            if ($resumeFiles1['name'] !== ""){
                $email_msg .= '<b>Rescume Link : </b>' . $Resume_Link .'<br>';
            }
            if ($coverFiles1['name'] !== "") {
                $email_msg .= '<b>Cover Letter Link : </b>' . $cover_Link .'<br>';
            }
            send_email(get_social_media_links()->site_email,'noreply@educki.com',"New Applicant",$email_msg);
            
        }
        return $career_id;
    }

}

?>