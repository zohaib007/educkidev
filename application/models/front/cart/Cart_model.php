<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart_model extends CI_Model {

    // 0 : Guest
    // 1 : Customer

    public function __construct() {
        $this->load->database();
        if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
        if ( !empty($_POST) ) $_POST = array_stripslash($_POST);
    }

    public function addToCart($user_id) {
        $this->db->query(" INSERT INTO `tbl_cart`(`cart_user_id`) VALUES ('" . $user_id . "')");
        return $this->db->insert_id();
    }

    public function getCartId($user_id) {

        $this->db->select('cart_id');
        $this->db->where('cart_user_id', $user_id);
        $query = $this->db->get('tbl_cart');
        if ($query->num_rows() > 0) {
            return $query->row('cart_id');
        } else {
            return $this->addToCart($user_id);
        }
    }

    /** check product have inventory > 0 : return true or false * */
    public function getProduct($productId) {
        $this->db->select('qty');
        $this->db->where('prod_id', $productId);
        $this->db->where('prod_status', 1);
        $this->db->where('prod_is_delete', 0);

        $query = $this->db->get('tbl_products');

        return $query;
    }

    /** update Cart by adding product or increaseing quntity * */
    public function addProduct($cartId, $productId, $qty) {
        $filter_array = array();
        $filters = ($this->input->post('filter_data'));
        if (!is_numeric($qty) || $qty <= 0)
            $qty = 1;
       /*$this->db->select('cart_detail_id, cart_prod_id, cart_prod_qnty, cart_details');
       $this->db->where('cart_id', $cartId);
       $this->db->where('cart_prod_id', $productId);
       $this->db->where('cart_details', $filter_array);
       $query = $this->db->get('tbl_cart_detail');
       if ($query->num_rows() > 0) {
           $result = $query->row();
           $this->db->query("UPDATE `tbl_cart_detail` SET `cart_prod_qnty`='" . ($result->cart_prod_qnty + $qty) . "' WHERE cart_detail_id = '" . $result->cart_detail_id . "'");
       } else {
           $this->db->query("INSERT INTO `tbl_cart_detail`(`cart_id`, `cart_prod_id`, `cart_prod_qnty`) VALUES ('" . $cartId . "','" . $productId . "','" . $qty . "')");
       }*/
        if (count($this->input->post('filter_data')) > 0) {
            $data = array();
            foreach ($filters as $key => $filter) {
                $data[$key]['filterName'] = trim($filter['slug']);
                $data[$key]['filtervalue'] = trim($filter['value']);
            }
            $filter_array = json_encode($data);//JSON_UNESCAPED_SLASHES,JSON_HEX_APOS
        }

        
        $this->db->select('cart_detail_id, cart_prod_id, cart_prod_qnty, cart_details');
        $this->db->where('cart_id', $cartId);
        $this->db->where('cart_prod_id', $productId);
        if(count($filter_array) > 0){
            $this->db->where('cart_details', $filter_array);
        }
        $query = $this->db->get('tbl_cart_detail');
        
        $prod_data = $this->db->query("SELECT qty FROM tbl_products WHERE prod_id = ".$productId)->row();
        
        $cart_data = $this->db->query("SELECT SUM(cart_prod_qnty) AS totalCartQty FROM tbl_cart_detail WHERE cart_prod_id = ".$productId." AND cart_id = ".$cartId."")->row();
        /*echo '<pre>';
        print_r($prod_data);
        print_r((int)$cart_data->totalCartQty);
        exit;*/
        if($prod_data->qty > (int)$cart_data->totalCartQty){
            if ($query->num_rows() > 0) {
                $result = $query->row();
                
                // $this->db->query(" 
                //                 UPDATE tbl_cart_detail 
                //                 SET cart_prod_qnty = '".($result->cart_prod_qnty + $qty)."' 
                //                 WHERE cart_detail_id = '" . $result->cart_detail_id . "'
                //             ");

                $updateData['cart_prod_qnty'] = $result->cart_prod_qnty + $qty;
                $this->db->where('cart_detail_id', $result->cart_detail_id);
                $this->db->update('tbl_cart_detail', $updateData);

                return 2;
                //echo 2;
            }else{

                $insertData['cart_id'] = $cartId;
                $insertData['cart_prod_id'] = $productId;
                $insertData['cart_prod_qnty'] = $qty;
                $insertData['cart_details'] = is_array($filter_array)?json_decode($filter_array):$filter_array;
                $this->db->insert('tbl_cart_detail', $insertData);

                // $this->db->query("
                //     INSERT INTO `tbl_cart_detail` 
                //     (`cart_id`, `cart_prod_id`, `cart_prod_qnty`, `cart_details`) 
                //     VALUES ($cartId, $productId, $qty, $filter_array)
                //      ");
                return 1;
                //echo 1;
            }
        }else{
            return 0;
            //echo 0;
        }
        
    }

    /** get Cart Item  * */
    public function getCartItems($cartId, $lang) {
        /**
         * Code By Haseeb
         * Code For Display Item In Card.
         * Apr-11-2017 1:17 PM
         */
        return $this->db->query("
								SELECT 
								cd.quantity AS cardQty, cd.cart_id AS cardId, cd.id AS cardMainId,
                                0.00 as collectiveDiscount,0 as isFreeShipppingDiscount,
								p.Id AS mId,p.SortOrder AS mSortOrder,p.Sku AS mSku,p.Price AS mPrice,p.SalePrice AS mSalePrice,p.Inventory AS mInventory,p.PricePound AS mPricePound,p.PriceEuro AS mPriceEuro,p.SalePricePound AS mSalePricePound,p.SalePriceEuro AS mSalePriceEuro,p.IsVisible AS mIsVisible,p.InventoryWarningLevel AS mInventoryWarningLevel,p.Categories AS mCategories,p.IsVisible AS mIsVisible,p.IsFreeShipping AS mIsFreeShipping,p.IsPreorderOnly AS mIsPreorderOnly,p.PreorderMessage AS mPreorderMessage,p.Weight AS mWeight,p.ShippingWieght AS mShippingWieght,p.Height AS mHeight,p.Width AS mWidth,p.Depth AS mDepth,p.DollarStartDate AS mDollarStartDate,p.DollarEndDate AS mDollarEndDate,p.PoundStartDate AS mPoundStartDate,p.PoundEndDate AS mPoundEndDate,p.EuroStartDate AS mEuroStartDate,p.EuroEndDate AS mEuroEndDate,pp.*
								FROM tbl_cart_detail cd
								JOIN tbl_products_lang_content pp ON cd.product_id = pp.ProductId
								JOIN tbl_products_en p ON p.Id = pp.ProductId
								WHERE p.IsDeleted = 0
								AND p.IsVisible = 1
								AND pp.LangIso = '" . $lang . "'
								AND cd.cart_id = '" . $cartId . "'
								");
        //return false;
    }

    /** update cart item quantities if out of stock or less  * */
    public function updateCartItems($cartId, $lang) {
        $notifications = "";
        $name = array();
        $query = $this->db->query("
								SELECT 
								cd.quantity AS cartQty, cd.cart_id AS cartId, cd.id AS cartMainId,
								0.00 as collectiveDiscount,0 as isFreeShipppingDiscount,
                                p.Id AS mId,p.SortOrder AS mSortOrder,p.Sku AS mSku,p.Price AS mPrice,p.SalePrice AS mSalePrice,p.Inventory AS mInventory,p.PricePound AS mPricePound,p.PriceEuro AS mPriceEuro,p.SalePricePound AS mSalePricePound,p.SalePriceEuro AS mSalePriceEuro,p.IsVisible AS mIsVisible,p.InventoryWarningLevel AS mInventoryWarningLevel,p.Categories AS mCategories,p.IsVisible AS mIsVisible,p.IsFreeShipping AS mIsFreeShipping,p.IsPreorderOnly AS mIsPreorderOnly,p.PreorderMessage AS mPreorderMessage,p.Weight AS mWeight,p.ShippingWieght AS mShippingWieght,p.Height AS mHeight,p.Width AS mWidth,p.Depth AS mDepth,p.DollarStartDate AS mDollarStartDate,p.DollarEndDate AS mDollarEndDate,p.PoundStartDate AS mPoundStartDate,p.PoundEndDate AS mPoundEndDate,p.EuroStartDate AS mEuroStartDate,p.EuroEndDate AS mEuroEndDate,pp.*
								FROM tbl_cart_detail cd
								JOIN tbl_products_lang_content pp ON cd.product_id = pp.ProductId
								JOIN tbl_products_en p ON p.Id = pp.ProductId
								WHERE p.IsDeleted = 0
								AND p.IsVisible = 1
								AND pp.LangIso = '" . $lang . "'
								AND cd.cart_id = '" . $cartId . "'
								")->result();
        if ($query) {
            foreach ($query as $row) {
                if ($row->mInventory == 0) {
                    $this->db->query("DELETE FROM `tbl_cart_detail` WHERE id='" . $row->cartMainId . "' ");
                    $name[] = $row->Name;
                } elseif ($row->mInventory < $row->cartQty) {
                    $this->db->query("UPDATE `tbl_cart_detail` SET `quantity`='" . $row->mInventory . "' WHERE id='" . $row->cartMainId . "' ");
                }
            }
        }
        if ($name) {
            $notifications = implode(", ", $name) . " are Out Of Stock";
        }

        return $notifications;
    }

    public function updateCartInfo($cart_detail_id, $prodId, $qty) {
        /**
         * Code By Haseeb
         * Code For Model to Update Cart Info.
         */

        $this->db->query("
						UPDATE tbl_cart_detail
						SET cart_prod_qnty = $qty 
						WHERE cart_detail_id = $cart_detail_id 
						AND cart_prod_id = $prodId");
        return true;
    }

    public function removeCartItem($cardId, $prodId, $detail_id = '') {
        /**
         * Code By Haseeb
         * Code For Model to Remove Cart Item.
         */
        if ($detail_id != '') {
            $this->db->query("DELETE FROM tbl_cart_detail
			WHERE cart_id = " . $cardId . " 
			AND cart_prod_id = " . $prodId . " 
			AND cart_detail_id = " . $detail_id . "");
        } else {
            $this->db->query("DELETE FROM tbl_cart_detail
			WHERE cart_id = " . $cardId . " 
			AND cart_prod_id = " . $prodId . "");
        }
        return true;
    }

    public function removeCart($cardId) {
        /**
         * Code By Haseeb
         * Code For Model to Remove Cart Item.
         */
        $this->db->query("DELETE FROM tbl_cart_detail
            WHERE cart_id = " . $cardId);
        $this->db->query("DELETE FROM tbl_cart
            WHERE cart_id = " . $cardId);
         
        return true;
    }

    public function updateCartSession($section, $dbData) {
        switch ($section) {
            case "billing":
                $this->db->select('id');
                $this->db->where('cart_id', $dbData['cart_id']);
                $query = $this->db->get('tbl_cart_billing');
                if ($query->num_rows() > 0) {
                    $id = $query->row('id');
                    $this->db->where('id', $id);
                    $this->db->update('tbl_cart_billing', $dbData);
                    return true;
                } else {
                    $this->db->insert('tbl_cart_billing', $dbData);
                    return true;
                }
                break;

            case "shipping":
                unset($dbData["ship_same_as_bill"]);

                $this->db->select('id');
                $this->db->where('cart_id', $dbData['cart_id']);
                $query = $this->db->get(' tbl_cart_shipping');
                if ($query->num_rows() > 0) {
                    $id = $query->row('id');
                    $this->db->where('id', $id);
                    $this->db->update(' tbl_cart_shipping', $dbData);
                    return true;
                } else {
                    $this->db->insert(' tbl_cart_shipping', $dbData);
                    return true;
                }
                break;
            case "coupon":
                $dbData['CustomerId'] = $this->session->userdata('customerId');
                if (empty($dbData['CustomerId'])) {
                    $dbData['CustomerId'] = $_COOKIE['maca_userId'];
                }
                $this->db->select('Id');
                $this->db->where('cart_id', $dbData['cart_id']);
                $query = $this->db->get('tbl_cart_user_parameters');
                if ($query->num_rows() > 0) {
                    $id = $query->row('Id');
                    $this->db->where('Id', $id);
                    $this->db->update('tbl_cart_user_parameters', $dbData);
                    return true;
                } else {
                    $this->db->insert('tbl_cart_user_parameters', $dbData);
                    return true;
                }

                break;
        }
    }

    public function confirm_order($data) {

        $is_all_local = 1;
        foreach ($data['cart_product_details'] as $product) {
            $product_details = getproduct_details($product->cart_prod_id);
            if (!empty($product_details)) {
                if($product_details->shipping!='Local Pick Up'){
                    $is_all_local = 0;
                    break;
                }
            }
        }

        $cartId = $data['cartid'];

        $buyer_details = getBuyerInfoById($data['userid']);

        $order['buyer_id'] = $buyer_details->user_id;
        $order['buyer_name'] = $buyer_details->user_fname . ' ' . $buyer_details->user_lname;
        $order['buyer_email'] = $buyer_details->user_email;
        if($is_all_local==0){
            $order['order_status'] = 'Awaiting Tracking';
        }else{
            $order['order_status'] = 'Awaiting Pickup';
        }


        $order['paypal_order_id'] = $this->session->userdata('paypal_order_id');
        $order['paypalOrderDetail'] = $data['orderDetail'];
        
        $orderDetailDecode = json_decode($data['orderDetail']);
        $order['paypal_payment_id'] =  @$orderDetailDecode->payment_details->payment_id;

        // echo '<pre>';
        // print_r($orderDetailDecode);

        $captureArray = array();
        foreach($orderDetailDecode->purchase_units as $selerNo){
            $captureArray[$selerNo->payee->email] = $selerNo->payment_summary->captures[0]->id;
        }

        $order['order_date'] = date('Y-m-d h:i:s');
        $order['order_sub_total'] = $data['cart_sub_total'];
        $order['order_grand_total'] = $data['total_shipment_amount'];
        $order['order_shipping_price'] = $data['shipment_sub_total'];
        $order['order_discount'] = $data['discount_amount'];
        $order['order_billing_address'] = $data['default_billing_address'];
        $order['order_shipping_address'] = $data['default_shipping_address'];
        $order['order_total_commission'] = $data['order_total_commission'];
        $order['order_sellers_info'] = $data['order_seller_info'];
        $order['order_paypal_response'] = $data['order_paypal_response'];
        $order['order_payment_method'] = $this->session->userdata('payment_method');

        /*echo "<pre>";
        print_r($order);
        exit;*/

        $newArraySellerInfo = array();
        
        if($order['order_sub_total'] > 0) { 
            $order_id = $this->common_model->commonSave('tbl_orders', $order);
           
            if ($order_id != null) {
                $seller_total = getSellerTotal($cartId);

                foreach ($seller_total as $seller) {
                    
                    $dbData['order_id'] = $order_id;
                    $dbData['store_id'] = $seller['store_id'];
                    $dbData['store_shipping_total'] = $seller['store_shipping_total'];
                    $dbData['store_commission_total'] = $seller['store_commission_total'];
                    $dbData['store_discount_price'] = $seller['store_discount_price'];
                    $dbData['store_total'] = $seller['store_total'];
                    $dbData['store_name'] = $seller['store_name'];
                    $dbData['store_email'] = $seller['store_email'];
                    $dbData['order_date'] = date('Y-m-d h:i:s');
                    $dbData['store_tier'] = $seller['store_tier_id'];

                    $dbData['paypal_capture_id'] = $captureArray[$seller['store_email']];
                    if($dbData['paypal_capture_id'] != ''){
                        $captureOrder = $this->paypal->CaptureOrder($this->session->userdata('token_type'), $this->session->userdata('access_token'), @$captureArray[$seller['store_email']]);
                        $dbData['paypalCaptureDetail'] = $captureOrder;
                    }

                    $infoId = $this->common_model->commonSave('tbl_order_seller_info', $dbData);
                    send_email(get_email($seller['prod_user_id']), "noreply@educki.com",'New Order placed', 'A new order is placed,Please login and review the order.');
                    $newArraySellerInfo[$seller['store_id']] = $infoId;

                }

                foreach ($data['cart_product_details'] as $product) {
                    
                    $product_details = getproduct_details($product->cart_prod_id);
                    if (!empty($product_details)) {                        
                        $seller_details = $this->common_model->getSellerInfoById($product_details->prod_user_id);
                        $order_products['order_id'] = $order_id;
                        $order_products['order_prod_id'] = $product_details->prod_id;
                        $order_products['order_prod_description'] = $product_details->prod_description;
                        $order_products['order_prod_name'] = $product_details->prod_title;
                        $order_products['order_prod_url'] = $product_details->prod_url;
                        $order_products['order_prod_image'] = $product_details->img_name;
                        $order_products['order_prod_qty'] = $product->cart_prod_qnty;
                        $order_products['order_prod_unit_price'] = $product->unit_prod_price;
                        $order_products['order_prod_total_price'] = $product->total_prod_price;
                        $order_products['seller_id'] = $seller_details->user_id;
                        $order_products['seller_name'] = $seller_details->user_fname . ' ' . $seller_details->user_lname;
                        $order_products['seller_email'] = $seller_details->user_email;
                        $order_products['order_prod_store_id'] = $product_details->prod_store_id;
                        $order_products['order_seller_info_id'] = $newArraySellerInfo[$product_details->prod_store_id];
                        $order_products['order_prod_store_name'] = $product_details->store_name;
                        $order_products['order_prod_looking_for'] = $product_details->looking_for;
                        $order_products['order_prod_onsale'] = $product_details->prod_onsale;
                        $order_products['order_prod_sale_price'] = $product_details->sale_price;
                        $order_products['order_prod_shipping'] = $product_details->shipping;
                        $order_products['order_prod_ship_price'] = $product_details->ship_price;
                        $order_products['order_prod_ship_days'] = $product_details->ship_days;
                        $order_products['order_prod_free_ship_days'] = $product_details->free_ship_days;
                        $order_products['order_prod_return'] = $product_details->prod_return;
                        $order_products['order_prod_cat_id'] = $product_details->prod_cat_id;
                        $order_products['order_prod_shipping_methods'] = $product_details->prod_shipping_methods;
                        if($product_details->shipping!='Local Pick Up'){
                            $order_products['order_prod_status'] = 'Awaiting Tracking';
                        }else{
                            $order_products['order_prod_status'] = 'Awaiting Pickup';
                        }
                        $order_products['order_prod_filters'] = $product->cart_details;
                        
                        $this->common_model->commonSave('tbl_order_products', $order_products);
                        $productData = $this->common_model->commonselect('tbl_products', 'prod_id', $product_details->prod_id)->row();
                        $qty = 0;
                        if ($productData->qty > 0) {
                            $qty = $productData->qty - $product->cart_prod_qnty;
                        }

                        $update_data['qty'] = $qty;
                        if($qty <= 0){
                            $update_data['prod_status'] = 0;
                        }
                        $this->common_model->commonUpdate('tbl_products', $update_data, 'prod_id', $product->cart_prod_id);
                    }
                }
                
                return $order_id;
            } else {
                return  0;
            }
        }
    }

    public function confirm_order_app($data) {
        $is_all_local = 1;
        foreach ($data['cart_product_details'] as $product) {
            $product_details = getproduct_details($product->cart_prod_id);
            if (!empty($product_details)) {
                if($product_details->shipping!='Local Pick Up'){
                    $is_all_local = 0;
                    break;
                }
            }
        }

        $cartId = $data['cartid'];
        $buyer_details = $this->common_model->getSellerInfoById($data['userid']);
        $order['buyer_id'] = $buyer_details->user_id;
        $order['buyer_name'] = $buyer_details->user_fname . ' ' . $buyer_details->user_lname;
        $order['buyer_email'] = $buyer_details->user_email;
        if($is_all_local==0){
            $order['order_status'] = 'Awaiting Tracking';
        }else{
            $order['order_status'] = 'Awaiting Pickup';
        }


        $order['paypal_order_id'] = $data['paypal_order_id'];
        $order['paypalOrderDetail'] = $data['orderDetail'];
        
        $orderDetailDecode = json_decode($data['orderDetail']);
        $order['paypal_payment_id'] =  @$orderDetailDecode->payment_details->payment_id;

        // echo '<pre>';
        // print_r($orderDetailDecode);

        $captureArray = array();
        foreach($orderDetailDecode->purchase_units as $selerNo){
            $captureArray[$selerNo->payee->email] = $selerNo->payment_summary->captures[0]->id;
        }

        $order['order_date'] = date('Y-m-d h:i:s');
        $order['order_sub_total'] = $data['cart_sub_total'];
        $order['order_grand_total'] = $data['total_shipment_amount'];
        $order['order_shipping_price'] = $data['shipment_sub_total'];
        $order['order_discount'] = $data['discount_amount'];
        $order['order_billing_address'] = $data['default_billing_address'];
        $order['order_shipping_address'] = $data['default_shipping_address'];
        $order['order_total_commission'] = $data['order_total_commission'];
        $order['order_sellers_info'] = $data['order_seller_info'];
        $order['order_paypal_response'] = $data['order_paypal_response'];
        $order['order_payment_method'] = 2;
        $newArraySellerInfo = array();
        
        if($order['order_sub_total'] > 0) { 
            $order_id = $this->common_model->commonSave('tbl_orders', $order);
           
            if ($order_id != null) {
                $seller_total = getSellerTotal($cartId);

                foreach ($seller_total as $seller) {
                    
                    $dbData['order_id'] = $order_id;
                    $dbData['store_id'] = $seller['store_id'];
                    $dbData['store_shipping_total'] = $seller['store_shipping_total'];
                    $dbData['store_commission_total'] = $seller['store_commission_total'];
                    $dbData['store_discount_price'] = $seller['store_discount_price'];
                    $dbData['store_total'] = $seller['store_total'];
                    $dbData['store_name'] = $seller['store_name'];
                    $dbData['store_email'] = $seller['store_email'];
                    $dbData['order_date'] = date('Y-m-d h:i:s');
                    $dbData['store_tier'] = $seller['store_tier_id'];

                    $dbData['paypal_capture_id'] = $captureArray[$seller['store_email']];
                    if($dbData['paypal_capture_id'] != ''){
                        $var = $this->paypal->GetToken();
                        $var = json_decode($var);
                        $captureOrder = $this->paypal->CaptureOrder($var->token_type, $var->access_token, @$captureArray[$seller['store_email']]);
                        $dbData['paypalCaptureDetail'] = $captureOrder;
                    }

                    $infoId = $this->common_model->commonSave('tbl_order_seller_info', $dbData);
                    send_email(get_email($seller['prod_user_id']), "noreply@educki.com",'New Order placed', 'A new order is placed,Please login and review the order.');
                    $newArraySellerInfo[$seller['store_id']] = $infoId;

                }

                foreach ($data['cart_product_details'] as $product) {
                    
                    $product_details = getproduct_details($product->cart_prod_id);
                    if (!empty($product_details)) {                        
                        $seller_details = $this->common_model->getSellerInfoById($product_details->prod_user_id);
                        $order_products['order_id'] = $order_id;
                        $order_products['order_prod_id'] = $product_details->prod_id;
                        $order_products['order_prod_description'] = $product_details->prod_description;
                        $order_products['order_prod_name'] = $product_details->prod_title;
                        $order_products['order_prod_url'] = $product_details->prod_url;
                        $order_products['order_prod_image'] = $product_details->img_name;
                        $order_products['order_prod_qty'] = $product->cart_prod_qnty;
                        $order_products['order_prod_unit_price'] = $product->unit_prod_price;
                        $order_products['order_prod_total_price'] = $product->total_prod_price;
                        $order_products['seller_id'] = $seller_details->user_id;
                        $order_products['seller_name'] = $seller_details->user_fname . ' ' . $seller_details->user_lname;
                        $order_products['seller_email'] = $seller_details->user_email;
                        $order_products['order_prod_store_id'] = $product_details->prod_store_id;
                        $order_products['order_seller_info_id'] = $newArraySellerInfo[$product_details->prod_store_id];
                        $order_products['order_prod_store_name'] = $product_details->store_name;
                        $order_products['order_prod_looking_for'] = $product_details->looking_for;
                        $order_products['order_prod_onsale'] = $product_details->prod_onsale;
                        $order_products['order_prod_sale_price'] = $product_details->sale_price;
                        $order_products['order_prod_shipping'] = $product_details->shipping;
                        $order_products['order_prod_ship_price'] = $product_details->ship_price;
                        $order_products['order_prod_ship_days'] = $product_details->ship_days;
                        $order_products['order_prod_free_ship_days'] = $product_details->free_ship_days;
                        $order_products['order_prod_return'] = $product_details->prod_return;
                        $order_products['order_prod_cat_id'] = $product_details->prod_cat_id;
                        $order_products['order_prod_shipping_methods'] = $product_details->prod_shipping_methods;
                        if($product_details->shipping!='Local Pick Up'){
                            $order_products['order_prod_status'] = 'Awaiting Tracking';
                        }else{
                            $order_products['order_prod_status'] = 'Awaiting Pickup';
                        }
                        $order_products['order_prod_filters'] = $product->cart_details;
                        
                        $this->common_model->commonSave('tbl_order_products', $order_products);
                        $productData = $this->common_model->commonselect('tbl_products', 'prod_id', $product_details->prod_id)->row();
                        $qty = 0;
                        if ($productData->qty > 0) {
                            $qty = $productData->qty - $product->cart_prod_qnty;
                        }

                        $update_data['qty'] = $qty;
                        if($qty <= 0){
                            $update_data['prod_status'] = 0;
                        }
                        $this->common_model->commonUpdate('tbl_products', $update_data, 'prod_id', $product->cart_prod_id);
                    }
                }
                
                return $order_id;
            } else {
                return  0;
            }
        }
    }
    
    public function addreorderProduct($cartId, $prod_id, $qty, $filters) {

        $filter_array = array();
        if (!is_numeric($qty) || $qty <= 0)
             $qty = 1;
       
        $this->db->select('cart_detail_id, cart_prod_id, cart_prod_qnty, cart_details');
        $this->db->where('cart_id', $cartId);
        $this->db->where('cart_prod_id', $prod_id);
        if($filters != ''){
            $filters = stripslashes($filters);
            $this->db->where('cart_details', $filters);
        }
        $query = $this->db->get('tbl_cart_detail');
        
        // $prod_data = $this->db->query("SELECT qty FROM tbl_products WHERE prod_id = $prod_id")->row();
        // $cart_data = $this->db->query("SELECT SUM(cart_prod_qnty) AS totalCartQty FROM tbl_cart_detail WHERE cart_prod_id = $prod_id AND cart_id = $cartId")->row();

        // if($prod_data->qty < $cart_data->totalCartQty){

            if ($query->num_rows() > 0) {
                $result = $query->row();
                
                $this->db->query(" 
                                UPDATE tbl_cart_detail 
                                SET cart_prod_qnty = '".$qty."' 
                                WHERE cart_detail_id = '" . $result->cart_detail_id . "'
                            ");
                return 2;
            }else{
                $insData['cart_id'] = $cartId;
                $insData['cart_prod_id'] = (int)$prod_id;
                //$insData['cprod_id'] = (int)$prod_id;

                $insData['cart_prod_qnty'] = (int)$qty;
                $insData['cart_details'] = stripslashes($filters);

                // var_dump($insData);die;

                $this->db->insert('tbl_cart_detail', $insData);

                // die('success');


                // $this->db->query("
                //     INSERT INTO `tbl_cart_detail`(`cart_id`, `cart_prod_id`, `cart_prod_qnty`, `cart_details`) 
                //     VALUES ('" . $cartId . "','" . $cart_prod_id . "','" . $cart_prod_qnty . "','" . $filters . "')");
            return 1;
            }
        // }else{
        //     return 0;
        // }

        /*
        $filter_array = array();
        if (!is_numeric($qty) || $qty <= 0)
            $qty = 1;
        $this->db->query("INSERT INTO `tbl_cart_detail`(`cart_id`, `cart_prod_id`, `cart_prod_qnty`, `cart_details`) VALUES ('" . $cartId . "','" . $productId . "','" . $qty . "','" . $filters . "')");
        echo $this->db->last_query();
        return 1;*/
    }
}
