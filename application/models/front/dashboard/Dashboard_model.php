<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function product_views($user_id) {
        $previous_week = strtotime("-1 week +1 day");
        $start_week    = strtotime("last sunday midnight", $previous_week);
        $end_week      = strtotime("next saturday", $start_week);
        $start_week    = date("Y-m-d", $start_week);
        $end_week      = date("Y-m-d", $end_week);

        $data['today'] =  $this->db->query("select count(*) as today from tbl_product_views where DATE(view_date) = '".date("Y-m-d")."' AND product_user_id = ".$user_id."")->row();

        $data['last_week_views'] =  $this->db->query("select count(*) as last_week from tbl_product_views where view_date <= '".$end_week."' 
                AND view_date >= '".$start_week."' AND product_user_id = ".$user_id."")->row();

        $data['last_month_views'] =  $this->db->query("select count(*) as last_month from tbl_product_views where view_date <= '".date("Y-n-j", strtotime("last day of previous month"))."' 
                AND view_date >= '".date("Y-n-j", strtotime("first day of previous month"))."' AND product_user_id = ".$user_id.""
                )->row();

        $data['last_year_views'] =  $this->db->query("select count(*) as last_year from tbl_product_views where view_date <= '".date("Y-n-j", strtotime("last year December 31st"))."' 
                AND view_date >= '".date("Y-n-j", strtotime("last year January 1st"))."' AND product_user_id = ".$user_id.""
                )->row();

        return $data;
    }

    public function most_view_product($user_id) {
        $this->db->select('COUNT(views.product_id) AS prod_occurrence,  prod.prod_title, prod.prod_url,
                            prod.prod_onsale,
                            prod.sale_price,views.product_id,
                            prod.prod_price,prod.prod_description');
        $this->db->from('tbl_product_views views ');
        $this->db->join('tbl_products prod', 'prod.prod_id = views.product_id');
        //$this->db->join('tbl_product_images images', 'images.img_prod_id = prod.prod_id');
        $this->db->where('views.product_store_id', $user_id);
        $this->db->where('prod.prod_status', '1');
        $this->db->where('prod.prod_is_delete', '0');
        $this->db->group_by('views.product_id');
        $this->db->order_by("prod_occurrence", "desc");
        $this->db->limit('5');
        $result = $this->db->get();
        //echo $this->db->last_query();die;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function orders($store_id) {
        $query = "SELECT 
                COUNT(DISTINCT order_prod.order_id) AS today_order
              FROM
                tbl_order_products order_prod
                JOIN tbl_orders
                  ON order_prod.order_id = tbl_orders.order_id
              WHERE order_prod.order_prod_store_id = " . $store_id . " 
              AND DATE(tbl_orders.order_date) = CURDATE()";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function get_user_orders($store_id,$order_status, $limit, $start) {
        $this->db->select('COUNT(tbl_order_products.`order_prod_qty`) AS order_qty,tbl_order_products.*,tbl_orders.*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        //$this->db->where('tbl_orders.traking_number', NULL);
        //$this->db->where('tbl_orders.order_status', $order_status);
        $this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by("tbl_orders.order_id", "desc");
        $this->db->limit($limit, $start);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
    }

    public function get_total_sales($store_id){
        return $this->db->query("SELECT SUM(o.`order_grand_total`) AS today_total_sale FROM tbl_orders o
            INNER JOIN tbl_order_products op ON o.`order_id` = op.`order_id`
            WHERE op.`order_prod_store_id` = ".$store_id." AND o.`order_date` >= '".date('Y-m-d 00:00:00')."' AND o.`order_date` <= '".date('Y-m-d 23:59:59')."'")->row('today_total_sale');

    }


    public function get_order_stats($store_id){
        return $this->db->query("SELECT 
            COUNT(DISTINCT o.`order_id`) AS total_orders,
            COUNT(DISTINCT 
            CASE
             WHEN o.`order_status` = 'completed' THEN o.`order_id`
            END
            ) AS completed,
            COUNT(DISTINCT 
            CASE
             WHEN o.`order_status` = 'Awaiting Tracking' THEN o.`order_id`
            END
            ) AS pending,
            COUNT(DISTINCT 
            CASE
             WHEN o.`order_status` = 'In Process'  || o.`order_status` = 'Awaiting Pickup' THEN o.`order_id`
            END
            ) AS processing,
            COUNT(DISTINCT 
            CASE
             WHEN o.`order_status` = 'cancel'  THEN o.`order_id`
            END
            ) AS cancel
            FROM tbl_order_products op
            INNER JOIN tbl_orders o ON op.`order_id` =  o.`order_id`
            WHERE op.`order_prod_store_id` = ".$store_id."")->row();
    }


    public function get_sales_per_month($store_id){
        return $this->db->query("SELECT (SUM(os.`store_total`) - SUM(os.`store_shipping_total`)) AS today_total_sale, MONTH(o.`order_date`) AS sale_month 
            FROM tbl_orders o
            INNER JOIN tbl_order_seller_info os ON os.order_id = o.order_id
            INNER JOIN tbl_order_products op ON o.`order_id` = op.`order_id`
            WHERE o.`order_status` = 'completed' AND op.`order_prod_store_id` = ".$store_id." AND YEAR(o.`order_date`) = '".date('Y')."' 
            GROUP BY MONTH(o.`order_date`)
            ORDER BY MONTH(o.`order_date`) ASC")->result();         
    }


    public function get_store_profit($store_id){
        $previous_week = strtotime("-1 week +1 day");
        $start_week    = strtotime("last sunday midnight", $previous_week);
        $end_week      = strtotime("next saturday", $start_week);
        $start_week    = date("Y-m-d", $start_week);
        $end_week      = date("Y-m-d", $end_week);
        $data['last_week'] =  $this->db->query("select SUM(store_total) as last_week from tbl_order_seller_info where order_date >= '".$start_week."' 
                AND order_date <= '".$end_week."' AND store_id = ".$store_id.""
                )->row();

        $data['last_month'] =  $this->db->query("select SUM(store_total) as last_month from tbl_order_seller_info where order_date <= '".date("Y-n-j", strtotime("last day of previous month"))."' 
                AND order_date >= '".date("Y-n-j", strtotime("first day of previous month"))."' AND store_id = ".$store_id.""
                )->row();

        $data['last_year'] =  $this->db->query("select SUM(store_total) as last_year from tbl_order_seller_info where order_date <= '".date("Y-n-j", strtotime("last year December 31st"))."' 
                AND order_date >= '".date("Y-n-j", strtotime("last year January 1st"))."' AND store_id = ".$store_id.""
                )->row();

            return $data;
    }

}

?>