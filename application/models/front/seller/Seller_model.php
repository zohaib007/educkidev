<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Seller_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function check_availability($store_name) {
        $where = "store_name = '" . $store_name . "'";
        $results = $this->common_model->commonselect_array('tbl_stores', $where, 'store_id')->row();
        if ($results == NULL) {
            return 0;
        } else {
            return 1;
        }
    }

    function create_store() {
        $tier_id = $this->input->post('tier');
        $user_id = $this->input->post('user_id');
        $tier['tier_data'] = $this->common_model->commonSelect('tbl_tier_list', 'tier_id', $tier_id)->row();
        $end_date = '';
        $time = strtotime(date("Y-m-d h:i:s"));
        if ($tier_id == 1) {
            $end_date = date('Y-m-d h:i:s');
            $subscription_data['sub_free_tier'] = 'Y';
        } elseif ($tier_id == 2) {
            $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
        } elseif ($tier_id == 3) {
            $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
        } else {
            $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
        }
        $data['store_name'] = $this->input->post('store_name');
        $data['store_url'] = $this->common_model->create_slug($this->input->post('store_name'), 'tbl_stores','store_url');
        $data['store_describe'] = $this->input->post('describe');
        $data['store_tier_id'] = $this->input->post('tier');
        $data['store_paypal_id'] = $this->input->post('paypal_id');
        $data['user_id'] = $this->input->post('user_id');
        $data['store_created_date'] = date('Y-m-d h:i:s');
        $data['store_end_date'] = $end_date;
        $store_id = $this->common_model->commonSave('tbl_stores', $data);
        $user_data['user_type'] = 'Seller';
        $this->common_model->commonUpdate('tbl_user', $user_data, 'user_id', $user_id);
        $_SESSION['usertype'] = $user_data['user_type'];
        $subscription_data['sub_tier_id'] = $tier_id;
        $subscription_data['sub_store_id'] = $store_id;
        $subscription_data['sub_start'] = date('Y-m-d h:i:s');
        $subscription_data['sub_ends'] = $end_date;
        $subscription_data['next_payment'] = date('d', strtotime('+1 months'));
        if(date('d')!=$subscription_data['next_payment']){
            $subscription_data['next_payment'] = date('Y-m-d h:i:s', strtotime('last day of next month'));
        }else{
            $subscription_data['next_payment'] = date('Y-m-d h:i:s', strtotime('+1 months'));
        }
        $subscription_data['tier_price'] = $tier['tier_data']->tier_amount;
        $subscription_id = $this->common_model->commonSave('tbl_user_store_subscription', $subscription_data);
        if ($store_id != '' && $subscription_id != '')
            return $store_id;
    }

    function myaccount() {
        $store_id = $this->input->post('store_id');
        $data['store_address'] = $this->input->post('store_address');
        $data['store_city'] = $this->input->post('store_city');
        $data['store_state'] = ($this->input->post('store_country') == "US" ? $this->input->post('store_state') : $this->input->post('other_state'));
        /*$data['store_state_other'] = ($this->input->post('store_country') != "US" ? $this->input->post('other_state') : "");*/
        $data['store_zip'] = $this->input->post('store_zip');
        $data['store_country'] = $this->input->post('store_country');
        $data['store_number'] = $this->input->post('store_number');
        $data['store_description'] = $this->input->post('store_description');
        $data['store_facebook_url'] = $this->input->post('store_facebook_url');
        $data['store_twitter_url'] = $this->input->post('store_twitter_url');
        $data['store_instagram_url'] = $this->input->post('store_instagram_url');
        $data['store_pintrest_url'] = $this->input->post('store_pintrest_url');
        $data['store_longitude'] = ($this->input->post('store_longitude') ? $this->input->post('store_longitude') : "");
        $data['store_latitude'] = ($this->input->post('store_latitude') ? $this->input->post('store_latitude') : "");
        $bannerFiles1 = $_FILES['store_banner_img'];
        $logoFiles1 = $_FILES['logo_img'];
        $logoimage = '';
        $bannerimage = '';
        if ($bannerFiles1['name'] !== "") {
            $config = array(
                'allowed_types' => 'jpg|jpeg|png|txt',
                'upload_path' => FCPATH . 'resources/seller_account_image/banner/',
                'file_name' => 'banner_' . date('Y_m_d_h_i_s'),
                'maintain_ratio' => false,
                'width' => 1100,
                'height' => 234,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('store_banner_img')) { //Print message if file is not uploaded
                $bannererror = array('bannererror' => $this->upload->display_errors(), 'content_view' => 'seller/my-account');
                $this->load->view("front/custom_template", $bannererror);
            } else {
                $bannerdataDP = $this->upload->data();
                $bannerimage = $bannerdataDP['file_name'];
                $this->load->library('image_lib');
                $configt['image_library'] = 'gd2';
                $configt['source_image'] = $bannerdataDP['full_path'];
                $configt['new_image'] = FCPATH . "resources/seller_account_image/banner/thumb/";
                $configt['maintain_ratio'] = false;
                $configt['width'] = 80;
                $configt['height'] = 80;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();
            }
        }
        if ($logoFiles1['name'] !== "") {

            $confignew = array(
                'allowed_types' => 'jpg|jpeg|png|txt',
                'upload_path' => FCPATH . 'resources/seller_account_image/logo/',
                'file_name' => 'logo_' . date('Y_m_d_h_i_s'),
                'maintain_ratio' => false,
                'width' => 150,
                'height' => 110,
            );
            $this->load->library('upload', $config);
            $this->upload->initialize($confignew);
            if (!$this->upload->do_upload('logo_img')) { //Print message if file is not uploaded
                $logoerror = array('logoerror' => $this->upload->display_errors(), 'content_view' => 'seller/my-account');
                $this->load->view("front/custom_template", $logoerror);
            } else {
                $logodataDP = $this->upload->data();
                $logoimage = $logodataDP['file_name'];
                $this->load->library('image_lib');
                $configt['image_library'] = 'gd2';
                $configt['source_image'] = $logodataDP['full_path'];
                $configt['new_image'] = FCPATH . "resources/seller_account_image/logo/thumb/";
                $configt['maintain_ratio'] = false;
                $configt['width'] = 80;
                $configt['height'] = 80;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();
            }
        }

        $data['store_logo_image'] = ($logoimage == '' ? $this->input->post('logo_img') : $logoimage);
        $data['store_banner_image'] = ($bannerimage == '' ? $this->input->post('store_banner_img') : $bannerimage);
        $this->common_model->commonUpdate('tbl_stores', $data, 'store_id', $store_id);
        return $store_id;
    }

    function edit_store() {
        $store_id = $this->input->post('store_id');
        $data['store_name'] = $this->input->post('store_name');
        $data['store_describe'] = $this->input->post('describe');
        $data['store_paypal_id'] = $this->input->post('paypal_id');
        $data['user_id'] = $this->input->post('user_id');
        $data['store_created_date'] = date('Y-m-d h:i:s');
        $data['store_end_date'] = $end_date;
        $this->common_model->commonUpdate('tbl_stores', $data, 'store_id', $store_id);
        return $store_id;
    }

    function tier_info($tier_id) {
        $response = $this->common_model->commonSelect('tbl_tier_list', 'tier_id', $tier_id)->row();
        $data['tier_id'] = $response->tier_id;
        $data['tier_name'] = $response->tier_name;
        $data['tier_title'] = $response->tier_title;
        $data['tier_fee'] = $response->tier_fee;
        $data['is_pickup'] = $response->is_pickup;
        $data['is_fb_store'] = $response->is_fb_store;
        $data['tier_months'] = $response->tier_months;
        $data['tier_amount'] = $response->tier_amount;
        $data['tier_savings'] = $response->tier_savings;
        return $data;
    }

    function paypal_details() {
        $PayPalReturnURL = 'http://educki.dev/seller/ipn';
        $PayPalCancelURL = 'http://educki.dev/seller/cancel';
        $PayPalCurrencyCode = 'USD';
        if ($_POST) { //Post Data received from product list page.
            $paypalmode = ($PayPalMode == 'sandbox') ? '.sandbox' : '';
            $ItemName = $_POST["itemname"]; //Item Name
            $ItemPrice = $_POST["itemprice"]; //Item Price
            $ItemNumber = $_POST["itemnumber"]; //Item Number
            $ItemDesc = $_POST["itemdesc"]; //Item description
            $ItemQty = $_POST["itemQty"]; // Item Quantity
            $ItemTotalPrice = ($ItemPrice * $ItemQty); //(Item Price x Quantity = Total) Get total amount of product; 
            //Other important variables like tax, shipping cost
            $TotalTaxAmount = 0.00;  //Sum of tax for all items in this order. 
            $HandalingCost = 0.00;  //Handling cost for this order.
            $InsuranceCost = 0.00;  //shipping insurance cost for this order.
            $ShippinDiscount = 0.00; //Shipping discount for this order. Specify this as negative number.
            $ShippinCost = 0.00; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
            //Grand total including all tax, insurance, shipping cost and discount
            $GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);
            //Parameters for SetExpressCheckout, which will be sent to PayPal
            $padata = '&METHOD=SetExpressCheckout' .
                    '&RETURNURL=' . urlencode($PayPalReturnURL) .
                    '&CANCELURL=' . urlencode($PayPalCancelURL) .
                    '&PAYMENTREQUEST_0_PAYMENTACTION=' . urlencode("SALE") .
                    '&L_PAYMENTREQUEST_0_NAME0=' . urlencode($ItemName) .
                    '&L_PAYMENTREQUEST_0_NUMBER0=' . urlencode($ItemNumber) .
                    '&L_PAYMENTREQUEST_0_DESC0=' . urlencode($ItemDesc) .
                    '&L_PAYMENTREQUEST_0_AMT0=' . urlencode($ItemPrice) .
                    '&L_PAYMENTREQUEST_0_QTY0=' . urlencode($ItemQty) .
                    '&NOSHIPPING=0' . //set 1 to hide buyer's shipping address, in-case products that do not require shipping

                    '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($ItemTotalPrice) .
                    '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($TotalTaxAmount) .
                    '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($ShippinCost) .
                    '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($HandalingCost) .
                    '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($ShippinDiscount) .
                    '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($InsuranceCost) .
                    '&PAYMENTREQUEST_0_AMT=' . urlencode($GrandTotal) .
                    '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($PayPalCurrencyCode) .
                    '&LOCALECODE=GB' . //PayPal pages to match the language on your website.
                    '&LOGOIMG=https://www.sanwebe.com/wp-content/themes/sanwebe/img/logo.png' . //site logo
                    '&CARTBORDERCOLOR=FFFFFF' . //border color of cart
                    '&ALLOWNOTE=1';

            ############# set session variable we need later for "DoExpressCheckoutPayment" #######
            $_SESSION['ItemName'] = $ItemName; //Item Name
            $_SESSION['ItemPrice'] = $ItemPrice; //Item Price
            $_SESSION['ItemNumber'] = $ItemNumber; //Item Number
            $_SESSION['ItemDesc'] = $ItemDesc; //Item description
            $_SESSION['ItemQty'] = $ItemQty; // Item Quantity
            $_SESSION['ItemTotalPrice'] = $ItemTotalPrice; //total amount of product; 
            $_SESSION['TotalTaxAmount'] = $TotalTaxAmount;  //Sum of tax for all items in this order. 
            $_SESSION['HandalingCost'] = $HandalingCost;  //Handling cost for this order.
            $_SESSION['InsuranceCost'] = $InsuranceCost;  //shipping insurance cost for this order.
            $_SESSION['ShippinDiscount'] = $ShippinDiscount; //Shipping discount for this order. Specify this as negative number.
            $_SESSION['ShippinCost'] = $ShippinCost; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
            $_SESSION['GrandTotal'] = $GrandTotal;
            //We need to execute the "SetExpressCheckOut" method to obtain paypal token
            $paypal = new MyPayPal();
            $httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

            //Respond according to message we receive from Paypal
            if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

                //Redirect user to PayPal store with Token received.
                $paypalurl = 'https://www' . $paypalmode . '.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $httpParsedResponseAr["TOKEN"] . '';
                header('Location: ' . $paypalurl);
            } else {
                //Show error message
                echo '<div style="color:red"><b>Error : </b>' . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '</div>';
                echo '<pre>';
                print_r($httpParsedResponseAr);
                echo '</pre>';
            }
        }
        if (isset($_GET["token"]) && isset($_GET["PayerID"])) {
            //we will be using these two variables to execute the "DoExpressCheckoutPayment"
            //Note: we haven't received any payment yet.

            $token = $_GET["token"];
            $payer_id = $_GET["PayerID"];

            //get session variables
            $ItemName = $_SESSION['ItemName']; //Item Name
            $ItemPrice = $_SESSION['ItemPrice']; //Item Price
            $ItemNumber = $_SESSION['ItemNumber']; //Item Number
            $ItemDesc = $_SESSION['ItemDesc']; //Item Number
            $ItemQty = $_SESSION['ItemQty']; // Item Quantity
            $ItemTotalPrice = $_SESSION['ItemTotalPrice']; //total amount of product; 
            $TotalTaxAmount = $_SESSION['TotalTaxAmount'];  //Sum of tax for all items in this order. 
            $HandalingCost = $_SESSION['HandalingCost'];  //Handling cost for this order.
            $InsuranceCost = $_SESSION['InsuranceCost'];  //shipping insurance cost for this order.
            $ShippinDiscount = $_SESSION['ShippinDiscount']; //Shipping discount for this order. Specify this as negative number.
            $ShippinCost = $_SESSION['ShippinCost']; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
            $GrandTotal = $_SESSION['GrandTotal'];

            $padata = '&TOKEN=' . urlencode($token) .
                    '&PAYERID=' . urlencode($payer_id) .
                    '&PAYMENTREQUEST_0_PAYMENTACTION=' . urlencode("SALE") .
                    //set item info here, otherwise we won't see product details later  
                    '&L_PAYMENTREQUEST_0_NAME0=' . urlencode($ItemName) .
                    '&L_PAYMENTREQUEST_0_NUMBER0=' . urlencode($ItemNumber) .
                    '&L_PAYMENTREQUEST_0_DESC0=' . urlencode($ItemDesc) .
                    '&L_PAYMENTREQUEST_0_AMT0=' . urlencode($ItemPrice) .
                    '&L_PAYMENTREQUEST_0_QTY0=' . urlencode($ItemQty) .
                    /*
                      //Additional products (L_PAYMENTREQUEST_0_NAME0 becomes L_PAYMENTREQUEST_0_NAME1 and so on)
                      '&L_PAYMENTREQUEST_0_NAME1='.urlencode($ItemName2).
                      '&L_PAYMENTREQUEST_0_NUMBER1='.urlencode($ItemNumber2).
                      '&L_PAYMENTREQUEST_0_DESC1=Description text'.
                      '&L_PAYMENTREQUEST_0_AMT1='.urlencode($ItemPrice2).
                      '&L_PAYMENTREQUEST_0_QTY1='. urlencode($ItemQty2).
                     */

                    '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($ItemTotalPrice) .
                    '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($TotalTaxAmount) .
                    '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($ShippinCost) .
                    '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($HandalingCost) .
                    '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($ShippinDiscount) .
                    '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($InsuranceCost) .
                    '&PAYMENTREQUEST_0_AMT=' . urlencode($GrandTotal) .
                    '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($PayPalCurrencyCode);

            //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
            $paypal = new MyPayPal();
            $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
            //Check if everything went ok..
            if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

                echo '<h2>Success</h2>';
                echo 'Your Transaction ID : ' . urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);

                /*
                  //Sometimes Payment are kept pending even when transaction is complete.
                  //hence we need to notify user about it and ask him manually approve the transiction
                 */

                if ('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {
                    echo '<div style="color:green">Payment Received! Your product will be sent to you very soon!</div>';
                } elseif ('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]) {
                    echo '<div style="color:red">Transaction Complete, but payment is still pending! ' .
                    'You need to manually authorize this payment in your <a target="_new" href="http://www.paypal.com">Paypal Account</a></div>';
                }

                // we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
                // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
                $padata = '&TOKEN=' . urlencode($token);
                $paypal = new MyPayPal();
                $httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

                if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

                    echo '<br /><b>Stuff to store in database :</b><br /><pre>';
                    /*
                      #### SAVE BUYER INFORMATION IN DATABASE ###
                      //see (https://www.sanwebe.com/2013/03/basic-php-mysqli-usage) for mysqli usage

                      $buyerName = $httpParsedResponseAr["FIRSTNAME"].' '.$httpParsedResponseAr["LASTNAME"];
                      $buyerEmail = $httpParsedResponseAr["EMAIL"];

                      //Open a new connection to the MySQL server
                      $mysqli = new mysqli('host','username','password','database_name');

                      //Output any connection error
                      if ($mysqli->connect_error) {
                      die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
                      }

                      $insert_row = $mysqli->query("INSERT INTO BuyerTable
                      (BuyerName,BuyerEmail,TransactionID,ItemName,ItemNumber, ItemAmount,ItemQTY)
                      VALUES ('$buyerName','$buyerEmail','$transactionID','$ItemName',$ItemNumber, $ItemTotalPrice,$ItemQTY)");

                      if($insert_row){
                      print 'Success! ID of last inserted record is : ' .$mysqli->insert_id .'<br />';
                      }else{
                      die('Error : ('. $mysqli->errno .') '. $mysqli->error);
                      }

                     */

                    echo '<pre>';
                    print_r($httpParsedResponseAr);
                    echo '</pre>';
                } else {
                    echo '<div style="color:red"><b>GetTransactionDetails failed:</b>' . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '</div>';
                    echo '<pre>';
                    print_r($httpParsedResponseAr);
                    echo '</pre>';
                }
            } else {
                echo '<div style="color:red"><b>Error : </b>' . urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]) . '</div>';
                echo '<pre>';
                print_r($httpParsedResponseAr);
                echo '</pre>';
            }
        }
    }

    public function get_favourite_shops($user_id, $sort='', $to=0, $from = '', $is_limit = 0)
    {
        $query = "
                SELECT
                store.store_id,store.store_name, store.store_url,store.store_banner_image,store.store_logo_image,store.store_city,store.store_state
                FROM tbl_fans fan
                INNER JOIN tbl_stores store ON store.store_id = fan.store_id
                INNER JOIN tbl_user user ON store.user_id = user.user_id
                WHERE store.store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1 AND fan.fan_id = ".$user_id." 
                ";
                #ORDER BY prod.prod_price ".$sort."
                
        if($is_limit == 1)
        {
            $query .= " LIMIT ".$to. "," .$from;
        }
        $result = $this->db->query($query);
        //echo $this->db->last_query();
        return $result;        
    }


    function paypal_availability($paypal_id) {
        $where = "store_paypal_id = '" . $paypal_id . "'";
        $results = $this->common_model->commonselect_array('tbl_stores', $where, 'store_id')->row();
        if ($results == NULL) {
            return 0;
        } else {
            return 1;
        }
    }

}

?>