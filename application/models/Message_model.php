<?php

class Message_model extends CI_Model
{   

    public function __construct()
    {

    }

    public function get_conversation_attr($conversation_id)
    {
        $this->db->select('*');
        $this->db->where('conversation_id', $conversation_id);
        $res = $this->db->get('tbl_chat_conversations');
        if ($res->num_rows() == 1) {
            return $res->row_array();
        } else {
            return false;
        }

    }

    /**
     * get Conversation id
     **/

    public function get_conversation_id($product_id, $seller, $buyer)
    {
        $this->db->select('conversation_id');
        $this->db->where('product_id', $product_id);
        $this->db->where('seller', $seller);
        $this->db->where('buyer', $buyer);
        $res = $this->db->get('tbl_chat_conversations');
        if ($res->num_rows() == 1) {
            return $res->row('conversation_id');
        } else {
            $this->db->select('conversation_id');
            $this->db->where('product_id', $product_id);
            $this->db->where('seller', $buyer);
            $this->db->where('buyer', $seller);
            $res = $this->db->get('tbl_chat_conversations');
            if ($res->num_rows() == 1) {
                return $res->row('conversation_id');
            } else {
                $newid = "ed".date('mdy').time();            
                $this->db->set('product_id', $product_id);
                $this->db->set('seller', $seller);
                $this->db->set('buyer', $buyer);
                $this->db->set('conversation_id', $newid);            
                $query = $this->db->insert('tbl_chat_conversations');
                return $newid;
            }
        }

    }

    /** Submit Message to chat, also used to init the conversation
     *
     * */
    public function submitMessage($sender, $product, $receiver, $message)
    {
        $conversation_id = $this->get_conversation_id($product,$receiver,$sender);
        $this->db->set('active_for', $sender . "," . $receiver);
        $this->db->where('conversation_id', $conversation_id);
        $this->db->update('tbl_chat_conversations');        
        $this->db->set('sender', $sender);
        $this->db->set('receiver', $receiver);
        $this->db->set('message', $message);
        $this->db->set('active_for',$sender.",".$receiver);
        $this->db->set('conversation_id', $conversation_id);

        $query = $this->db->insert('tbl_messages');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /** Fetch All Previous Conversation of user for that product
     *
     * */
    public function fetchMessages($user, $conversation_id)
    {
        $this->db->select('timestamp, sender, message');
        $this->db->select('(select user_fname from tbl_user where user_id = sender) as sendername');
        $this->db->select('(select user_lname from tbl_user where user_id = sender) as sender_lname');

        /*$this->db->select('(select admin_fname from tbl_admin where admin_id = admin) as admin_fname');
        $this->db->select('(select admin_lname from tbl_admin where admin_id = admin) as admin_lname');*/

        $this->db->where("FIND_IN_SET('$user',active_for) !=", 0);
        $this->db->where("(sender = '$user' OR receiver = '$user') ");
        $this->db->where('conversation_id', $conversation_id);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get('tbl_messages');

        $messages = $query->result_array();

        $this->db->set('is_read', '1');
        $this->db->where('receiver', $user);
        $this->db->where('conversation_id', $conversation_id);
        $this->db->where('is_read', '0');
        $this->db->update('tbl_messages');
        //return $messages;

        return $query->result();
    }

    /** Fetch All Previous Conversation of user for that product
     *
     * */
    public function fetch_unread_Messages($user, $conversation_id)
    {
        $this->db->select('timestamp, sender, admin,  message');
        $this->db->select('(select user_fname from tbl_user where user_id = sender) as sendername');
        $this->db->select('(select user_lname from tbl_user where user_id = sender) as sender_lname');
        $this->db->select('(select admin_fname from tbl_admin where admin_id = admin) as admin_fname');
        $this->db->select('(select admin_lname from tbl_admin where admin_id = admin) as admin_lname');
        $this->db->where("(receiver = '$user' OR admin != '0') ");
        $this->db->where('conversation_id', $conversation_id);
        $this->db->where('is_read', '0');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get('tbl_messages');

        $messages = $query->result_array();

        $this->db->set('is_read', '1');
        $this->db->where('receiver', $user);
        $this->db->where('conversation_id', $conversation_id);
        $this->db->where('is_read', '0');
        $this->db->update('tbl_messages');
        return $messages;

        //return $query->result_array();
    }

    /** Fetch all Conversations of particular user as a list
     *
     * public function fetchAllConversations($userId, $isAdmin = true) old function
     * */

    public function fetchAllConversations($userId, $per_page = '', $page = '')
    {
        //echo "per page model".$per_page."<br>";
        //echo "page model".$page."<br>";
        $this->db->select('*');
        $this->db->select('(select user_fname from tbl_user where user_id = buyer) as buyername');
        $this->db->select('(select user_lname from tbl_user where user_id = buyer) as buyer_lname');
        $this->db->select('(select user_email from tbl_user where user_id = buyer) as buyer_email');
        $this->db->select('(select user_fname from tbl_user where user_id = seller) as sellername');
        $this->db->select('(select user_lname from tbl_user where user_id = seller) as seller_lname');
        $this->db->select('(select user_email from tbl_user where user_id = seller) as seller_email');
        /*$this->db->select('(select message from tbl_messages where receiver = buyer OR receiver = seller AND tbl_messages.conversation_id = tbl_chat_conversations.conversation_id order by id DESC LIMIT 1) as latest_message');*/
        $this->db->select('(SELECT COUNT(id) FROM tbl_messages where receiver = buyer AND tbl_messages.conversation_id = tbl_chat_conversations.conversation_id and is_read="0") as buyer_unread');
        $this->db->select('(SELECT COUNT(id) FROM tbl_messages where receiver = seller AND tbl_messages.conversation_id = tbl_chat_conversations.conversation_id and is_read="0") as seller_unread');
        $this->db->where("(buyer = '$userId' OR seller = '$userId')");
        $this->db->where("FIND_IN_SET('".$userId."',active_for) !=", 0);
        $this->db->order_by("id", "DESC");

        $query = '';
        if ($per_page >0) {
            //echo "paging query<br>";
            $this->db->limit($per_page, $page);
            $query = $this->db->get('tbl_chat_conversations');
        } else {
            ///echo "simple query<br>";
            $query = $this->db->get('tbl_chat_conversations');
        }        

        /*echo "<pre>";
        print_r($query->result_array());
        exit;*/

        return $query->result();
    }

    public function DeleteConversation($user, $conversation_id)
    {
        try {
            $conversation = $this->get_conversation_attr($conversation_id);
            $active_for   = explode(',', $conversation['active_for']);
            if (($key = array_search($user, $active_for)) !== false) {
                unset($active_for[$key]);
            }
            $active_for = implode(" ", $active_for);

            $this->db->set('active_for', $active_for);
            $this->db->where('conversation_id', $conversation_id);
            $this->db->update('tbl_chat_conversations');

            $this->db->set('active_for', $active_for);
            $this->db->where('conversation_id', $conversation_id);
            $this->db->update('tbl_messages');

        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
            exit;
        }

        return true;
    }

    /** get product detail bu slug
     */

    public function getProductDetailBySlug($prodSlug)
    {
        $this->db->select('id, userId');
        $this->db->where('slug', $prodSlug);
        $query = $this->db->get('tbl_products');
        return $query->row();
    }

    /** Admin calling functions **/
    public function fetchMessagesByAdmin($conversation_id)
    {
        $this->db->select('id, timestamp, sender, receiver, admin, message, need_assistance, conversation_id');
        $this->db->select('(select user_fname from tbl_user where user_id = sender) as sendername');
        $this->db->select('(select user_fname from tbl_user where user_id = sender) as sender_fname');
        $this->db->select('(select user_lname from tbl_user where user_id = sender) as sender_lname');
        $this->db->select('(select user_fname from tbl_user where user_id = receiver) as receivername');
        $this->db->select('(select user_fname from tbl_user where user_id = receiver) as receiver_fname');
        $this->db->select('(select user_lname from tbl_user where user_id = receiver) as receiver_lname');
        $this->db->select('(select admin_name from tbl_admin where admin_id = admin) as adminname');
        $this->db->select('(select admin_fname from tbl_admin where admin_id = admin) as admin_fname');
        $this->db->select('(select admin_lname from tbl_admin where admin_id = admin) as admin_lname');

        $this->db->where('conversation_id', $conversation_id);
        $this->db->where('active', '1');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get('tbl_messages');

        $messages = $query->result_array();

        return $messages;

        //return $query->result_array();
    }

    public function deleteChatByAdmin($conversation_id)
    {
        $this->db->set('active_for', '');
        $this->db->where('conversation_id', $conversation_id);
        $this->db->update('tbl_chat_conversations');
        return true;
    }

    public function deleteMsgByAdmin($msg_id)
    {
        $this->db->set('active', '0');
        $this->db->where('id', $msg_id);
        $this->db->update('tbl_messages');
        return true;
    }
}
