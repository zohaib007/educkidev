<?php
class Product_model extends CI_Model {
	
	
	public function prod_listing($strWhere="")
	{
		$query = "
				SELECT 
				prod.*, store.store_name, store.store_url, images.img_name, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
				FROM tbl_products prod 
				INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
				INNER JOIN tbl_user user ON store.user_id = user.user_id
				LEFT JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
				INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
				INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
				LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
				LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id

				WHERE
				prod_is_delete = 0 AND user.user_is_delete = 0 AND user.user_status = 1 AND store.store_is_hide = 0
				".$strWhere."
				GROUP BY prod.prod_id
				ORDER BY prod.prod_id DESC
				";
		$result = $this->db->query($query);
		return $result;
	
	}
	
	public function get_product_data($strWhere)
	{
		$query = "
				SELECT 
				prod.*, store.store_name, store.store_url,
				category.cat_name as catName, category.cat_id as catId, category.cat_url as catURL,
				subCat.cat_name as subCatName, subCat.cat_id as subCatId, subCat.cat_url as subCatURL,
				subSubCat.cat_name as subSubCatName, subSubCat.cat_id as subSubCatId, subSubCat.cat_url as subSubCatURL
				FROM tbl_products prod 
				INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
				INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
				INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
				LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
				LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id

				WHERE
				prod.prod_id > 0
				".$strWhere."
				GROUP BY prod.prod_id
				ORDER BY prod.prod_id DESC
				";
		$result = $this->db->query($query);
		return $result;
	}
	public function image_delete($id)
	{
		$select = "SELECT `prod_image_path` FROM `tbl_product_image` WHERE `prod_image_id`=".$id."";
		$image  = $this->db->query($select);
		
		$rootpath = base_url('resources/prod_images/');
		$rootpaththumb = base_url('resources/prod_images/thumb/');
		if ($image->num_rows > 0)
		{
			foreach ($image->result() as $row)
			{
				$image = $row->prod_image_path;
				$rootpath.$image;
				@unlink($rootpath.$image);
				@unlink($rootpaththumb.$image);
				// $this->db->delete('tbl_gall_image_path', array('tbl_cat_id' => $val)); 
			}
			
			$this->db->where('prod_image_id', $id);
			$this->db->delete('tbl_product_image');
		}
	}
	
	
	
	public function get_prod_cat($cat_where)
	{
		$query = "
				SELECT
				tbl_product_category.*
				
				FROM tbl_product_category
				
				INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
				INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
				#INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
				
				WHERE prod_cat_id > 0
				".$cat_where."
				GROUP BY prod_cat_category_id";
		
		$query = $this->db->query($query);
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
	public function get_related_product_data_edit($relp_id)
	{
		
		if(!empty($relp_id))
		{
			$query = "	SELECT
							tbl_related_products.*,
							tbl_products.prod_name, 
							tbl_one.prod_name AS prod_one, tbl_one.prod_status AS prod_one_status, tbl_one.prod_is_delete AS prod_one_delete,
							tbl_two.prod_name AS prod_two, tbl_two.prod_status AS prod_two_status, tbl_two.prod_is_delete AS prod_two_delete,
							tbl_three.prod_name AS prod_three, tbl_three.prod_status AS prod_three_status, tbl_three.prod_is_delete AS prod_three_delete,
							tbl_four.prod_name AS prod_four, tbl_four.prod_status AS prod_four_status, tbl_four.prod_is_delete AS prod_four_delete,
							tbl_five.prod_name AS prod_five, tbl_five.prod_status AS prod_five_status, tbl_five.prod_is_delete AS prod_five_delete,
							tbl_six.prod_name As prod_six, tbl_six.prod_status As prod_six_status, tbl_six.prod_is_delete As prod_six_delete
						
						FROM tbl_products
						
						LEFT JOIN tbl_related_products ON prod_id = relp_prod_id
						
						LEFT JOIN tbl_products AS tbl_one ON relp_prod_one_id = tbl_one.prod_id
						LEFT JOIN tbl_products AS tbl_two ON relp_prod_two_id = tbl_two.prod_id
						LEFT JOIN tbl_products AS tbl_three ON relp_prod_three_id = tbl_three.prod_id
						LEFT JOIN tbl_products AS tbl_four ON relp_prod_four_id = tbl_four.prod_id
						LEFT JOIN tbl_products AS tbl_five ON relp_prod_five_id = tbl_five.prod_id
						LEFT JOIN tbl_products AS tbl_six ON relp_prod_six_id = tbl_six.prod_id
						
						WHERE
						tbl_products.prod_is_delete = 0
						
						AND
						tbl_related_products.relp_id = ".$relp_id."
						AND
						(
						relp_prod_one_id = tbl_one.prod_id
						OR
						relp_prod_two_id = tbl_two.prod_id
						OR
						relp_prod_three_id = tbl_three.prod_id
						OR
						relp_prod_four_id = tbl_four.prod_id
						OR
						relp_prod_five_id = tbl_five.prod_id
						OR
						relp_prod_six_id = tbl_six.prod_id)";
			
			$query = $this->db->query($query);
			
			
			if($query->num_rows()>0)
			{
				return $query->row_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
	}
	
	
	public function get_region_price($prod_id)
	{
		$query = "
				SELECT *
				
				FROM tbl_product_region_prices
				
				WHERE prod_region_prod_id = ".$prod_id."
				";
		
		$query = $this->db->query($query);
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	#############################################################################
	################ Related product start here #################################
	#############################################################################
	
	public function related_products($id,$id1,$id2,$id3,$id4,$id5)
	{
		$id = strtolower($id);
		$query = "
				SELECT
				prod_id, prod_name
				
				FROM tbl_products
				
				INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
				
				INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
				INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
				#INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
				
				WHERE
				(
				LOWER(prod_name) LIKE '".$id."%'
				OR
				LOWER(prod_sku) LIKE '".$id."%'
				) 
				AND
				prod_is_delete = 0
				AND
				prod_status = 1
				AND
				prod_id NOT IN('".$id1."', '".$id2."', '".$id3."', '".$id4."', '".$id5."')
				GROUP BY prod_id
				ORDER BY prod_name ASC
				";
				
		$query = $this->db->query($query);
		
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	public function related_products_edit($id,$id1,$id2,$id3,$id4,$id5,$prod_id)
	{
		$id = strtolower($id);
		$query = "
				SELECT
				prod_id, prod_name
				
				FROM tbl_products
				
				INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
				
				INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
				INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
				#INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
			
				WHERE		
				(
				LOWER(prod_name) LIKE '".$id."%'
				OR
				LOWER(prod_sku) LIKE '".$id."%'
				)
				AND
				prod_is_delete = 0
				AND				
				prod_status = 1
				AND
				prod_id NOT IN('".$id1."', '".$id2."', '".$id3."', '".$id4."', '".$id5."', '".$prod_id."')
				GROUP BY prod_id
				
				";
				
		$query = $this->db->query($query);
		
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	public function get_related_product_data()
	{
		
		
		$query = "	SELECT
						tbl_related_products.*,
						tbl_products.prod_name, 
						tbl_one.prod_name AS one,
						tbl_two.prod_name AS two,
						tbl_three.prod_name AS three,
						tbl_four.prod_name AS four,
						tbl_five.prod_name AS five,
						tbl_six.prod_name As six
					
					FROM tbl_products
					
					INNER JOIN tbl_related_products ON prod_id = relp_prod_id
					
					INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
					INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
					#INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
					
					
					INNER JOIN tbl_products AS tbl_one ON relp_prod_one_id = tbl_one.prod_id
					INNER JOIN tbl_products AS tbl_two ON relp_prod_two_id = tbl_two.prod_id
					INNER JOIN tbl_products AS tbl_three ON relp_prod_three_id = tbl_three.prod_id
					INNER JOIN tbl_products AS tbl_four ON relp_prod_four_id = tbl_four.prod_id
					INNER JOIN tbl_products AS tbl_five ON relp_prod_five_id = tbl_five.prod_id
					INNER JOIN tbl_products AS tbl_six ON relp_prod_six_id = tbl_six.prod_id
					
					WHERE
					prod_is_delete = 0
					AND
					prod_status = 1
					AND (
					relp_prod_one_id = tbl_one.prod_id
					OR
					relp_prod_two_id = tbl_two.prod_id
					OR
					relp_prod_three_id = tbl_three.prod_id
					OR
					relp_prod_four_id = tbl_four.prod_id
					OR
					relp_prod_five_id = tbl_five.prod_id
					OR
					relp_prod_six_id = tbl_six.prod_id
					)
					";
		
		$query = $this->db->query($query);
		
		
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}
	
	
	public function  product_export_csv($str_where = "")
	{
		$query = "
				SELECT
				prod_date, prod_name, prod_sku,	prod_quantity, prod_price,
				
				CASE prod_status
					WHEN 1 THEN 'Active'
					WHEN 0 THEN  'InActive'													 
				END  AS   prod_status 
					
				FROM tbl_products
				
				INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
				INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
				INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
				#INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
				
				WHERE prod_id > 0
				AND prod_is_delete = 0
				".$str_where."
				GROUP BY prod_id
				ORDER BY prod_date DESC
				";
			
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
				return NULL;
	}
	
	
	
	public function product_review_listing($strWhere="", $recordperpage=20, $page=0, $nRecordCount=0) //"ASC"
	{
		
		$query = "
					SELECT
					tbl_products.prod_name, tbl_products.prod_url, tbl_products.prod_id, tbl_products.prod_sku, 
					tbl_reviews.*
					#, tbl_user.user_fname, tbl_user.user_lname, user_email
					
					FROM tbl_products
					
					INNER JOIN tbl_reviews ON prod_id = review_prod_id
					#LEFT JOIN tbl_user ON review_user_id = user_id
					
					".$strWhere."
					
					
					
					ORDER BY  tbl_reviews.review_id DESC
				";
				 
		if($nRecordCount > 0)
		{
			$query .= " LIMIT ".(int)$page.", ".(int)$recordperpage;	
		}	 
	
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}
	}
	
	
	/*public function product_review_detail($strWhere="")
	{
		$query = "
					SELECT
					tbl_product.prod_name, tbl_product.prod_url, tbl_product.prod_id, tbl_product.prod_sku, 
					tbl_reviews.*,
					tbl_user.user_fname, tbl_user.user_lname, user_email
					
					FROM tbl_product
					
					INNER JOIN tbl_reviews ON prod_id = review_prod_id
					LEFT JOIN tbl_user ON review_user_id = user_id
					
					INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
					INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
					INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
					INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
					
					".$strWhere."
					
					ORDER BY  tbl_reviews.review_id DESC
					GROUP BY prod_id
				";
				 
		
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}
	}*/
	
	
	
	
	
}

?>
