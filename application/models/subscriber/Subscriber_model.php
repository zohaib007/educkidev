<?php
 class Subscriber_model extends CI_Model{
    public function __construct()
 	{
 		$this->load->database();
 	}
	
	function getSubscriber($strWhere="") //"ASC"
	{

		$query ="SELECT * FROM  subscriber WHERE  sub_id > 0 ".$strWhere." ORDER By sub_id DESC ";	
		$query = $this->db->query($query);
			 if ($query->num_rows() > 0) return $query->result_array();
					return NULL;

	}
	///////// GET EMAIL MESSAGES LIST///////////
	function getEmailMessages($strWhere="") //"ASC"
	{

		$query ="SELECT * FROM tbl_email_temp WHERE  e_email_id > 0 ORDER BY date_created DESC ".$strWhere;	
		$query = $this->db->query($query);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;

	}
	///////MMMMMMMMMMMMM  ADD NEWSLETTER MESSAGES NOT USE IN BITGLASS /////////////////
	 function add_news($data)
        {

        $query = $this->db->insert('tbl_email_temp', $data);
      
        $sql = $this->db->insert_id();
        return $sql;
            
        }
	
	//MMMMMMMMMMMMMMMMMMM  EDIT NEWS  MMMMMMMMMMMMMMMMM  
	
	function edit_news($id)
    {
         $query = $this->db->get_where('tbl_email_temp', array('e_email_id' => $id));
         //print_r($query->result_array());
        return $query->result_array();
    }
	
	//MMMMMMMMMMMMMMMMMMM  EDIT NEWS NOT USE IN BITGLASS MMMMMMMMMMMMMMMMM  
	
	 function submit_editnew($data, $id) 
	 {
        $this->db->where('e_email_id', $id);
        $this->db->update('tbl_email_temp', $data);
    }
	
	//mmmmmmmmmmmmmmmmmmmm
	function getLeadByEmail($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0) //"ASC"
	{

		$query ="SELECT * FROM subscriber WHERE  sub_id > 0 ".$strWhere;	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY sub_id DESC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
	
			$query = $this->db->query($query);
			 if ($query->num_rows() > 0) return $query->result_array();
					return NULL;

	}
	
	 function listing_sub()
        {
            $query = $this->db->get_where('subscriber', array('is_active' =>1));
            return $query->result_array();
        }
    


}
?>