<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Career_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function view_details($user_id) {

        $data = array('career_user_status' => 2);
        $this->common_model->commonUpdate('tbl_careers_applied_user', $data, 'career_user_id', $user_id);
        $where = 'tbl_careers_applied_user.career_user_id = ' . $user_id;
        $jointable = 'tbl_career';
        $joincondition = 'tbl_career.career_id = tbl_careers_applied_user.career_id';
        $jointype = 'inner';
        $results = $this->career_applied_user_listing("tbl_careers_applied_user", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, '', $order = 'ASC')->result_array();
        return $results;
    }

    function career_applied_user_listing($tabel_name, $where = '', $jointable = '', $joincondition = '', $jointype = '', $page = 0, $per_page = 0, $is_count = 0, $orderby = '', $order = 'ASC') {

        $query = "SELECT * 
                  FROM " . $tabel_name . "
                 ";
        if ($jointable != '') {

            $query .= $jointype . " JOIN " . $jointable . " ON " . $joincondition;
        }
        if ($where != '') {

            $query .= " WHERE " . $where;
        }

        if ($orderby != '') {
            $query .= " ORDER BY " . $orderby . " " . $order;
        }
        if ($is_count > 0) {
            $query .= " LIMIT " . (int) $page . ", " . (int) $per_page;
        }
        $result = $this->db->query($query);
        return $result;
    }

    function appliedusers_delete($user_id) {

        if ($user_id == '' || !is_numeric($user_id)) {
            redirect("admin/communication/appliedusers");
            exit;
        }
        $data['record'] = $this->common_model->commonselect('tbl_careers_applied_user', 'career_user_id', $user_id)->row_array();
        if ($data['record'] == '') {
            redirect("admin/communication/applieduser");
            exit;
        }
        $this->common_model->commonDelete('tbl_careers_applied_user', 'career_user_id', $user_id);
        return '0';
    }

    function add_carrier_page() {
        $bannerFiles1 = $_FILES['banner_image'];
        $FirstFiles1 = $_FILES['first_image'];
        $secondFiles1 = $_FILES['second_image'];
        $ThirdFiles1 = $_FILES['third_image'];
        $bannerimage = $this->image_upload($bannerFiles1,'banner_image');
        $firstImage = $this->image_upload($FirstFiles1,'first_image');
        $secondimage = $this->image_upload($secondFiles1,'second_image');
        $thirdimage = $this->image_upload($ThirdFiles1,'third_image');
        $array = array(
            'career_page_title' => stripcslashes($this->input->post('pg_title')),
            'career_page_description' =>  stripcslashes($this->input->post('pg_description')),
            'career_page_banner_image' => $bannerimage,
            'career_page_banner_title' => stripcslashes($this->input->post('banner_title')),
            'career_page_title_first' => stripcslashes($this->input->post('first_image_title')),
            'career_page_description_first' =>  stripcslashes($this->input->post('first_image_description')),
            'career_page_image_first' => $firstImage,
            'career_page_title_second' => stripcslashes($this->input->post('second_image_title')),
            'career_page_description_second' =>  stripcslashes($this->input->post('second_image_description')),
            'career_page_image_second' => $secondimage,
            'career_page_title_third' => stripcslashes($this->input->post('third_image_title')),
            'career_page_description_third' =>  stripcslashes($this->input->post('third_image_description')),
            'career_page_image_third' => $thirdimage,
            'meta_title' => stripcslashes($this->input->post('meta_title')),
            'meta_keyword' => stripcslashes($this->input->post('meta_keywords')),
            'meta_description' => stripcslashes($this->input->post('meta_description'))
        );
        $this->common_model->commonSave('tbl_career_page', $array);
        return true;
    }
    
    function edit_carrier_page($page_id){
        $bannerFiles1 = $_FILES['banner_image'];
        $FirstFiles1 = $_FILES['first_image'];
        $secondFiles1 = $_FILES['second_image'];
        $ThirdFiles1 = $_FILES['third_image'];
        $bannerimage = $this->image_upload($bannerFiles1,'banner_image','b_image');
        $firstImage = $this->image_upload($FirstFiles1,'first_image','fir_name');
        $secondimage = $this->image_upload($secondFiles1,'second_image','sec_image');
        $thirdimage = $this->image_upload($ThirdFiles1,'third_image','thir_image');
        
        $array = array(
            'career_page_title' => stripcslashes($this->input->post('pg_title')),
            'career_page_description' => stripcslashes($this->input->post('pg_description')),
            'career_page_banner_image' => ($bannerimage == ''? $this->input->post('banner_image'):$bannerimage),
            'career_page_banner_title' => stripcslashes($this->input->post('banner_title')),
            'career_page_title_first' => stripcslashes($this->input->post('first_image_title')),
            'career_page_description_first' => stripcslashes( $this->input->post('first_image_description')),
            'career_page_image_first' => ($firstImage == '' ? $this->input->post('first_image') : $firstImage ),
            'career_page_title_second' => stripcslashes($this->input->post('second_image_title')),
            'career_page_description_second' => stripcslashes($this->input->post('second_image_description')),
            'career_page_image_second' => ($secondimage == '' ? $this->input->post('second_image') : $secondimage),
            'career_page_title_third' => stripcslashes($this->input->post('third_image_title')),
            'career_page_description_third' => stripcslashes($this->input->post('third_image_description')),
            'career_page_image_third' => ($thirdimage == ''? $this->input->post('third_image') : $thirdimage),
            'meta_title' => stripcslashes($this->input->post('meta_title')),
            'meta_keyword' => stripcslashes($this->input->post('meta_keywords')),
            'meta_description' => stripcslashes($this->input->post('meta_description'))
        );

        $result = $this->common_model->commonUpdate('tbl_career_page', $array, 'carrier_page_id', $page_id);
       return $result;
    }
    
    function image_upload($Files1,$file_name,$hidden_file) {
        
        $filename = array();
        if ($Files1['name'] !== "") {
            $config = array(
                'allowed_types' => 'jpg|jpeg|png',
                'upload_path' => FCPATH . 'resources/career_image/career_page/',
                'file_name' => 'image_' . date('Y_m_d_h_i_s'),
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload(''.$file_name.'')) { //Print message if file is not uploaded
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                redirect("admin/career/edit_career_page/1",$data);
            } else {
                $dataDP = $this->upload->data();
                return $filename = $dataDP['file_name'];
            }
        }else{
            return $this->input->post($hidden_file);
        }
    }

}

?>