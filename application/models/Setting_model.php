<?php

class Setting_model extends CI_Model{

  

   public function __construct()
	{

		$this->load->database();

	}

	function listing() {
		$id = $this->db->get('tbl_site_setting');
		return  $data=$id->row();
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN
           function add_setting($array) {
               
               
               $id=$this->db->get('tbl_site_setting');
               $data=$id->result();
               foreach($data as $value)    
               {
                   $last=$value->id;
                   
               }
               $count=count($data);
              
               if($count > 0)
               {
                      $this->db->where('id', $last);
                      $this->db->update('tbl_site_setting', $array);
                      $return_result=$this->db->get('tbl_site_setting');
                   
               }
                else {

                 $query = $this->db->insert('tbl_site_setting', $array);

                }

                return $return_result->result();
             
          }
            
    // MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    
    
    function listing_current_date_graph($start,$end)
    {
        
        $query="SELECT SUM(CAST(CAST(tblcustomerorder.`order_qty` AS DECIMAL(10,2))*CAST(tblcustomerorder.`order_price` AS DECIMAL(10,2)) AS DECIMAL(10,2))) 
                as total_amout,DAY(tblcustomerinfo.`cu_order_date`) as day, MONTHNAME(tblcustomerinfo.`cu_order_date`) as month FROM 
                `tblcustomerinfo` JOIN `tblcustomerorder` ON tblcustomerinfo.`cu_order_no`= tblcustomerorder.`order_cu_id`
                WHERE (cu_order_date between '$start' and '$end') 
                GROUP BY MONTHNAME(tblcustomerinfo.`cu_order_date`), DAY(tblcustomerinfo.`cu_order_date`) order by tblcustomerinfo.`cu_order_date` ASC"; 
          
            $query = $this->db->query($query);
            return $query->result_array();
        
    }



    function get_order_date($date,$date2)
        {
            $query="SELECT SUM(CAST(CAST(tblcustomerorder.`order_qty` AS DECIMAL(10,2))*CAST(tblcustomerorder.`order_price` AS DECIMAL(10,2)) AS DECIMAL(10,2))) 
                as total_amout,DAY(tblcustomerinfo.`cu_order_date`) as day, MONTHNAME(tblcustomerinfo.`cu_order_date`) as month FROM 
                `tblcustomerinfo` JOIN `tblcustomerorder` ON tblcustomerinfo.`cu_order_no`= tblcustomerorder.`order_cu_id`
                WHERE (cu_order_date between '$date' and '$date2') 
                GROUP BY MONTHNAME(tblcustomerinfo.`cu_order_date`), DAY(tblcustomerinfo.`cu_order_date`) order by tblcustomerinfo.`cu_order_date` ASC"; 
          
            $query = $this->db->query($query);
            return $query->result_array();
        }
        function get_prod_cat()
        {
            $query="SELECT DISTINCT cat_id, cat_name from tblcustomerorder 
                join tbl_product on tblcustomerorder.order_prod_id=tbl_product.prod_id 
                join tbl_categories on tbl_categories.cat_id=tbl_product.prod_main_cat ";
                $query = $this->db->query($query);
            return $query->result_array();
            
        }
        
        function get_selected_prod($date,$date2,$id)
        {
             $query="SELECT SUM(CAST(CAST(tblcustomerorder.`order_qty` AS DECIMAL(10,2))*CAST(tblcustomerorder.`order_price` AS DECIMAL(10,2)) AS DECIMAL(10,2))) 
                as total_amout,DAY(tblcustomerinfo.`cu_order_date`) as day, MONTHNAME(tblcustomerinfo.`cu_order_date`) as month FROM 
                `tblcustomerinfo` JOIN `tblcustomerorder` ON tblcustomerinfo.`cu_order_no`= tblcustomerorder.`order_cu_id`
                WHERE (cu_order_date between '$date' and '$date2' and order_prod_id='$id') 
                GROUP BY MONTHNAME(tblcustomerinfo.`cu_order_date`), DAY(tblcustomerinfo.`cu_order_date`) order by tblcustomerinfo.`cu_order_date` ASC"; 
          
            $query = $this->db->query($query);
            return $query->result_array();
            
        }
        
        function get_product_order_report($date = '',$date2 = '', $mainCat = '', $midCat = '', $subCat = '', $prodID = '')
        {
            $crt = "";
            if($mainCat != ''){
                $crt .= " AND (prod_main_cat='$mainCat')";
            } if($midCat != ''){
                $crt .= " AND (prod_middle_cat='$midCat')";
            } if($subCat != ''){
                $crt .= " AND (prod_sub_cat='$subCat')";
            } if($prodID != ''){
                $crt .= " AND (prod_id='$prodID')";
            } 
            
          

            if($date != '' && $date2 != ''){
                    $crt .= " AND (cu_order_date between '$date' and '$date2')";
            }
            
          

            $query="SELECT SUM(CAST(CAST(tblcustomerorder.`order_qty` AS DECIMAL(10,2))*CAST(tblcustomerorder.`order_price` AS DECIMAL(10,2)) AS DECIMAL(10,2))) 
                as total_amout,DAY(tblcustomerinfo.`cu_order_date`) as day, MONTHNAME(tblcustomerinfo.`cu_order_date`) as month FROM 
                `tblcustomerinfo` JOIN `tblcustomerorder` ON tblcustomerinfo.`cu_order_no`= tblcustomerorder.`order_cu_id`
            join tbl_product on tblcustomerorder.order_prod_id=tbl_product.prod_id 
                WHERE (1 = 1) ".$crt." 
                GROUP BY MONTHNAME(tblcustomerinfo.`cu_order_date`), DAY(tblcustomerinfo.`cu_order_date`) order by tblcustomerinfo.`cu_order_date` ASC"; 
          
            $query = $this->db->query($query);
            return $query->result_array();
            
        }






























        function commonUpdate($table,$col,$val)
    {

        $this->db->where($col,$val);
        $result = $this->db->get($table);
        $data=$result->result();
        
        foreach ($data as $value) {
            $value->is_active;
            
    }
  
        if($value->is_active==0)
        {
             $this->db->where($col,$val);
             $this->db->update($table, array('is_active'=>1));
             $status = 1;
            
        }
        else
        {
           $this->db->where($col,$val);
           $this->db->update($table, array('is_active'=>0));
           $status = 0;
        }
        
        return $status;
 
        
    }
      function commonDelete($table,$col,$val)

    {

	  $this->db->where($col,$val);

	     $this->db->update($table, array('is_delete'=>1));

	  return "done";
    }
    function edit_news($id)
    {
         $query = $this->db->get_where('newsletter', array('nl_id' => $id));
         //print_r($query->result_array());
        return $query->result_array();
    }
  
      function submit_editnew($data, $id) {
//          print_r($data);
//          exit;
        $this->db->where('nl_id', $id);
        $this->db->update('newsletter', $data);
    }
    //////////////////////////////////////////////////////////////
    
          function listing_sub()
        {
            $query = $this->db->get_where('subscriber', array('is_delete' =>0));
            return $query->result_array();
        }
    
    
     function add_sub($data)
        {
//         print_r($data);
//         exit;

        $query = $this->db->insert('subscriber', $data);
      
        $sql = $this->db->insert_id();
        return $sql;
            
        }

function get_prod_title($tbl,$where,$id)
	 {
		return	$this->db->get_where($tbl,array("prod_sub_cat"=>$id)); 

		
	 }









































    function commonselect($tablename,$col_name,$value, $order_by='', $order="ASC")
	 {
 
		$this->db->select('*');

		$this->db->from($tablename);

		$this->db->where($col_name,$value); 

		if($order_by!=''){

			$this->db->order_by($order_by, $order);	

		}

		$query = $this->db->get(); 
		return $query;

	 }

	

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	 function commonselect2($tablename,$col_name,$value,$col_name2,$value2)

	 {

		$this->db->select('*');

		$this->db->from($tablename);

		$this->db->where($col_name,$value); 

		$this->db->where($col_name2,$value2); 

		$query = $this->db->get();      

		return $query;

	 }



// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	function getCombox($tablename)

	{

	   $this->db->select('*');

	   $this->db->from($tablename);

		//$this->db->order_by("com_name", "asc"); 
	   $query = $this->db->get();	 		  

   	   return $query;

	}

 

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN


function getCombox2($tablename)

	{

	   $this->db->select('*');

	   $this->db->from($tablename);

		$this->db->order_by("com_name", "asc"); 
	   $query = $this->db->get();	 		  

   	   return $query;

	}
	
	

	 function getComboxuser($tablename,$where)

	 {

		$this->db->select('*');

		$this->db->from($tablename);

		$this->db->where('user_id',$where); 

		$query = $this->db->get();      

		return $query;

	 }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	 function  getComboxpages($tablename,$where)

	 {

		$this->db->select('*');

		$this->db->from($tablename);

		$this->db->where('page_sefurl',$where); 

		$query = $this->db->get();      

		return $query;

	 }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function commonSave($table,$data)
    {

	  $this->db->insert($table,$data); 

	  return $this->db->insert_id();

	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN



// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function commonUpdate2($table,$data,$col,$val,$col2,$val2)
    {

	  $this->db->where($col,$val);

	  $this->db->where($col2,$val2);

      $this->db->update($table,$data); 

	}
	// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function commonUpdate3($table,$data,$col,$val,$col2,$val2 ,$col3,$val3)
    {

	  $this->db->where($col,$val);

	  $this->db->where($col2,$val2);
	  $this->db->where($col3,$val3);

      $this->db->update($table,$data); 

	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN



    function commonDelete2($table,$col,$val,$col2,$val2)

    {

	  $this->db->where($col,$val);

	   $this->db->where($col2,$val2);

	  $this->db->delete($table);

	  

	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	 function getCommon($table)

	 {

	 	  $this->db->select('*');

		  $this->db->from($table);

		  $query = $this->db->get();

		  return $query;

	 }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	function recordExistCommon($table,$field_name,$field_value)

	{

	   

	   if(trim($field_value) != '') 

	   {

		   $this->db->select($field_name);

	       $this->db->from($table);

		   $this->db->where($field_name,$field_value); 

		   $query = $this->db->get();

		   if ($query->num_rows() > 0)

		   {

				

				return true;

		   }

		   else

		   {

				return false;

		   } 

	   }

	   else

	   {

	      return false;

	   }	   		 		   

			

	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	function recordExistCommon2($table,$field_name,$field_value,$field_name2,$field_value2)

	{

	   

	   if(trim($field_value) != '') 

	   {

		   $this->db->select($field_name);

	       $this->db->from($table);

		   $this->db->where($field_name,$field_value); 

		   $this->db->where($field_name2,$field_value2); 

		   $query = $this->db->get();

		   if ($query->num_rows() > 0)

		   {

				

				return true;

		   }

		   else

		   {

				return false;

		   } 

	   }

	   else

	   {

	      return false;

	   }	   		 		   

			

	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	function getSingleValue($table,$where,$fieldname)

	{

			 $q="select $fieldname from $table where $where";

			

			$query = $this->db->query($q);

			if ($query->num_rows() > 0)

			{

				  foreach ($query->result() as $row)

				  return $row->$fieldname;//$row[$fieldname];

			}

			else

				  return 'N/A';  

		 

	 } // function close getSingleValue

	 
	//////update/////////
	   function update($tableName,$arr,$where)
	{
	$str = $this->db->update_string($tableName, $arr, $where);
	$rs=$this->db->query($str);
    return $rs;
	}
//////////////////update///////////////
// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	 function common_get_colum_value($col , $table , $condition_str)

	{

		$res ='';

		$this->db->select($col);

		$this->db->from($table);

		$this->db->where($condition_str);

		$result = $this->db->get();

		if($result->num_rows()>0)

		{

			foreach($result->result_array() as $row)

			{

				return $res = $row[$col];

			}

		}

		return $res;

	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	function query($query)
	{

		return $result=$this->db->query($query);

	}

	function common_where($tbl,$where,$orderBy="",$orderType="")
	 {
			$this->db->where($where); 
			if($orderBy!="")
				$this->db->order_by($orderBy,$orderType);
			
			return $this->db->get($tbl);
	 }
	 
	 function countRecord($tbl,$where="")
	 {
 		return $this->db->query("SELECT COUNT(*) AS Num FROM  ".$tbl." ".$where)->row()->Num;
	 }


//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
	function countQuery($table,$select, $where="")
	{
		 
		$this->db->select($select);
        $this->db->from($table);
		if($where!="")
		{
		$this->db->where($where);
		}
		$query = $this->db->get();
	    return $query->num_rows();
	}
	
    
    public function strip_word_html($text, $allowed_tags = '')
    {	
        mb_regex_encoding('UTF-8');
        $search = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
        $replace = array('\'', '\'', '"', '"', '-');
        $text = preg_replace($search, $replace, $text);
		
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        
		if(mb_stripos($text, '/*') !== FALSE)
		{
            $text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
        }
        
		$text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
        $text = strip_tags($text, $allowed_tags);
        $text = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
        $search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
        $replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
        $text = preg_replace($search, $replace, $text);
        $num_matches = preg_match_all("/\<!--/u", $text, $matches);
        if($num_matches)
		{
              $text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
        }
        return $text;
    } 
	 

	

	

}	 

?>