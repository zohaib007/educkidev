<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'educkidev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.xGe/>GR0n+r*t8olkU8+,7JRz~Jnfu |w1x]t<?(/Ep=A(R;^;NsaZgDe>k{NZA');
define('SECURE_AUTH_KEY',  '#9qDUU4*Hx!ADa6}>99@F9}}vVJ<33]Oo|*7*fwq`;18KqLMb[h%40} In*h^?`L');
define('LOGGED_IN_KEY',    'W/#G6OeL5-c49;&#}z67#M~*l&a3p.#^,s)nGzgc_%teNP!l.o@g;79036>i.On7');
define('NONCE_KEY',        'X1X`;%T@*Y]!D~1hki^|Dk2(e801&{d-mJ)j`s6.M`lgL5NLUAZ5D?Y/[>(h(lJ*');
define('AUTH_SALT',        'kEyZD.5}39V7IX@%faO.~[,)@)u2fVHoPnCSHQ#dv*sz31T|iOzFJ9{#UV:#dCq<');
define('SECURE_AUTH_SALT', 'T`zUi=4OtlUR<2}`LwfzJugH=DYIxo]}cHZ8ZVFwN98HX_osh63% G~mL<Kmv#4L');
define('LOGGED_IN_SALT',   '=&1OI+`aRD2RCxur.|_,$K)7a/Agj^i1DCu6O*ZBII03874k<0%qc 3IFpqx@_;0');
define('NONCE_SALT',       '663qJhr{V(5Jm9UG})m(DHk&%[-$Wfx&b0oVM^:kaWOq-ruJk2Myijg39k-S4dFL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
